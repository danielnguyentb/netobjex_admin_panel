<?php
/*
-------------------------------------------------------------
Copyright (C) 2004-2009  by Manolo G?mez
http://www.hessianphp.org

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

You can find the GNU General Public License here
http://www.gnu.org/licenses/lgpl.html
or in the license.txt file in your source directory.

If you have any questions or comments, please email:
vegeta.ec@gmail.com

*/

/**
 * Represents a date time value. Works with ISO datetime format like
 *
 * "YYYY-MM-DD HH:mm:ss"
 *
 * @package default
 * @author Manolo G?mez
 * @copyright Copyright (c) 2008
 * @version 1.0
 * @access public
 **/
namespace App\Libs;

class SimpleDateTime{
	var $day;
	var $month;
	var $year;
	var $hour;
	var $minute;
	var $second;
	var $_timestamp;
	var $weekDay;

	/**
	 * Constructor
	 *
	 * @param mixed date object, string or timestamp to use as a base for the object
	 * @return SimpleDateTime
	 **/
	function __construct($date='now'){
		//echo $date;
		if(is_string($date)) {
			// control to strip the microsecond part
			if(preg_match('/([\d- :]+)\.\d+/',$date,$groups)){
				$date = $groups[1];
			}
			$this->setTimestamp(strtotime($date));
			/*list($this->year, $this->month, $this->day, $this->hour, $this->minute, $this->second) =
                   sscanf($date, "%04u-%02u-%02u %02u:%02u:%02u");
			$this->sync();*/
        }
		elseif(is_numeric($date)) { // 18000 is the ts for 1970-01-01
			//$str = date("Y-m-d H:i:s", $date);
			//$this->sync($str);
			$this->setTimestamp($date);
		}
		elseif($date instanceof SimpleDateTime)
			$this->setTimestamp($date->_timestamp);
		elseif($date instanceof DateTime){
			$str = $date->format('Y-m-d H:i:s');
			$this->setTimestamp(strtotime($str));
		}else
			throw new Exception('Unsuported date/time datatype conversion:'.gettype($date));
	}

	public static function now(){
		return new SimpleDateTime('now');
	}


	/**
	 * Uses a timestamp integer to build this object's fields
	 *
	 * @param integer ts timestamp to decode
	 **/
        public function toJsDate()
        {
            return $this->year.'-'.$this->month.'-'.$this->day.'T'.$this->hour.':'.$this->minute.':'.$this->second;
        }
        
	public function setTimestamp($ts){
		if($ts==-1 || $ts===false) {
			throw new Exception('FATAL: Incorrect timestamp '.$ts);
		}
		$this->_timestamp = $ts;
		$this->year = date('Y',$ts);
		$this->month = date('m',$ts);
		$this->day = date('d',$ts);
		$this->hour = date('H',$ts);
		$this->minute = date('i',$ts);
		$this->second = date('s',$ts);
		$this->weekDay = date('w',$ts);
		//$this->_timestamp = mktime($this->hour,$this->minute,$this->second,$this->month,$this->day,$this->year,0);
	}


	/**
	 * Rebuilds object's internal timestamp.
	 *
	 * A call to this function is needed when adding or substracting days or minutes to the date
	 **/
	public function sync(){
		$this->_timestamp = mktime($this->hour,$this->minute,$this->second,$this->month,$this->day,$this->year);
		$this->setTimestamp($this->_timestamp);
	}

	/**
	 * returns a new SimpleDateTime object including only the time seccion
	 * @return SimpleDateTime
	 */
	public function asTime(){
		return new SimpleDateTime($this->getTime());
	}

	/**
	 * returns a new SimpleDateTime object including only the date part
	 * @return SimpleDateTime
	 */
	public function asDate() {
		return new SimpleDateTime($this->getDate());
	}

	/**
	 * @return string Time string in format HH:MM:ss
	 **/
	public function getTime(){
		return $this->hour.':'.$this->minute.':'.$this->second;
	}

	/**
	 * @return string Date string in ISO format yyyy-mm-dd
	 **/
	public function getDate(){
		return $this->year.'-'.$this->month.'-'.$this->day;
	}

	public function getTimestamp(){
		return $this->_timestamp;
	}

	public function getWeekDay(){
		return $this->weekDay;
	}

	public function getDayLight(){
		return $this->dayLightSave;
	}

	public function getLocalWeekDay(){
		return date('l',$this->_timestamp);
	}

	public function daysInMonth(){
		return date('t',$this->_timestamp);
	}

	public function isBefore($time){
		if(SimpleDateTime::compare($this,$time) == -1)
			return true;
		return false;
	}

	public function isAfter($time){
		if(SimpleDateTime::compare($this,$time) == 1)
			return true;
		return false;
	}

	public function equals($time){
		if(SimpleDateTime::compare($this,$time) == 0)
			return true;
		return false;
	}

	public function getDateTime(){
		return $this->getDate().' '.$this->getTime();
	}

	public function format($format){
    	return date($format, $this->_timestamp);
	}

    public function toIso8601()
    {
        $out = gmDate("Y-m-d\TH:i:s", $this->_timestamp);
        $milliseconds = round(($this->_timestamp - floor($this->_timestamp)) * 1000);
        if ($milliseconds<10) $milliseconds = '00'.$milliseconds;
        else if ($milliseconds<100) $milliseconds = '0'.$milliseconds;
        return $out.'.'.$milliseconds.'Z';
    }

	public function strftime($format){
		return strftime($format,$this->_timestamp);
	}

	public function gmstrftime($format){
		return gmstrftime($format,$this->_timestamp);
	}

	public function daysDiff($other){
		$diff = $this->dateDiff($other);
		return $diff['days'];
	}


	/**
	 * Calculates the difference between two DateTime objects.
	 * Returns an associative array containing a timespan expressed in days, hours, minutes and seconds.
	 *
	 * @param DateTime other Object to compare with
	 * @return array array with difference information
	 **/
	public function dateDiff($other){
		$laterDate = max($other->_timestamp,$this->_timestamp);
		$earlierDate = min($other->_timestamp,$this->_timestamp);

		// OJO, QUE TAMBIEN RESTA CON HORAS, MINUTOS Y SEGUNDOS, fuente: foros php.net
		//returns an array of numeric values representing days, hours, minutes & seconds respectively
		$ret=array('days'=>0,'hours'=>0,'minutes'=>0,'seconds'=>0);

		$totalsec = $laterDate - $earlierDate;
		if ($totalsec >= 86400) {
			$ret['days'] = floor($totalsec/86400);
			$totalsec = $totalsec % 86400;
		}
		if ($totalsec >= 3600) {
			$ret['hours'] = floor($totalsec/3600);
			$totalsec = $totalsec % 3600;
		}
		if ($totalsec >= 60) {
			$ret['minutes'] = floor($totalsec/60);
		}
		$ret['seconds'] = $totalsec % 60;
		return $ret;
	}

	/**
	 * Compares two date time values (t1 and t2).
	 * Can work with DateTime objects, string values and integer timestamps
	 *
	 * Possible return values are:
	 * -1: t1 < t2
	 *  0: t1 = t2
	 *  1: t1 > t2
	 *
	 * @param mixed time1 First time value
	 * @param mixed time2 Second time value
	 * @return int Result of the comparison
	 **/
	public static function compare($time1,$time2){
		// delegamos encontrar el tipo de lo que se este pasando a otra funci?n,
		// una especie de overload
		$ts1 = SimpleDateTime::findTimestamp($time1);
		$ts2 = SimpleDateTime::findTimestamp($time2);

		if($ts1<$ts2)
			return -1; // 1 menor que 2
		if($ts1>$ts2)
			return 1; // 1 mayor que 2
		if($ts1===$ts2)
			return 0; // 1 igual a 2
	}

	public function monthName($value=null){
		//int mktime ( int hour , int minute , int second , int month , int day , int year , int daylight_saving)
		if($value==null){
			if($this)
				$ts = $this->_timestamp;
			else {
				throw new Exception('Missing parameter in static call to monthName');
			}
		}elseif(is_object($value) && is_a($value,'DateTime'))
			$ts = $value->_timestamp;
		else {
			$ts = mktime(0, 0, 0, $value);
		}
		if($ts==-1)
			throw new Exception('Incorrect timestamp');
		return strftime("%B",$ts);
	}

	/**
	 * Returns the timestamp contained in $dateObj
	 * Can work with DateTime objects, string values and integer timestamps
	 *
	 * @param mixed dateObj variable to search
	 * @return integer timestamp
	 **/

	public static function findTimestamp($dateObj){
		if(is_object($dateObj) && is_a($dateObj,'SimpleDateTime'))
			return $dateObj->getTimestamp();
		if(is_int($dateObj))
			return $dateObj;
		if(is_string($dateObj))
			return strtotime($dateObj);
		throw new Exception('FATAL: Cannot find timestamp of '.gettype($dateObj));
	}

	function __call($method,$params){
		$reg = "/(add|sub|substract)(day|minute|second|hour|year|month)s/";
		$method = strtolower($method);
		if(preg_match($reg, $method, $matches)){
			$op = $matches[1];
			$part = strtolower($matches[2]);
			$num=1;
			if(!empty($params[0])){
				$num = $params[0];
			}
			return $this->__dateOperation($op,$part,$num);
		}
		return $this; //false
	}

	protected function __dateOperation($op,$part,$num){
		$parts = array('day','minute','second','hour','year','month');
		if(!in_array($part,$parts)) {
			throw new Exception("operation: '$part' is not a part of DateTime");
		}
		switch($op){
			case 'add':
			case '+': $this->$part += $num; break;
			case 'sub':
			case 'substract':
			case '-': $this->$part -= $num; break;
		}
		$this->sync();
		return $this;
	}

	/**
	 * Suma un n?mero de unidades al datetime
	 * @param string $part year, month, day, minute, second
	 * @param int $num Unidades a sumar
	 * @return SimpleDateTime
	 */
	function add($part,$num=1){
		return $this->__dateOperation('+',$part,$num);
	}

	/**
	 * Resta un n?mero de unidades al datetime
	 * @param string $part year, month, day, minute, second
	 * @param int $num Unidades a restar
	 * @return SimpleDateTime
	 */
	function sub($part,$num=1){
		return $this->__dateOperation('-',$part,$num);
	}

	/**
	 * @see(sub)
	 * @param $part
	 * @param $num
	 * @return unknown_type
	 */
	function substract($part,$num=1){
		return $this->__dateOperation('-',$part,$num);
	}

	function __toString(){
		return $this->getDateTime();
	}

    public function getHour()
    {
        return $this->hour;
    }
    
    /**
	 * @return string Time string in format HH:MM A/PM
	 **/
    public function to12hTime()
    {
    	if ((int)$this->hour < 12) return $this->hour.':'.$this->minute .' AM';
    	else return ((int)$this->hour -12).':'.$this->minute .' PM';
    }
}
?>