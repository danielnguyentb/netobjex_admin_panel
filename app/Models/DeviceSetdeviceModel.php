<?php

namespace App\Models;

use \Session as Session;
use App\Models\DeviceManagementModel as DeviceManagement;

use \ErrorException AS ErrorException;

class DeviceSetdeviceModel extends BaseModel
{

    protected $_object = 'device-set-devices';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $dataBuffer = Session::get('dataBuffer');
        $deviceSet = self::find(['where' => ['setid' => $dataBuffer['deviceSetId']]]);

        if (!empty($deviceSet->error)) {
            return trans('core.not found');
        }

        $deviceSetIds = [];

        foreach ($deviceSet as $k => $v) {
            $deviceSetIds[] = $v->deviceid;
        }

        $data = DeviceManagement::find([
            'include' => [
                'relation' => 'location',
                'scope' => [
                    'fields' => ['name']
                ]
            ],
            'where' => [
                'id' => [
                    'inq' => $deviceSetIds
                ]
            ]
        ]);

        return $data;

    }
}