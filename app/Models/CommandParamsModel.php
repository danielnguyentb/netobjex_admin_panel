<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/14/15
 * Time: 2:19 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class CommandParamsModel extends BaseModel
{

    protected $_object = 'command-params';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

}