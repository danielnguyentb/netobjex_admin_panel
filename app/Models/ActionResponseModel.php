<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class ActionResponseModel extends BaseModel
{

    protected $_object = 'action-responses';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);
        $responseType = array(
            '1' => 'Notification',
            '2' => 'SMS',
            '3' => 'Video',
            '4' => 'Call',
            '5' => 'Coupon',
            '6' => 'Image',
            '7' => 'URL',
            '8' => 'Feed'
        );

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $v->responsetype = $responseType[$v->responsetype];
        }

        return $data;
    }

}