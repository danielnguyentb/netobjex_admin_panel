<?php

namespace App\Models;

use App\Models\EnterpriseModel as EnterPrise;

use \ErrorException AS ErrorException;

class RuleManageModel extends BaseModel
{

    protected $_object = 'rules';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $enterPriseData = EnterPrise::find([]);
        $enterPriseKeyVal = [];

        foreach ($enterPriseData as $k => $v) {
            $enterPriseKeyVal[$v->id] = $v->name;
        }

        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $data[$k]->enterprisegatewayid = (isset($enterPriseKeyVal[$v->enterprisegatewayid])) ? $enterPriseKeyVal[$v->enterprisegatewayid] : $v->enterprisegatewayid;
        }


        return $data;
    }

}