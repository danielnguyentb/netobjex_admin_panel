<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class EnterpriseModel extends BaseModel
{

    protected $_object = 'enterprise-gateways';

    public function insert($data)
    {
        return $this->_call(
            $this->_object,
            $data,
            'POST'
        );
    }

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new ErrorException($data->error->message);

        $typeInstance = ['', 'REDMINE', 'SUGARCRM', 'CIVICRM'];

        foreach ($data as $k => $v) {
            if (isset($typeInstance[$v->etype])) $data[$k]->etype = $typeInstance[$v->etype];
        }

        return $data;
    }
}