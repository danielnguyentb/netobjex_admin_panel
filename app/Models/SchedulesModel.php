<?php

namespace App\Models;

class SchedulesModel extends BaseModel
{

    protected $_object = 'schedules';

    public function getByAdCampaignId($adCampaignId)
    {
        return $this->find(
            [
                'where' => [
                    'adcampaignid' => $adCampaignId
                ]
            ]
        );
    }

    public function getBySurveyId($surveyId)
    {
        return $this->find(
            [
                'where' => [
                    'surveyid' => $surveyId
                ]
            ]
        );
    }

}