<?php
/**
 * Created by PhpStorm.
 * User: AnhNguyen
 */

namespace App\Models;

use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileModel extends BaseModel
{

    protected $_object = 'files';

    private $_fileType = [
        '1' => 'AVATAR',
        '2' => 'FFUND_LOGO',
        '3' => 'FCAST_USER_CONTACT_LIST',
        '4' => 'FCAST_FILE_ATTACHMENT',
        '5' => 'LIBSERRA_LOGO',
        '6' => 'BUSINESS_INFO_LOGO',
        '7' => 'MEDIA_GALLERY',
        '8' => 'COUPON_IMAGE',
        '9' => 'OFFER_IMAGE',
        '10' => 'AGENT411_AGENT_IMAGE',
        '11' => 'AGENT411_LISTING_IMAGE',
        '12' => 'AGENT411_BROKER_IMAGE',
        '13' => 'LIBSERRA_OTHER',
        '14' => 'BULKIMPORT_XLS',
        '15' => 'BULKIMPORT_ZIP',
        '16' => 'AGENT411_LISTING_XLS',
        '17' => 'AGENT411_LISTING_IMAGE_XLS',
        '18' => 'APPMOBILE_CAMPAIGN_IMPORT',
        '19' => 'TICKETS',
        '20' => 'APPMOBILE_CAMPAIGN_IMAGE',
        '21' => 'MPN_CERTIFICATE',
        '22' => 'INVOICE',
        '23' => 'FCAST_INDIVIDUAL_ANALYTIC'
    ];

    public function upload($file, $type = 'COUPON_IMAGE')
    {
        if (!$file instanceof UploadedFile || $type == '') return false;

        return $this->_call(
            implode('/', [$this->_object, 'upload']),
            [
                'query' => [
                    'type' => $type
                ],
                'multipart' => [
                    [
                        'name' => 'image',
                        'contents' => fopen($file->getRealPath(), 'r'),
                        'filename' => $file->getClientOriginalName()
                    ]
                ]
            ],
            'POST'
        );
    }

}