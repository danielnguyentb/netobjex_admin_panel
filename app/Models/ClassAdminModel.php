<?php

namespace App\Models;

use App\Models\EnterpriseModel as EnterPrise;

use \ErrorException AS ErrorException;

class ClassAdminModel extends BaseModel
{
    protected $_object = 'classes';

    public function getTable(){
        return $this->_call(
            $this->_object . '/getTables',
            [],
            'GET'
        );
    }

}