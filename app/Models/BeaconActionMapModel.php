<?php

namespace App\Models;

use \App\Models\BeaconManagementModel as BeaconManagement;
use \App\Models\BeaconScheduleModel as BeaconSchedule;

use \ErrorException AS ErrorException;

class BeaconActionMapModel extends BaseModel
{

    protected $_object = 'beacon-action-maps';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $beaconData = BeaconManagement::find();
        $beaconShedule = BeaconSchedule::find();

        $beaconKeyVal = [];
        $beaconSheduleKeyVal = [];

        foreach ($beaconData as $k => $v) {
            $beaconKeyVal[$v->id] = $v->name;
        }

        foreach ($beaconShedule as $k => $v) {
            $beaconSheduleKeyVal[$v->mapid] = (isset($beaconKeyVal[$v->beaconid])) ? $beaconKeyVal[$v->beaconid] : '';
        }

        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $v->beaconName = (isset($beaconSheduleKeyVal[$v->id])) ? $beaconSheduleKeyVal[$v->id] : '';
        }

        return $data;
    }

    public static function searDataByDate($filter = []) {
        $data = self::find($filter);
        return $data;
    }
}