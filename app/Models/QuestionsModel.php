<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/14/15
 * Time: 2:19 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class QuestionsModel extends BaseModel
{

    protected $_object = 'survey-questions';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);


        foreach ($data as $k => $v) {
            $data[$k]->question = (strlen($data[$k]->question) > 20 ? (substr($data[$k]->question, 0, 20) . '...') : $data[$k]->question) . ' (' . '<span style="font-family:tiny">' . (string)$data[$k]->id . '</span>' . ')';
            $data[$k]->numorder = '<input class="numberOrder" questionId="' . $data[$k]->id . '" style="border: solid 1px; width: 30px;" curValue="' . $data[$k]->numorder . '" type="text" value="' . $data[$k]->numorder . '">';
        }

        return $data;
    }

}