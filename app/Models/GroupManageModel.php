<?php

namespace App\Models;

use App\Models\CustomersModel as Customers;

use \Session as Session;
use \ErrorException AS ErrorException;

class GroupManageModel extends BaseModel
{

    protected $_object = 'groups';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $userData = Session::get('user');

        if ($userData->org != '') {
            $data = CustomersModel::find(['where' => ['parentid' => $userData->org]]);
            return ['callback' =>true, 'data' => $data];
        }

        return Customers::getInstance()->getAll();
    }

}