<?php 

namespace App\Models;

use \ErrorException AS ErrorException;

class ContentModel extends BaseModel
{

	protected $_object = 'contents';

    public static function findInclude_1($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        foreach($data as $k => $v) {
            $v->activeflag = ($v->activeflag)?'Active':'inActive';
        }

        return $data;
    }
}