<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/14/15
 * Time: 2:19 PM
 */

namespace App\Models;

class CommandsModel extends BaseModel
{

    protected $_object = 'commands';

    public function getCmdReturnTypes()
    {
        return [
            ''       => trans('core.Select Return Type'),
            'int'    => trans('core.Int'),
            'string' => trans('core.String'),
            'bool'   => trans('core.Bool')
        ];
    }

}