<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/16/15
 * Time: 3:01 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class LogsModel extends BaseModel
{

    protected $_object = 'logs';

    public function getVisitsPerDay($filters = array('fromDate'=>null, 'toDate'=>null))
    {
        return $this->_call(
            $this->_object . '/getVisitsPerDay',
            $filters
        );
    }

    public function getVisitsPerLocation($filters = array('fromDate'=>null, 'toDate'=>null))
    {
        return $this->_call(
            $this->_object . '/getVisitsPerLocation',
            $filters
        );
    }

    public function getVisitsPerOS($filters = array('fromDate'=>null, 'toDate'=>null))
    {
        return $this->_call(
            $this->_object . '/getVisitsPerOS',
            $filters
        );
    }

    public function getVisitsPerBeacon($filters = array('fromDate'=>null, 'toDate'=>null, 'locationId'=>null, 'keyword'=>''))
    {
        return $this->_call(
            $this->_object . '/getVisitsPerBeacon',
            $filters
        );
    }

    public static function findInclude($request = [])
    {
        
        $data = self::getInstance()->getVisitsPerBeacon($request['filters']);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        return $data;
    }

}