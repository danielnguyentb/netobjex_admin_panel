<?php

namespace App\Models;

use App\Models\ServiceModel as Service;

use \ErrorException AS ErrorException;

class BeaconSetEditorModel extends BaseModel
{

    protected $_object = 'beacon-sets';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);
        $service = Service::getInstance()->getUUID();
        $serviceSize = sizeof($service);

        foreach ($data as $k => $v) {
            $ex = explode(',', $v->uuids);
            $sizeOfEx = sizeof($ex);

            if ($serviceSize == $sizeOfEx) {
                $data[$k]->uuids = '*';
            } else if ($sizeOfEx == 1) {
                $data[$k]->uuids = $v->uuids;
            } else if ($sizeOfEx > 1) {
                $data[$k]->uuids = 'Multiple values';
            } else {
                $data[$k]->uuids = '';
            }
        }

        return $data;
    }

}