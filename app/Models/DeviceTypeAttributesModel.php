<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/14/15
 * Time: 2:19 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class DeviceTypeAttributesModel extends BaseModel
{

    protected $_object = 'device-type-attributes';

    public function getByDeviceType($deviceType = NULL)
    {
        return $this->_call(
            $this->_object . '/get-by-device-type',
            ['deviceType' => $deviceType],
            'GET'
        );
    }

    public static function findInclude($filter = [])
    {
        $data = self::find($filter);

        if (isset($data->error)) throw new ErrorException($data->error->message);

        $controlTypes = self::getInstance()->getAttrControlTypes();

        foreach ($data as $d) {
            $d->controltype = $controlTypes[$d->controltype];
        }

        return $data;
    }

    public function getAttrControlTypes()
    {
        return [
            1 => 'TextBox',
            2 => 'ProtectedTextBox',
            3 => 'CheckBox',
            4 => 'RadioButton',
            5 => 'DropdownBox'
        ];
    }

}