<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class SurveysModel extends BaseModel
{
    protected $_object = 'surveys';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $data[$k]->name = (strlen($data[$k]->name) > 20 ? (substr($data[$k]->name, 0, 20) . '...') : $data[$k]->name) . ' (' . '<span style="font-family:tiny">' . (string)$data[$k]->id . '</span>' . ')';
            $data[$k]->startdate = date_format(date_create($data[$k]->startdate), 'm/d/Y');
            $data[$k]->enddate = date_format(date_create($data[$k]->enddate), 'm/d/Y');
            $data[$k]->published = $data[$k]->published ? 'Yes' : 'No';
        }
        return $data;
    }

    public function getAnalytics($surveyId, $filters = array('fromDate' => null, 'toDate' => null, 'locationid' => null))
    {
        return $this->_call(
            $this->_object . '/' . $surveyId . '/analytics',
            $filters
        );
    }

    public function getRawData($surveyId, $filters = array('fromDate' => null, 'toDate' => null))
    {
        return $this->_call(
            $this->_object . '/' . $surveyId . '/get-raw-data',
            $filters
        );
    }
}