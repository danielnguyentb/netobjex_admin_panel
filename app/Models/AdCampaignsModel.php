<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/16/15
 * Time: 3:01 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class AdCampaignsModel extends BaseModel
{

    protected $_object = 'ad-campaigns';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new ErrorException($data->error->message);

        $types = self::getInstance()->getAdCampaignTypes();

        foreach ($data as $d) {
            $d->type = $types[$d->type];
        }

        return $data;
    }

    public function getAdCampaignTypes()
    {
        return [
            2 => trans('core.BannerAd'),
            1 => trans('core.VideoAd')
        ];
    }
    public function getRawData($adCampaignId, $filters = array('fromDate'=>null, 'toDate'=>null))
    {
        return $this->_call(
            $this->_object . '/'.$adCampaignId.'/get-raw-data',
            $filters
        );
    }

}