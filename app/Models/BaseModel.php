<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/23/2015
 * Time: 10:27 AM
 */

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use RNCryptor\Decryptor AS Decryptor;
use RNCryptor\Encryptor AS Encryptor;

use \Config AS Config;
use \Session AS Session;

abstract class BaseModel
{
    private $_client;
    private $_uri            = '';
    private $_encrypt        = FALSE;
    private $_encryptKey     = '';
    private $_authKey        = '';
    private $_isSkipCheckKey = '';

    protected $_filter = [];
    protected $_object;
    protected static $instance;

    public function __construct()
    {
        $this->_encrypt        = Config::get('api.isHMAC');
        $this->_uri            = Config::get('api.apiUrl');
        $this->_encryptKey     = Config::get('api.encryptKey');
        $this->_authKey        = Config::get('api.authKey');
        $this->_isSkipCheckKey = Config::get('api.isSkipCheckKey');

        $this->_client = new Client([
            'base_uri' => $this->_uri
        ]);
    }

    protected function _call($method, $params = [], $type = 'GET', $isHMAC = null, $isSkipCheck = FALSE)
    {
        $HMAC = $isHMAC !== null ? $isHMAC : $this->_encrypt;
        $response = array();

        if (!is_array($params) || empty($params)) {
            $params = [];
        } else {
            $include = (array_key_exists('query', $params) || array_key_exists('body', $params) || array_key_exists('json', $params) || array_key_exists('multipart', $params));

            if ($type == 'GET' && !$include) $params = ['query' => $params];

            if ($type != 'GET' && !$include) $params = ['json' => $params];

            if ($HMAC) {
                $encryptor = new Encryptor();

                $params['body'] = $encryptor->encrypt(
                    json_encode($params['json']),
                    $this->_encryptKey
                );

                unset($params['json']);
            }
        }

        try {
            if ($isSkipCheck) $params['headers'][$this->_isSkipCheckKey] = 1;

            $params['headers'][$this->_authKey] = $this->getAccessToken();
            $response = $this->_client->request($type, $method, $params);
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }

        $response = $response->getBody()->getContents();

        if ($HMAC) {
            $decryptor = new Decryptor();
            $response = $decryptor->decrypt($response, $this->_encryptKey);
        }

        return json_decode($response);
    }

    public function setAccessToken($token = '')
    {
        Session::set('API-Access-Token', $token);
        return $this;
    }

    private function getAccessToken()
    {
        return Session::get('API-Access-Token');
    }

    public static function call($method, $params = [], $type = 'GET', $isHMAC = null)
    {
        return self::getInstance()->_call($method, $params, $type, $isHMAC);
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!isset(static::$instance[static::class])) {
            static::$instance[static::class] = new static();
        }

        return static::$instance[static::class];
    }

    public static function create($data)
    {
        return self::getInstance()->_call(
            self::getInstance()->_object,
            [
                'json' => $data
            ],
            'PUT'
        );
    }

    public static function find($filter = [], $isSkipCheck = FALSE)
    {
        return self::getInstance()->_call(
            self::getInstance()->_object,
            [
                'filter' => (object)$filter
            ],
            'GET',
            NULL,
            $isSkipCheck
        );
    }

    public static function findById($id, $filter = [], $isSkipCheck = FALSE)
    {
        return self::getInstance()->_call(
            implode('/', [self::getInstance()->_object, $id]),
            [
                'filter' => (object)$filter
            ],
            'GET',
            NULL,
            $isSkipCheck
        );
    }

    public static function isExists($value, $fieldText, $id = NULL, $fieldName = 'name')
    {
        $response = ['status' => TRUE, 'message' => ''];

        if ($id) {
            $filters = [
                'where' => [
                    'and' => [
                        [
                            $fieldName => $value
                        ],
                        [
                            'id' => [
                                'neq' => $id
                            ]
                        ]
                    ]
                ]
            ];
        } else {
            $filters = ['where' => [$fieldName => $value]];
        }

        $data = self::getInstance()->_call(
            self::getInstance()->_object,
            ['filter' => (object) $filters]
        );

        if (isset($data->error)) {
            $response['status']  = FALSE;
            $response['message'] = $data->error->message;
        } else {
            if (count($data) > 0) {
                $response['status']  = FALSE;
                $response['message'] = trans('core.:fieldText is already exists', ['fieldText' => $fieldText]);
            }
        }

        return (object) $response;
    }

    public static function deleteById($id)
    {
        return self::getInstance()->_call(
            implode('/', [self::getInstance()->_object, $id]),
            [],
            'DELETE'
        );
    }

    public static function deleteAll($where = [])
    {
        return self::getInstance()->_call(
            self::getInstance()->_object,
            [
                'where' => $where
            ],
            'DELETE'
        );
    }

    public static function exists($id)
    {
        return self::getInstance()->_call(
            implode('/', [self::getInstance()->_object, $id, 'exists'])
        );
    }

    public static function countCondition($where = [], $isSkipCheck = FALSE)
    {

        return self::getInstance()->_call(
            implode('/', [self::getInstance()->_object, 'count']),
            [
                'where' => $where
            ],
            'GET',
            NULL,
            $isSkipCheck
        );
    }

    public static function updateById($id, $data)
    {
        return self::getInstance()->_call(
            implode('/', [self::getInstance()->_object, $id]),
            $data,
            'PUT'
        );
    }

    public static function updateAll($where, $data)
    {

        return self::getInstance()->_call(
            self::getInstance()->_object . '/update',
            [
                'query' => [
                    'where' => $where
                ],
                'json' => $data
            ],
            'POST'
        );
    }
}