<?php

namespace App\Models;

use \App\Models\DeviceTypesModel as DeviceType;

use \ErrorException AS ErrorException;

class DeviceManagementModel extends BaseModel
{

    protected $_object = 'devices';

    public function insert($data)
    {
        return $this->_call(
            $this->_object,
            $data,
            'POST'
        );
    }

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $deviceType = DeviceType::find([]);
        $data = self::find($filter);
        $deviceTypeKeyValue = [];

        foreach ($deviceType as $k => $v) {
            $deviceTypeKeyValue[$v->id] = $v->name;
        }

        foreach ($data as $k => $v) {
            $v->parentid = ($v->parentid === NULL) ? 'NO PARENT' : $v->parentid;
            $v->type = (isset($deviceTypeKeyValue[$v->type])) ? ($deviceTypeKeyValue[$v->type]) : $v->type;
        }

        return $data;
    }

    public function getByDeviceSet($filter = [], $isSkipCheck = FALSE)
    {
        return $this->_call(
            $this->_object . '/getByDeviceSet',
            $filter,
            'GET',
            NULL,
            $isSkipCheck
        );
    }

}