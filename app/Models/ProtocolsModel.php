<?php

namespace App\Models;

class ProtocolsModel extends BaseModel
{

    protected $_object = 'protocols';

    public function get($id)
    {
        return $this->_call(
            $this->_object . '/' . $id,
            [],
            'GET'
        );
    }

    public function getAll()
    {
        return $this->_call(
            $this->_object,
            [],
            'GET'
        );
    }

    public function update($id, $data)
    {
        return $this->_call(
            $this->_object . '/' . $id,
            [
                'name' => $data['name']
            ],
            'PUT'
        );
    }

    public function insert($data)
    {
        $param = ['name' => $data['name']];

        if ($data['customerid']) $param['customerid'] = $data['customerid'];

        return $this->_call(
            $this->_object,
            $param,
            'POST'
        );
    }
}