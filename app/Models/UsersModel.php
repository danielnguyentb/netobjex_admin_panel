<?php
/**
 * Created by PhpStorm.
 * User: AnhNguyen
 * Date: 12/4/15
 * Time: 7:00 PM
 */

namespace App\Models;

use App\Models\CustomersModel as Customers;

class UsersModel extends BaseModel
{

    protected $_object = 'users';

    public function authenticate($username, $password, $remember = FALSE)
    {
        return $this->_call(
            $this->_object . '/authenticate',
            [
                'username' => $username,
                'password' => $password,
                'remember' => $remember
            ],
            'POST'
        );
    }

    public function reLogin()
    {
        return $this->_call(
            $this->_object . '/resources',
            [],
            'GET'
        );
    }

    public function getOrgList()
    {
        return $this->_call(
            $this->_object . '/organization'
        );
    }

    public function getRoleList()
    {
        return $this->_call(
            $this->_object . '/roles'
        );
    }

    public function getUserRole($uid)
    {
        return $this->_call(
            $this->_object . '/' . $uid . '/roles'
        );
    }

    public function saveUserRoles($uid, $data)
    {
        return $this->_call(
            $this->_object . '/' . $uid . '/roles',
            $data,
            'POST'
        );
    }

    public function logout()
    {
        $this->_call(
            $this->_object . '/logout',
            [],
            'POST'
        );
    }

    public function confirm($email, $token)
    {
        return $this->_call(
            $this->_object . '/confirm-reset',
            [
                'email' => $email,
                'token' => $token
            ],
            'POST'
        );
    }

    public function reset($email)
    {
        return $this->_call(
            $this->_object . '/reset',
            [
                'email' => $email
            ],
            'POST'
        );
    }

    public function get()
    {
        return $this->_call(
            $this->_object . '/me'
        );
    }

    public function update($uid, $data)
    {
        return $this->_call(
            $this->_object . '/' . $uid,
            [
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'address1' => $data['address1'],
                'address2' => $data['address2'],
                'city' => $data['city'],
                'country' => $data['country_selected'],
                'state' => $data['state_selected'],
                'password' => $data['password'],
                'zip' => $data['zip']
            ],
            'PUT'
        );
    }

    public function checkCurrentPassword($uid, $password)
    {
        return $this->_call(
            $this->_object . '/check-password',
            [
                'uid' => $uid,
                'password' => $password
            ],
            'POST'
        );
    }

    public function getAllByCustomerId($customerId)
    {
        return $this->_call(
            $this->_object . '/getByCustomer',
            [
                'customerId' => $customerId
            ]
        );
    }

    public function findByOrg($orgId)
    {
        return $this->_call(
            $this->_object . '/findByOrg',
            [
                'orgId' => $orgId
            ]
        );
    }

    public function getUserPermissionsByClass($classId = null, $userId = null) {
        return $this->_call(
            $this->_object . '/getUserPermissionsByClass',
            [
                'classId' => $classId,
                'userId' => $userId,
            ]
        );
    }

    public function setCurrentOrg($customerId)
    {
        return $this->_call(
            $this->_object . '/setCurrentOrg',
            [
                'customerId' => $customerId
            ],
            'POST'
        );
    }

    public function getRoleAdmin()
    {
        return $this->_call(
            $this->_object . '/getRoleAdmin'
        );
    }

    public static function findInclude($filter = [])
    {
        array_push($filter['fields'], 'customerid', 'created', 'disabled');
        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $org = '';
            $role = array();

            if ($v->customerid) {
                $customer = Customers::findById($v->customerid);

                if (is_object($customer) && !empty($customer->name)) {
                    $org = $customer->name;
                }
            }


            $roleUser = self::GetInstance()->getUserRole($v->id);
            $role_selected = array();


            if ($roleUser) {

                foreach ($roleUser as $r) {
                    array_push($role, $r->role->name);
                }
            }
            $role = implode(',', $role);
            $data[$k]->customerid = strlen($org) > 10 ? (substr($org, 0, 10) . '...') : $org;
            $data[$k]->role = $role;

        }

        return $data;
    }

    public function deleteRolePermissions($id)
    {
        return $this->_call(
            $this->_object.'/rolePermissions/'.$id,
            [],
            'DELETE'
        );
    }


    public function updatePermissions($id, $data)
    {
        return $this->_call(
            $this->_object.'/rolePermissions',
            [
                'query' => [
                    'where' => ['id' => $id]
                ],
                'json' => $data
            ],
            'PUT'
        );
    }

}