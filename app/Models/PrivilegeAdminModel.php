<?php

namespace App\Models;

use App\Models\ClassAdminModel as ClassAdmin;
use App\Models\UsersModel as Users;

use \ErrorException AS ErrorException;

class PrivilegeAdminModel extends BaseModel
{

    protected $_object = 'users/getRolePermissions';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new ErrorException($data->error->message);

        $roleArr = [];
        $classArr = [];
        $roles = Users::getInstance()->getRoleList();

        if (isset($roles->error)) throw new ErrorException($roles->error->message);

        foreach($roles as $k=>$v) {
            $roleArr[$v->id] = $v->name;
        }

        $classes = ClassAdmin::find();

        if (isset($classes->error)) throw new ErrorException($classes->error->message);

        foreach($classes as $k=>$v) {
            $classArr[$v->id] = $v->name;
        }

        foreach($data as $k=>$v) {

            if(isset ($roleArr[$v->rid]) ) {
                $data[$k]->rid = $roleArr[$v->rid];
            }

            if(isset ($classArr[$v->object]) ) {
                $data[$k]->object = $classArr[$v->object];
            }
        }

        return ['callback' =>true, 'data' => $data];

    }

}