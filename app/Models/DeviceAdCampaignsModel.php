<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/16/15
 * Time: 3:01 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class DeviceAdCampaignsModel extends BaseModel
{

    protected $_object = 'device-ad-campaigns';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

    public function getByCampaignId($campaignId)
    {
        return $this->find(
            [
                'where' => [
                    'campaignid' => $campaignId
                ]
            ]
        );
    }

}