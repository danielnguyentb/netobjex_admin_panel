<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class AlternateSurveyModel extends BaseModel
{

    protected $_object = 'alternate-surveys';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

}