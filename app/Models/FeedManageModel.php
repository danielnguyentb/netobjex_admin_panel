<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class FeedManageModel extends BaseModel
{

    protected $_object = 'feeds';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new \ErrorException($data->error->message);

        $types = self::getTypeOfLink();

        foreach ($data as $k => $v) {
            if (isset($types[$v->type])) {
                $data[$k]->type = $types[$v->type];
            }
        }

        return $data;
    }

    public static function getTypeOfLink()
    {
        return [
            '1' => trans('core.FaceBook'),
            '2' => trans('core.LinkedIn'),
            '3' => trans('core.RSS'),
            '4' => trans('core.Twitter')
        ];
    }
}