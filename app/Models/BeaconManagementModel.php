<?php

namespace App\Models;

use \ErrorException AS ErrorException;
use \Response as Response;

class BeaconManagementModel extends BaseModel
{

    protected $_object = 'beacons';

    public function getAll()
    {
        return $this->_call(
            $this->_object,
            [],
            'GET'
        );
    }

    public function getByBeaconSet($filter = [], $isSkipCheck = FALSE)
    {
        return $this->_call(
            $this->_object . '/getByBeaconSet',
            $filter,
            'GET',
            NULL,
            $isSkipCheck
        );
    }

    public function insert($data)
    {
        return $this->_call(
            $this->_object,
            $data,
            'POST'
        );
    }

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (!empty($data->error)) {
            throw new ErrorException($data->error->message);
        }

        foreach ($data as $k => $v) {
            $v->activeflag = ($v->activeflag) ? 'Active' : 'inActive';
        }

        return $data;
    }
}