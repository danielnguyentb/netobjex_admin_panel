<?php

namespace App\Models;

class ServiceModel extends BaseModel
{

    protected $_object = 'services';

    public function getUUID()
    {
        return $this->_call(
            $this->_object . '/getCustomerUUIDs',
            [],
            'GET'
        );
    }

    public function getVariableMapping($deviceTypeId = null)
    {
        return $this->_call(
            $this->_object . '/getVariableMappings?deviceTypeId=' . $deviceTypeId,
            [],
            'GET'
        );
    }
}