<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/16/15
 * Time: 11:01 AM
 */

namespace App\Models;

class ParametersModel extends BaseModel
{

    protected $_object = 'command-params';

    public function getParamDataTypes()
    {
        return [
            'Int'      => trans('core.Int'),
            'String'   => trans('core.String'),
            'Datetime' => trans('core.Datetime')
        ];
    }

}