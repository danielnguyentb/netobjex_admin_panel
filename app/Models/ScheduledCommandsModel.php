<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/16/15
 * Time: 3:01 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;

class ScheduledCommandsModel extends BaseModel
{

    protected $_object = 'scheduled-commands';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

    public function getScheduledCommandRepeatPeriod()
    {
        return [
            1 => trans('core.SECOND'),
            2 => trans('core.MINUTE'),
            3 => trans('core.HOUR'),
            4 => trans('core.DAY'),
            5 => trans('core.WEEK'),
            6 => trans('core.MONTH')

        ];
    }

    public function getScheduledCommandEveryNum()
    {
        $data = [];

        for ($i = 0; $i < 61; $i++) {
            $data[] = $i;
        }

        return $data;
    }

}