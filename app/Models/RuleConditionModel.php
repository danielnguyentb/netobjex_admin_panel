<?php

namespace App\Models;

use App\Models\DeviceTypeAttributesModel as DeviceTypeAttr;

use \Session as Session;
use \ErrorException AS ErrorException;


class RuleConditionModel extends BaseModel
{

    protected $_object = 'rule-conditions';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $dataBuffer = Session::get('dataBuffer');
        $data = self::find(['where' => ['ruleid' => $dataBuffer['ruleId']]]);
        $attr = DeviceTypeAttr::getInstance()->getByDeviceType($dataBuffer['deviceTypeId']);


        foreach ($data as $k => $v) {
            $data[$k]->checkbox = '<input type="checkbox">';
            $data[$k]->devicetypeattributeid = self::genSelectAttr($attr, $v->devicetypeattributeid);
            $data[$k]->operand = self::genSelectOporator($v->operand);
            $data[$k]->value = "<input type='text' value='$v->value' name='value[]' required>";
            $selected = "<select name='isand[]'>";
            foreach (['true' => 'And', 'false' => 'Or'] as $k1 => $v1) {
                if ($k1 == $v->isand) {
                    $selected .= "<option value='$k1' selected>$v1</option>";
                } else {
                    $selected .= "<option value='$k1' >$v1</option>";
                }
            }
            $selected .= "</select>";
            $data[$k]->isand = $selected;
        }
        return $data;
    }

    public static function genSelectOporator($selected = null)
    {
        $data = self::getOperator();
        $selectBox = '<select name="operand[]">';

        foreach ($data['valKey'] as $k => $v) {
            if ($k == $selected) {
                $selectBox .= "<option value='$k' selected>$v</option>";
            } else {
                $selectBox .= "<option value='$k' >$v</option>";
            }
        }
        $selectBox .= '</select>';
        return $selectBox;
    }

    public static function genSelectAttr($data = [], $selected = null)
    {
        $selectBox = '<select name="attribute[]">';

        foreach ($data as $k => $v) {
            if ($v->id == $selected) {
                $selectBox .= "<option value='$v->id' selected>$v->name</option>";
            } else {
                $selectBox .= "<option value='$v->id' >$v->name</option>";
            }
        }
        $selectBox .= '</select>';
        return $selectBox;
    }

    public static function getOperator()
    {
        return [
            'keyVal' => ['=' => 1, '<' => 2, '>' => 3, '<>' => 4, '>=' => 5, '<=' => 6, 'in' => 7],
            'valKey' => ['1' => '=', '2' => '<', '3' => '>', '4' => '<>', '5' => '>=', '6' => '<=', '7' => 'in']
        ];
    }

}