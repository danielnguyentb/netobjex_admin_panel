<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 12/14/15
 * Time: 2:19 PM
 */

namespace App\Models;

use \ErrorException AS ErrorException;
use \Session as Session;

class DeviceAttributeModel extends BaseModel
{

    protected $_object = 'device-attributes';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $dataBuffer = Session::get('dataBuffer');
        $data = self::find(['where' => ['deviceid' => $dataBuffer['deviceId'], 'required' => false]]);
        if (isset($data->error)) throw new ErrorException($data->error->message);

        foreach ($data as $k => $v) {
            $data[$k]->option = '<input type="checkbox">';
        }

        return $data;
    }

}