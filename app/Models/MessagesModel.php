<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class MessagesModel extends BaseModel
{

    protected $_object = 'messages';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        if (isset($data->error)) throw new ErrorException($data->error->message);

        $types = self::getInstance()->getMessageTypes();

        foreach ($data as $d) {
            if (isset($types[$d->messagetype])) $d->messagetype = $types[$d->messagetype];
            else $d->messagetype = '';
        }

        return $data;
    }

    public function getMessageTypes()
    {
        return [
            1 => trans('core.EMAIL'),
            2 => trans('core.SMS'),
            3 => trans('core.MPN')
        ];
    }

}