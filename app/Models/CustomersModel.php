<?php
/**
 * Created by PhpStorm.
 * User: AnhNguyen
 * Date: 12/4/15
 * Time: 7:07 PM
 */

namespace App\Models;

class CustomersModel extends BaseModel
{

    protected $_object = 'customers';

    public function getAll()
    {
        return $this->_call(
            $this->_object
        );
    }

    public function get($uid)
    {
        return $this->_call(
            $this->_object . '/' . $uid,
            [],
            'GET'
        );
    }

    public function getByTree()
    {
        return $this->_call(
            $this->_object . '/get-by-tree',
            [],
            'GET'
        );
    }

}