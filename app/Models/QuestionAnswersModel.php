<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class QuestionAnswersModel extends BaseModel
{

    protected $_object = 'survey-question-answers';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

}