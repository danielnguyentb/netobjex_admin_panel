<?php

namespace App\Models;

use \ErrorException AS ErrorException;

class MessageVariableMappingsModel extends BaseModel
{

    protected $_object = 'message-variable-mappings';

    public static function findInclude($filter = [])
    {
        if (!getCurrentCustomerId()) throw new ErrorException(trans('core.Organization is required'));

        $data = self::find($filter);

        return $data;
    }

}