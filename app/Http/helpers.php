<?php
/**
 * Created by PhpStorm.
 * User: AnhNguyen
 * Date: 12/7/15
 * Time: 3:38 PM
 */

use RNCryptor\Encryptor as Encryptor;
use RNCryptor\Decryptor as Decryptor;

use \App\Libs\SimpleDateTime AS SimpleDateTime;

use \App\Models\TimezoneModel AS Timezones;

use \Illuminate\Http\Exception AS Exception;

use \Request AS Request;
use \Response AS Response;

function rncEncrypt($input)
{
    $encrypt = new Encryptor();

    return $encrypt->encrypt($input, \Config::get('encryptKey'));
}

function rncDecrypt($input)
{
    $decrypt = new Decryptor();

    return $decrypt->decrypt($input, \Config::get('encryptKey'));
}

function debug($value, $label = null)
{
    $label = get_tracelog(debug_backtrace(), $label);

    echo getdebug($value, $label);
    exit();
}

function getdebug($value, $label = null)
{
    $value = htmlentities(print_r($value, true));

    return "<pre>$label$value</pre>";
}

function get_tracelog($trace, $label = null)
{
    $line  = $trace[0]['line'];
    $file  = is_set($trace[1]['file']);
    $func  = $trace[1]['function'];
    $class = is_set($trace[1]['class']);
    $log   = "<span style='color:#FF3300'>-- $file - line:$line - $class-$func()</span><br/>";

    if ($label) $log .= "<span style='color:#FF99CC'>$label</span> ";

    return $log;
}

function is_set(&$var, $substitute = null)
{
    return isset($var) ? $var : $substitute;
}

function getCurrentCustomerId()
{
    return \Session::get('user')->org;
}

function getCurrentUserId()
{
    return \Session::get('user')->uid;
}

function concat_error(\Illuminate\Validation\Validator $validator) {

    return implode("\n", $validator->getMessageBag()->all());
}

function getCurrentAccessToken()
{
    return \Session::get('user')->token;
}

function genDownloadUrl($id)
{
    return \Config::get('api.downloadUrl') . substr($id, 0, 3) . '/' . $id;
}

function setData($data, $index, $type = 'text', $format = DATE_ISO8601, $isTimestamp = FALSE, $defaultReturn = NULL)
{
    if (is_array($data)) $data = (object) $data;

    if (!empty($data->{$index})) {
        switch ($type) {
            case 'date':
                $output = date($format, ($isTimestamp ? $data->{$index} : strtotime($data->{$index})));
                break;
            case 'bool':
                $output = $data->{$index} == 1 || $data->{$index} == TRUE ? TRUE : FALSE;
                break;
            case 'fromTime':
                $output = genFromTime($data->{$index});
                break;
            case 'toTime':
                $output = genToTime($data->{$index});
                break;
            default:
                $output = $data->{$index};
                break;
        }

        return $output;
    }

    return $defaultReturn;
}

function genFromTime($date)
{
    $dateObj = new SimpleDateTime($date);

    return ((int) $dateObj->format('H') * 60) + $dateObj->format('i');
}

function genToTime($date)
{
    $dateObj = new SimpleDateTime($date);

    return (((int) $dateObj->format('H') == 0 ? 24 : (int) $dateObj->format('H')) * 60) + $dateObj->format('i');
}

function renderTimeFromDate($date)
{
    $dateObj = new SimpleDateTime($date);

    return (object) [
        'hour'   => $dateObj->format('g'),
        'minute' => $dateObj->format('i'),
        'period' => $dateObj->format('A')
    ];
}

function renderTimezone()
{
    $response = ['status' => TRUE, 'message' => ''];
    $result   = [];

    try {
        $tzs = Timezones::find();

        if (!empty($tzs->error)) {
            $response['status']  = FALSE;
            $response['message'] = $tzs->error->message;

            return $response;
        }

        foreach ($tzs as $tz) {
            $gmt = convertUnsignedToSignedInt($tz->rawoffset / (1000 * 60 * 60));
            $gmt = (($gmt < 0) ? '-' : '+') . (abs($gmt) < 10 ? '0' . (int) abs($gmt) : (int) abs($gmt)) . ':' . ((is_float($gmt)) ? '30' : '00');

            $result[$tz->id] = "(GMT{$gmt}) {$tz->name}";
        }
    } catch (Exception $e) {
        $response['status']  = FALSE;
        $response['message'] = $e->getMessage();

        return $response;
    }

    return $result;
}

function convertUnsignedToSignedInt($val)
{
    if (PHP_INT_SIZE == 8) {
        $i = (int) $val;

        if ($i & 0x80000000) return $i - 0x100000000;

        return $i;
    }

    return $val;
}

function buildDropDownOpts($data, $kField = 'id', $vField = 'name', $custom = FALSE, $kFieldPattern = [], $vFieldPattern = [])
{
    $result = [];

    if ($data) {
        foreach ($data as $d) {
            if (!$custom) {
                $key   = $d->$kField;
                $value = $d->$vField;
            } else {
                if ($kFieldPattern) {
                    $key = '';

                    foreach ($kFieldPattern as $kfp) {
                        if ($kfp['isFieldName']) {
                            $key .= $d->{$kfp['item']};
                        } else {
                            $key .= $kfp['item'];
                        }
                    }
                } else {
                    $key = $d->$kField;
                }

                if ($vFieldPattern) {
                    $value = '';

                    foreach ($vFieldPattern as $vfp) {
                        if ($vfp['isFieldName']) {
                            $value .= $d->{$vfp['item']};
                        } else {
                            $value .= $vfp['item'];
                        }
                    }
                } else {
                    $value = $d->$vField;
                }
            }

            $result[$key] = $value;
        }
    }

    return $result;
}

function buildSurveyAnswerTypeDropDown()
{
    $result = ['1'=> 'FREE_TEXT', '2'=> 'CHECKBOX', '3'=> 'RADIOBUTTON', '4'=> 'JAVASCRIPT'];

    return $result;

}

function buildOrgDropDown($orgList, $selected = '')
{
    $html  = '<select id="orgList" class="chosen-select input-md form-control" style="margin-left: -50px">';
    $html .= '<option value="">' . trans('core.My Dashboard') . '</option>';

    if (!empty($orgList) && empty($orgList->error)) {
        foreach ($orgList as $p) {
            $pSelected = $selected == $p->id ? ' selected="selected"' : '';

            $html .= '<option value="' . $p->id . '"' . $pSelected . '>' . $p->name . '</option>';

            if (!empty($p->childs)) {
                foreach ($p->childs as $c) {
                    $cSelected = $selected == $c->id ? ' selected="selected"' : '';

                    $html .= '<option value="' . $c->id . '"' . $cSelected . '>-- ' . $c->name . '</option>';
                }
            }
        }
    }

    $html .= '</select>';

    return $html;
}

function buildSideBar()
{
    $html           = '';
    $userSession    = \Session::get('user');
    $mappingSidebar = \Config::get('mapping_sidebar');
    $route          = \Request::segment(1);

    if (!empty($userSession->resources)) {
        foreach ($userSession->resources as $parent) {
            $hasChild       = !empty($parent->childs) ? TRUE : FALSE;
            $parentMapData  = $mappingSidebar[$parent->key];
            $parentTitle    = trans("core.{$parentMapData['trans']}");

            $html .=
                '<li' . ($route == $parentMapData['route'] ? ' class="active"' : '') . '>' .
                    '<a href="' . ($hasChild ? "#{$parentMapData['action']}" : action($parentMapData['action'])) . '" title="' . $parentTitle . '"' . ($hasChild ? ' data-toggle="collapse"' : '') . '>' .
                        '<em class="icon-' . $parentMapData['icon'] . '"></em>' .
                        '<span>' . $parentTitle . '</span>' .
                    '</a>';

            if ($hasChild) {
                $html .=
                    '<ul id="' . $parentMapData['action'] . '" class="nav sidebar-subnav collapse">' .
                        '<li class="sidebar-subnav-header">' . $parentTitle . '</li>';

                foreach ($parent->childs as $child) {
                    if( isset($mappingSidebar[$child->key]) ) {

                        $childMapData = $mappingSidebar[$child->key];
                        $childTitle   = trans("core.{$childMapData['trans']}");

                        $html .=
                            '<li' . ($route == $childMapData['route'] ? ' class="active"' : '') . '>' .
                            '<a href="' . action($childMapData['action']) . '" title="' . $childTitle . '">' .
                            '<span>' . $childTitle . '</span>' .
                            '</a>' .
                            '</li>';

                    }
                }

                $html .= '</ul>';
            }

            $html .= '</li>';
        }
    }

    return $html;
}
function exportRawData($data, $camType = 'survey')
    {   
        $content = '
        <?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>Quick report</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style type="text/css">
              body {  font-family: verdana, helvetica, sans-serif; margin: 0px; padding: 0; }
              h1 { font-weight: bold; font-size: 150%; border-bottom-style: solid; border-bottom-width: 2px; margin-top: 0px; padding-bottom: 0.5ex; color: #eeeeee; }
              h2 { font-size: 130%; padding-bottom: 0.5ex; color: #009ACE; border-bottom-style: solid; border-bottom-width: 2px; }
              h3 { font-size: 110%; padding-bottom: 0.5ex; color: #000000; }
              th { text-align: left; background-color: #009ACE; color: #eeeeee; }
          #ReportHeader { padding: 10px; background-color: #009ACE; color: #eeeeee; border-bottom-style: solid; border-bottom-width: 2px; border-color: #999999; }
          #ReportHeader th { width: 25%; white-space: nowrap; vertical-align: top; }
          #ReportHeader td { vertical-align: top; color: #eeeeee; }
          #ReportNotes { padding: 10px; background-color: #eeeeee; font-size: 80%; border-bottom-style: solid; border-bottom-width: 2px; border-color: #999999; }
                  .ReportSQL { margin-bottom: 10px; padding: 10px; display: block; background-color: #eeeeee; font-family: monospace; }
          #ReportDetails { margin-left: 10px; margin-right: 10px; margin-bottom: 10px; }
          #ReportDetails td, th { font-size: 80%; margin-left: 2px; margin-right: 2px; }
          #ReportDetails th { border-bottom-color: #777777; border-bottom-style: solid; border-bottom-width: 2px; }
                  .ReportDetailsOddDataRow { background-color: #dddddd; }
                  .ReportDetailsEvenDataRow { background-color: #eeeeee; }
                  .ReportTableHeaderCell { background-color: #dddddd; color: #009ACE; vertical-align: top; font-size: 80%; white-space: nowrap; }
                  .ReportTableValueCell { vertical-align: top; font-size: 80%; white-space: nowrap; }
                  .ReportTableInfo { font-size: 80%; font-style: italic; }
          #ReportFooter { font-weight: bold; font-size: 80%; text-align: right; background-color: #009ACE; color: #eeeeee; margin-top: 10px; padding: 2px; border-bottom-style: solid; border-bottom-width: 2px; border-top-style: solid; border-top-width: 2px; border-color: #999999; }
          #ReportFooter a { color: #ffffff; text-decoration: none; }
              </style>
          </head>
          <body>
            <div id="ReportHeader"><h1>Quick report</h1><b>Generated: </b>'.date('l jS \of F Y h:i:s A').'<br /><b>Database: </b>netobjex on xqms@microservice-prod.c1my1t3hoxlc.us-west-2.rds.amazonaws.com:5432<br /></div>
            <div id="ReportDetails">
              <h2>Query results</h2>
              <table>
                <tr>';
                if($camType == 'survey')
                {
                    $content .= '
                  <th class="ReportTableHeaderCell" width="25%">mobile</th>
                  <th class="ReportTableHeaderCell" width="25%">question</th>
                  <th class="ReportTableHeaderCell" width="25%">answer</th>
                  <th class="ReportTableHeaderCell" width="25%">created</th>';
                }
                else
                {
                    $content .= '
                  <th class="ReportTableHeaderCell" width="50%">deviceID</th>
                  <th class="ReportTableHeaderCell" width="50%">created</th>';
                }
                
              $content .='</tr>';
              if(!empty($data))
              {
                if($camType == 'survey')
                {
                    foreach($data as $k=>$v)
                    {
                        $content .='<tr class="ReportDetailsEvenDataRow">';
                        $content .='<td class="ReportTableValueCell">'.$v->mobile.'</td>';
                        $content .= '<td class="ReportTableValueCell">'.$v->question.'</td>';
                        $content .= '<td class="ReportTableValueCell">'.$v->answer.'</td>';
                        $content .= '<td class="ReportTableValueCell">'.$v->created.'</td></tr>';
                    }      
                }
                else
                {   
                    foreach($data as $k=>$v)
                    {
                        $content .='<tr class="ReportDetailsEvenDataRow">';
                        $content .='<td class="ReportTableValueCell">'.$v->deviceid.'</td>';
                        $content .= '<td class="ReportTableValueCell">'.$v->created.'</td></tr>';
                    }      
                }
                
              }
              
            $content .='</table>
            <br />
            <p class="ReportTableInfo">'.(sizeof($data)).' rows with 4 columns retrieved.</p>            
        </div>
        <div id="ReportFooter">
            Report generated by <a href="http://www.pgadmin.org/">pgAdmin III</a></div>
            <br />
        </body>
        </html>
        ';
       
        header("Content-Type: text/html; charset=utf-8");
        header("Content-Disposition: attachment; filename=".(($camType == 'survey')?"survey_":"adcampaign_")."".date('YmdHis', time()) . ".html");
        header("Content-Length: ". strlen($content));
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        echo $content;exit();
    }