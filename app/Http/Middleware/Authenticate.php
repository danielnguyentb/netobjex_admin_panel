<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Response;
use App\Models\UsersModel as Users;
use League\Flysystem\Exception;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Session::get('user')) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                if ($accessToken = \Request::cookie('siteAuth')) {
                    try {
                        if (\Config::get('api.isHMAC')) $accessToken = rncDecrypt($accessToken);

                        Users::getInstance()->setAccessToken($accessToken);
                        $user = Users::getInstance()->reLogin();

                        if (!empty($user->error)) {
                            if ($user->error->code == 'INVALID_TOKEN') {
                                Users::getInstance()->logout();
                                \Session::flush();
                            }

                            return redirect()->guest('/');
                        } else {
                            Users::getInstance()->setAccessToken($accessToken);
                            \Session::set('user', $user);

                            return redirect('dashboard');
                        }
                    } catch (Exception $e) {
                        return response($e->getMessage(), 400);
                    }
                } else {
                    return redirect()->guest("/?_b=" . urlencode($request->url()));
                }
            }
        }

        return $next($request);
    }
}
