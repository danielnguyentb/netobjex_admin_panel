<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'UserController@login');
Route::post('/do-login', 'UserController@doLogin');
Route::post('/get-users-by-customer', 'UserController@getUsersByCustomerId');
Route::post('/set-selected-org-user', 'UserController@setSelectedOrgUser');
Route::post('/get-campaign-by-type', 'DashboardController@getCampaignByType');
Route::post('/get-visit-per-day', 'DashboardController@getVisitsPerDay');
Route::post('/get-visit-per-location', 'DashboardController@getVisitsPerLocation');
Route::post('/get-visit-per-os', 'DashboardController@getVisitsPerOS');
Route::post('/get-visit-per-beacon', 'DashboardController@getVisitsPerBeacon');
Route::resource('dashboard', 'DashboardController');

//Datatable common endpoint
Route::get('datatable', 'DatatableController@index');

// Group Admin
Route::get('group-management', 'GroupManagementController@index');
Route::get('group-management/form/{id?}', 'GroupManagementController@get');
Route::post('group-management/save/{id?}', 'GroupManagementController@save');
Route::post('group-management/delete/{id}', 'GroupManagementController@delete');

Route::get('enterprise-gateway', 'EnterpriseGatewayController@index');
Route::get('enterprise-gateway/form/{id?}', 'EnterpriseGatewayController@get');
Route::post('enterprise-gateway/save/{id?}', 'EnterpriseGatewayController@save');
Route::post('enterprise-gateway/delete/{id}', 'EnterpriseGatewayController@delete');

// Asset Management
Route::get('asset-management', 'AssetManagementController@index');
Route::post('asset-management/get-major-ids-by-uuid', 'AssetManagementController@getMajorIdsByUUID');
Route::post('asset-management/get-minor-ids-by-uuid-and-major-id', 'AssetManagementController@getMinorIdsByUUIDAndMajorId');
Route::post('asset-management/search-beacons', 'AssetManagementController@searchBeacons');
Route::post('asset-management/search-devices', 'AssetManagementController@searchDevices');
Route::get('asset-management/get-beacon/{id}', 'AssetManagementController@getBeacon');
Route::get('asset-management/get-device/{id}', 'AssetManagementController@getDevice');

Route::get('beacon-management', 'BeaconManagementController@index');
Route::get('beacon-management/form/{id?}', 'BeaconManagementController@get');
Route::post('beacon-management/save/{id?}', 'BeaconManagementController@save');
Route::post('beacon-management/delete/{id}', 'BeaconManagementController@delete');

Route::get('device-management', 'DeviceManagementController@index');
Route::get('device-management/form/{id?}', 'DeviceManagementController@get');
Route::post('device-management/save/{id?}', 'DeviceManagementController@save');
Route::post('device-management/delete/{id}', 'DeviceManagementController@delete');
Route::get('device-management/attribute-by-devicetype/{id}', 'DeviceManagementController@getAttr');
Route::get('device-management/option-attribute', 'DeviceManagementController@getOptionAttr');
Route::get('device-management/option-attribute-load-form-add', 'DeviceManagementController@optionAttributeLoadFormAdd');
Route::post('device-management/option-attribute-value-save', 'DeviceManagementController@optionAttributeValueSave');
Route::post('device-management/option-attribute-value-delete/{id}', 'DeviceManagementController@optionAttributeValueDelete');

Route::get('device-set-editor', 'DeviceSetEditorController@index');
Route::get('device-set-editor/form/{id?}', 'DeviceSetEditorController@get');
Route::post('device-set-editor/save/{id?}', 'DeviceSetEditorController@save');
Route::post('device-set-editor/delete/{id}', 'DeviceSetEditorController@delete');
Route::get('device-set-editor/search-device-by-something/{type?}/{value?}', 'DeviceSetEditorController@searchDeviceBySomething');
Route::post('device-set-editor/delete-device-set-device/{id}', 'DeviceSetEditorController@deleteDeviceSetDevice');

Route::get('beacon-set-editor', 'BeaconSetEditorController@index');
Route::get('beacon-set-editor/form/{id?}', 'BeaconSetEditorController@get');
Route::post('beacon-set-editor/save/{id?}', 'BeaconSetEditorController@save');
Route::post('beacon-set-editor/delete/{id}', 'BeaconSetEditorController@delete');
Route::post('beacon-set-editor/get-by-beacon-set', 'BeaconSetEditorController@getByBeaconSet');

Route::get('location', 'LocationController@index');
Route::get('location/form/{id?}', 'LocationController@get');
Route::post('location/save/{id?}', 'LocationController@save');
Route::post('location/delete/{id}', 'LocationController@delete');

Route::get('feed-management', 'FeedManagementController@index');
Route::get('feed-management/form/{id?}', 'FeedManagementController@get');
Route::post('feed-management/save/{id?}', 'FeedManagementController@save');
Route::post('feed-management/delete/{id}', 'FeedManagementController@delete');

Route::get('rule-management', 'RuleManagementController@index');
Route::get('rule-management/form/{id?}', 'RuleManagementController@get');
Route::post('rule-management/save/{id?}', 'RuleManagementController@save');
Route::post('rule-management/delete/{id}', 'RuleManagementController@delete');
Route::get('rule-management/get-device-editor-by-device/{id?}', 'RuleManagementController@getDeviceEditorByDevice');
Route::get('rule-management/get-attr-by-device/{id?}', 'RuleManagementController@getAttrByDevice');
Route::get('rule-management/get-command-by-device/{id?}', 'RuleManagementController@getCommandByDevice');
Route::post('rule-management/delete-condition/{id}', 'RuleManagementController@deleteCondition');
Route::get('rule-management/get-route-by-type/{id}', 'RuleManagementController@getRouteByType');
Route::get('rule-management/get-variable-map', 'RuleManagementController@getVariableMap');
Route::post('rule-management/save-variable-map', 'RuleManagementController@saveVariableMap');

Route::get('scheduled-commands', 'ScheduledCommandsController@index');
Route::get('scheduled-commands/form/{scId?}', 'ScheduledCommandsController@get');
Route::post('scheduled-commands/save/{scId?}', 'ScheduledCommandsController@save');
Route::post('scheduled-commands/delete/{scId}', 'ScheduledCommandsController@delete');
Route::post('scheduled-commands/get-cmds-by-device', 'ScheduledCommandsController@getCmdsByDeviceId');
Route::post('scheduled-commands/get-params-by-command', 'ScheduledCommandsController@getParamsByCommandId');
Route::post('scheduled-commands/run-command', 'ScheduledCommandsController@runCommand');

Route::get('messaging', 'MessagingController@index');
Route::get('messaging/form/{mId?}', 'MessagingController@get');
Route::post('messaging/save/{mId?}', 'MessagingController@save');
Route::post('messaging/delete/{mId}', 'MessagingController@delete');

// Marketing
Route::get('surveys', 'SurveysController@index');
// Route::post('surveys', 'SurveysController@export');
Route::get('surveys/form/{surveyId?}', 'SurveysController@get');
Route::get('surveys/{surveyId?}/question/form/{questionId?}', 'SurveysController@getQuestion');
Route::post('surveys/{surveyId}/question/save/{questionId?}', 'SurveysController@saveQuestion');
Route::post('surveys/{surveyId}/question/delete/{questionId}', 'SurveysController@deleteQuestion');
Route::post('surveys/save/{surveyId?}', 'SurveysController@save');
Route::post('surveys/delete/{surveyId?}', 'SurveysController@delete');
Route::post('surveys/raw-data', 'SurveysController@export');
Route::post('surveys/change-number-order-question', 'SurveysController@ChangeOrderQuestion');
Route::get('surveys/result/{surveyId?}', 'SurveysController@result');
Route::get('surveys/analytic', 'SurveysController@analytic');
Route::get('surveys/question/get-routing', 'SurveysController@getRouting');

Route::get('ad-campaign', 'AdCampaignController@index');
Route::get('ad-campaign/form/{acId?}', 'AdCampaignController@get');
Route::post('ad-campaign/save/{acId?}', 'AdCampaignController@save');
Route::post('ad-campaign/delete/{acId}', 'AdCampaignController@delete');
Route::post('ad-campaign/raw-data', 'AdCampaignController@export');

Route::get('beacon-campaigns', 'BeaconCampaignsController@index');
Route::get('beacon-campaigns/form/{acId?}', 'BeaconCampaignsController@get');
Route::post('beacon-campaigns/save/{acId?}', 'BeaconCampaignsController@save');
Route::post('beacon-campaigns/delete/{acId}', 'BeaconCampaignsController@delete');

Route::get('beacon-campaigns/get-calender-data/{startdate}/{enddate}', 'BeaconCampaignsController@getCalenderData');

Route::get('beacon-notifications', 'BeaconNotificationsController@index');
Route::get('beacon-notifications/form/{id?}', 'BeaconNotificationsController@get');
Route::post('beacon-notifications/save/{id?}', 'BeaconNotificationsController@save');
Route::post('beacon-notifications/delete/{id}', 'BeaconNotificationsController@delete');


// My Account
Route::get('my-account', 'UserController@myAccount');
Route::post('my-account', 'UserController@myAccount');

// Super Admin
Route::get('super-admin', 'SuperAdminController@index');
Route::post('super-admin/get-major-ids-by-uuid', 'SuperAdminController@getMajorIdsByUUID');
Route::post('super-admin/get-minor-ids-by-uuid-and-major-id', 'SuperAdminController@getMinorIdsByUUIDAndMajorId');
Route::post('super-admin/search-beacons', 'SuperAdminController@searchBeacons');
Route::post('super-admin/search-devices', 'SuperAdminController@searchDevices');
Route::get('super-admin/get-beacon/{id}', 'SuperAdminController@getBeacon');
Route::get('super-admin/get-device/{id}', 'SuperAdminController@getDevice');

Route::get('user-admin', 'UserAdminController@index');
Route::get('user-admin/form/{userId?}', 'UserAdminController@get');
Route::post('user-admin/save/{userId?}', 'UserAdminController@save');
Route::post('user-admin/change-status', 'UserAdminController@changeStatus');
Route::post('user-admin/delete/{userId?}', 'UserAdminController@delete');

Route::get('organization-editor', 'OrganizationEditorController@index');
Route::get('organization-editor/form/{orgId?}', 'OrganizationEditorController@get');
Route::post('organization-editor/save/{orgId?}', 'OrganizationEditorController@save');
Route::post('organization-editor/delete/{orgId}', 'OrganizationEditorController@delete');

Route::get('device-type-management', 'DeviceTypeManagementController@index');
Route::get('device-type-management/form/{dtId?}', 'DeviceTypeManagementController@get');
Route::post('device-type-management/save/{dtId?}', 'DeviceTypeManagementController@save');
Route::post('device-type-management/delete/{dtId}', 'DeviceTypeManagementController@delete');

Route::get('device-type-management/attribute/form/{attrId?}', 'DeviceTypeManagementController@getAttribute');
Route::post('device-type-management/{dtypeId}/attribute/save/{attrId?}', 'DeviceTypeManagementController@saveAttribute');
Route::post('device-type-management/attribute/delete/{attrId}', 'DeviceTypeManagementController@deleteAttribute');

Route::get('device-type-management/command/form/{cmdId?}', 'DeviceTypeManagementController@getCommand');
Route::post('device-type-management/{dtypeId}/command/save/{cmdId?}', 'DeviceTypeManagementController@saveCommand');
Route::post('device-type-management/command/delete/{cmdId}', 'DeviceTypeManagementController@deleteCommand');

Route::get('device-type-management/parameter/form/{paramId?}', 'DeviceTypeManagementController@getParameter');
Route::post('device-type-management/{cmdId}/parameter/save/{paramId?}', 'DeviceTypeManagementController@saveParameter');
Route::post('device-type-management/parameter/delete/{paramId}', 'DeviceTypeManagementController@deleteParameter');

Route::get('device-type-management/configuration/form/{cfgId?}', 'DeviceTypeManagementController@getConfiguration');
Route::post('device-type-management/{dtypeId}/configuration/save/{cfgId?}', 'DeviceTypeManagementController@saveConfiguration');
Route::post('device-type-management/configuration/delete/{cfgId}', 'DeviceTypeManagementController@deleteConfiguration');

//Protocol
Route::get('protocol-management', 'ProtocolManagementController@index');
Route::get('protocol-management/form/{protocolId?}', 'ProtocolManagementController@get');
Route::post('protocol-management/delete/{protocolId?}', 'ProtocolManagementController@delete');
Route::post('protocol-management/save/{protocolId?}', 'ProtocolManagementController@save');

// User
Route::get('forgot', 'UserController@forgot');
Route::post('forgot', 'UserController@forgot');
Route::get('logout', 'UserController@logout');
Route::get('confirm', 'UserController@confirm');

// Privilege admin
Route::get('privilege-admin', 'PrivilegeAdminController@index');
Route::get('privilege-admin/form/{id?}', 'PrivilegeAdminController@get');
Route::post('privilege-admin/save/{id?}', 'PrivilegeAdminController@save');
Route::post('privilege-admin/delete/{id}', 'PrivilegeAdminController@delete');

// Permissions admin
Route::get('permissions-admin', 'PermissionsAdminController@index');

Route::get('permissions-admin/form/{id?}', 'PermissionsAdminController@get');
Route::post('permissions-admin/save/{id?}', 'PermissionsAdminController@save');
Route::post('permissions-admin/delete/{id}', 'PermissionsAdminController@delete');
Route::get('user-permissions-by-class', 'PermissionsAdminController@userPermissionsByClass');

// Class admin
Route::get('class-admin', 'ClassAdminController@index');
Route::get('class-admin/form/{id?}', 'ClassAdminController@get');
Route::post('class-admin/save/{id?}', 'ClassAdminController@save');
Route::post('class-admin/delete/{id}', 'ClassAdminController@delete');
