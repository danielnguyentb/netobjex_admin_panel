<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\EnterpriseModel as EnterPrise;
use App\Models\CustomersModel as Customers;
use \ErrorException as ErrorException;
use \Exception as Exception;

class EnterpriseGatewayController extends Controller
{
    public function index()
    {
        $data = [];
        $data['existCustomerId'] = getCurrentCustomerId();
        $data['title'] = trans('core.Enterprise Gateway');

        return view('enterprise_gateway.index', $data);
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'name' => '',
            'url' => '',
            'etype' => '',
            'username' => '',
            'password' => '',
            'etypes' => [[1, trans('core.REDMINE')], [2, trans('core.SUGARCRM')], [3, trans('core.CIVICRM')]],
            'form' => 'add'
        ];

        if ($id != null) {
            $enterprise = EnterPrise::getInstance()->findById($id);

            if (!empty($enterprise->error)) {
                trans('core.not found');
            }
            $data['id'] = $enterprise->id;
            $data['name'] = $enterprise->name;
            $data['url'] = $enterprise->url;
            $data['etype'] = $enterprise->etype;
            $data['username'] = $enterprise->username;
            $data['password'] = $enterprise->password;
            $data['form'] = 'update';
        }

        return view('enterprise_gateway.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'url' => 'required|url',
                    'username' => 'required|max:255|min:2',
                    'password' => 'required|max:255|min:2',
                    'etype' => 'required|in:CIVICRM,REDMINE,SUGARCRM',
                ],
                [],
                [
                    'etype' => trans('core.Type')
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $post = \Request::all();
            $isExists = EnterPrise::isExists($post['name'], trans('core.Enterprise Gateway Name'), $id);

            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [];
            $data['name'] = $post['name'];
            $data['username'] = $post['username'];
            $data['password'] = $post['password'];
            $data['url'] = $post['url'];

            $etype = $post['etype'];
            $etypeArr = ['REDMINE' => 1, 'SUGARCRM' => 2, 'CIVICRM' => 3];
            $data['etype'] = $etypeArr[$etype];

            if ($id) {
                $obj = EnterPrise::updateById($id, $data);
                $response['message'] = trans('core.Enterprise Gateways  has been updated');

                if (!empty($obj->error)) {
                    $response['message'] = $obj->error->message;
                    $response['status'] = FALSE;
                }

            } else {

                $obj = EnterPrise::getInstance()->insert($data);
                $response['message'] = trans('core.Enterprise has been saved');

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = EnterPrise::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = trans('core.Deleted enterprise successfully');
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

}