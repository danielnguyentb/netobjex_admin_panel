<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;
use \Validator AS Validator;

use \App\Models\ScheduledCommandsModel AS ScheduledCommands;
use \App\Models\DeviceManagementModel AS Devices;
use \App\Models\DeviceTypesModel AS DeviceTypes;
use \App\Models\CommandsModel AS Commands;
use \App\Models\CommandParamsModel AS CommandParams;
use \App\Models\CommandParamValuesModel AS CommandParamValues;

use \Config AS Config;


class ScheduledCommandsController extends Controller
{

    public function index()
    {
        $data = [
            'isSelectedOrg' => getCurrentCustomerId(),
            'title'         => trans('core.Scheduled Commands')
        ];

        return view('scheduled_commands.index', $data);
    }

    public function get($scId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($scId != NULL) {
            try {
                $scheduledCommand = ScheduledCommands::findById($scId);
                if (!empty($scheduledCommand->error)) throw new ErrorException($scheduledCommand->error->message);

                $data                = (array) $scheduledCommand;
                $data['form']        = 'edit';
                $data['startdate']   = setData($scheduledCommand, 'startdate', 'date', 'm/d/Y');
                $data['enddate']     = setData($scheduledCommand, 'enddate', 'date', 'm/d/Y');
                $data['exectime']    = setData($scheduledCommand, 'exectime', 'date', 'm/d/Y H:i');
                $data['showEndDate'] = $data['enddate'] != NULL ? TRUE : FALSE;

                // get device
                $device = Devices::findById($scheduledCommand->deviceid);
                if (!empty($device->error)) throw new ErrorException($device->error->message);

                // get device type
                $device     = property_exists($device, 'type') ? $device : $device[0];
                $deviceType = DeviceTypes::findById($device->type);
                if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

                // get commands
                $commands = Commands::find(['where' => ['dtype' => $deviceType->id]]);
                if (!empty($commands->error)) throw new ErrorException($commands->error->message);

                $data['commandsList'] = buildDropDownOpts($commands);

                // get command params values
                $commandParamValues = CommandParamValues::find(['where' => ['scheduledcommandid' => $scheduledCommand->id]]);
                if (!empty($commandParamValues->error)) throw new ErrorException($commandParamValues->error->message);

                $allCmdParams = CommandParams::find(['where' => ['commandid' => $scheduledCommand->commandid]]);
                if (!empty($allCmdParams->error)) throw new ErrorException($allCmdParams->error->message);

                // get command params name
                if (!empty($commandParamValues)) {
                    foreach ($allCmdParams as &$acp) {
                        $acp->value = '';

                        foreach ($commandParamValues as $cpv) {
                            if ($acp->id == $cpv->commandparamid) {
                                $acp->value = $cpv->value;
                                break;
                            }
                        }
                    }
                }

                $data['allCmdParams'] = $allCmdParams;
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'         => 'add',
                'id'           => '',
                'name'         => '',
                'deviceid'     => '',
                'startdate'    => '',
                'enddate'      => '',
                'repeat'       => FALSE,
                'everynum'     => 0,
                'repeatperiod' => 0,
                'exectime'     => '',
                'showEndDate'  => FALSE,
                'commandid'    => '',

                'commandsList'       => [],
                'commandParamValues' => [],
                'allCmdParams'       => []
            ];
        }



        $data['devicesList']    = array_merge(['' => 'Select Device'], buildDropDownOpts(Devices::find()));

        $data['repeatPeriodsList']   = ScheduledCommands::getInstance()->getScheduledCommandRepeatPeriod();
        $data['repeatEveryNumList'] = ScheduledCommands::getInstance()->getScheduledCommandEveryNum();

        return view('scheduled_commands.edit_scheduled_commands', $data);
    }

    public function save($scId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $messages = [];
            $rules    = [
                'name'       => 'required|min:2|max:255',
                'device'     => 'required',
                'start_date' => 'date_format:m/d/Y|after:tomorrow|required',
                'command'    => 'required',
                'end_date'   => 'date_format:m/d/Y|after:start_date',
                'exec_time'  => 'date_format:m/d/Y H:i'
            ];

            if (isset($post['show_end_date']) && $post['show_end_date'] == 1) $rules['end_date'] .= '|required';

            if (!empty($post['command'])) {
                $i = 1;

                foreach ($post as $k => $v) {
                    if (preg_match('/^param_/', $k)) {
                        $rule       = 'required';
                        $fieldIndex = explode('_', $k);

                        if ($fieldIndex[2] == 'Int') {
                            $rule .= '|integer';
                            $messages["$k.integer"] = "The parameter $i field must be integer";
                        } elseif ($fieldIndex[2] == 'String') {
                            $rule .= '|string';
                            $messages["$k.string"] = "The parameter $i field must be string";
                        } else {
                            $rule .= '|date_format:m/d/Y';
                            $messages["$k.date_format"] = "The parameter $i field format must be m/d/Y H:i";
                        }

                        $rules[$k] = $rule;

                        $messages["$k.required"] = "The parameter $i field is required";

                        $i++;
                    }
                }
            }

            $validator = Validator::make($post, $rules, $messages);

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $dataScheduledCommand = [
                'name'         => setData($post, 'name'),
                'deviceid'     => setData($post, 'device'),
                'startdate'    => setData($post, 'start_date', 'date'),
                'enddate'      => (isset($post['show_end_date']) && $post['show_end_date'] == 1) ? setData($post, 'end_date', 'date') : NULL,
                'repeat'       => setData($post, 'show_repeat', 'bool', DATE_ISO8601, FALSE, FALSE),
                'everynum'     => setData($post, 'every_num'),
                'repeatperiod' => setData($post, 'repeat_period'),
                'exectime'     => setData($post, 'exec_time', 'date'),
                'commandid'    => setData($post, 'command')
            ];

            $isEdit = $scId != NULL ? TRUE : FALSE;

            $posted = $isEdit ? ScheduledCommands::updateById($post['id'], $dataScheduledCommand) : ScheduledCommands::create($dataScheduledCommand);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }

            // delete all old command param values
            if ($isEdit) {
                $deleted = CommandParamValues::deleteAll(['scheduledcommandid' => $posted->id]);

                if (!empty($deleted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $deleted->error->message;

                    return $response;
                }
            }

            foreach ($post as $k => $v) {
                if (preg_match('/^param_/', $k)) {
                    $fieldIndex = explode('_', $k);

                    $record = [
                        'value' => $v,
                        'scheduledcommandid' => $posted->id,
                        'commandparamid'     => $fieldIndex[1]
                    ];

                    $saved = CommandParamValues::create($record);

                    if (!empty($saved->error)) {
                        $response['status']  = FALSE;
                        $response['message'] = $saved->error->message;
                    }
                }
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($scId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deleted = ScheduledCommands::deleteById($scId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted scheduled command successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getCmdsByDeviceId()
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deviceId = Request::get('deviceId');
            $device   = Devices::findById($deviceId);

            if (!empty($device->error)) {
                $response['status']  = FALSE;
                $response['message'] = $device->error->message;
            }

            $cmds = Commands::find(['where' => ['dtype' => $device->type]]);

            if (!empty($cmds->error)) {
                $response['status']  = FALSE;
                $response['message'] = $cmds->error->message;
            }

            $response['data'] = $cmds;
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getParamsByCommandId()
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $cmdId  = Request::get('cmdId');
            $params = CommandParams::find(['where' => ['commandid' => $cmdId]]);

            if (!empty($params->error)) {
                $response['status']  = FALSE;
                $response['message'] = $params->error->message;
            }

            $response['data'] = $params;
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function runCommand()
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $scId = Request::get('scId');
            $params = [
                'deviceId'      => '',
                'deviceType'    => '',
                'command'       => '',
                'correlationID' => uniqid('admin_' . time()),
                'queueName'     => ''
            ];

            $scheduledCommand = ScheduledCommands::findById($scId);
            if (!empty($scheduledCommand->error)) throw new ErrorException($scheduledCommand->error->message);

            // get device
            $device = Devices::findById($scheduledCommand->deviceid);
            if (!empty($device->error)) throw new ErrorException($device->error->message);

            // get device type
            $device     = is_object($device) ? $device : $device[0];
            $deviceType = DeviceTypes::findById($device->type);
            if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

            $params['deviceId']   = $device->deviceid;
            $params['deviceType'] = $deviceType->name;
            $params['queueName']  = $deviceType->queuename;

            // get commands
            $command = Commands::find(['where' => ['dtype' => $deviceType->id, 'id' => $scheduledCommand->commandid]]);
            if (!empty($command->error)) throw new ErrorException($command->error->message);

            $command = is_object($command) ? $command : $command[0];
            $params['command'] = $command->name;

            // get command params values
            $commandParamValues = CommandParamValues::find(['where' => ['scheduledcommandid' => $scheduledCommand->id]]);
            if (!empty($commandParamValues->error)) throw new ErrorException($commandParamValues->error->message);

            $allCmdParams = CommandParams::find(['where' => ['commandid' => $scheduledCommand->commandid]]);
            if (!empty($allCmdParams->error)) throw new ErrorException($allCmdParams->error->message);

            // get command params name
            $cpvArr = [];

            if (!empty($commandParamValues)) {
                foreach ($allCmdParams as &$acp) {
                    $acp->value = '';

                    foreach ($commandParamValues as $cpv) {
                        if ($acp->id == $cpv->commandparamid) {
                            $acp->value = $cpv->value;
                            break;
                        }
                    }

                    $cpvArr[] = "\"{$acp->name}\":\"{$acp->value}\"";
                }
            }

            $params = json_encode($params);

            if ($cpvArr) {
                $cpvString = implode(',', $cpvArr);

                $params = rtrim($params, '}') . ",$cpvString}";
            }

            $res = $this->_sendCommand($params);

            if ($res['httpCode'] != 200) {
                $response['status']  = FALSE;
                $response['message'] = $res['errors'];
            } else {
                $response['message'] = trans('core.Run Scheduled Command Successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    private function _sendCommand($params)
    {
        $ch = curl_init(Config::get('api.microServicesUrl') . Config::get('api.publishCommand'));

        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params)
        ]);

        $response = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errors   = curl_error($ch);

        curl_close($ch);

        return ['httpCode' => $httpCode, 'errors' => $errors, 'response' => $response];
    }

}