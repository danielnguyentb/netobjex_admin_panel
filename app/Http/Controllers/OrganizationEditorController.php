<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \ErrorException AS ErrorException;
use \Exception AS Exception;
use \Validator AS Validator;

use App\Models\CustomersModel AS Customers;
use App\Models\CountriesModel AS Countries;
use App\Models\FileModel AS Files;


class OrganizationEditorController extends Controller
{

    public function index()
    {
        $data = [
            'countriesList' => Countries::getInstance()->GetCoutries(),
            'statesList'    => Countries::getInstance()->GetStates(),
            'title'         => trans('core.Organization Editor')
        ];

        return view('organization_editor.index', $data);
    }

    public function get($orgId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if($orgId !== NULL) { //Edit form
            try {
                $org = Customers::findById($orgId);

                if (isset($org->error)) throw new ErrorException($org->error->message);

                $data         = (array) $org;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'       => 'add',
                'id'         => '',
                'name'       => '',
                'email'      => '',
                'address1'   => '',
                'address2'   => '',
                'city'       => '',
                'state'      => '',
                'zip'        => '',
                'country'    => '',
                'phone'      => '',
                'logofileid' => ''
            ];
        }

        $data['countriesList'] = Countries::getInstance()->GetCoutries();
        $data['statesList']    = Countries::getInstance()->GetStates();

        return view('organization_editor.edit_organization', $data);
    }

    public function save($orgId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if(!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();
            $file = Request::file('image');

            $validator = Validator::make(
                $post,
                [
                    'organization_name' => 'required|min:2|max:255',
                    'email'             => 'required|min:2|max:255|email',
                    'address1'          => 'required|min:2|max:255',
                    'city'              => 'required|min:2|max:255',
                    'zip_code'          => 'required|regex:/^\d{5}(?:[-\s]\d{4})?$/',
                    'phone'             => 'required',
                    'image'             => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000'
                ]
            );

            if($validator->fails()) throw new ErrorException(concat_error($validator));

            $isExists = Customers::isExists($post['organization_name'], trans('core.Organization Name'), $orgId);
            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'userid'    => getCurrentUserId(),
                'name'      => setData($post, 'organization_name'),
                'email'     => setData($post, 'email'),
                'address1'  => setData($post, 'address1'),
                'address2'  => setData($post, 'address2'),
                'city'      => setData($post, 'city'),
                'state'     => setData($post, 'state'),
                'zip'       => setData($post, 'zip_code'),
                'country'   => setData($post, 'country'),
                'phone'     => setData($post, 'phone'),
                'kibanaurl' => setData($post, 'kibanaurl')
            ];

            if ($file) {
                //Upload to api
                $uploaded = Files::getInstance()->upload($file);

                if (!isset($uploaded->error)) {
                    $data['logofileid'] = $uploaded[0]->id;
                } else {
                    $response['status']  = FALSE;
                    $response['message'] = $uploaded->error->message;
                }
            }

            $posted = $orgId != NULL ? Customers::updateById($post['id'], $data) : Customers::create($data);

            if (isset($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($id)
    {
        if (Request::isMethod('post')) {
            $response = ['status' => TRUE, 'message' => '', 'data' => []];

            try {
                $deleted = Customers::deleteById($id);

                if (isset($deleted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $deleted->error->message;
                } else {
                    $response['message'] = trans('core.Deleted organization successfully');
                }
            } catch (Exception $e) {
                $response['status']  = FALSE;
                $response['message'] = $e->getMessage();
            }

            return Response::json($response);
        }
    }

}