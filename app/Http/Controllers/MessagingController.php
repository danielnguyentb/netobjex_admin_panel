<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;
use \Validator AS Validator;

use \App\Models\MessagesModel AS Messages;
use \App\Models\MessageVariablesModel AS MessageVariables;

class MessagingController extends Controller
{

    public function index()
    {
        $data = [
            'isSelectedOrg' => getCurrentCustomerId(),
            'title'         => trans('core.Messaging')
        ];

        return view('messaging.index', $data);
    }

    public function get($mId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($mId != NULL) {
            try {
                $message = Messages::findById($mId);
                if (!empty($message->error)) throw new ErrorException($message->error->message);

                $data         = (array) $message;
                $data['form'] = 'edit';

                // get variables
                $variables = MessageVariables::find(['where' => ['messageid' => $mId]]);
                if (!empty($variables->error)) throw new ErrorException($variables->error->message);

                $data['variablesList'] = buildDropDownOpts(
                    $variables,
                    'id',
                    'name',
                    TRUE,
                    [
                        ['item' => 'vars[-]', 'isFieldName' => FALSE],
                        ['item' => 'id', 'isFieldName' => TRUE],
                        ['item' => '[-]old', 'isFieldName' => FALSE]
                    ]
                );
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'          => 'add',
                'id'            => '',
                'name'          => '',
                'messagetype'   => 1,
                'mfrom'         => '',
                'mto'           => '',
                'replyto'       => '',
                'subject'       => '',
                'body'          => '',
                'variablesList' => []
            ];
        }

        $data['messageTypesList'] = Messages::getInstance()->getMessageTypes();

        return view('messaging.edit_messaging', $data);
    }

    public function save($mId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $messages = [];
            $rules    = [
                'name'         => 'required|min:2|max:255',
                'type'         => 'required',
                'message_body' => 'required|min:2'
            ];

            if ($post['type'] == 1) {
                $rules['from']    = 'email|required';
                $rules['to']      = 'email|required|different:from';
                $rules['subject'] = 'min:2|max:255|required';
            }

            $validator = Validator::make($post, $rules, $messages);

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $dataMessage = [
                'name'        => setData($post, 'name'),
                'messagetype' => intval($post['type']),
                'mfrom'       => setData($post, 'from'),
                'mto'         => setData($post, 'to'),
                'replyto'     => setData($post, 'reply_to'),
                'subject'     => setData($post, 'subject'),
                'body'        => setData($post, 'message_body')
            ];

            $isEdit = $mId != NULL ? TRUE : FALSE;

            $posted = $isEdit ? Messages::updateById($post['id'], $dataMessage) : Messages::create($dataMessage);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }

            if (!empty($post['varsList'])) {
                $vars = json_decode($post['varsList']);

                foreach ($vars as $v) {
                    $dataMessageVariable = [
                        'name' => $v->name
                    ];

                    $var = explode('[-]', $v->id);

                    if ($var[2] == 'old') {
                        $postedMV = MessageVariables::updateById($var[1], $dataMessageVariable);
                    } else {
                        $dataMessageVariable['messageid'] = $posted->id;

                        $postedMV = MessageVariables::create($dataMessageVariable);
                    }

                    if (!empty($postedMV->error)) {
                        $response['status']  = FALSE;
                        $response['message'] = $postedMV->error->message;
                    }
                }
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($mId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deleted = Messages::deleteById($mId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted message successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

}