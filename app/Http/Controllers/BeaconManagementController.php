<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\BeaconManagementModel as BeaconManagement;
use App\Models\CustomersModel as Customers;
use App\Models\LocationModel as Location;
use \ErrorException as ErrorException;
use \Exception as Exception;

class BeaconManagementController extends Controller
{
    private $_locations = [], $_groups = [], $_isLoad = 0;

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Beacon Management')
        ];

        return view('beacon_management.index', $data);
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'uuidvalue' => '',
            'majorid' => '',
            'name' => '',
            'minorid' => '',
            'description' => '',
            'groupid' => '',
            'locationid' => '',
            'lost' => false,
            'form' => 'add',
        ];

        if ($this->_isLoad == 0) {
            $this->_locations = Location::find([]);
            $this->_groups = Customers::getInstance()->getByTree();
            $this->_isLoad = 1;
        }
        $data['locations'] = $this->_locations->error?[]:$this->_locations;
        $data['groups'] = $this->_groups->error?[]:$this->_groups;

        if ($id != null) {
            $beacon = BeaconManagement::getInstance()->findById($id);

            if (!empty($beacon->error)) {
                trans('core.not found');
            }
            $data['id'] = $beacon->id;
            $data['uuidvalue'] = $beacon->uuidvalue;
            $data['majorid'] = $beacon->majorid;
            $data['name'] = $beacon->name;
            $data['minorid'] = $beacon->minorid;
            $data['description'] = $beacon->description;
            $data['groupid'] = $beacon->groupid;
            $data['locationid'] = $beacon->locationid;
            $data['lost'] = $beacon->lost;
            $data['form'] = 'update';
        }

        return view('beacon_management.edit', $data);

    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'uuidvalue' => 'required|max:255|min:2',
                    'majorid' => 'required|max:255|min:2',
                    'name' => 'required|max:255|min:2',
                    'minorid' => 'required|max:255|min:2',
                    'groupid' => 'required|max:255|min:2',
                    'locationid' => 'required|max:255|min:2',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $isExists = BeaconManagement::isExists($params['name'], trans('core.Beacon Name'), $id);

            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'uuidvalue' => $params['uuidvalue'],
                'majorid' => $params['majorid'],
                'name' => $params['name'],
                'minorid' => $params['minorid'],
                'description' => $params['description'],
                'groupid' => $params['groupid'],
                'locationid' => $params['locationid']
            ];
            $data['lost'] = (isset($params['lost'])) ? true : false;

            if ($id) {

                $obj = BeaconManagement::updateById($id, $data);
                $data['message'] = trans('core.Beacon has been updated');

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $data['customerid'] = getCurrentCustomerId();
                $obj = BeaconManagement::getInstance()->insert($data);
                $response['message'] = trans('core.Beacon has been saved');

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();

    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = BeaconManagement::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = trans('core.Deleted beacon successfully');
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}