<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use \Session as Session;

use App\Models\UsersModel as Users;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');

        if(Session::get('user')) {
            $this->_assignGlobalVars();
        }
    }

    protected function _assignGlobalVars()
    {
        $userData    = \Session::get('user');
        $orgList     = Users::getInstance()->getOrgList();
        $sideBar     = buildSideBar();

        \View::share('orgListHtml', buildOrgDropdown($orgList, $userData->org));
        \View::share('sideBarHtml', $sideBar);
        \View::share('curCustomerId', $userData->org);
        \View::share('curUserId', $userData->userUnder);
    }
    

}
