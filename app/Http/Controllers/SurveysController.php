<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\SurveysModel as Surveys;
use App\Models\CustomersModel as Customers;
use App\Models\DeviceSurveyModel as DeviceSurvey;
use App\Models\DeviceManagementModel as Devices;
use App\Models\DeviceSetEditorModel as DeviceSets;
use App\Models\ScheduleTimeModel as ScheduleTime;
use App\Models\SchedulesModel as Schedules;
use App\Models\TimezoneModel as Timezone;
use App\Models\QuestionsModel as SurveyQuestion;
use App\Models\QuestionAnswersModel as QuestionAnswers;
use App\Models\AlternateSurveyModel as AlternateSurveys;
use \App\Models\FileModel as Files;
use \App\Models\LocationModel as Locations;
use \Request AS Request;
use \Response AS Response;
use \ErrorException AS ErrorException;
use \Exception AS Exception;

class SurveysController extends Controller
{

    public function index()
    {
    	$data = [];
        $data['existCustomerId'] = getCurrentCustomerId();
        $data['title'] = trans('core.Surveys');

        return view('surveys.index', $data);
    }

    public function get($surveyId = NULL)
    {
    	$response = ['status'=>TRUE];
    	try
    	{
    		if(!Request::isMethod('get')) throw new ErrorException();
    		
    		if($surveyId != NULL)
    		{
    			$survey = Surveys::findById($surveyId);
                
                if(is_object($survey))
                {
                    if($survey->backimageid)
                    {
                        $survey->backimageid = \Config::get('api.downloadUrl').substr($survey->backimageid, 0, 3) . '/' . $survey->backimageid;
                    }
                    else
                    {
                        $survey->backimageid = url().'/img/no_img.png';
                    }

                    if($survey->logoimageid)
                    {
                        $survey->logoimageid = \Config::get('api.downloadUrl').substr($survey->logoimageid, 0, 3) . '/' . $survey->logoimageid;
                    }
                    else
                    {
                        $survey->logoimageid = url().'/img/no_img.png';   
                    }

                    if($survey->sponsorimageid)
                    {
                        $survey->sponsorimageid = \Config::get('api.downloadUrl').substr($survey->sponsorimageid, 0, 3) . '/' . $survey->sponsorimageid;
                    }
                    else
                    {
                         $survey->sponsorimageid = url().'/img/no_img.png';
                    }
                }

                $data = (array)$survey;
                $data['form'] = 'edit';

    			$deviceSurvey = DeviceSurvey::find([
                    'where'=>[
                        'surveyid' => $surveyId,
                    ]
                ]);
                
    			if($deviceSurvey)
    			{
    				$devices = array();
    				$deviceSets = array();

    				foreach($deviceSurvey as $k=>$v)
    				{
    					if($v->deviceid)
    					{
    						array_push($devices, $v->deviceid);
    					}
    					else if($v->devicesetid)
    					{
    						array_push($deviceSets, $v->devicesetid);
    					}
    				}
    				$data['devices'] = $devices;
    				$data['deviceSets'] = $deviceSets;
    			}

                $alternateSurvey = AlternateSurveys::find([
                    'where'=>[
                        'surveyid'=> $surveyId
                        ]
                    ]);

                $schedule = Schedules::find([
                    'where'=>[
                        'surveyid'=> $surveyId
                    ]
                ]);
                
                if($schedule)
                {
                    foreach($schedule as $k=>$v)
                    {
                        $data['timezoneid']     = $v->timezoneid;
                        $data['eachmonday']     = $v->eachmonday?true:false;
                        $data['eachtuesday']    = $v->eachtuesday?true:false;
                        $data['eachwednesday']  = $v->eachwednesday?true:false;
                        $data['eachthirsday']   = $v->eachthirsday?true:false;
                        $data['eachfriday']     = $v->eachfriday?true:false;
                        $data['eachsaturday']   = $v->eachsaturday?true:false;
                        $data['eachsunday']     = $v->eachsunday?true:false;
                        $data['eachday']        = $v->eachday?true:false;

                        $startDate  =   renderTimeFromDate($v->startdate);
                        $endDate    =   renderTimeFromDate($v->enddate);

                        $data['starthour']      = $startDate->hour;
                        $data['startminute']    = $startDate->minute;
                        $data['startutc']       = $startDate->period;

                        $data['endhour']        = $endDate->hour;
                        $data['endminute']      = $endDate->minute;
                        $data['endutc']         = $endDate->period;
                    }
                }
                else
                {
                    $data['timezoneid']     = '';
                    $data['eachmonday']    = false;
                    $data['eachtuesday']   = false;
                    $data['eachwednesday'] = false;
                    $data['eachthirsday']  = false;
                    $data['eachfriday']    = false;
                    $data['eachsaturday']  = false;
                    $data['eachsunday']    = false;
                    $data['eachday']       = false;
                    $data['starthour']      = '';
                    $data['startminute']    = '';
                    $data['startutc']       = '';
                    $data['endhour']        = '';
                    $data['endminute']      = '';
                    $data['endutc']         = '';
                }
                
    		}
    		else
    		{
    			$data = [
    				'form' 		  	=> 'add',
    				'id'   		  	=> '',
    				'name' 		  	=> '',
    				'description' 	=> '',
    				'backimageid'	=> '',
    				'logoimageid'   => '',
    				'sponsorimageid'=> '',
    				'startdate'     => date('m/d/Y'),
    				'enddate'       => date('m/d/Y'),
    				'devices'	    => array(),
    				'deviceSets'    => array(),
                    'timezoneid'    =>  '',
                    'eachmonday'    => false,
                    'eachtuesday'   => false,
                    'eachwednesday' => false,
                    'eachthirsday'  => false,
                    'eachfriday'    => false,
                    'eachsaturday'  => false,
                    'eachsunday'    => false,
                    'eachday'       => false,
                    'starthour'     => '',
                    'startminute'   => '',
                    'startutc'      => '',
                    'endhour'       => '',
                    'endminute'     => '',
                    'endutc'        => '',
                    'surveytype'    => null
    			];

                $listAlterSurveys = Surveys::find([
                    'fields'=>[
                       'id'=>true, 'name'=>true
                       ]
                ]);
    		}

            $listDevice = [];
            $listDeviceSet =[];

    		$device_all = Devices::find([
		       'fields'=>[
		        'deviceid',
		        'name'
		       ]
		      ]);

            if($device_all)
            {
                foreach($device_all as $k=>$v)
                {
                    $listDevice[$v->deviceid]  = $v->name;
                }
            }

    		$deviceSet_all = DeviceSets::find([
    			'fields'=>[
    				'id',
    				'name'
    			]
    		]);
    		
            if($deviceSet_all)
            {
                foreach($deviceSet_all as $k=>$v)
                {
                    $listDeviceSet[$v->id]  = $v->name;
                }
            }
            
            
            $data['listTimezone']    = renderTimezone();
            $data['listHour']        = ScheduleTime::GetHoursTime();
            $data['listMinutes']     = ScheduleTime::GetMinutesTime();
            $data['listUtc']         = ScheduleTime::GetUTC();
            $data['listDevice']      = $listDevice;
            $data['listDeviceSet']   = $listDeviceSet;

    		return view('surveys.edit_survey', $data);
    	}
    	catch(Exception $e)
    	{
    		return Response::json(array('status'=>FALSE, 'message'=>$e->getMessage()));
    	}
    }


    public function delete($id)
    {
        if (Request::isMethod('post')) {
            $response = ['status' => TRUE, 'message' => '', 'data' => []];

            try {
                $deleted = Surveys::deleteById($id);

                if (!empty($deleted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $deleted->error->message;
                } else {
                    $response['message'] = trans('core.Deleted Survey successfully');
                }
            } catch (Exception $e) {
                $response['status']  = FALSE;
                $response['message'] = $e->getMessage();
            }

            return Response::json($response);
        }
    }

    public function save($surveyId = NULL)
    {
        try
        {
            $response = ['status'=>TRUE,'message'=>'', 'data'=>[]];

            if(!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();
            $files = Request::file();
            
            $rules = [
                'name'        => 'required|max:255|min:2',
                'description' => 'max:255',
                'devices'     => 'array|required',
                'surveytype'  => 'required|in:1,2,3',
                'startdate'   => 'required|date|date_format:m/d/Y',
                'enddate'     => 'required|date|date_format:m/d/Y|after:startdate',
                'start_period'    => 'required|in:AM,PM',
                'end_period'      => 'required|in:AM,PM',
                'backimageid' => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
                'logoimg'     => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
                'sponsorimg'  => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000'
            ];

            $validator = \Validator::make($post, $rules);

            if($validator->fails()) throw new ErrorException(concat_error($validator));

            $c = Surveys::countCondition(
                [
                'name' => '%'.$post["name"].'%',
                'id'   => [
                        'neq' => (!empty($post['id'])?$post['id']:null)
                        ]
                ]
            );

            if(is_object($c) && !empty($c->count) && $c->count >0)
            {
                throw new ErrorException(trans('core.A Survey by that name already exists. Please select another.'));
            }
            // print_r($post);die;
            if (!empty($post['startdate'])) {
                $post['startdate'] .= " {$post['starthour']}:{$post['startminute']} {$post['start_period']}";
            } else {
                $post['startdate'] = NULL;
            }

            if (!empty($post['enddate'])) {
                $post['enddate'] .= " {$post['endhour']}:{$post['endminute']} {$post['end_period']}";
            } else {
                $post['enddate'] = NULL;
            }

            $dataSurvey = [
                    'name'          => $post['name'],
                    'surveytype'    => $post['surveytype'],
                    'description'   => $post['description'],
                    'startdate'     => setData($post, 'startdate', 'date'),
                    'enddate'       => setData($post, 'enddate', 'date')
                ];
            
            if ($files) {
                //Upload to api
                foreach ($files as $k => $v) {
                    $uploaded = Files::getInstance()->upload($v);

                    if (empty($uploaded->error)) {
                        $dataSurvey[$k] = $uploaded[0]->id;
                    } else {
                        $response['status']  = FALSE;
                        $response['message'] = $uploaded->error->message;
                    }
                }
            }

            $isEdit = $surveyId != NULL ? TRUE : FALSE;
            if(!$isEdit)
            {
                $data['numofquestions'] = 0;
            }
            $posted = $isEdit ? Surveys::updateById($post['id'], $dataSurvey) : Surveys::create($dataSurvey);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }

            $savedDeviceSurveys = $this->_saveDeviceSurveys($post, $posted->id, $isEdit);

            if (!empty($savedDeviceSurveys->error)) {
                $response['status']  = FALSE;
                $response['message'] = $savedDeviceSurveys->error->message;
            }

            $savedSchedules = $this->_saveSchedules($post, $posted->id, $isEdit);

            if (!empty($savedSchedules->error)) {
                $response['status']  = FALSE;
                $response['message'] = $savedSchedules->error->message;
            }

            $savedAlternate = $this->_saveAlternateSurveys($post, $posted->id, $isEdit);

            if (!empty($savedAlternate->error)) {
                $response['status']  = FALSE;
                $response['message'] = $savedAlternate->error->message;
            }
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        return Response::json($response);
    }

    private function _saveDeviceSurveys($data, $surveyId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];

        if ($isEdit) {
            $deleted = DeviceSurvey::deleteAll(['surveyid' => $surveyId]);
            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;

                return $response;
            }
        }

        if (isset($data['devices'])) {
            foreach ($data['devices'] as $d) {
                $inserted = DeviceSurvey::create(['deviceid' => $d, 'surveyid' => $surveyId]);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }

        if (isset($data['deviceSets'])) {
            foreach ($data['deviceSets'] as $ds) {
                $inserted = DeviceSurvey::create(['devicesetid' => $ds, 'surveyid' => $surveyId]);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }

        return $response;
    }

    private function _saveSchedules($data, $surveyId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];
        
        $params = [
            'surveyid'      => $surveyId,
            'startdate'     => setData($data, 'startdate', 'date'),
            'enddate'       => setData($data, 'enddate', 'date'),
            'eachday'       => setData($data, 'eachday', 'bool'),
            'eachmonday'    => setData($data, 'eachmonday', 'bool'),
            'eachtuesday'   => setData($data, 'eachtuesday', 'bool'),
            'eachwednesday' => setData($data, 'eachwednesday', 'bool'),
            'eachthirsday'  => setData($data, 'eachthirsday', 'bool'),
            'eachfriday'    => setData($data, 'eachfriday', 'bool'),
            'eachsaturday'  => setData($data, 'eachsaturday', 'bool'),
            'eachsunday'    => setData($data, 'eachsunday', 'bool'),
            'timezoneid'    => setData($data, 'timezone'),
            'fromtime'      => setData($data, 'startdate', 'fromTime', '', FALSE, 0),
            'totime'        => setData($data, 'enddate', 'toTime', '', FALSE, 0)
        ];
        
        $saved = $isEdit ? Schedules::updateAll(['surveyid' => $surveyId], $params) : Schedules::create($params);

        if (!empty($saved->error)) {
            $response['status']  = FALSE;
            $response['message'] = $saved->error->message;
        }

        return $response;
    }

    private function _saveAlternateSurveys($data, $surveyId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];

        if ($isEdit) {
            $deleted = AlternateSurveys::deleteAll(['surveyid' => $surveyId]);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;

                return $response;
            }
        }

        if(isset($data['alterSurveys']))
        {
            foreach ($data['alterSurveys'] as $d) {
                $inserted = AlternateSurveys::create(['alternateid' => $d, 'surveyid' => $surveyId]);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }

        return $response;
    }

    public function getRouting()
    {
        if (!Request::isMethod('get')) return FALSE;
        $response = ['status'=>TRUE, 'data'=> array()];
        try
        {
            $post = Request::all();

            $surveyId = isset($post['surveyId'])?$post['surveyId']:null;
            $questionId = isset($post['questionId'])?$post['questionId']:null;
            $valueTypeNum = isset($post['valueTypeNum'])?$post['valueTypeNum']:1;

            if($questionId)
            {

                $nextQuestions = SurveyQuestion::find([
                    'where'=>[
                    'surveyid'=> $surveyId,
                    'id'=>[
                    'neq'=> $questionId
                    ]
                    ]
                    ]);  
            }
            else
            {   
                $nextQuestions = SurveyQuestion::find([
                    'where'=>[
                    'surveyid'=> $surveyId
                    ]
                    ]);

            }
            
            
            $html  = '<select class="chosen-select input-md form-control" name="answer[order]['.$valueTypeNum.']">';
            if($nextQuestions)
            {

                foreach($nextQuestions as $k=>$v)
                {
                    $html.='<option value="'.$v->id.'">'.$v->numorder.'</option>';

                }
            }
            $html  .= '</select>';

            $response['data'] = $html;
            
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        echo json_encode($response);
        exit;
    }

    function getQuestion($surveyId,$questionId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;
        $survey = Surveys::findById($surveyId);

        $conditional = 'display:none;';

        if ($questionId != NULL) {
            try {
                $question = SurveyQuestion::findById($questionId);
                
                if (!empty($question->error)) throw new ErrorException($question->error->message);

                $hasQuestionImage = $question->questionimageid?true:false;
                

                $question->questionimageid = $hasQuestionImage?(\Config::get('api.downloadUrl').substr($question->questionimageid, 0, 3) . '/' . $question->questionimageid):url().'/img/no_img.png';
                
                $data         = (array) $question;
                $data['hasQuestionImage'] = $hasQuestionImage;
                $data['form'] = 'edit';
                $data['javascript'] = '';
                $QuestionAnswers = QuestionAnswers::find([
                    'where'=>[
                        'questionid' => $questionId
                    ]
                ]);
                
                $totalOrder =  $survey->numofquestions;

                if($question->answertype == '2' && $totalOrder != $question->numorder)
                {
                        $conditional = 'display: block;';
                }
                $nxQ = array();
                if($totalOrder != $question->numorder)
                {
                    $nextQuestions = SurveyQuestion::find([
                            'where'=>[
                                'surveyid'=> $question->surveyid,
                                'id'=>[
                                    'neq'=> $questionId
                                ]
                            ]
                        ]);
                    
                    if($nextQuestions)
                    {
                        foreach($nextQuestions as $k=>$v)
                        {
                            $nxQ[$v->id] = $v->numorder;
                        }
                    }
                    $data['noRouting'] = false;
                }
                else
                {
                    $data['noRouting'] = true;
                }
                $data['nextquestions'] = $nxQ;


                if($QuestionAnswers)
                {
                    if($question->answertype !=4)
                    {
                        foreach($QuestionAnswers as $k=>$v)
                        {
                            $data['answers'][$k]['id']   = $v->id;
                            $data['answers'][$k]['name'] = $v->answer;
                            $data['answers'][$k]['nextquestionid'] = $v->nextquestionid;
                            $data['answers'][$k]['answerimageid'] = $v->answerimageid?(\Config::get('api.downloadUrl').substr($v->answerimageid, 0, 3) . '/' . $v->answerimageid):url().'/img/no_img.png';
                            $data['answers'][$k]['isAnswerimage'] = $v->answerimageid?true:false;
                        }    
                    }
                    else
                    {
                        $data['answers'] = array();
                        $data['javascript'] = $QuestionAnswers[0]->answer;
                    }
                    
                }


            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'            => 'add',
                'id'              => '',
                'question'        => '',
                'javascript'      => '',
                'answertype'      => '',
                'routing'         => '',
                'numofpanels'     => '',
                'answerimagesize' => '',
                'questionimageid' => url().'/img/no_img.png',
                'answers'         => array(),
                'nextquestions'   => array(),
                'noRouting'       => true,
                'hasQuestionImage'=> false
            ];

        }

        $data['surveyId'] = $surveyId;
        
        $data['listSurveyAnswerType']  = buildSurveyAnswerTypeDropDown();

        $data['surveytype'] = $survey->surveytype;

        $data['select_routing'] = $this->dropdown('routing', array('1'=>'Automatic', '2'=>'Conditional', '3'=>'Terminal'),$data['routing'],'class="chosen-select input-md form-control" id="routing"', $conditional);

        if($survey->surveytype == 3 || $survey->surveytype == null)
        {
            
            return view('surveys.edit_question', $data);    
        }
        else
        {
            // $data['select_routing'] = $this->buildRadioRouting('routing', array('1'=>'Automatic', '2'=>'Conditional', '3'=>'Terminal'), $data['routing']);
            return view('surveys.edit_question_panel', $data);       
        }
        
    }


    function saveQuestion($surveyId, $questionId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $survey = Surveys::findById($surveyId);

            if (!empty($survey->error)) throw new ErrorException($survey->error->message);

            $post  = Request::all();
            $files = Request::file();
             
            $rules = [
                    'question'        => 'required|min:2|max:255',
                    'answertype'      => 'required|numeric|between:1,4',
                    'numofpanels'     => 'required|numeric|between:2,6',
                    'questionimageid' => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000'
            ];

            if(isset($post['answertype']) && ($post['answertype'] == 2 || $post['answertype'] ==3) && $survey->surveytype == 3)
            {
                $rules['answer.name'] = 'required|array|min:2';
            }


            $validator = \Validator::make(
                $post,
                $rules
            );
            
            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $c = SurveyQuestion::countCondition(
                [
                'surveyid' => $surveyId,
                'question' => '%'.$post["question"].'%',
                'id'   => [
                        'neq' => (!empty($post['id'])?$post['id']:null)
                        ]
                ]
            );

            if(is_object($c) && !empty($c->count) && $c->count >0)
            {
                throw new ErrorException(trans('core.A Question by that name already exists. Please select another.'));
            }

            if(isset($post['answertype']) && ($post['answertype'] == 2 || $post['answertype'] ==3))
            {
                $flag = '';
                $answers = $post['answer']['name'];
                foreach ($answers as $key => $val) {
                    $flag = $val;
                    unset($answers[$key]);
                    break;
                }
                $this->compairAnswer($answers, $flag);
            }

           

            $data = [
                'question'         => setData($post, 'question'),
                'answertype'       => setData($post, 'answertype'),
                'surveyid'         => $survey->id,
                'numofpanels'      => setData($post, 'numofpanels'),
                'answerimagesize'  => setData($post, 'answerimagesize'),
                'routing'          => setData($post, 'routing')
            ];

            if (isset($files['questionimageid'])) {
                    $questionimageid = Files::getInstance()->upload($files['questionimageid']);

                    if (empty($questionimageid->error)) {
                        $data['questionimageid'] = $questionimageid[0]->id;
                    } else {
                        $response['status']  = FALSE;
                        $response['message'] = $questionimageid->error->message;
                    }
            }

            $isEdit = $questionId != NULL ? TRUE : FALSE;
            if(!$isEdit)
            {
                $data['numorder'] = (int)$survey->numofquestions + 1;
            }
            
            $posted = $isEdit ? SurveyQuestion::updateById($post['id'], $data) : SurveyQuestion::create($data);

            
            $this->_saveQuestionAnswer($post, $posted->id, $isEdit);
            

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
            else
            {
                if(!$isEdit)
                {
                    Surveys::updateById($surveyId, ['numofquestions'=> (int)$survey->numofquestions + 1]);
                }
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }
    private function compairAnswer($list, $flag)
    {
        if ($list) {
            $isCompair = false;
            foreach ($list as $key => $val) {
                if ($flag == $val) {
                    $isCompair = true;
                }
            }

            if ($isCompair) {
                throw new ErrorException(trans('core.Should not have the same answers for current question with selected type. Please update answer list.'));
            } else {
                foreach ($list as $key => $val) {
                    $flag = $val;
                    unset($list[$key]);
                    $this->compairAnswer($list, $flag);
                    break;
                }
            }
        } else {
            return true;
        }

    }    

    private function dropdown($name,$options=[], $selected, $extra='', $conditional) {
        $html = '';

        foreach($options as $value => $text) {
            $set_selected = '';
            if($value == $selected) {
                $set_selected = 'selected';
            }

            $html .= '<option '.($value=="2"?'style="'.$conditional.'" class="conditional"':"").' value="'.$value.'" '.$set_selected.'>'.$text.'</option>';
        }

        return '<select name="'.$name.'" '.$extra.'>'.$html.'</select>';
    }


    private function _saveQuestionAnswer($data, $questionId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];

        if ($isEdit) {
            $qanswers = QuestionAnswers::find(['where'=>['questionid'=>$questionId],'fields'=>['id'=>true]]);
            if($qanswers)
            {
                foreach($qanswers as $k=>$v)
                {
                    $deleted = QuestionAnswers::deleteById($v->id);

                    if (!empty($deleted->error)) {
                        $response['status']  = FALSE;
                        $response['message'] = $deleted->error->message;

                        return $response;
                    }
                }
            }
        }
        
        if(($data['answertype'] == 2 || $data['answertype'] == 3) && isset($data['answer']))
        {

            foreach ($data['answer']['name'] as $k=>$v) {
                
                $params = ['answer' => $v, 'questionid' => $questionId, 'nextquestionid'=>(is_array($data['answer']) && array_key_exists('order', $data['answer']) && $data['routing']==2)?$data['answer']['order'][$k]:null];                
                if(isset($data['answer']['image']))
                {
                    $answerimageid = Files::getInstance()->upload($data['answer']['image'][$k]);

                    if (empty($answerimageid->error)) {
                        $params['answerimageid'] = $answerimageid[0]->id;
                    } else {
                        $response['status']  = FALSE;
                        $response['message'] = $answerimageid->error->message;
                    }
                }

                $inserted = QuestionAnswers::create($params);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }
        else if(isset($data['answertype']) && $data['answertype'] == 4 && isset($data['javascript']))
        {
            $inserted = QuestionAnswers::create(['answer' => $data['javascript'], 'questionid' => $questionid,'nextquestionid'=>null]);
        }

        return $response;
    }

    function deleteQuestion($surveyId, $questionId = NULL)
    {
        if (Request::isMethod('post')) {
            $response = ['status' => TRUE, 'message' => '', 'data' => []];

            try {
                $deleted = SurveyQuestion::deleteById($questionId);

                if (!empty($deleted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $deleted->error->message;
                } else {
                    $survey = Surveys::findById($surveyId);
                    Surveys::updateById($surveyId, ['numofquestions'=>((int)$survey->numofquestions -1)]);
                    $response['message'] = trans('core.Deleted Survey successfully');
                }
            } catch (Exception $e) {
                $response['status']  = FALSE;
                $response['message'] = $e->getMessage();
            }

            return Response::json($response);
        }
    }

    function ChangeOrderQuestion()
    {
        $response = array('status'=>TRUE);
        try
        {
            if(!Request::isMethod('post')) throw new ErrorException();
            
            $post = Request::all();

            $number   = !empty($post['number'])?$post['number']:'';
            $questionId = !empty($post['questionId'])?$post['questionId']: null;
            $surveyId = !empty($post['surveyId'])?$post['surveyId']: null;

            if($questionId)
            {
                $curQuestion = SurveyQuestion::findById($questionId);
                
                if(!empty($curQuestion->error))
                {
                    $response['status'] = FALSE;
                    $response['message'] = $curQuestion->error->message;
                }

                $curOrder = $curQuestion->numorder;
                
                $surveyQuestion = SurveyQuestion::find(['where'=>['surveyid'=>$surveyId]]);

                $countQuestion = SurveyQuestion::countCondition(['surveyid'=>$surveyId]);

                if(!empty($countQuestion->error))
                {
                    $response['status'] = FALSE;
                    $response['message'] = $countQuestion->error->message;
                }

                $maxOrder = $countQuestion->count;

                if($maxOrder > 0)
                {
                    if($number > $maxOrder)
                    {
                        foreach($surveyQuestion as $k=>$v)
                        {
                            $data = [];
                            if($curOrder < $v->numorder && $v->numorder < $number)
                            {
                                $data = ['numorder'=> $v->numorder - 1];
                                SurveyQuestion::updateById($v->id, $data);
                            }
                        }
                        $number = $maxOrder;
                    }
                    else
                    {
                        foreach ($surveyQuestion as $k=>$v) {
                          if($v->numorder == $number)
                          {
                              $data = ['numorder'=> $curOrder];
                              SurveyQuestion::updateById($v->id, $data);
                          }
                      }
                    }
                    surveyQuestion::updateById($curQuestion->id, ['numorder'=>$number]);
                }
            }
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        
        return Response::json($response);
    }

    function analytic()
    {
        $response = array('status'=>TRUE);
        try{
            if(!Request::isMethod('get')) throw new ErrorException();

            $post = Request::all();
            $surveyId = isset($post['surveyId'])?$post['surveyId']:null;
            $startDate = isset($post['startDate'])?$post['startDate']:null;
            $endDate  = isset($post['endDate'])?$post['endDate']:null;
            $locationId  = isset($post['locationId'])?$post['locationId']:null;

            $filters = [
                    'fromDate'=>$startDate,
                    'endDate' => $endDate,
                    'locationid'=> $locationId
            ];

            $anls = Surveys::GetInstance()->getAnalytics($surveyId, $filters);
            
            $response = $anls->respondents;
            $dropouts = $anls->dropouts;
            $answers  = $anls->answers;
            $results  = $anls->results;

            
            $ansArr = array();
            if($answers)
            {

                foreach($answers as $key=>$val)
                {
                    if($val->question)
                        $ansArr[$val->question][] = array('answer'=>$val->answer,'count'=>$val->count);
                }

            }
            $result['ansList'] = $ansArr;
            if($results)
            {
                $raw = '';

                foreach($results as $val)
                {
                    if($val->mobile)
                    {
                        $raw.= '<tr><td>'.$val->mobile.'</td><td>'.$val->created.'</td></tr>';
                    }
                }
                $result['rawData'] = $raw;
            }
            if($response)
            {
                foreach($response as $val)
                {
                    $result['numOfRespondents'] = isset($val->count)?$val->count:0;
                }
            }
            if($dropouts)
            {
                foreach($dropouts as $val)
                {

                    $result['numOfDropouts'] = isset($val->count)?$val->count:0;
                }
            }
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();   
        }

        echo json_encode($result);
        exit();
    }

    function result($surveyId = NULL)
    {
        try{
            if(!Request::isMethod('get')) throw new ErrorException(); 
            
            $post = Request::all();

            if($surveyId)
            {
                $anls = Surveys::GetInstance()->getAnalytics($surveyId);
                $survey = Surveys::findById($surveyId);

                if(!empty($anls->error))
                {
                    $response['status'] = FALSE;
                    $response['message'] = $anls->error->message;
                }

                $response = $anls->respondents;
                $dropouts = $anls->dropouts;
                $answers  = $anls->answers;
                $results  = $anls->results;

                $locations = Locations::find([
                    'fields'=>[
                        'id'  =>true, 
                        'name'=>true]
                ]);
                
                $locationList = [];

                if($locations)
                {
                    foreach ($locations as $k => $v) {
                        $locationList[$v->id] = $v->name;
                    }
                }
                
                $data = [
                    'surveyId'     => $surveyId,
                    'surveyName'   => $survey->name,
                    'results'      => $results,
                    'locationList' => $locationList,
                    'startDate'    => isset($post['startDate'])?$post['startDate']:date('m/d/Y',strtotime("-1 week +1 day")),
                    'endDate'      => isset($post['endDate'])?$post['endDate']:date('m/d/Y')
                ];
                
                return view('surveys.result', $data);
            }
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    function export()
    {

        $response = ['status'=>TRUE];
        try{
            $post = Request::all();
            
            $startDate = isset($post['startDate'])?$post['startDate']:null;
            $endDate = isset($post['endDate'])?$post['endDate']:null;
            $surveyId = isset($post['surveyId'])?$post['surveyId']:null;

            //get survey rawData
            $filters = [
                'where'=>[
                    'fromDate'=> $startDate,
                    'endDate' => $endDate
                    ]
            ];

            $data = Surveys::GetInstance()->getRawData($surveyId, $filters);
            
            $response['html']= exportRawData($data);
        }
        catch(Exception $e)
        {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }
}
