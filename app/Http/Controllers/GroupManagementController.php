<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\GroupManageModel as GroupManage;
use App\Models\CustomersModel as Customer;
use App\Models\CountriesModel AS Countries;
use App\Models\UsersModel;
use \ErrorException as ErrorException;
use Mockery\Exception;


class GroupManagementController extends Controller
{

    public function index()
    {

        $userData = \Session::get('user');

        if ($userData->org != '') { // if user selected an org then set role is CustomerAdmin
            \Session::set('userRole', 'CustomerAdmin');
        } else { // check role if user have not selected org yet
            $role = UsersModel::getInstance()->getUserRole($userData->uid);

            if (empty($role->error)) {

                if (sizeof($role) != 0) {
                    \Session::set('userRole', $role[0]->role->name);
                }
            }
        }

        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'role' => \Session::get('userRole'),
            'title' => trans('core.Group Management')
        ];
        return view('group_management.index', $data);
    }


    public function get($id = null)
    {
        $userData = \Session::get('user');
        $data = [
            'id' => '',
            'name' => '',
            'parentid' => '',
            'address1' => '',
            'address2' => '',
            'city' => '',
            'zip' => '',
            'country' => '',
            'state' => '',
            'phone' => '',
            'email' => '',
            'form' => 'add',
            'role' => \Session::get('userRole'),
            'parentId' => $userData->org
        ];

        $data['countriesList'] = Countries::getInstance()->GetCoutries();
        $data['statesList'] = Countries::getInstance()->GetStates();
        $data['parents'] = Customer::find(['where' => ['parentid' => $userData->org]]);

        if ($id != null) {

            $object = Customer::getInstance()->findById($id);

            if (!empty($object->error)) {
                trans('core.not found');
            }

            $data['id'] = $object->id;
            $data['name'] = $object->name;
            $data['parentid'] = $object->parentid;
            $data['address1'] = $object->address1;
            $data['address2'] = $object->address2;
            $data['city'] = $object->city;
            $data['zip'] = $object->zip;
            $data['country'] = $object->country;
            $data['state'] = $object->state;
            $data['phone'] = $object->phone;
            $data['email'] = $object->email;
            $data['form'] = 'edit';
        }

        return view('group_management.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        $userData = \Session::get('user');

        if ($userData->org != '' && \Session::get('userRole') != 'CustomerAdmin') {
            $response['status'] = FALSE;
            $response['message'] = trans('core.Access deny');
            return json_encode($response);
        }

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'parentid' => 'required|max:255|min:2',
                    'address1' => 'required|max:255|min:2',
                    'address2' => 'required|max:255|min:2',
                    'city' => 'required|max:255|min:2',
                    'zip' => 'required|max:255|min:2',
                    'country' => 'required|max:255|min:2',
                    'state' => 'required|max:255|min:2',
                    'phone' => 'required|max:255|min:2',
                    'email' => 'required|max:255|min:2',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));
            $params = \Request::all();
            $data = [
                'name' => $params['name'],
                'parentid' => \Session::get('user')->org,
                'address1' => $params['address1'],
                'address2' => $params['address2'],
                'city' => $params['city'],
                'zip' => $params['zip'],
                'country' => $params['country'],
                'state' => $params['state'],
                'phone' => $params['phone'],
                'email' => $params['email'],
            ];

            if ($id) {
                $data['parentid'] = $params['parentid'];
                $obj = Customer::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $obj = Customer::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = Customer::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

}