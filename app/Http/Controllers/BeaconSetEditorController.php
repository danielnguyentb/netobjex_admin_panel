<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\BeaconSetEditorModel as BeaconSetEditor;
use App\Models\BeaconManagementModel as BeaconManage;
use App\Models\ServiceModel as Service;
use \ErrorException as ErrorException;
use \Exception as Exception;

class BeaconSetEditorController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.BeaconSet Editor')
        ];
        return view('beacon_set_editor.index', $data);
    }

    public function getByBeaconSet()
    {
        $params = \Request::all();


        $minorids = (isset($params['minorids'])) ? $params['minorids'] : '';
        $majorids = (isset($params['majorids'])) ? $params['majorids'] : '';
        $uuids = (isset($params['uuidItem'])) ? implode(',', $params['uuidItem']) : '';

        $response = BeaconManage::getInstance()->getByBeaconSet([
            'uuids' => $uuids,
            'majorIds' => $majorids,
            'minorIds' => $minorids,
        ]);

        if (!empty($response->error)) {
            return trans('core.No data to display');
        }

        return view('beacon_set_editor.get-by-beacon', ['data' => $response]);
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'uuids' => '',
            'majorids' => '',
            'minorids' => '',
            'sizeUuid' => 0,
            'uuidArr' => [],
            'name' => '',
            'form' => 'add'
        ];
        $service = Service::getInstance()->getUUID();
        $data['services'] = $service;

        if ($id != null) {
            $beaconSet = BeaconSetEditor::getInstance()->findById($id);

            if (!empty($beaconSet->error)) {
                return trans('core.not found');

            }

            $uuidArr = explode(',', $beaconSet->uuids);
            $data['id'] = $beaconSet->id;
            $data['uuids'] = $beaconSet->uuids;
            $data['majorids'] = $beaconSet->majorids;
            $data['minorids'] = $beaconSet->minorids;
            $data['name'] = $beaconSet->name;
            $data['form'] = 'update';
            $data['uuidArr'] = $uuidArr;
            $data['sizeUuid'] = sizeof($uuidArr);
        }

        return view('beacon_set_editor.edit', $data);

    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'majorids' => 'required|max:255|min:2',
                    'majorids' => 'required|max:255|min:2',
                    'name' => 'required|max:255|min:2',
                ]
            );
            $params = \Request::all();

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            if (!isset($params['uuidItem'])) {
                return ['status' => FALSE, 'message' => trans('core.Uuids are requested'), 'data' => []];
            }
            if (sizeof($params['uuidItem']) == 0) {
                return ['status' => FALSE, 'message' => trans('core.Uuids are requested'), 'data' => []];
            }

            $data = [
                'majorids' => $params['majorids'],
                'minorids' => $params['minorids'],
                'name' => $params['name'],
                'uuids' => implode(',', $params['uuidItem'])
            ];

            if ($id) {
                $obj = BeaconSetEditor::updateById($id, $data);
                $data['message'] = '';

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {

                $data['customerid'] = getCurrentCustomerId();
                $obj = BeaconSetEditor::create($data);
                $response['message'] = '';

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();

    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = BeaconSetEditor::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

}