<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\ClassAdminModel as ClassAdmin;
use \App\Models\UsersModel AS Users;

use \Session as Session;
use \Exception as Exception;

class PermissionsAdminController extends Controller
{

    public function index()
    {

        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Rule Management')
        ];

        $classes = ClassAdmin::find();


        if (isset($classes->error)) throw new ErrorException($classes->error->message);

        $data['classes'] = $classes;
        $curUserData = Session::get('user');
        $data['users'] = [];

        if($curUserData->org != '') {
            $obj = Users::getInstance()->findByOrg($curUserData->org);

            if (isset($obj->error)) throw new ErrorException($obj->error->message);

            $data['users'] = $obj;
        }
        return view('permissions_admin.index', $data);
    }



    public function get($id = null)
    {

        $data = [
            'id' => '',
            'name' => '',
            'active' => false,
            'devicetypeid' => '',
            'devicesetid' => '',
            'queuetopicname' => '',
            'interval' => '',
            'enterprisegatewayid' => '',
            'messageid' => '',
            'commandid' => '',
            'form' => 'add',
        ];



        if ($id != null) {
            $object = RuleManage::getInstance()->findById($id);

            if (!empty($object->error)) {
                return trans('core.not found');
            }

            $data['form'] = 'edit';

        }

        return view('permissions_admin.edit', $data);
    }

    public function userPermissionsByClass() {
        $params = \Request::all();
        var_dump($params);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'devicetypeid' => 'max:255|min:2',
                    'devicesetid' => 'max:255|min:2',
                    'queuetopicname' => 'required|max:255|min:2',
                    'interval' => 'required|numeric',
                    'enterprisegatewayid' => 'max:255|min:2',
                    'messageid' => 'max:255|min:2',
                    'commandid' => 'max:255|min:2'
                ]
            );

            if ($validator->fails()) throw new Exception(concat_error($validator));

            $params = \Request::all();


            if (isset($params['operand'])) {
                foreach ($params['operand'] as $k => $v) {
                    $validatorCondition = \Validator::make(
                        ['operand' => $v],
                        [
                            'operand' => 'required|integer|in:1,2,3,4,5,6,7',
                        ]
                    );
                    if ($validatorCondition->fails()) throw new Exception(concat_error($validatorCondition));
                }
            }

            $data = [
                'name' => $params['name'],
                'devicetypeid' => $params['devicetypeid'],
                'queuetopicname' => $params['queuetopicname'],
                'interval' => $params['interval'],
                'customerid' => getCurrentCustomerId()
            ];


            $data['active'] = (isset($params['active'])) ? true : false;

            if ($params['devicetypeid'] != '') {
                $data['devicetypeid'] = $params['devicetypeid'];
            }
            if ($params['devicesetid'] != '') {
                $data['devicesetid'] = $params['devicesetid'];
            }
            if ($params['enterprisegatewayid'] != '') {
                $data['enterprisegatewayid'] = $params['enterprisegatewayid'];
            }
            if ($params['messageid'] != '') {
                $data['messageid'] = $params['messageid'];
            }
            if ($params['commandid'] != '') {
                $data['commandid'] = $params['commandid'];
            }

            if ($id) {
                $obj = RuleManage::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['operand'])) {
                        $obj = RuleCondition::deleteAll(['ruleid' => $id]);

                        if (!empty($obj->error)) {
                            throw new Exception(concat_error($obj->error->message));
                        }

                        $this->saveCondition($id, [
                            'attribute' => $params['attribute'],
                            'value' => $params['value'],
                            'operand' => $params['operand'],
                            'isand' => $params['isand'],
                        ]);
                    }
                }

            } else {
                $obj = RuleManage::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['operand'])) {
                        $this->saveCondition($obj->id, [
                            'attribute' => $params['attribute'],
                            'value' => $params['value'],
                            'operand' => $params['operand'],
                            'isand' => $params['isand'],
                        ]);
                    }
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    private function saveCondition($ruleId = null, $data = [])
    {
        if ($ruleId == null) {
            return;
        }

        foreach ($data['attribute'] as $k => $v) {
            $conditionData = [
                'devicetypeattributeid' => $v,
                'ruleid' => $ruleId,
                'value' => $data['value'][$k],
                'operand' => $data['operand'][$k],
                'isand' => $data['isand'][$k]
            ];
            $obj = RuleCondition::create($conditionData);

            if (!empty($obj->error)) {
                throw new Exception($obj);
            }
        }

    }

    public function deleteCondition($id = null)
    {

        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = RuleCondition::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = RuleManage::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}