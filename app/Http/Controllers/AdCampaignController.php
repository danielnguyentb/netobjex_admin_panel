<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;
use \Validator AS Validator;

use \App\Models\AdCampaignsModel AS AdCampaigns;
use \App\Models\SchedulesModel AS Schedules;
use \App\Models\DeviceManagementModel AS Devices;
use \App\Models\DeviceSetEditorModel AS DeviceSets;
use \App\Models\DeviceAdCampaignsModel AS DeviceAdCampaigns;
use \App\Models\ScheduleTimeModel AS ScheduleTimes;
use \App\Models\FileModel AS Files;


class AdCampaignController extends Controller
{

    public function index()
    {
        $data = [
            'isSelectedOrg' => getCurrentCustomerId(),
            'title'         => trans('core.Ad Campaign')
        ];

        return view('ad_campaign.index', $data);
    }

    public function get($acId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        try {
            $devicesList = $this->_getDevicesList();
            if (!empty($devicesList['status']) && !$devicesList['status']) throw new ErrorException($devicesList['message']);

            $deviceSetsList = $this->_getDeviceSetsList();
            if (!empty($deviceSetsList['status']) && !$deviceSetsList['status']) throw new ErrorException($deviceSetsList['message']);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        if ($acId != NULL) {
            try {
                $adCampaign = AdCampaigns::findById($acId);

                if (!empty($adCampaign->error)) throw new ErrorException($adCampaign->error->message);

                $data              = (array) $adCampaign;
                $data['form']      = 'edit';
                $data['startDate'] = setData($adCampaign, 'startdate', 'date', 'm/d/Y');
                $data['endDate']   = setData($adCampaign, 'enddate', 'date', 'm/d/Y');

                $resGetSchedulesData = $this->_getSchedulesData($data, $adCampaign->id);

                if (!empty($resGetSchedulesData['status']) && !$resGetSchedulesData['status']) {
                    throw new ErrorException($resGetSchedulesData['message']);
                }

                $resGetSelectedDevicesAndDeviceSets = $this->_getSelectedDevicesAndDeviceSets(
                    $data, $adCampaign->id, $devicesList, $deviceSetsList
                );

                if (!empty($resGetSelectedDevicesAndDeviceSets['status']) && !$resGetSelectedDevicesAndDeviceSets['status']) {
                    throw new ErrorException($resGetSelectedDevicesAndDeviceSets['message']);
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'           => 'add',
                'id'             => '',
                'name'           => '',
                'type'           => '',
                'url'            => '',
                'devices'        => [],
                'deviceSets'     => [],
                'startDate'      => '',
                'endDate'        => '',
                'backimageid'    => '',
                'logoimageid'    => '',
                'sponsorimageid' => ''
            ];

            $this->_getDefaultSchedulesData($data);
        }

        $data['devicesList']    = $devicesList;
        $data['deviceSetsList'] = $deviceSetsList;
        $data['hoursList']      = ScheduleTimes::GetHoursTime();
        $data['minutesList']    = ScheduleTimes::GetMinutesTime();
        $data['periodsList']    = ScheduleTimes::GetUTC();
        $data['timezonesList']  = renderTimezone();

        if (!empty($data['timezonesList']['status']) && !$data['timezonesList']['status']) {
            throw new ErrorException($data['timezonesList']['message']);
        }

        $data['campaignTypesList'] = AdCampaigns::getInstance()->getAdCampaignTypes();

        return view('ad_campaign.edit_ad_campaign', $data);
    }

    public function save($acId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post  = Request::all();
            $files = Request::file();

            $validator = Validator::make(
                $post,
                [
                    'name'           => 'required|min:2|max:255',
                    'type'           => 'required',
                    'url'            => 'required|min:2|max:255|url',
                    'devices'        => 'required',
                    'start_date'     => 'date_format:m/d/Y|after:tomorrow',
                    'end_date'       => 'date_format:m/d/Y|after:start_date',
                    'backimageid'    => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
                    'logoimageid'    => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
                    'sponsorimageid' => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            if (!empty($post['start_date'])) {
                $post['start_date'] .= " {$post['start_hour']}:{$post['start_minute']} {$post['start_period']}";
            } else {
                $post['start_date'] = NULL;
            }

            if (!empty($post['end_date'])) {
                $post['end_date'] .= " {$post['end_hour']}:{$post['end_minute']} {$post['end_period']}";
            } else {
                $post['end_date'] = NULL;
            }

            $dataAdCampaign = [
                'name'      => setData($post, 'name'),
                'url'       => setData($post, 'url'),
                'type'      => $post['type'],
                'startdate' => setData($post, 'start_date', 'date'),
                'enddate'   => setData($post, 'end_date', 'date')
            ];

            if ($files) {
                //Upload to api
                foreach ($files as $k => $v) {
                    $uploaded = Files::getInstance()->upload($v);

                    if (empty($uploaded->error)) {
                        $dataAdCampaign[$k] = $uploaded[0]->id;
                    } else {
                        $response['status']  = FALSE;
                        $response['message'] = $uploaded->error->message;
                    }
                }
            }

            $isEdit = $acId != NULL ? TRUE : FALSE;

            $posted = $isEdit ? AdCampaigns::updateById($post['id'], $dataAdCampaign) : AdCampaigns::create($dataAdCampaign);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }

            $savedDeviceAdCampaigns = $this->_saveDeviceAdCampaigns($post, $posted->id, $isEdit);

            if (!empty($savedDeviceAdCampaigns->error)) {
                $response['status']  = FALSE;
                $response['message'] = $savedDeviceAdCampaigns->error->message;
            }

            $savedSchedules = $this->_saveSchedules($post, $posted->id, $isEdit);

            if (!empty($savedSchedules->error)) {
                $response['status']  = FALSE;
                $response['message'] = $savedSchedules->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($acId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deleted = AdCampaigns::deleteById($acId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted ad campaign successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    private function _saveDeviceAdCampaigns($data, $adCampaignId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];

        if ($isEdit) {
            $deleted = DeviceAdCampaigns::deleteAll(['campaignid' => $adCampaignId]);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;

                return $response;
            }
        }

        if (isset($data['devices'])) {
            foreach ($data['devices'] as $d) {
                $inserted = DeviceAdCampaigns::create(['deviceid' => $d, 'campaignid' => $adCampaignId]);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }

        if (isset($data['deviceSets'])) {
            foreach ($data['deviceSets'] as $ds) {
                $inserted = DeviceAdCampaigns::create(['devicesetid' => $ds, 'campaignid' => $adCampaignId]);

                if (!empty($inserted->error)) {
                    $response['status']  = FALSE;
                    $response['message'] = $inserted->error->message;

                    return $response;
                }
            }
        }

        return $response;
    }

    private function _saveSchedules($data, $adCampaignId, $isEdit)
    {
        $response = ['status' => TRUE, 'message' => ''];

        $params = [
            'adcampaignid'  => $adCampaignId,
            'startdate'     => setData($data, 'start_date', 'date'),
            'enddate'       => setData($data, 'end_date', 'date'),
            'eachday'       => setData($data, 'eachDay', 'bool'),
            'eachmonday'    => setData($data, 'eachMonday', 'bool'),
            'eachtuesday'   => setData($data, 'eachTuesday', 'bool'),
            'eachwednesday' => setData($data, 'eachWednesday', 'bool'),
            'eachthirsday'  => setData($data, 'eachThursday', 'bool'),
            'eachfriday'    => setData($data, 'eachFriday', 'bool'),
            'eachsaturday'  => setData($data, 'eachSaturday', 'bool'),
            'eachsunday'    => setData($data, 'eachSunday', 'bool'),
            'timezoneid'    => setData($data, 'timezone'),
            'fromtime'      => setData($data, 'start_date', 'fromTime', '', FALSE, 0),
            'totime'        => setData($data, 'end_date', 'toTime', '', FALSE, 0)
        ];

        $saved = $isEdit ? Schedules::updateAll(['adcampaignid' => $adCampaignId], $params) : Schedules::create($params);

        if (!empty($saved->error)) {
            $response['status']  = FALSE;
            $response['message'] = $saved->error->message;
        }

        return $response;
    }

    private function _getDevicesList()
    {
        $response = ['status' => TRUE, 'message' => ''];
        $devices  = Devices::find();

        if (!empty($devices->error)) {
            $response['status']  = FALSE;
            $response['message'] = $devices->error->message;

            return $response;
        }

        return buildDropDownOpts($devices, 'deviceid');
    }

    private function _getDeviceSetsList()
    {
        $response   = ['status' => TRUE, 'message' => ''];
        $deviceSets = DeviceSets::find();

        if (!empty($deviceSets->error)) {
            $response['status']  = FALSE;
            $response['message'] = $deviceSets->error->message;

            return $response;
        }

        return buildDropDownOpts($deviceSets, 'id');
    }

    private function _getSchedulesData(&$data, $adCampaignId)
    {
        $response = ['status' => TRUE, 'message' => ''];
        $schedule = Schedules::getInstance()->getByAdCampaignId($adCampaignId);

        if (!empty($schedule->error)) {
            $response['status']  = FALSE;
            $response['message'] = $schedule->error->message;

            return $response;
        }

        if ($schedule) {
            $schedule  = $schedule[0];
            $startDate = renderTimeFromDate($schedule->startdate);
            $endDate   = renderTimeFromDate($schedule->enddate);

            $data['startHour']     = $startDate->hour;
            $data['startMinute']   = $startDate->minute;
            $data['startPeriod']   = $startDate->period;
            $data['endHour']       = $endDate->hour;
            $data['endMinute']     = $endDate->minute;
            $data['endPeriod']     = $endDate->period;
            $data['eachDay']       = $schedule->eachday;
            $data['eachMonday']    = $schedule->eachmonday;
            $data['eachTuesday']   = $schedule->eachtuesday;
            $data['eachWednesday'] = $schedule->eachwednesday;
            $data['eachThursday']  = $schedule->eachthirsday;
            $data['eachFriday']    = $schedule->eachfriday;
            $data['eachSaturday']  = $schedule->eachsaturday;
            $data['eachSunday']    = $schedule->eachsunday;
            $data['fromTime']      = $schedule->fromtime;
            $data['toTime']        = $schedule->totime;
            $data['timeZoneId']    = $schedule->timezoneid;
        } else {
            $this->_getDefaultSchedulesData($data);
        }
    }

    private function _getDefaultSchedulesData(&$data)
    {
        $data['startHour']     = '';
        $data['startMinute']   = '';
        $data['startPeriod']   = '';
        $data['endHour']       = '';
        $data['endMinute']     = '';
        $data['endPeriod']     = '';
        $data['eachDay']       = FALSE;
        $data['eachMonday']    = FALSE;
        $data['eachTuesday']   = FALSE;
        $data['eachWednesday'] = FALSE;
        $data['eachThursday']  = FALSE;
        $data['eachFriday']    = FALSE;
        $data['eachSaturday']  = FALSE;
        $data['eachSunday']    = FALSE;
        $data['fromTime']      = '';
        $data['toTime']        = '';
        $data['timeZoneId']    = '';
    }

    private function _getSelectedDevicesAndDeviceSets(&$data, $adCampaignId, $devicesList, $deviceSetsList)
    {
        $response           = ['status' => TRUE, 'message' => ''];
        $data['devices']    = [];
        $data['deviceSets'] = [];
        $deviceAdCampaign   = DeviceAdCampaigns::getInstance()->getByCampaignId($adCampaignId);

        if (!empty($deviceAdCampaign->error)) {
            $response['status'] = FALSE;
            $response['message'] = $deviceAdCampaign->error->message;

            return $response;
        }

        foreach ($deviceAdCampaign as $dac) {
            if (array_key_exists($dac->deviceid, $devicesList)) $data['devices'][] = $dac->deviceid;
            if (array_key_exists($dac->devicesetid, $deviceSetsList)) $data['deviceSets'][] = $dac->devicesetid;
        }
    }

    function export()
    {
        $response = ['status' => TRUE];
        try {
            $post = Request::all();

            $startDate = isset($post['startDate']) ? $post['startDate'] : null;
            $endDate = isset($post['endDate']) ? $post['endDate'] : null;
            $adCampaignId = isset($post['adCampaignId']) ? $post['adCampaignId'] : null;

            //get survey rawData
            $filters = [
                'where' => [
                    'fromDate' => $startDate,
                    'endDate' => $endDate
                ]
            ];

            $data = AdCampaigns::GetInstance()->getRawData($adCampaignId, $filters);
            if (empty($data->error)) {
                $response['html'] = exportRawData($data, 'adcampaign');
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

}