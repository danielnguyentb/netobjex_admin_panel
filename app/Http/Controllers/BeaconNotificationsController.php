<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\ActionResponseModel as ActionResponse;
use App\Models\ContentModel as Content;
use App\Models\FileModel AS Files;
use App\Models\FeedManageModel AS FeedManage;
use \Exception as Exception;
use \Session as Session;

class BeaconNotificationsController extends Controller
{

    public function index()
    {
        $data = [
            'title' => trans('core.Beacon Notifications')
        ];

        return view('beacon_notifications.index', $data);
    }

    private function typeNameView($type = null)
    {

        if ($type == null) {
            return '';
        }

        $typeName = [
            '1' => 'beacon_notifications.edit-notification',
            '6' => 'beacon_notifications.edit-image',
            '7' => 'beacon_notifications.edit-url',
            '8' => 'beacon_notifications.edit-feed',
        ];
        return $typeName[$type];
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'name' => '',
            'description' => '',
            'iconid' => '',
            'contenttext' => '',
            'storedfileid' => '',
            'networkid' => '',
            'contenturl' => '',
            'form' => 'add'
        ];

        $view = '';

        if ($id != null) {
            $notification = ActionResponse::getInstance()->findById($id);

            if (!empty($notification->error)) {
                return trans('core.not found');
            }

            $content = Content::getInstance()->findById($notification->contentid);

            if (!empty($content->error)) {
                return trans('core.not found');
            }

            $data['id'] = $notification->id;
            $data['description'] = $notification->description;
            $data['type'] = $notification->responsetype;
            $data['name'] = $content->name;
            $data['iconid'] = $notification->iconid;

            $data['contenttext'] = $content->contenttext;
            $data['storedfileid'] = $content->storedfileid;
            $data['networkid'] = $content->networkid;
            $data['contenturl'] = $content->contenturl;

            $data['form'] = 'edit';
            $view = $this->typeNameView($notification->responsetype);

        } else {
            $params = \Request::all();

            if (!isset($params['type'])) {
                return '';
            }
            $data['type'] = $params['type'];
            $view = $this->typeNameView($params['type']);
        }
        if ($data['type'] == 8) {
            $data['feed'] = FeedManage::find();
        }
        return view($view, $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'description' => 'required|max:255|min:2',
                    'image' => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
                    'type' => 'required|in:1,2,3,4,5,6,7,8'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $this->validation($params['type'], $params);

            $data = [
                'name' => $params['name'],
                'description' => $params['description'],
                'customerid' => getCurrentCustomerId()
            ];

            if (isset($params['contenttext'])) {
                $data['contenttext'] = $params['contenttext'];
            }

            if (isset($params['contenturl'])) {
                $data['contenturl'] = $params['contenturl'];
            }
            if (isset($params['networkid'])) {
                $data['networkid'] = $params['networkid'];
            }

            $file = \Request::file('image');
            $storedFileId = \Request::file('storedfileid');

            if ($file) {
                $uploaded = Files::getInstance()->upload($file);

                if (empty($uploaded->error)) {
                    $data['iconid'] = $uploaded[0]->id;
                    $data['iconurl'] = genDownloadUrl($uploaded[0]->id);
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = $uploaded->error->message;
                }
            }

            if ($storedFileId) {
                $uploaded = Files::getInstance()->upload($storedFileId);

                if (empty($uploaded->error)) {
                    $data['storedfileid'] = $uploaded[0]->id;
                } else {
                    $response['status'] = FALSE;
                    $response['message'] = $uploaded->error->message;
                }
            }

            if ($id) { // update notification
                $obj = ActionResponse::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {

                    $obj2 = Content::updateAll(['id' => $obj->contentid], $data);

                    if (!empty($obj2->error)) {
                        $response['status'] = FALSE;
                        $response['message'] = $obj2->error->message;
                    }

                }
            } else { // create new beacon notification
                $obj = Content::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {

                    $data['responsetype'] = $params['type'];
                    $data['contentid'] = $obj->id;

                    $obj2 = ActionResponse::create($data);

                    if (!empty($obj2->error)) {
                        $response['status'] = FALSE;
                        $response['message'] = $obj2->error->message;
                    }
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    private function validation($type = null, $params = [])
    {

        if ($type == null) {
            return;
        }

        if ($type == '1') {
            $validator = \Validator::make(
                $params,
                [
                    'contenttext' => 'required|max:255|min:2',

                ]
            );
        } else if ($type == '6') {
//            $validator = \Validator::make(
//                $params,
//                [
//                    'storedfileid' => 'required|image|mimes:png,jpg,jpeg,bmp,gif|max:1000',
//
//                ]
//            );
        } else if ($type == '7') {
            $validator = \Validator::make(
                $params,
                [
                    'contenturl' => 'required|url',

                ]
            );
        } else {
            $validator = \Validator::make(
                $params,
                [
                    'networkid' => 'required|max:255|min:2',

                ]
            );
        }

        if ($validator->fails()) throw new Exception(concat_error($validator));

    }


    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {

            $contentRes = ActionResponse::getInstance()->findById($id);
            $deleted = ActionResponse::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            }

            if (empty($contentRes->error)) {

                $contentDel = Content::deleteById($contentRes->contentid);

                if (!empty($contentDel->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $response->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }
}