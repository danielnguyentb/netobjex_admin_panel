<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\UsersModel as Users;
use App\Models\CustomersModel as Customers;

// use App\Http\Requests\Request;
// use App\Models\FileModel;
// use League\Flysystem\Exception;
// use Validator;
use \Request AS Request;
use \Response AS Response;
use \ErrorException AS ErrorException;
use \Exception AS Exception;

class UserAdminController extends Controller
{

    public function index()
    {
        $data = [
            'title' => trans('core.User Admin')
        ];

        return view('user_admin.index', $data);
    }

    public function get($userId = NULL)
    {
        try {
            if (!Request::isMethod('get')) return FALSE;

            if ($userId != NULL) {
                $user = Users::findById($userId);

                if (!empty($user->error)) throw new ErrorException($user->error->message);

                $data = (array)$user;
                $data['form'] = 'edit';
            } else {
                $data = [
                    'form' => 'add',
                    'id' => '',
                    'firstname' => '',
                    'lastname' => '',
                    'email' => '',
                    'customerid' => '',
                ];
            }

            $orgs = Customers::find();
            $dataOrg = [null => 'Select Organization..'];

            foreach ($orgs as $v) {
                $dataOrg[$v->id] = $v->name;
            }
            $data['listOrg'] = $dataOrg;

            $roles = Users::getInstance()->getRoleList();

            $listRole = [];
            foreach ($roles as $v) {
                $listRole[$v->id] = $v->name;
            }
            $data['listRole'] = $listRole;

            $roleUser = Users::getInstance()->getUserRole($userId);

            $role_selected = array();

            if ($userId && $roleUser) {
                foreach ($roleUser as $v) {
                    array_push($role_selected, $v->roleid);
                }
            }
            $data['role'] = $role_selected;

            return view('user_admin.edit', $data);

        } catch (Exception $e) {
            return Response::json(array('status' => FALSE, 'message' => $e->getMessage()));
        }
    }

    public function changeStatus()
    {
        $response = array('status' => TRUE);

        if (Request::isMethod('post')) {
            $post = Request::all();

            $uid = !empty($post['id']) ? $post['id'] : '';
            $value = !empty($post['status']) ? $post['status'] : FALSE;

            if ($uid) {
                $user = Users::findById($uid);
                if ($user) {
                    $data['disabled'] = $value;

                    $c = Users::updateById($uid, $data);
                    if (!empty($c->error)) {
                        $response['status'] = FALSE;
                        $response['message'] = $c->error->message;
                    }
                }
            }

        }
        return Response::json($response);
    }

    public function save($userId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        try {
            if (!Request::isMethod('post')) throw new ErrorException();
            $post = Request::all();

            if (!array_key_exists('roles', $post)) {
                throw new ErrorException('Role is a required field');
            }

            $rules = [
                'firstname' => 'required|max:255|min:2',
                'lastname' => 'required|max:255|min:2',
                'oldPassword' => 'required|alpha_num|between:4,20',
                'confirmPassword' => 'required|alpha_num|between:4,20|same:oldPassword',
                'email' => 'required|email|max:255|min:2',
                'roles' => 'required'
            ];

            $messages = [
                'org.required' => 'Organization is a required field',
                'oldPassword.required' => 'Password is a required field'
            ];

            $listRole = Users::getInstance()->getRoleList();


            foreach ($listRole as $k => $v) {
                if ($v->name == 'Admin') {
                    $roleAdminId = $v->id;
                }
            }
            if ($post['roles'] && !in_array($roleAdminId, $post['roles'])) {
                $rules['org'] = 'required';
            }

            $validator = \Validator::make(
                $post,
                $rules,
                $messages
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $c = Users::countCondition(
                [
                    'email' => [
                        'regexp' => '/' . $post['email'] . '/i'
                    ],
                    'id' => [
                        'neq' => (!empty($post['id']) ? $post['id'] : null)
                    ]
                ]
            );

            if (is_object($c) && !empty($c->count) && $c->count > 0) {
                throw new ErrorException(trans('core.A User by that email already exists. Please select another.'));
            }

            $data = [
                'firstname' => !empty($post['firstname']) ? $post['firstname'] : '',
                'username' => !empty($post['email']) ? $post['email'] : '',
                'email' => !empty($post['email']) ? $post['email'] : '',
                'lastname' => !empty($post['lastname']) ? $post['lastname'] : ''
            ];

            $data['customerid'] = $post['org'] ? $post['org'] : null;

            if ($post['id']) {
                $match_pwd = Users::getInstance()->checkCurrentPassword($post['id'], $post['confirmPassword']);

                if (!empty($match_pwd->match) && $match_pwd->match) {

                    $c = Users::updateById($post['id'], $data);

                    if (!empty($c->error)) {
                        throw new Exception($c->error->message);
                    }
                } else {
                    throw new ErrorException(trans('core.Password does not match our records'));
                }

                $roles = $post['roles'];

                $c = Users::getInstance()->saveUserRoles($post['id'], $roles);

                if (!empty($c->error)) {
                    throw new ErrorException($c->error->message);
                }
            } else {

                $data['password'] = $post['confirmPassword'];

                $obj = Users::create($data);

                if (!empty($obj->error)) {
                    throw new ErrorException($c->error->message);
                }

                $roles = $post['roles'];

                $c = Users::getInstance()->saveUserRoles($obj->id, $roles);

                if (!empty($c->error)) {
                    throw new ErrorException($c->error->message);
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($userId = NULL)
    {

        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        try {
            if (!Request::isMethod('post')) return FALSE;


            $obj = Users::deleteById($userId);

            if (!empty($obj->error)) {
                throw new ErrorException($obj->error->message);
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

}