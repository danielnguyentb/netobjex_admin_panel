<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\DeviceSetEditorModel as DeviceSetEditor;
use App\Models\DeviceManagementModel as DeviceManagement;
use App\Models\CustomersModel as Customers;
use App\Models\LocationModel as Location;
use App\Models\DeviceTypesModel as DeviceType;
use App\Models\DeviceSetdeviceModel as DeviceSetdevice;
use \ErrorException as ErrorException;
use \Exception as Exception;
use \Session as Session;

class DeviceSetEditorController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.DeviceSet Editor')
        ];
        return view('device_set_editor.index', $data);
    }


    public function get($id = null)
    {
        $data = [
            'id' => '',
            'name' => '',
            'devicetypeid' => '',
            'customerid' => '',
            'form' => 'add'
        ];

        $data['locations'] = Location::find([]);
        $data['groups'] = Customers::getInstance()->getByTree();
        $data['types'] = DeviceType::find([]);

        if ($id != null) {
            $deviceSet = DeviceSetEditor::getInstance()->findById($id);

            if (!empty($deviceSet->error)) {
                trans('core.not found');
            }
            $data['id'] = $deviceSet->id;
            $data['name'] = $deviceSet->name;
            $data['devicetypeid'] = $deviceSet->devicetypeid;
            $data['customerid'] = $deviceSet->customerid;
            $data['form'] = 'edit';
            Session::set('dataBuffer', ['deviceSetId' => $deviceSet->id, 'form' => 'edit']);
        }

        return view('device_set_editor.edit', $data);

    }

    public function searchDeviceBySomething($type = null, $value = null)
    {
        $html = '<tr><td colspan="4">' . trans('No data to ') . '</td></tr>';

        if ($type === null || $value === null) {
            return $html;
        }

        $filter = ['where' => ['locationid' => $value]];

        if ($type == 0) {
            $filter = [
                'where' => [
                    'name' => [
                        'like' => "%$value%"
                    ]
                ]
            ];
        }

        $result = DeviceManagement::find($filter);

        if (!empty($result->error)) {
            return $html;
        }
        $locations = Location::find([]);
        $locationsKeyName = [];

        foreach ($locations as $key => $v) {
            $locationsKeyName[$v->id] = $v->name;
        }
        return view('device_set_editor.device-search-result', ['data' => $result, 'locationName' => $locationsKeyName]);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'devicetypeid' => 'required|max:255|min:2',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $data = [
                'name' => $params['name'],
                'devicetypeid' => $params['devicetypeid'],
            ];

            if ($id) { // update if exist deviceSetId

                $obj = DeviceSetEditor::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['deviceId'])) {
                        $this->saveDeviceToDeviceSet($id, $params['deviceId']);
                    }
                }

            } else { // Create new deviceSet
                $data['customerid'] = getCurrentCustomerId();
                $obj = DeviceSetEditor::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['deviceId'])) {
                        $this->saveDeviceToDeviceSet($obj->id, $params['deviceId']);
                    }
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();

    }

    private function saveDeviceToDeviceSet($deviceSetId = null, $param = [])
    {

        if (sizeof($param) == 0) return;

        foreach ($param as $k => $v) {
            $obj = DeviceSetdevice::create(['deviceid' => $v, 'setid' => $deviceSetId]);

            if (!empty($obj->error)) {
                var_dump($obj->error->message);
                die;
            }
        }
    }

    public function deleteDeviceSetDevice($id = null)
    {

        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = DeviceSetdevice::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);

    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = DeviceSetEditor::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

}