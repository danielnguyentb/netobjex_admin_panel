<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\FeedManageModel as Feed;
use App\Models\CustomersModel as Customers;

class FeedManagementController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Feed Management')
        ];
        return view('feed_management.index', $data);
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'name' => '',
            'type' => '',
            'url' => '',
            'userid' => '',
            'password' => '',
            'interval' => '',
            'form' => 'add',
            'types' => Feed::getInstance()->getTypeOfLink()
        ];


        if ($id != null) {
            $object = Feed::getInstance()->findById($id);

            if (!empty($object->error)) {
                trans('core.not found');
            }

            $data['id'] = $object->id;
            $data['name'] = $object->name;
            $data['type'] = $object->type;
            $data['url'] = $object->url;
            $data['userid'] = $object->userid;
            $data['password'] = $object->password;
            $data['interval'] = $object->interval;
            $data['form'] = 'edit';
        }

        return view('feed_management.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'type' => 'required|in:1,2,3,4',
                    'url' => 'required|url',
                    'userid' => 'required|max:255|min:2',
                    'password' => 'required|max:255|min:2',
                    'interval' => 'required|numeric',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $data = [
                'name' => $params['name'],
                'type' => $params['type'],
                'url' => $params['url'],
                'userid' => $params['userid'],
                'password' => $params['password'],
                'interval' => $params['interval'],
                'customerid' => getCurrentCustomerId()
            ];

            if ($id) {
                $obj = Feed::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $obj = Feed::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = Feed::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

}