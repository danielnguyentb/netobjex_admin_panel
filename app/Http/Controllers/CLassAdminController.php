<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\ClassAdminModel as ClassAdmin;

use \Session as Session;
use \Exception as Exception;

class ClassAdminController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Class Admin')
        ];
        return view('class_admin.index', $data);
    }


    public function get($id = null)
    {

        $data = [
            'id' => '',
            'name' => '',
            'class' => '',
            'form' => 'add',
        ];

        $tables = ClassAdmin::getInstance()->getTable();

        if (isset($tables->error)) throw new ErrorException($tables->error->message);

        $data['tags'] = $tables;

        if ($id != null) {
            $object = ClassAdmin::getInstance()->findById($id);

            if (!empty($object->error)) {
                return trans('core.not found');
            }

            $data['form'] = 'edit';
            $data['id'] = $object->id;
            $data['name'] = $object->name;
            $data['class'] = $object->class;

        }

        return view('class_admin.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'class' => 'max:255|min:2',
                ]
            );

            if ($validator->fails()) throw new Exception(concat_error($validator));

            $params = \Request::all();
            $data = [
                'name' => $params['name'],
                'class' => $params['class'],
            ];

            if ($id) {
                $obj = ClassAdmin::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $obj = ClassAdmin::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = ClassAdmin::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}