<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \App\Models\BeaconActionMapModel as BeaconAction;
use \App\Models\BeaconManagementModel as BeaconManagement;
use \App\Models\BeaconSetEditorModel as BeaconSet;
use \App\Models\ActionResponseModel as ActionResponse;
use \App\Models\BeaconScheduleModel as BeaconSchedule;
use \App\Models\SchedulesModel as Schedules;

use \ErrorException AS ErrorException;


class BeaconCampaignsController extends Controller
{

    public function index()
    {
        $data['existCustomerId'] = getCurrentCustomerId();
        $data['icons'] = ActionResponse::find();
        $data['auth'] = false;
        $data['title'] = trans('core.Beacon Campaigns');

        if (empty($data['icons']->error)) {
            $data['auth'] = true;
        }

        return view('beacon_campaigns.index', $data);
    }

    public function get($id = null)
    {

        $data = [
            'id'                   => '',
            'name'                 => '',
            'beaconid'             => '',
            'beaconsetid'          => '',
            'startdate'            => '',
            'enddate'              => '',
            'eachday'              => false,
            'eachmonday'           => false,
            'eachtuesday'          => false,
            'eachwednesday'        => false,
            'eachthirsday'         => false,
            'eachfriday'           => false,
            'eachsaturday'         => false,
            'eachsunday'           => false,
            'startHour'            => '',
            'startMinute'          => '',
            'startApm'             => '',
            'endHour'              => '',
            'endMinute'            => '',
            'endApm'               => '',
            'backcolor'            => 'AB2CA6',
            'textcolor'            => 'FFFFFF',
            'enternearresponseid'  => '',
            'enterfarresponseid'   => '',
            'enterimmedresponseid' => '',
            'exitnearresponseid'   => '',
            'exitfarresponseid'    => '',
            'exitimmedresponseid'  => '',
            'beaconArr'            => [],
            'beaconSetArr'         => [],
            'form'                 => 'add'
        ];
        $data['timezonesList'] = renderTimezone();
        $data['beacons'] = BeaconManagement::find();
        $data['beaconSets'] = BeaconSet::find();
        $data['timezoneid'] = '';

        $prepend = array('01', '02', '03', '04', '05', '06', '07', '08', '09');
        $prependMi = array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09');
        $data['hours'] = array_merge($prepend, range(10, 12));
        $data['minutes'] = array_merge($prependMi, range(10, 59));

        if ($id != null) {
            $obj = BeaconAction::getInstance()->findById($id);

            if (!empty($obj->error)) throw new ErrorException($obj->error->message);

            $obj2 = Schedules::getInstance()->find(['where' => ['beaconcampaignid' => $obj->id]]);

            if (!empty($obj2->error)) throw new ErrorException($obj2->error->message);

            $obj3 = BeaconSchedule::getInstance()->find(['where' => ['mapid' => $obj->id]]);

            if (!empty($obj3->error)) throw new ErrorException($obj3->error->message);

            $data['id'] = $obj->id;
            $data['name'] = $obj->name;

            $data['enternearresponseid'] = $this->makeIcon($obj->enternearresponseid);
            $data['enterfarresponseid'] = $this->makeIcon($obj->enterfarresponseid);
            $data['enterimmedresponseid'] = $this->makeIcon($obj->enterimmedresponseid);
            $data['exitnearresponseid'] = $this->makeIcon($obj->exitnearresponseid);
            $data['exitfarresponseid'] = $this->makeIcon($obj->exitfarresponseid);
            $data['exitimmedresponseid'] = $this->makeIcon($obj->exitimmedresponseid);

            $sDateFomat = date_create($obj->startdate);
            $eDateFomat = date_create($obj->enddate);

            $data['startdate'] = date_format($sDateFomat, "m/d/Y");
            $data['enddate'] = date_format($eDateFomat, "m/d/Y");

            $data['starttime'] = $obj->starttime;
            $data['endtime'] = $obj->endtime;

            $data['backcolor'] = $obj->backcolor;
            $data['textcolor'] = $obj->textcolor;

            $StartHourObject = renderTimeFromDate($obj->startdate);
            $endHourObject = renderTimeFromDate($obj->enddate);

            $data['startHour'] = $StartHourObject->hour;
            $data['startMinute'] = $StartHourObject->minute;
            $data['startApm'] = $StartHourObject->period;

            $data['endHour'] = $endHourObject->hour;
            $data['endMinute'] = $endHourObject->minute;
            $data['endApm'] = $endHourObject->period;

            if (isset($obj2[0])) {
                $data['eachday'] = $obj2[0]->eachday;
                $data['eachmonday'] = $obj2[0]->eachmonday;
                $data['eachtuesday'] = $obj2[0]->eachtuesday;
                $data['eachwednesday'] = $obj2[0]->eachwednesday;
                $data['eachthirsday'] = $obj2[0]->eachthirsday;
                $data['eachfriday'] = $obj2[0]->eachfriday;
                $data['eachsaturday'] = $obj2[0]->eachsaturday;
                $data['eachsunday'] = $obj2[0]->eachsunday;
                $data['timezoneid'] = $obj2[0]->timezoneid;
            }

            $beaconArr = [];
            $beaconSetArr = [];

            foreach ($obj3 as $k => $v) {
                if ($v->beaconid != null || $v->beaconid != '') {
                    $beaconArr[] = $v->beaconid;
                }
                if ($v->beaconsetid != null || $v->beaconsetid != '') {
                    $beaconSetArr[] = $v->beaconsetid;
                }
            }

            $data['beaconArr'] = $beaconArr;
            $data['beaconSetArr'] = $beaconSetArr;

            $data['form'] = 'edit';
        }

        return view('beacon_campaigns.edit', $data);
    }

    public function getCalenderData($startdate, $enddate)
    {
        if (!strtotime($startdate) || !strtotime($enddate) || strtotime($startdate) > strtotime($enddate)) return json_encode([]);

        $startdate = date('Y-m-d', strtotime($startdate)) . " 00:00:00";

        $enddate = date('Y-m-d', strtotime($enddate)) . " 23:59:59.999";

        $where = [
            'where' => [
                'and' => [
                    ['startdate' => ['lte' => $enddate]],
                    ['enddate' => ['gte' => $startdate]]
                ]
            ]
        ];

        $data = BeaconAction::getInstance()->find($where);

        if (!empty($data->error)) return json_encode([]);

        return json_encode($data);
    }


    private function makeIcon($iconId = '', $top = 0, $left = 0)
    {

        if ($iconId == '' || $iconId === null) {
            return '';
        }

        $urkIcon = genDownloadUrl($iconId);
        $img = "<img style='position: absolute; right: auto; bottom: auto; left: 0px; top: 0px;'";
        $img .= "data-id='$iconId' class='ui-draggable-handle item-1 ui-draggable' src='$urkIcon' />";
        return $img;
    }


    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name'      => 'required|max:255|min:2',
                    'startdate' => 'required|date_format:m/d/Y',
                    'enddate'   => 'required|date_format:m/d/Y',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $startEndDate = [];

            $startEndDate['startDate'] = "{$params['startdate']} {$params['startHour']}:{$params['startMinute']} {$params['startApm']}";
            $startEndDate['endDate'] = "{$params['enddate']} {$params['endHour']}:{$params['endMinute']} {$params['endApm']}";

            $data = [
                'name' => $params['name'],

                'startdate' => setData($startEndDate, 'startDate', 'date'),
                'enddate'   => setData($startEndDate, 'endDate', 'date'),

                'starttime' => setData($startEndDate, 'startDate', 'date'),
                'endtime'   => setData($startEndDate, 'endDate', 'date'),

                'starttimestr' => $params['startApm'],
                'endtimestr'   => $params['endApm'],

                'fromtime' => setData($startEndDate, 'startDate', 'fromTime', '', FALSE, 0),
                'totime'   => setData($startEndDate, 'endDate', 'toTime', '', FALSE, 0),

                'backcolor'  => $params['backcolor'],
                'textcolor'  => $params['textcolor'],
                'timezoneid' => $params['timezoneid'],
                'customerid' => getCurrentCustomerId()
            ];


            $data['beaconid'] = (isset($params['beaconid'])) ? implode(",", $params['beaconid']) : '';
            $data['beaconsetid'] = (isset($params['beaconsetid'])) ? implode(",", $params['beaconsetid']) : '';

            $data['eachday'] = (isset($params['beaconsetid'])) ? true : false;
            $data['eachmonday'] = (isset($params['eachmonday'])) ? true : false;
            $data['eachtuesday'] = (isset($params['eachtuesday'])) ? true : false;
            $data['eachwednesday'] = (isset($params['eachwednesday'])) ? true : false;
            $data['eachthirsday'] = (isset($params['eachthirsday'])) ? true : false;
            $data['eachfriday'] = (isset($params['eachfriday'])) ? true : false;
            $data['eachsaturday'] = (isset($params['eachsaturday'])) ? true : false;
            $data['eachsunday'] = (isset($params['eachsunday'])) ? true : false;

            if ($params['enternearresponseid'] != '') {
                $data['enternearresponseid'] = $params['enternearresponseid'];
            }
            if ($params['enterfarresponseid'] != '') {
                $data['enterfarresponseid'] = $params['enterfarresponseid'];
            }
            if ($params['enterimmedresponseid'] != '') {
                $data['enterimmedresponseid'] = $params['enterimmedresponseid'];
            }
            if ($params['exitnearresponseid'] != '') {
                $data['exitnearresponseid'] = $params['exitnearresponseid'];
            }
            if ($params['exitfarresponseid'] != '') {
                $data['exitfarresponseid'] = $params['exitfarresponseid'];
            }
            if ($params['exitimmedresponseid'] != '') {
                $data['exitimmedresponseid'] = $params['exitimmedresponseid'];
            }

            if ($id) {

                $obj = BeaconAction::updateById($id, $data);

                if (!empty($obj->error)) throw new ErrorException($obj->error->message);

                $obj2 = Schedules::updateAll(['beaconcampaignid' => $id], $data);

                if (!empty($obj2->error)) throw new ErrorException($obj2->error->message);

                $obj3 = BeaconSchedule::deleteAll(['mapid' => $id]);

                if (!empty($obj3->error)) throw new ErrorException($obj2->error->message);

                if (isset($params['beaconsetid'])) {
                    $this->addBeaconSchedule($params['beaconsetid'], 'beaconsetid', $id);
                }

                if (isset($params['beaconid'])) {
                    $this->addBeaconSchedule($params['beaconid'], 'beaconid', $id);
                }

            } else { // create new beacon campaign

                $obj = BeaconAction::create($data);

                if (!empty($obj->error)) throw new ErrorException($obj->error->message);

                $data['mapid'] = $obj->id;

                if (isset($params['beaconsetid'])) {
                    $this->addBeaconSchedule($params['beaconsetid'], 'beaconsetid', $obj->id);
                }

                if (isset($params['beaconid'])) {
                    $this->addBeaconSchedule($params['beaconid'], 'beaconid', $obj->id);
                }

                $data['beaconcampaignid'] = $obj->id;
                $obj3 = Schedules::create($data);

                if (!empty($obj3->error)) {
                    throw new ErrorException($obj3->error->message);
                }

            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    private function addBeaconSchedule($data = [], $nameOf = null, $mapId = null)
    {

        foreach ($data as $k => $v) {
            BeaconSchedule::create([
                $nameOf => $v,
                'mapid' => $mapId
            ]);
        }
    }


    public function delete($id = null)
    {
        if ($id === null) throw new ErrorException(trans('core.Id is required'));

        $response = array('status' => TRUE, 'message' => '', 'data' => []);
        $obj = BeaconAction::getInstance()->findById($id);

        if (!empty($obj->error)) throw new ErrorException($obj->error->message);

        $beaconCampaignId = $obj->id;

        try {
            BeaconAction::deleteById($id);
            Schedules::getInstance()->deleteAll(['beaconcampaignid' => $beaconCampaignId]);
            BeaconSchedule::getInstance()->deleteAll(['mapid' => $beaconCampaignId]);
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}