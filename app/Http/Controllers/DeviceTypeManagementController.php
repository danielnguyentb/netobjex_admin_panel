<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;
use \Validator AS Validator;

use \App\Models\DeviceTypesModel AS DeviceTypes;
use \App\Models\ProtocolsModel AS Protocols;
use \App\Models\FileModel AS Files;
use \App\Models\CommandsModel AS Commands;
use \App\Models\ConfigurationsModel AS Configurations;
use \App\Models\ParametersModel AS Parameters;
use \App\Models\DeviceTypeAttributesModel AS DeviceTypeAttributes;


class DeviceTypeManagementController extends Controller
{

    public function index()
    {
        $data = [
            'paramDataTypes' => Parameters::getInstance()->getParamDataTypes(),
            'title'          => trans('core.Device Type Management')
        ];

        return view('device_type_management.index', $data);
    }

    public function get($dtId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($dtId != NULL) {
            try {
                $deviceType = DeviceTypes::findById($dtId);

                if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

                $data         = (array) $deviceType;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'         => 'add',
                'id'           => '',
                'name'         => '',
                'protocolid'   => '',
                'queuename'    => '',
                'statusviewer' => '',
                'imageid'      => ''
            ];
        }

        try {
            $protocols = Protocols::find();

            if (!empty($protocols->error)) throw new ErrorException($protocols->error->message);

            $data['protocolsList'] = buildDropDownOpts($protocols);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return view('device_type_management.edit_device_type', $data);
    }

    public function save($dtId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();
            $file = Request::file('image');

            $validator = Validator::make(
                $post,
                [
                    'device_type'   => 'required|min:2|max:255',
                    'queue_name'    => 'required|min:2|max:255',
                    'status_viewer' => 'required|min:2|max:255',
                    'image'         => 'image|mimes:png,jpg,jpeg,bmp,gif|max:1000'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $isExists = DeviceTypes::isExists($post['device_type'], trans('core.Device Type Name'), $dtId);
            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'name'         => setData($post, 'device_type'),
                'protocolid'   => $post['protocol'],
                'queuename'    => setData($post, 'queue_name'),
                'statusviewer' => setData($post, 'status_viewer')
            ];

            if ($file) {
                //Upload to api
                $uploaded = Files::getInstance()->upload($file);

                if (empty($uploaded->error)) {
                    $data['imageid'] = $uploaded[0]->id;
                } else {
                    $response['status']  = FALSE;
                    $response['message'] = $uploaded->error->message;
                }
            }

            $posted = $dtId != NULL ? DeviceTypes::updateById($post['id'], $data) : DeviceTypes::create($data);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($dtId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deleted = DeviceTypes::deleteById($dtId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted device type successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getAttribute($attrId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($attrId != NULL) {
            try {
                $attribute = DeviceTypeAttributes::findById($attrId);

                if (!empty($attribute->error)) throw new ErrorException($attribute->error->message);

                $data         = (array) $attribute;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'        => 'add',
                'id'          => '',
                'name'        => '',
                'controltype' => ''
            ];
        }

        $data['controlTypesList'] = DeviceTypeAttributes::getInstance()->getAttrControlTypes();

        return view('device_type_management.edit_attribute', $data);
    }

    public function saveAttribute($dtypeId, $attrId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deviceType = DeviceTypes::findById($dtypeId);

            if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

            $post = Request::all();

            $validator = Validator::make(
                $post,
                [
                    'name'        => 'required|min:2|max:255',
                    'control_type' => 'required',
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $isExists = DeviceTypeAttributes::isExists($post['name'], trans('core.Attribute'), $attrId);
            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'name'         => setData($post, 'name'),
                'controltype'  => setData($post, 'control_type'),
                'devicetypeid' => $deviceType->id
            ];

            $posted = $attrId != NULL ? DeviceTypeAttributes::updateById($post['id'], $data) : DeviceTypeAttributes::create($data);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function deleteAttribute($attrId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $attribute = DeviceTypeAttributes::findById($attrId);

            if (!empty($attribute->error)) throw new ErrorException($attribute->error->message);

            $deleted = DeviceTypeAttributes::deleteById($attrId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted attribute successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getCommand($cmdId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($cmdId != NULL) {
            try {
                $command = Commands::findById($cmdId);

                if (!empty($command->error)) throw new ErrorException($command->error->message);

                $data         = (array) $command;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'          => 'add',
                'id'            => '',
                'name'          => '',
                'commandstring' => '',
                'returntype'    => ''
            ];
        }

        $data['returnTypesList'] = Commands::getInstance()->getCmdReturnTypes();

        return view('device_type_management.edit_command', $data);
    }

    public function saveCommand($dtypeId, $cmdId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deviceType = DeviceTypes::findById($dtypeId);

            if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

            $post = Request::all();

            $validator = Validator::make(
                $post,
                [
                    'command_name'   => 'required|min:2|max:255',
                    'command_string' => 'required|min:2|max:255',
                    'return_type'    => 'required'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $isExists = Commands::isExists($post['command_name'], trans('core.Command Name'), $cmdId);
            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'name'          => setData($post, 'command_name'),
                'commandstring' => setData($post, 'command_string'),
                'dtype'         => $deviceType->id,
                'returntype'    => setData($post, 'return_type')
            ];

            $posted = $cmdId != NULL ? Commands::updateById($post['id'], $data) : Commands::create($data);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function deleteCommand($cmdId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $command = Commands::findById($cmdId);

            if (!empty($command->error)) throw new ErrorException($command->error->message);

            $deleted = Commands::deleteById($cmdId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted command successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getConfiguration($cfgId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($cfgId != NULL) {
            try {
                $configuration = Configurations::findById($cfgId);

                if (!empty($configuration->error)) throw new ErrorException($configuration->error->message);

                $data         = (array) $configuration;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'     => 'add',
                'id'       => '',
                'name'     => '',
                'variable' => '',
                'value'    => ''
            ];
        }

        return view('device_type_management.edit_configuration', $data);
    }

    public function saveConfiguration($dtypeId, $cfgId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $deviceType = DeviceTypes::findById($dtypeId);

            if (!empty($deviceType->error)) throw new ErrorException($deviceType->error->message);

            $post = Request::all();

            $validator = Validator::make(
                $post,
                [
                    'variable' => 'required|min:2|max:255',
                    'value'    => 'required|min:2|max:255'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $isExists = Configurations::isExists($post['variable'], trans('core.Variable'), $cfgId, 'variable');
            if (!$isExists->status) throw new ErrorException($isExists->message);

            $data = [
                'variable'     => setData($post, 'variable'),
                'value'        => setData($post, 'value'),
                'devicetypeid' => $deviceType->id
            ];

            $posted = $cfgId != NULL ? Configurations::updateById($post['id'], $data) : Configurations::create($data);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function deleteConfiguration($cfgId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $configuration = Configurations::findById($cfgId);

            if (!empty($configuration->error)) throw new ErrorException($configuration->error->message);

            $deleted = Configurations::deleteById($cfgId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted variable successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getParameter($paramId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($paramId != NULL) {
            try {
                $parameter = Parameters::findById($paramId);

                if (!empty($parameter->error)) throw new ErrorException($parameter->error->message);

                $data         = (array) $parameter;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form'         => 'add',
                'id'           => '',
                'name'         => '',
                'defaultvalue' => ''
            ];
        }

        $data['dataTypesList'] = Parameters::getInstance()->getParamDataTypes();

        return view('device_type_management.edit_parameter', $data);
    }

    public function saveParameter($cmdId, $paramId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $command = Commands::findById($cmdId);

            if (!empty($command->error)) throw new ErrorException($command->error->message);

            $post = Request::all();

            $validator = Validator::make(
                $post,
                [
                    'parameter' => 'required|min:2|max:255',
                    'data_type' => 'required'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $data = [
                'name'         => setData($post, 'parameter'),
                'defaultvalue' => setData($post, 'data_type'),
                'commandid'    => $command->id
            ];

            $posted = $paramId != NULL ? Parameters::updateById($post['id'], $data) : $posted = Parameters::create($data);

            if (!empty($posted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $posted->error->message;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function deleteParameter($paramId)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $parameter = Parameters::findById($paramId);

            if (!empty($parameter->error)) throw new ErrorException($parameter->error->message);

            $deleted = Parameters::deleteById($paramId);

            if (!empty($deleted->error)) {
                $response['status']  = FALSE;
                $response['message'] = $deleted->error->message;
            } else {
                $response['message'] = trans('core.Deleted parameter successfully');
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

}