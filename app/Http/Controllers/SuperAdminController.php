<?php
/**
 * Created by PhpStorm.
 * User: chungdd
 * Date: 1/14/16
 * Time: 11:20 AM
 */

namespace App\Http\Controllers;

use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;

use \App\Models\BeaconManagementModel AS Beacons;
use \App\Models\BeaconSetEditorModel AS BeaconSets;
use \App\Models\DeviceManagementModel AS Devices;
use \App\Models\DeviceSetEditorModel AS DeviceSets;
use \App\Models\LocationModel AS Locations;
use \App\Models\DeviceTypesModel AS DeviceTypes;
use \App\Models\CustomersModel AS Customers;

class SuperAdminController extends Controller
{

    private $_isSkipCheck = TRUE;

    public function index()
    {
        $defaultOpts = ['' => '*'];
        try {
            // top counter
            $beaconsCount = Beacons::countCondition([], $this->_isSkipCheck);
            if (isset($beaconsCount->error)) throw new ErrorException($beaconsCount->error->message);

            $beaconSetsCount = BeaconSets::countCondition([], $this->_isSkipCheck);
            if (isset($beaconSetsCount->error)) throw new ErrorException($beaconSetsCount->error->message);

            $devicesCount = Devices::countCondition([], $this->_isSkipCheck);
            if (isset($devicesCount->error)) throw new ErrorException($devicesCount->error->message);

            $deviceSetsCount = DeviceSets::countCondition([], $this->_isSkipCheck);
            if (isset($deviceSetsCount->error)) throw new ErrorException($deviceSetsCount->error->message);

            $locationsCount = Locations::countCondition([], $this->_isSkipCheck);
            if (isset($locationsCount->error)) throw new ErrorException($locationsCount->error->message);

            // dropdown data
            $UUIDs = Beacons::find(['fields' => ['uuidvalue']], $this->_isSkipCheck);
            if (isset($UUIDs->error)) throw new ErrorException($UUIDs->error->message);

            $beaconSets = BeaconSets::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
            if (isset($beaconSets->error)) throw new ErrorException($beaconSets->error->message);

            $deviceTypes = DeviceTypes::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
            if (isset($deviceTypes->error)) throw new ErrorException($deviceTypes->error->message);

            $locations = Locations::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
            if (isset($locations->error)) throw new ErrorException($locations->error->message);

            $deviceSets = DeviceSets::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
            if (isset($deviceSets->error)) throw new ErrorException($deviceSets->error->message);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        $data = [
            'title'           => trans('core.Super Admin'),

            'beaconsCount'    => $beaconsCount->count,
            'beaconSetsCount' => $beaconSetsCount->count,
            'devicesCount'    => $devicesCount->count,
            'deviceSetsCount' => $deviceSetsCount->count,
            'locationsCount'  => $locationsCount->count,

            'UUIDsList'       => array_merge($defaultOpts, buildDropDownOpts($UUIDs, 'uuidvalue', 'uuidvalue')),
            'majorIdsList'    => $defaultOpts,
            'minorIdsList'    => $defaultOpts,
            'beaconSetsList'  => array_merge($defaultOpts, buildDropDownOpts($beaconSets)),
            'deviceTypesList' => array_merge($defaultOpts, buildDropDownOpts($deviceTypes)),
            'locationsList'   => array_merge($defaultOpts, buildDropDownOpts($locations)),
            'deviceSetsList'  => array_merge($defaultOpts, buildDropDownOpts($deviceSets))
        ];

        return view('super_admin.index', $data);
    }

    public function getMajorIdsByUUID()
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $UUID     = Request::get('uuid');

            $majorIds = Beacons::find(['fields' => ['majorid'], 'where' => ['uuidvalue' => $UUID]], $this->_isSkipCheck);

            if (!empty($majorIds->error)) {
                $response['status']  = FALSE;
                $response['message'] = $majorIds->error->message;
            }

            $response['data'] = $majorIds;
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getMinorIdsByUUIDAndMajorId()
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post     = Request::all();
            $minorIds = Beacons::find(['fields' => ['minorid'], 'where' => ['uuidvalue' => $post['uuid'], 'majorid' => $post['majorId']]], $this->_isSkipCheck);

            if (!empty($minorIds->error)) {
                $response['status']  = FALSE;
                $response['message'] = $minorIds->error->message;
            }

            $response['data'] = $minorIds;
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function searchBeacons()
    {
        $response = [
            'status'  => TRUE,
            'message' => '',
            'data'    => [
                'dataTable' => [],
                'dataMaps'  => [
                    'markers' => [],
                    'center'  => 'new google.maps.LatLng(43.792892, -100.829931)'
                ]
            ]
        ];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post  = Request::all();
            $where = [];

            if ($post['searchBy'] == 'beacon') {
                if (!empty($post['uuid'])) $where['uuidvalue'] = $post['uuid'];
                if (!empty($post['majorId'])) $where['majorid'] = $post['majorId'];
                if (!empty($post['minorId'])) $where['minorid'] = $post['minorId'];
                if ($post['activeFlag'] == 'true') $where['activeflag'] = 'true';

                $beacons = Beacons::find(['where' => $where], $this->_isSkipCheck);
            } else {
                if (!empty($post['beaconSetId'])) $where['beaconSetId'] = $post['beaconSetId'];

                $beacons = Beacons::getInstance()->getByBeaconSet($where, $this->_isSkipCheck);
            }

            if (!empty($beacons->error)) {
                $response['status']  = FALSE;
                $response['message'] = $beacons->error->message;
            }

            $this->_prepareResults($beacons, $response);
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function searchDevices()
    {
        $response = [
            'status'  => TRUE,
            'message' => '',
            'data'    => [
                'dataTable' => [],
                'dataMaps'  => [
                    'markers' => [],
                    'center'  => 'new google.maps.LatLng(43.792892, -100.829931)'
                ]
            ]
        ];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post  = Request::all();
            $where = [];

            if ($post['searchBy'] == 'device') {
                if (!empty($post['deviceTypeId'])) $where['type'] = $post['deviceTypeId'];
                if (!empty($post['locationId'])) $where['locationid'] = $post['locationId'];

                $devices = Devices::find(['where' => $where], $this->_isSkipCheck);
            } else {
                if (!empty($post['deviceSetId'])) {
                    $where['deviceSetId'] = $post['deviceSetId'];

                    $devices = Devices::getInstance()->getByDeviceSet($where, $this->_isSkipCheck);
                } else {
                    $devices = Devices::find([], $this->_isSkipCheck);
                }
            }

            if (!empty($devices->error)) {
                $response['status']  = FALSE;
                $response['message'] = $devices->error->message;
            }

            $this->_prepareResults($devices, $response, 'device');
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function getBeacon($id)
    {
        if (!Request::isMethod('get') || !$id) return FALSE;

        try {
            $beacon = Beacons::findById($id, $this->_isSkipCheck);
            if (!empty($beacon->error)) throw new ErrorException($beacon->error->message);

            $data['beacon'] = [
                ['label' => 'ID', 'value' => $beacon->id],
                ['label' => 'Name', 'value' => $beacon->name]
            ];

            $locLabel = 'Location';
            $locValue = '';

            if ($beacon->locationid) {
                $locations = Locations::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
                if (isset($locations->error)) throw new ErrorException($locations->error->message);

                $locations = buildDropDownOpts($locations);

                if (array_key_exists($beacon->locationid, $locations)) $locValue = $locations[$beacon->locationid];
            }

            $data['beacon'][] = ['label' => $locLabel, 'value' => $locValue];

            $groupLabel = 'Organization';
            $groupValue = '';

            if ($beacon->groupid) {
                $groups = Customers::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
                if (isset($groups->error)) throw new ErrorException($groups->error->message);

                $groups = buildDropDownOpts($groups);

                if (array_key_exists($beacon->groupid, $groups)) $groupValue = $groups[$beacon->groupid];
            }

            $data['beacon'][] = ['label' => $groupLabel, 'value' => $groupValue];

            $data['beacon'][] = ['label' => 'UUID', 'value' => $beacon->uuidvalue];
            $data['beacon'][] = ['label' => 'Major ID', 'value' => $beacon->majorid];
            $data['beacon'][] = ['label' => 'Minor ID', 'value' => $beacon->minorid];
            $data['beacon'][] = ['label' => 'Description', 'value' => $beacon->description];
            $data['beacon'][] = ['label' => 'Lost', 'value' => $beacon->lost ? trans('core.Yes') : trans('core.No')];
            $data['beacon'][] = ['label' => 'Active', 'value' => $beacon->activeflag ? trans('core.Yes') : trans('core.No')];
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return view('super_admin.view_beacon', $data);
    }

    public function getDevice($id)
    {
        if (!Request::isMethod('get') || !$id) return FALSE;

        try {
            $device = Devices::findById($id, $this->_isSkipCheck);

            if (isset($device->error)) throw new ErrorException($device->error->message);

            $data['device'] = [
                ['label' => 'ID', 'value' => $device->id],
                ['label' => 'Name', 'value' => $device->name]
            ];

            $customerLabel = 'Organization';
            $customerValue = '';

            if ($device->customerid) {
                $customers = Customers::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
                if (isset($customers->error)) throw new ErrorException($customers->error->message);

                $customers = buildDropDownOpts($customers);

                if (array_key_exists($device->customerid, $customers)) $customerValue = $customers[$device->customerid];
            }

            $data['device'][] = ['label' => $customerLabel, 'value' => $customerValue];

            $locLabel = 'Location';
            $locValue = '';

            if ($device->locationid) {
                $locations = Locations::find(['fields' => ['id', 'name']], $this->_isSkipCheck);
                if (isset($locations->error)) throw new ErrorException($locations->error->message);

                $locations = buildDropDownOpts($locations);

                if (array_key_exists($device->locationid, $locations)) $locValue = $locations[$device->locationid];
            }

            $data['device'][] = ['label' => $locLabel, 'value' => $locValue];

            $data['device'][] = ['label' => 'Description', 'value' => $device->description];
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return view('super_admin.view_device', $data);
    }

    private function _prepareResults($data, &$result, $type = 'beacon')
    {
        if ($type == 'device') {
            $deviceTypes = DeviceTypes::find(['fields' => ['id', 'name']], $this->_isSkipCheck);

            if (!empty($deviceTypes->error)) {
                $result['status']  = FALSE;
                $result['message'] = $deviceTypes->error->message;
            }

            $deviceTypes = buildDropDownOpts($deviceTypes);

            $locations = Locations::find(['fields' => ['id', 'name', 'gpslat', 'gpslng']], $this->_isSkipCheck);

            if (!empty($locations->error)) {
                $result['status']  = FALSE;
                $result['message'] = $locations->error->message;
            }

            $locations = buildDropDownOpts(
                $locations,
                'id',
                'name',
                TRUE,
                [],
                [
                    ['item' => 'gpslat', 'isFieldName' => TRUE],
                    ['item' => '[-]', 'isFieldName' => FALSE],
                    ['item' => 'gpslng', 'isFieldName' => TRUE]
                ]
            );
        } else {
            $deviceTypes = [];
            $locations   = [];
        }

        $customers = Customers::find(['fields' => ['id', 'name']], $this->_isSkipCheck);

        if (!empty($customers->error)) {
            $result['status']  = FALSE;
            $result['message'] = $customers->error->message;
        }

        $customers = buildDropDownOpts($customers);
        $inUSA     = FALSE;

        foreach ($data as &$d) {
            if ($type == 'beacon') {
                $lat = $d->gpslat;
                $lng = $d->gpslng;

                $d->groupid    = array_key_exists($d->groupid, $customers) ? $customers[$d->groupid] : '';
                $d->activeflag = $d->activeflag ? trans('core.Active') : trans('core.Inactive');
            } else {
                if (array_key_exists($d->locationid, $locations)) {
                    $loc = explode('[-]', $locations[$d->locationid]);

                    $lat = $loc[0];
                    $lng = $loc[1];
                } else {
                    $lat = '';
                    $lng = '';
                }

                $d->customerid = array_key_exists($d->customerid, $customers) ? $customers[$d->customerid] : '';
                $d->type       = array_key_exists($d->type, $deviceTypes) ? $deviceTypes[$d->type] : '';
            }

            if ($lat && $lng) {
                $result['data']['dataMaps']['markers'][] = "new google.maps.LatLng($lat, $lng)";

                if (!$inUSA) {
                    $geoCode = json_decode(@file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=false"));
                    $address = $geoCode->results[0]->formatted_address;

                    if (strpos($address, 'USA')) $inUSA = TRUE;
                }
            }
        }

        if (!$inUSA && $result['data']['dataMaps']['markers']) {
            $result['data']['dataMaps']['center'] = $result['data']['dataMaps']['markers'][0];
        }

        $result['data']['dataTable'] = $data;
    }

}