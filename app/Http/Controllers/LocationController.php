<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\CountriesModel AS Countries;
use App\Models\LocationModel AS Location;

class LocationController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Location')
        ];
        return view('location.index', $data);
    }

    public function get($id = null)
    {
        $data = [
            'id' => '',
            'name' => '',
            'address' => '',
            'address2' => '',
            'city' => '',
            'zip' => '',
            'country' => 'US',
            'state' => '',
            'gpslat' => '',
            'gpslng' => '',
            'description' => '',
            'form' => 'add',
        ];

        $data['countriesList'] = Countries::getInstance()->GetCoutries();
        $data['statesList'] = Countries::getInstance()->GetStates();

        if ($id != null) {

            $object = Location::getInstance()->findById($id);

            if (!empty($object->error)) {
                trans('core.not found');
            }

            $data['id'] = $object->id;
            $data['name'] = $object->name;
            $data['address'] = $object->address;
            $data['address2'] = $object->address2;
            $data['city'] = $object->city;
            $data['zip'] = $object->zip;
            $data['country'] = $object->country;
            $data['state'] = $object->state;
            $data['gpslng'] = $object->gpslng;
            $data['gpslat'] = $object->gpslat;
            $data['description'] = $object->description;
            $data['form'] = 'edit';
        }

        return view('location.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'address' => 'required|max:255|min:2',
                    'city' => 'required|max:255|min:2',
                    'zip' => 'required|max:255|min:2'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));
            $params = \Request::all();
            $data = [
                'name' => $params['name'],
                'address' => $params['address'],
                'address2' => $params['address2'],
                'city' => $params['city'],
                'zip' => $params['zip'],
                'country' => $params['country'],
                'state' => ($params['state'] == 1) ? $params['state-drop'] : $params['state-text'],
                'gpslat' => $params['gpslat'],
                'gpslng' => $params['gpslng'],
                'description' => $params['description'],
                'customerid' => getCurrentCustomerId()
            ];

            if ($id) {
                $obj = Location::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $obj = Location::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = Location::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }
}