<?php
/**
 * Created by PhpStorm.
 * User: AnhNguyen
 * Date: 12/10/15
 * Time: 4:58 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Response as Response;

class DatatableController extends Controller
{

    public function index(Request $request)
    {

        $res = (object)[
            'data' => [],
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
            'draw' => 1
        ];

        try {

            $drawId = intval($request->input('draw'));
            $res->draw = $drawId;

            $object = $request->input('_object');
            $objectModel = "App\\Models\\{$object}Model";
            if(!class_exists($objectModel)) throw new \ErrorException();
            if(!method_exists($objectModel, 'find')) throw new \ErrorException();

            $query = [
                'fields' => [
                    'id'
                ],
                'where' => [],
                'filters' => []
            ];

            //Columns
            $columnsDef = $request->input('columns');

            if(count($columnsDef) == 0) throw new \ErrorException();

            $search = $request->input('search')['value'];
            foreach($columnsDef as $k => $v) {
                if($v['searchable'] == 'false') continue;

                $query['fields'][] = $v['data'];

                if($search != '') {
                    if(!isset($query['where']['or'])) $query['where']['or'] = [];

                    $slc = [];
                    $slc[$v['data']] = [
                        'regexp' => "/$search/i"
                    ];
                    $query['where']['or'][] = $slc;
                }

            }
            
            $extWhere = $request->input('_where');
            if(is_array($extWhere) && !empty($extWhere)) {

                $query['where'] = array_merge($query['where'], $extWhere);
            }

            //Order column
            $order = $request->input('order');
            if(is_array($order) && count($order) != 0) {

                $query['order'] = [];
                $name = '';
                $dir = '';
                foreach($order as $o) {
                    if(!isset($o['column']) || !isset($o['dir']) || !isset($columnsDef[$o['column']])) continue;

                    $name = $columnsDef[$o['column']]['data'];
                    $dir = strtoupper($o['dir']);
                    $query['order'][] = "$name $dir";
                }
            }

            $total = call_user_func("$objectModel::countCondition", $query['where']);
            if(isset($total->count)) {
                $res->recordsTotal = $total->count;
                $res->recordsFiltered = $total->count;
            }

            //Num of rows
            $start = intval($request->input('start'));
            $length = intval($request->input('length'));
            if($length != 0 && $length != 1000000) {

                $query['limit'] = $length;
                $query['skip'] = $start;
            }
            
            $filters = $request->input('_filters');

            if(is_array($filters) && !empty($filters)) {

                $query['filters'] = array_merge($query['filters'], $filters);
            }

            if(method_exists($objectModel, "findInclude")) {
                $response = call_user_func("$objectModel::findInclude", $query);

                if(isset($response->callback)) {
                    $response = $response['data'];
                    $res->recordsTotal = count($response);
                    $res->recordsFiltered = count($response);
                }
            } else {
                $response = call_user_func("$objectModel::find", $query);
            }

            if(isset($response->error)) throw new \ErrorException($response->error->message);

            foreach($response as $k => $v) {
                $v->DT_RowId = $v->id;
                $v->action = '';
                $res->data[] = $v;
            }

        } catch (\Exception $e) {
            $res->error = $e->getMessage();
        }

        return Response::json($res);
    }

}