<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;
use App\Http\Requests\Request;
use App\Models\DeviceManagementModel as DeviceManagement;
use App\Models\CustomersModel as Customers;
use App\Models\LocationModel as Location;
use App\Models\DeviceTypesModel as DeviceType;
use App\Models\DeviceTypeAttributesModel as DeviceTypeAttr;
use App\Models\DeviceAttributeModel as DeviceAttr;
use \ErrorException as ErrorException;
use \Exception as Exception;
use \Session as Session;

class DeviceManagementController extends Controller
{
    public function index()
    {

        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Device Management')
        ];
        return view('device_management.index', $data);
    }

    public function get($id = null) {
        $data = [
            'id' => '',
            'deviceid' => '',
            'name' => '',
            'type' => '',
            'parentid' => '',
            'locationid' => '',
            'customerid' => '',
            'tags' => '',
            'form' => 'add'
        ];

        $data['locations'] = Location::find([]);
        $data['groups'] = Customers::find([]);
        $data['types'] = DeviceType::find([]);
        $data['parents'] = DeviceManagement::find([]);
        $diveTypeId = null;

        if($id != null) {
            $device = DeviceManagement::getInstance()->findById($id);

            if (!empty($device->error)) {
                return trans('core.not found');
            }

            $data['id'] = $device->id;
            $data['deviceid'] = $device->deviceid;
            $data['name'] = $device->name;
            $data['type'] = $device->type;
            $data['parentid'] = $device->parentid;
            $data['locationid'] = $device->locationid;
            $data['customerid'] = $device->customerid;
            $data['tags'] = $device->tags;
            $data['form'] = 'update';

            $attrValueByDeviceId =  DeviceAttr::find( ['where' => ['deviceid' => $id, 'required' => true] ] );
            $attrValue = $this->getAttrByDeviceType($attrValueByDeviceId);
            $data['required'] = $attrValue['required'];

            Session::set('dataBuffer', ['deviceIdType' => $device->type, 'deviceId' => $id, 'form' => 'edit' ]);

        } else {

            if( isset($data['types'][0])){
                $diveTypeId = $data['types'][0]->id;
            }
            Session::set('dataBuffer', ['deviceIdType' => $diveTypeId, 'deviceId' => '', 'form' => 'add' ]);
            $attrByDeviceType = DeviceTypeAttr::getInstance()->getByDeviceType($diveTypeId);
            $attr = $this->getAttrByDeviceType($attrByDeviceType);
            $data['required'] = $attr['required'];
        }

        return view('device_management.edit', $data);
    }

    public function getAttr($id = null) {
        $attrByDeviceType = DeviceTypeAttr::getInstance()->getByDeviceType($id);
        $data = $this->getAttrByDeviceType($attrByDeviceType);
        $data['form'] = 'add';
        return view('device_management.attributes', $data);
    }

    public function optionAttributeLoadFormAdd() {
        return view('device_management.attribute-add');
    }

    public function optionAttributeValueSave() {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try{
            $validator = \Validator::make(
                \Request::all(),
                [
                    'value'   => 'required|max:255|min:2',
                    'name'       => 'required|max:255|min:2',
                ]
            );

            if($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            $dataBuffer = Session::get('dataBuffer');
            $attrResutl =  DeviceAttr::create([
                "deviceid" => $dataBuffer['deviceId'],
                "devicetypeattributeid" => $dataBuffer['deviceId'],
                "name" => $params['name'],
                "value" => $params['value'],
                "required" => false
            ]);

            if(!empty($attrResutl->error)) {
                $response['status'] = FALSE;
                $response['message'] = $attrResutl->error->message;
            }

        }   catch(Exception $e){
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();

    }

    public function optionAttributeValueDelete($id = null) {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if($id === null){
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = DeviceAttr::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


    private function getAttrByDeviceType($data = []) {
        $required = []; $option = [];

        foreach($data as $k => $v){
            $value = (isset($v->value))?$v->value:'';
            if($v->required === true){
                $required[] = ['id' => $v->id, 'name' => $v->name, 'value' => $value];
            } else {
                $option[] = ['id' => $v->id, 'name' => $v->name, 'value' => $value];
            }
        }

        return ['required' => $required, 'option' =>$option];
    }


    public function save($id = null) {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try{
            $validator = \Validator::make(
                \Request::all(),
                [
                    'deviceid'   => 'required|max:255|min:2',
                    'name'       => 'required|max:255|min:2',
                    'type'       => 'max:255|min:2',
                    'locationid' => 'max:255|min:2',
                    'customerid' => 'max:255|min:2',
                    'parentid'   => 'max:255|min:2'
                ]
            );

            if($validator->fails()) throw new ErrorException(concat_error($validator));

            $params = \Request::all();
            
            $data = [
                'deviceid'     => $params['deviceid'],
                'name'       => $params['name'],
                'type'          => $params['type'],
                'locationid'   => isset($params['locationid'])?$params['locationid']:null,
                'customerid'       => $params['customerid'],
                'tags'    => $params['tags']
            ];

            if( $params['parentid'] != ''){
                $data['parentid'] = $params['parentid'];
            }

            if($id) { // update device if exist deviceId

                $obj = DeviceManagement::updateById($id, $data);
                $data['message'] = '';

                if( !empty($obj->error)){
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else{

                    $dataBuffer = Session::get('dataBuffer');

                    if($dataBuffer['deviceIdType'] !=  $params['type']){ // delete all attribute of device if deviceType is changed
                        DeviceAttr::deleteAll(['deviceid' => $id, 'required' =>true ]);
                        // save new attribute value to device
                        if( isset($params['attrrequiredname'], $params['attrrequiredid'], $params['attrrequiredvalue']) ) {
                            $this->saveAttributeValue($id, $params );
                        }
                    } else { // update attribute value if device type not changed

                        if( isset($params['attrrequiredname'], $params['attrrequiredid'], $params['attrrequiredvalue']) ) {
                            $this->updateAttributeValue($params);
                        }
                    }
                }
            } else { // create new device
                $obj = DeviceManagement::create($data);
                $response['message'] = '';

                if(!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    // save value attrtibutes to device attributes
                    if( isset($params['attrrequiredname'], $params['attrrequiredid'], $params['attrrequiredvalue']) ) {
                        $this->saveAttributeValue($obj->id, $params);
                    }

                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();

    }

    private function saveAttributeValue($diviceId = null, $params = []) {

        foreach($params['attrrequiredname'] as $k => $v){
            $attrResutl =  DeviceAttr::create([
                "deviceid" => $diviceId,
                "devicetypeattributeid" => $params['attrrequiredid'][$k],
                "name" => $v,
                "value" => $params['attrrequiredvalue'][$k],
                "required" => true
            ]);

            if(!empty($attrResutl->error)) {
                return $attrResutl->error->message;
            }
        }

    }

    private function updateAttributeValue($params = []) {

        foreach($params['attrrequiredid'] as $k => $v){
            $attrResutl =  DeviceAttr::updateById($v,[
                "value" => $params['attrrequiredvalue'][$k],
            ]);

            if(!empty($attrResutl->error)) {
                return $attrResutl->error->message;
            }
        }

    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if($id === null){
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = DeviceManagement::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }




}