<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use \App\Models\UsersModel AS Users;
use \App\Models\CountriesModel AS Countries;

use \Exception AS Exception;
use \Session AS Session;
use \Request AS Request;
use \Config AS Config;
use \Response AS Response;
use \Cookie AS Cookie;

class UserController extends Controller
{

    public function __construct()
    {
        if (\Session::get('user')) {
            $this->_assignGlobalVars();
        }
    }

    public function myAccount()
    {
        $data = [
            'countries' => Countries::getInstance()->GetCoutries(),
            'states'    => Countries::getInstance()->getStates(),
            'title'     => trans('core.My Account')
        ];

        $user = Users::getInstance()->get();

        if ($user&& !$user->error) {
            $data = array_merge($data, [
                'firstname'        => $user->firstname,
                'lastname'         => $user->lastname,
                'email'            => $user->email,
                'country_selected' => $user->country,
                'state_selected'   => $user->state,
                'zip'              => $user->zip,
                'address1'         => $user->address1,
                'address2'         => $user->address2,
                'city'             => $user->city
            ]);

            if (\Request::isMethod('post')) {
                try {
                    $post = \Request::all();

                    $data = array_merge($data, [
                        'firstname' => !empty($post['firstname']) ? $post['firstname'] : '',
                        'email'     => !empty($post['email']) ? $post['email'] : '',
                        'lastname'  => !empty($post['lastname']) ? $post['lastname'] : '',
                        'address1'  => !empty($post['address1']) ? $post['address1'] : '',
                        'address2'  => !empty($post['address2']) ? $post['address2'] : '',
                        'city'      => !empty($post['city']) ? $post['city'] : '',
                        'state'     => !empty($post['state']) ? $post['state'] : '',
                        'zip'       => !empty($post['zip_code']) ? $post['zipCode'] : '',
                        'country'   => !empty($post['country']) ? $post['country'] : ''
                    ]);

                    if (!empty($post['old_password'])) {
                        $match_pwd = Users::getInstance()->checkCurrentPassword($user->id, $post['old_password']);

                        if (!empty($match_pwd->match) && $match_pwd->match) {
                            $rules = ['New Password' => 'new_password', 'Confirm Password' => 'confirmPassword'];

                            foreach ($rules as $k => $v) {
                                if (empty(trim($post[$v]))) {
                                    throw new Exception(trans('core.' . $k . ' is a required field'));
                                    break;
                                }
                            }

                            if ($post['new_password'] != $post['confirmPassword']) {
                                throw new Exception(trans('core.Not same as New Password'));
                            }

                            $data['password'] = $post['new_password'];
                        } else {
                            throw new Exception(trans('core.Password does not match our records'));
                        }
                    }

                    $update = Users::updateById($user->id, $data);

                    if (!empty($update->error)) {
                        throw new Exception(trans('core.' . $update->error->message));
                    } else {
                        $data['response']['status']  = TRUE;
                        $data['response']['message'] = trans('core.User has been updated');
                    }
                } catch (Exception $e) {
                    $data['response']['status']  = FALSE;
                    $data['response']['message'] = $e->getMessage();
                }
            }
        }

        return view('user.my_account', $data);
    }

    public function login()
    {
        if (!empty(Session::get('user'))) return redirect('dashboard');

        $data = [
            'isRemember' => Request::cookie('siteAuth') ? TRUE : FALSE,
            'title'      => trans('core.Login')
        ];

        return view('user.login', $data);
    }

    public function doLogin()
    {
        $response = array('status' => TRUE, 'message' => '');
        $remember = FALSE;

        try {
            $post     = Request::all();
            $username = $post['email'];
            $password = $post['password'];
            $remember = !empty($post['remember']) ? TRUE : FALSE;
            $user     = Users::getInstance()->authenticate($username, $password, $remember);

            if (isset($user->error)) {
                $response['status']  = FALSE;
                $response['message'] = $user->error->message;
            } else {
                Users::getInstance()->setAccessToken($user->token);
                Session::set('user', $user);
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        if ($response['status'] && $remember) {
            $cookieName  = Config::get('api.rememberCookieName');
            $cookieValue = Config::get('api.isHMAC') ? rncEncrypt($user->token) : $user->token;
            $expires     = 60 * 24 * Config::get('api.rememberExpires');

            return Response::json($response)->withCookie(cookie($cookieName, $cookieValue, $expires));
        }

        return Response::json($response);
    }

    public function logout()
    {
        Users::getInstance()->logout();
        Session::flush();

        $cookie = Cookie::forget('siteAuth');

        return redirect('/')->withCookie($cookie);
    }

    public function forgot()
    {
        $data = ['title' => trans('core.Reset Password')];

        if (Request::isMethod('post')) {

            $act = Users::getInstance()->reset(Request::get('email'));

            $data['response'] = [];

            if (!empty($act->error)) {
                $data['response']['status'] = FALSE;
                $data['response']['message'] = $act->error->message;
            } else {
                $data['response']['status'] = TRUE;
                $data['response']['message'] = 'Success';
            }
        }
        return view('user.forgot', $data);
    }

    public function confirm()
    {
        $data = [];

        if (Request::isMethod('get')) {
            $email = Request::get('email');
            $token = Request::get('token');

            $act = Users::getInstance()->confirm($email, $token);

            if (!empty($act->error)) {
                $data['response']['status'] = FALSE;
                $data['response']['message'] = $act->error->message;
                return view('user.error_confirm', $data);
            } else {
                $data['response']['status'] = TRUE;
                $data['response']['message'] = 'Success';

                $data['response']['password'] = $act->password;
                return view('user.confirm', $data);
            }
        }
    }

    public function getUsersByCustomerId()
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        try {
            $users = Users::getInstance()->getAllByCustomerId(Request::get('customerId'));

            if (isset($users->error)) {
                $response['status']  = FALSE;
                $response['message'] = $users->error->message;
            } else {
                $response['data'] = $users;
            }
        } catch (Exception $e) {
            $response['status']  = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function setSelectedOrgUser()
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        try {
            $post        = Request::all();
            $curUserData = Session::get('user');

            $curUserData->org       = $post['customerId'];
            $curUserData->userUnder = $post['userId'];

            Session::set('user', $curUserData);
            Users::getInstance()->setCurrentOrg($post['customerId']);
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

} 