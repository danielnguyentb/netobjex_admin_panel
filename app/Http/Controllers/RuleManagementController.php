<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\RuleManageModel as RuleManage;
use App\Models\DeviceTypesModel as DeviceType;
use App\Models\DeviceSetEditorModel as DeviceSetEditor;
use App\Models\DeviceTypeAttributesModel as DeviceTypeAttr;
use App\Models\EnterpriseModel as Enterprise;
use App\Models\CommandsModel as Command;
use App\Models\CommandParamsModel as CommandsParam;
use App\Models\CommandParamValuesModel as CommandsParamsValue;
use App\Models\MessagesModel as Message;
use App\Models\MessageVariablesModel as MessageVariables;
use App\Models\MessageVariableMappingsModel as MessageVariablesMapping;
use App\Models\ServiceModel as Service;
use App\Models\RuleConditionModel as RuleCondition;

use \Session as Session;
use \Exception as Exception;

class RuleManagementController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Rule Management')
        ];
        return view('rule_management.index', $data);
    }

    private function getEditorByDeviceType($id = null)
    {
        return DeviceSetEditor::find(['where' => ['devicetypeid' => $id]]);
    }

    public function getDeviceEditorByDevice($id = null)
    {
        if ($id === null) {
            return [];
        }
        $data = DeviceSetEditor::find(['where' => ['devicetypeid' => $id]]);
        return view('rule_management.editor-drop', ['data' => $data]);
    }

    public function getCommandByDevice($id)
    {
        if ($id === null) {
            return [];
        }
        $data = Command::find(['where' => ['dtype' => $id]]);
        return view('rule_management.command', ['data' => $data]);
    }

    public function getAttrByDevice($id = null)
    {
        if ($id === null) {
            return [];
        }

        $attr = DeviceTypeAttr::getInstance()->getByDeviceType($id);

        if (!empty($attr->error) || sizeof($attr) == 0) {
            return '';
        }

        $operator = RuleCondition::getInstance()->getOperator();
        return view('rule_management.attr-row', ['data' => $attr, 'operators' => $operator]);
    }

    public function getRouteByType($id = null)
    {
        $data = [];

        if ($id == null) {
            return $data;
        }

        if ($id == 1) {
            $data = Enterprise::find([]);
        } else if ($id == 2) {
            $data = Message::find([]);
        } else {
            $data = Command::find([]);
        }

        return view('rule_management.route-option', ['data' => $data]);
    }

    public function getVariableMap()
    {
        $params = \Request::all();

        if ($params['type'] == null || $params['id'] == null || $params['deviceTypeId'] == null) {
            return;
        }

        $variable = [];
        $mapping = [];
        $routId = $params['id'];
        $view = 'rule_management.route-message-mapping';

        if ($params['type'] == 2) {
            $variable = MessageVariables::find(['where' => ['messageid' => $params['id']]]);
            $mapping = Service::getInstance()->getVariableMapping($params['deviceTypeId']);
        } else if ($params['type'] == 3) {

            $variable = CommandsParam::find(['where' => ['commandid' => $params['id']]]);
            $view = 'rule_management.route-command-mapping';
        }

        return view($view, ['variable' => $variable, 'mapping' => $mapping, 'routId' => $routId]);
    }

    public function saveVariableMap()
    {

        $params = \Request::all();
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($params['routeType'] == 1) {
            return \Response::json($response);
        }

        if (!isset($params['variable-value'], $params['mapping'])) {
            return \Response::json($response);
        }

        foreach ($params['variable-value'] as $k => $v) {

            $exist = (object)array('error' => true);

            if ($params['routeType'] == 2) {

                $data = ['variableid' => $v, 'mapping' => $params['mapping'][$k]];
                $exist = MessageVariablesMapping::find(['where' => ['variableid' => $v]]);
            } else if ($params['routeType'] == 3) {

                $data = ['commandparamid' => $v, 'scheduledcommandid' => $params['scheduledcommandid'][$k], 'value' => $params['mapping'][$k]];
                $exist = CommandsParamsValue::find(['where' => ['commandparamid' => $v]]);
            } else {
                return \Response::json($response);
            }

            if (empty($exist->error)) {

                if (sizeof($exist) == 0) {
                    if ($params['routeType'] == 2) {
                        MessageVariablesMapping::create($data);
                    } else {
                        CommandsParamsValue::create($data);
                    }

                } else {
                    if ($params['routeType'] == 2) {
                        MessageVariablesMapping::updateAll(['variableid' => $v], $data);
                    } else {
                        CommandsParamsValue::updateAll(['commandparamid' => $v], $data);
                    }

                }
            } else {
                $response['status'] = FALSE;
                $response['message'] = 'Some error occurred';
            }

        }

        return \Response::json($response);
    }

    public function get($id = null)
    {

        $data = [
            'id' => '',
            'name' => '',
            'active' => false,
            'devicetypeid' => '',
            'devicesetid' => '',
            'queuetopicname' => '',
            'interval' => '',
            'enterprisegatewayid' => '',
            'messageid' => '',
            'commandid' => '',
            'form' => 'add',
        ];
        $data['types'] = DeviceType::find([]);
        $data['EGs'] = Enterprise::find([]);
        $data['messages'] = Message::find([]);
        $data['commands'] = [];

        if ($id != null) {
            $object = RuleManage::getInstance()->findById($id);

            if (!empty($object->error)) {
                return trans('core.not found');
            }

            $data['id'] = $object->id;
            $data['active'] = $object->active;
            $data['name'] = $object->name;
            $data['devicetypeid'] = $object->devicetypeid;
            $data['devicesetid'] = $object->devicesetid;
            $data['queuetopicname'] = $object->queuetopicname;
            $data['interval'] = $object->interval;
            $data['enterprisegatewayid'] = $object->enterprisegatewayid;
            $data['messageid'] = $object->messageid;
            $data['commandid'] = $object->commandid;
            $data['deviceSetEditors'] = $this->getEditorByDeviceType($object->devicetypeid);
            $data['deviceAttr'] = $this->getAttrByDevice($object->devicetypeid);
            $data['commands'] = Command::find(['where' => ['dtype' => $object->devicetypeid]]);
            Session::set('dataBuffer', ['ruleId' => $object->id, 'form' => 'edit', 'deviceTypeId' => $object->devicetypeid]);
            $data['form'] = 'edit';

        } else {
            $data['deviceSetEditors'] = isset($data['types'][0]->id) ? $this->getEditorByDeviceType($data['types'][0]->id) : [];
            $data['deviceAttr'] = isset($data['types'][0]->id) ? $this->getAttrByDevice($data['types'][0]->id) : [];
            if (isset($data['types'][0]->id)) {
                $data['commands'] = Command::find(['where' => ['dtype' => $data['types'][0]->id]]);
            }
        }

        return view('rule_management.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'name' => 'required|max:255|min:2',
                    'devicetypeid' => 'max:255|min:2',
                    'devicesetid' => 'max:255|min:2',
                    'queuetopicname' => 'required|max:255|min:2',
                    'interval' => 'required|numeric',
                    'enterprisegatewayid' => 'max:255|min:2',
                    'messageid' => 'max:255|min:2',
                    'commandid' => 'max:255|min:2'
                ]
            );

            if ($validator->fails()) throw new Exception(concat_error($validator));

            $params = \Request::all();


            if (isset($params['operand'])) {
                foreach ($params['operand'] as $k => $v) {
                    $validatorCondition = \Validator::make(
                        ['operand' => $v],
                        [
                            'operand' => 'required|integer|in:1,2,3,4,5,6,7',
                        ]
                    );
                    if ($validatorCondition->fails()) throw new Exception(concat_error($validatorCondition));
                }
            }

            $data = [
                'name' => $params['name'],
                'devicetypeid' => $params['devicetypeid'],
                'queuetopicname' => $params['queuetopicname'],
                'interval' => $params['interval'],
                'customerid' => getCurrentCustomerId()
            ];


            $data['active'] = (isset($params['active'])) ? true : false;

            if ($params['devicetypeid'] != '') {
                $data['devicetypeid'] = $params['devicetypeid'];
            }
            if ($params['devicesetid'] != '') {
                $data['devicesetid'] = $params['devicesetid'];
            }
            if ($params['enterprisegatewayid'] != '') {
                $data['enterprisegatewayid'] = $params['enterprisegatewayid'];
            }
            if ($params['messageid'] != '') {
                $data['messageid'] = $params['messageid'];
            }
            if ($params['commandid'] != '') {
                $data['commandid'] = $params['commandid'];
            }

            if ($id) {
                $obj = RuleManage::updateById($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['operand'])) {
                        $obj = RuleCondition::deleteAll(['ruleid' => $id]);

                        if (!empty($obj->error)) {
                            throw new Exception(concat_error($obj->error->message));
                        }

                        $this->saveCondition($id, [
                            'attribute' => $params['attribute'],
                            'value' => $params['value'],
                            'operand' => $params['operand'],
                            'isand' => $params['isand'],
                        ]);
                    }
                }

            } else {
                $obj = RuleManage::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                } else {
                    if (isset($params['operand'])) {
                        $this->saveCondition($obj->id, [
                            'attribute' => $params['attribute'],
                            'value' => $params['value'],
                            'operand' => $params['operand'],
                            'isand' => $params['isand'],
                        ]);
                    }
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }

    private function saveCondition($ruleId = null, $data = [])
    {
        if ($ruleId == null) {
            return;
        }

        foreach ($data['attribute'] as $k => $v) {
            $conditionData = [
                'devicetypeattributeid' => $v,
                'ruleid' => $ruleId,
                'value' => $data['value'][$k],
                'operand' => $data['operand'][$k],
                'isand' => $data['isand'][$k]
            ];
            $obj = RuleCondition::create($conditionData);

            if (!empty($obj->error)) {
                throw new Exception($obj);
            }
        }

    }

    public function deleteCondition($id = null)
    {

        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = RuleCondition::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = RuleManage::deleteById($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}