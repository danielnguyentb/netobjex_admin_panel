<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\ProtocolsModel as Protocols;
use App\Models\CustomersModel as Customers;
// use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

// use Exception;
// use Validator;
use \Request AS Request;
use \Response AS Response;
use \ErrorException AS ErrorException;
use \Exception AS Exception;

class ProtocolManagementController extends Controller
{

    public function index()
    {
        $data = [
            'title' => trans('core.Protocol Management')
        ];

        return view('protocol_management.index', $data);
    }

    public function get($protocolId = NULL)
    {
        if (!Request::isMethod('get')) return FALSE;

        if ($protocolId !== NULL) {
            try {
                $protocol = Protocols::findById($protocolId);

                if (!empty($protocol->error)) throw new ErrorException($protocol->error->message);

                $data = (array)$protocol;
                $data['form'] = 'edit';
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            $data = [
                'form' => 'add',
                'id' => '',
                'name' => ''
            ];
        }

        return view('protocol_management.edit_protocol', $data);
    }

    public function save($protocolId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $validator = \Validator::make(
                $post,
                [
                    'name' => 'required|max:255|min:2'
                ]
            );

            if ($validator->fails()) throw new ErrorException(concat_error($validator));

            $c = Protocols::countCondition(
                [
                    'name' => [
                        'regexp' => '/' . $post['name'] . '/i'
                    ],
                    'id' => [
                        'neq' => $protocolId
                    ]
                ]
            );


            if (is_object($c) && !empty($c->count) && $c->count > 0) {
                throw new ErrorException('A Protocol by that name already exists. Please select another name.');
            }

            $data = [
                'name' => !empty($post['name']) ? $post['name'] : ''
            ];

            if ($protocolId != NULL) {
                $posted = Protocols::updateById($post['id'], $data);
            } else {
                $cus = \Session::get('user');

                if ($cus->org) {
                    $data['customerid'] = $cus->org;
                }

                $posted = Protocols::create($data);
            }

            if (!empty($posted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $posted->error->message;
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

    public function delete($protocolId = NULL)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];
        try {
            if (!Request::isMethod('post')) return FALSE;


            $obj = Protocols::deleteById($protocolId);

            if (!empty($obj->error)) {
                throw new ErrorException($obj->error->message);
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return Response::json($response);
    }

}