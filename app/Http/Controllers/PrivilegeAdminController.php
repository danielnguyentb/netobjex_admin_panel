<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\PrivilegeAdminModel as PrivilegeAdmin;
use App\Models\ClassAdminModel as ClassAdmin;
use App\Models\UsersModel as Users;
use App\Models\PrivilegeAdminRolePermissionsModel as PrivilegeAdminRolePermissions;

use \Session as Session;
use \Exception as Exception;
use \ErrorException AS ErrorException;

class PrivilegeAdminController extends Controller
{

    public function index()
    {
        $data = [
            'existCustomerId' => getCurrentCustomerId(),
            'title' => trans('core.Privilege Admin')
        ];
        return view('privilege_admin.index', $data);
    }

    public function get($id = null)
    {

        $roles = Users::getInstance()->getRoleList();

        if (isset($roles->error)) throw new ErrorException($roles->error->message);

        $classes = ClassAdmin::find();

        if (isset($classes->error)) throw new ErrorException($classes->error->message);

        $data = [
            'id' => '',
            'rid' => '',
            'object' => '',
            'parent' => '',
            'access' => true,
            'vvv' => '',
            'e' => '',
            'd' => '',
            'form' => 'add',
            'roles' => $roles,
            'classes' => $classes,
        ];

        if ($id != null) {

            $where = [
                'where' => [
                    'id' => $id
                ]
            ];

            $object = PrivilegeAdmin::getInstance()->find($where);

            if (!empty($object->error)) {
                return trans('core.not found');
            }

            $data['id'] = $object[0]->id;
            $data['rid'] = $object[0]->rid;
            $data['object'] = $object[0]->object;
            $data['vvv'] = $object[0]->v;
            $data['e'] = $object[0]->e;
            $data['d'] = $object[0]->d;
            $data['access'] = $object[0]->access;
            $data['form'] = 'edit';

        }

//        var_dump($data); die;
        return view('privilege_admin.edit', $data);
    }

    public function save($id = null)
    {
        $response = ['status' => TRUE, 'message' => '', 'data' => []];

        try {
            $validator = \Validator::make(
                \Request::all(),
                [
                    'rid' => 'required|max:255|min:2',
                    'object' => 'max:255|min:2',
                ]
            );

            if ($validator->fails()) throw new Exception(concat_error($validator));

            $params = \Request::all();

            $v = (isset($params['v']))?true:false;
            $d = (isset($params['d']))?true:false;
            $e = (isset($params['e']))?true:false;
            $access = ($params['access'] == 'all') ? true:false;
            $data = [
                'rid' => $params['rid'],
                'object' => $params['object'],
                'access' => $access,
                'parent' => $params['object'],
                'v' => $v,
                'e' => $e,
                'd' => $d
            ];

            if ($id) {

                $obj = Users::getInstance()->updatePermissions($id, $data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }

            } else {
                $obj = PrivilegeAdminRolePermissions::create($data);

                if (!empty($obj->error)) {
                    $response['status'] = FALSE;
                    $response['message'] = $obj->error->message;
                }
            }

        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
        exit();
    }


    public function delete($id = null)
    {
        $response = array('status' => TRUE, 'message' => '', 'data' => []);

        if ($id === null) {
            $response['message'] = trans('core.Id is required');
            return \Response::json($response);
        }

        try {
            $deleted = Users::getInstance()->deleteRolePermissions($id);

            if (!empty($deleted->error)) {
                $response['status'] = FALSE;
                $response['message'] = $response->error->message;
            } else {
                $response['message'] = '';
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }


}