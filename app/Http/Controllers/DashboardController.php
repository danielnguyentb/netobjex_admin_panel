<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/10/15
 * Time: 2:46 PM
 */

namespace App\Http\Controllers;

use App\Models\CustomersModel as Customer;
use App\Models\AdCampaignsModel as AdCampaigns;
use App\Libs\SimpleDateTime AS SimpleDateTime;
use App\Models\LogsModel as Logs;
use App\Models\LocationModel as Location;

use App\Models\SurveysModel as Surveys;
use \Request AS Request;
use \Response AS Response;
use \Exception AS Exception;
use \ErrorException AS ErrorException;

class DashboardController extends Controller
{

    public function index()
    {
        $customerId = getCurrentCustomerId();
        $data = ['title' => trans('core.Dashboard')];

        $curDate = new SimpleDateTime();

        $endDate = str_replace('+00:00', '.000Z', gmdate('c', strtotime($curDate->getDate() . ' ' . $curDate->getTime())));

        $startDate = $curDate->sub('day', 7);
        $startDate = str_replace('+00:00', '.000Z', gmdate('c', strtotime($startDate->getDate() . ' ' . $startDate->getTime())));

        $data['startdate'] = $startDate;
        $data['enddate'] = $endDate;

        $locations = Location::find(['fields' => ['id' => true, 'name' => true]]);
        $locationList = ['' => 'All'];

        if ($locations && !$locations->error) {
            foreach ($locations as $v) {
                $locationList[$v->id] = $v->name;
            }
        }

        $data['locations'] = $locationList;

        if ($customerId) {
            $customer = Customer::findById($customerId);
            
            if (empty($customer->error) && !empty($customer->kibanaurl)) {


                $url = $customer->kibanaurl;
                $url = str_replace('$TODATE', $endDate, $url);
                $url = str_replace('$FROMDATE', $startDate, $url);
                $url = str_replace('$DEVICELIST', '*', $url);
                $url = str_replace('https://', 'https://kibanaadmin:blahblahblah@', $url);

                $adCampaigns = AdCampaigns::find();

                $campaigns = [];
                if ($adCampaigns && !$adCampaigns->error) {
                    foreach ($adCampaigns as $k => $v) {
                        $campaigns[$v->id] = $v->name;
                    }
                }

                $data['kibanaurl'] = $url;

                $data['campaigns'] = $campaigns;

                return view('dashboard.loader', $data);
            } else {
                return view('dashboard.index', $data);
            }
        }
        return view('dashboard.index', $data);
    }

    public function getCampaignByType()
    {
        $response = ['status' => TRUE, 'data' => []];
        try {
            if (!Request::isMethod('post')) return FALSE;

            $post = Request::all();

            $type = isset($post['type']) ? $post['type'] : '';

            if ($type == 'Advertising') {
                $data = AdCampaigns::find();
            } else if ($type == 'Survey') {
                $data = Surveys::find();
            }

            $campaigns = [];
            if ($data && !$data->error) {
                foreach ($data as $k => $v) {
                    $campaigns[$k]['id'] = $v->id;
                    $campaigns[$k]['name'] = $v->name;
                }

            }
            $response['data'] = $campaigns;


        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return json_encode($response);
    }

    public function getVisitsPerDay()
    {
        $response = ['status' => TRUE];

        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $startdate = isset($post['startDate']) ? $post['startDate'] : null;
            $enddate = isset($post['endDate']) ? $post['endDate'] : null;

            $data = Logs::getInstance()->getVisitsPerDay(['fromDate' => $startdate, 'toDate' => $enddate]);

            if (empty($data->error)) {
                $uniqueData = [];
                if (!empty($data)) {
                    foreach ($data as $k) {
                        $uniqueData[$k->vdate] = $k->count;
                    }
                }
                $response['data'] = $uniqueData;
            } else {
                $response['status'] = FALSE;
                $response['message'] = $data->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }

        return \Response::json($response);
    }

    public function getVisitsPerLocation()
    {
        $response = ['status' => TRUE];
        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $startdate = isset($post['startDate']) ? $post['startDate'] : null;
            $enddate = isset($post['endDate']) ? $post['endDate'] : null;

            $data = Logs::getInstance()->getVisitsPerLocation(['fromDate' => $startdate, 'toDate' => $enddate]);

            if (empty($data->error)) {
                $res = [];

                if ($data) {
                    foreach ($data as $k => $v) {
                        $res[$v->name] = $v->count;
                    }
                }

                $response['data'] = $res;
            } else {
                $response['status'] = FALSE;
                $response['message'] = $data->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        return \Response::json($response);
    }

    public function getVisitsPerBeacon()
    {
        $response = ['status' => TRUE];
        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $startdate = isset($post['startDate']) ? $post['startDate'] : null;
            $enddate = isset($post['endDate']) ? $post['endDate'] : null;
            $locationId = isset($post['locationId']) ? $post['locationId'] : null;
            $keyword = isset($post['keyword']) ? $post['keyword'] : '';

            $data = Logs::getInstance()->getVisitsPerBeacon(['fromDate' => $startdate, 'toDate' => $enddate, 'locationId' => $locationId, 'keyword' => $keyword]);

            if (empty($data->error)) {

                $response['data'] = $data;
            } else {
                $response['status'] = FALSE;
                $response['message'] = $data->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        return \Response::json($response);
    }

    public function getVisitsPerOS()
    {
        $response = ['status' => TRUE];
        try {
            if (!Request::isMethod('post')) throw new ErrorException();

            $post = Request::all();

            $startdate = isset($post['startDate']) ? $post['startDate'] : null;
            $enddate = isset($post['endDate']) ? $post['endDate'] : null;

            $res = [];
            $data = Logs::getInstance()->getVisitsPerOS(['fromDate' => $startdate, 'toDate' => $enddate]);

            if (empty($data->error)) {
                if ($data) {
                    foreach ($data as $k) {
                        if ($k->os == 1) {
                            $res['Android'] = $k->count;
                        }
                        if ($k->os == 2) {
                            $res['iOS'] = $k->count;
                        }
                        if ($k->os == 3) {
                            $res['Windows'] = $k->count;
                        }
                    }
                }
                $response['data'] = $res;
            } else {
                $response['status'] = FALSE;
                $response['message'] = $data->error->message;
            }
        } catch (Exception $e) {
            $response['status'] = FALSE;
            $response['message'] = $e->getMessage();
        }
        return \Response::json($response);
    }

} 