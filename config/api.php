<?php
/**
 * Created by PhpStorm.
 * User: Chung
 * Date: 11/23/2015
 * Time: 10:28 AM
 */
return [
    'apiUrl'             => $_ENV['apiUrl'],
    'authKey'            => $_ENV['authKey'],
    'isSkipCheckKey'     => $_ENV['isSkipCheckKey'],
    'encryptKey'         => $_ENV['encryptKey'],
    'isHMAC'             => $_ENV['isHMAC'],
    'rememberExpires'    => $_ENV['rememberExpires'],
    'rememberCookieName' => $_ENV['rememberCookieName'],
    'downloadUrl'        => $_ENV['downloadUrl'],
    'microServicesUrl'   => $_ENV['microServicesUrl'],
    'publishCommand'     => $_ENV['publishCommand']
];