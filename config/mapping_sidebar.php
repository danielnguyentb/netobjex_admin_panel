<?php

return [
    'dashboard' => [
        'trans'  => 'Dashboard',
        'icon'   => 'grid',
        'action' => 'DashboardController@index',
        'route'  => 'dashboard'
    ],
    'project' => [
        'trans' => 'Project Management',
        'icon'  => 'notebook',
        'action'=> 'project_management',
        'route' => 'project-management'
    ],
    'drone' => [
        'trans'  => 'Drone Project',
        'icon'   => '',
        'action' => 'DroneManagementController@index',
        'route'  => 'drone-management'
    ],
    'groupAdmin' => [
        'trans'  => 'Group Admin',
        'icon'   => 'users',
        'action' => 'group_admin',
        'route'  => 'group-admin'
    ],
    'group' => [
        'trans'  => 'Group Management',
        'icon'   => '',
        'action' => 'GroupManagementController@index',
        'route'  => 'group-management'
    ],
    'enterprise' => [
        'trans'  => 'Enterprise Gateway',
        'icon'   => '',
        'action' => 'EnterpriseGatewayController@index',
        'route'  => 'enterprise-gateway'
    ],
    'assets' => [
        'trans'  => 'Asset Management',
        'icon'   => 'screen-smartphone',
        'action' => 'device_admin',
        'route'  => 'asset-management'
    ],
    'beaconAdmin' => [
        'trans'  => 'Landing Page',
        'icon'   => '',
        'action' => 'AssetManagementController@index',
        'route'  => 'asset-management'
    ],
    'beacon' => [
        'trans'  => 'Beacon Management',
        'icon'   => '',
        'action' => 'BeaconManagementController@index',
        'route'  => 'beacon-management'
    ],
    'device' => [
        'trans'  => 'Device Management',
        'icon'   => '',
        'action' => 'DeviceManagementController@index',
        'route'  => 'device-management'
    ],
    'deviceSet' => [
        'trans'  => 'DeviceSet Editor',
        'icon'   => '',
        'action' => 'DeviceSetEditorController@index',
        'route'  => 'device-set-editor'
    ],
    'beaconSet' => [
        'trans'  => 'BeaconSet Editor',
        'icon'   => '',
        'action' => 'BeaconSetEditorController@index',
        'route'  => 'beacon-set-editor'
    ],
    'location' => [
        'trans'  => 'Location',
        'icon'   => '',
        'action' => 'LocationController@index',
        'route'  => 'location'
    ],
    'feed' => [
        'trans'  => 'Feed Management',
        'icon'   => '',
        'action' => 'FeedManagementController@index',
        'route'  => 'feed-management'
    ],
    'rule' => [
        'trans'  => 'Rule Management',
        'icon'   => '',
        'action' => 'RuleManagementController@index',
        'route'  => 'rule-management'
    ],
    'scheduled' => [
        'trans'  => 'Scheduled Commands',
        'icon'   => '',
        'action' => 'ScheduledCommandsController@index',
        'route'  => 'scheduled-commands'
    ],
    'messaging' => [
        'trans'  => 'Messaging',
        'icon'   => '',
        'action' => 'MessagingController@index',
        'route'  => 'messaging'
    ],
    'marketing' => [
        'trans'  => 'Marketing',
        'icon'   => 'graph',
        'action' => 'marketing',
        'route'  => 'marketing'
    ],
    'surveys' => [
        'trans'  => 'Surveys',
        'icon'   => '',
        'action' => 'SurveysController@index',
        'route'  => 'surveys'
    ],
    'adCampaign' => [
        'trans'  => 'Ad Campaign',
        'icon'   => '',
        'action' => 'AdCampaignController@index',
        'route'  => 'ad-campaign'
    ],
    'beaconCampaign' => [
        'trans'  => 'Beacon Campaigns',
        'icon'   => '',
        'action' => 'BeaconCampaignsController@index',
        'route'  => 'beacon-campaigns'
    ],
    'beaconNotifications' => [
        'trans'  => 'Beacon Notifications',
        'icon'   => '',
        'action' => 'BeaconNotificationsController@index',
        'route'  => 'beacon-notifications'
    ],
    'myAccount' => [
        'trans'  => 'My Account',
        'icon'   => 'user',
        'action' => 'UserController@myAccount',
        'route'  => 'my-account'
    ],
    'superAdmin' => [
        'trans'  => 'Super Admin',
        'icon'   => 'eyeglasses',
        'action' => 'super_admin',
        'route'  => 'super-admin'
    ],
    'beaconSuperAdmin' => [
        'trans'  => 'Landing Page',
        'icon'   => '',
        'action' => 'SuperAdminController@index',
        'route'  => 'super-admin'
    ],
    'userAdmin' => [
        'trans'  => 'User Admin',
        'icon'   => '',
        'action' => 'UserAdminController@index',
        'route'  => 'user-admin'
    ],
    'organizationEditor' => [
        'trans'  => 'Organization Editor',
        'icon'   => '',
        'action' => 'OrganizationEditorController@index',
        'route'  => 'organization-editor'
    ],
    'deviceType' => [
        'trans'  => 'Device Type Management',
        'icon'   => '',
        'action' => 'DeviceTypeManagementController@index',
        'route'  => 'device-type-management'
    ],
    'protocol' => [
        'trans'  => 'Protocol Management',
        'icon'   => '',
        'action' => 'ProtocolManagementController@index',
        'route'  => 'protocol-management'
    ]
];