var callBackDataTable = {
    build: function() {
        (function($, app) {
            "use strict";

            var instance = new function() {


                var $table,
                    dataTable;

                $table = $('#deviceOptionAttribute');

                dataTable = (new (app.getCls('dataTable'))()).render(
                    $table,
                    $('.add-button-option'),
                    {
                        colDefs  : [
                            {mData: 'option'},
                            {mData: 'name'},
                            {mData: 'value'},
                            {
                                mData: 'action',
                                searchable: false,
                                orderable : false,
                                className: 'row-action'
                            }
                        ],
                        endpoint: {
                            GET   : app.vars.baseUrl + '/datatable',
                            POST: app.vars.baseUrl + '/device-management/option-attribute-value-save',
                            FORM: app.vars.baseUrl + '/device-management/option-attribute-load-form-add',
                            DELETE: app.vars.baseUrl + '/device-management/option-attribute-value-delete'
                        },
                        extraData: {
                            _object: 'DeviceAttribute'
                        },
                        callbacks: {
                            onRowCallback: function(row, data) {
                                var actions = '<div class="icon-trash icon_row delete"></div>';
                                $('.row-action', row).html(actions);
                            }
                        }
                    }
                );



            };


            app.addCls('organizationManage', instance);


        })($, $.app);
    }
}

