
(function($, app) {
    "use strict";
    
    var instance = new function() {
        var dataTable;
        this.run = function(){
           var wrap = $('.dashboard'),
               searchButton = $('.search', wrap),
               searchVisit = $('.search-visit', wrap),
               locationSelect = $('select[name="location"]', wrap);
           $('#start-date-dashboard', wrap).datepicker('setDate', new Date($('#start-date-dashboard', wrap).val()));
           $('#end-date-dashboard', wrap).datepicker('setDate', new Date($('#end-date-dashboard', wrap).val()));
           $('#start-date-dashboard', wrap).datepicker('update');
           $('#end-date-dashboard', wrap).datepicker('update');
           $('select[name="location"]', wrap).chosen({disable_search_threshold: 4});

           searchButton.on('click', function(e){
                getFilter(e);
           });

           var toDate = jQuery("#start-date-dashboard",wrap).val(),
           fromDate = jQuery("#end-date-dashboard",wrap).val();

            var getFilter = function(e)
            {
                toDate = jQuery("#start-date-dashboard",wrap).val();
                fromDate = jQuery("#end-date-dashboard",wrap).val();

                initPerDay(fromDate, toDate);
                initPerLocation(fromDate, toDate);
                initPerOS(fromDate, toDate);
            };

            var initPerDay = function(startdate, enddate){
                app.ajax(
                    app.vars.baseUrl + '/get-visit-per-day',
                    {startDate:startdate, endDate: enddate,_token: app.vars.csrf},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var res = response.data;
                            var chartData = [];
                            if(Object.size(res))
                            {
                                for(var i in res)
                                {
                                    chartData.push([i, res[i]]);
                                }
                            }
                            
                            var data = [{
                                "label": "No of Visits",
                                "color": "#f5994e",
                                "data": chartData
                            }];
                            var options = {
                                series: {
                                    lines: {
                                        show: true,
                                        fill: 0.01
                                    },
                                    points: {
                                        show: true,
                                        radius: 4
                                    }
                                },
                                grid: {
                                    borderColor: '#eee',
                                    borderWidth: 1,
                                    hoverable: true,
                                    backgroundColor: '#fcfcfc'
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: function (label, x, y) { return x + ' : ' + y; }
                                },
                                xaxis: {
                                    tickColor: '#eee',
                                    mode: 'categories'
                                },
                                yaxis: {
                                    tickColor: '#eee'
                                },
                                shadowSize: 0
                            };

                            var chart = $('.chart-line', wrap);
                            if(chart.length)
                                $.plot(chart, data, options);
                        }
                    }
                );
            }

            Object.size = function(obj) {
                var size = 0, key;
                for (key in obj) {
                    if (obj.hasOwnProperty(key)) size++;
                }
                return size;
            };


            var initPerLocation = function(startdate, enddate){
              app.ajax(
                    app.vars.baseUrl + '/get-visit-per-os',
                    {startDate:startdate, endDate: enddate,_token: app.vars.csrf},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var res = response.data;
                            if(Object.size(res)==3)
                            {
                                var data1 = [
                                { 
                                    "color" : "#39C558",
                                    "data" : res.Android,
                                    "label" : "Android"
                                },
                                { 
                                    "color" : "#00b4ff",
                                    "data" : res.iOS,
                                    "label" : "iOS"
                                },
                                { 
                                    "color" : "#FFBE41",
                                    "data" : res.Windows,
                                    "label" : "Windows"
                                }
                                ];
                            }
                            else if(!Object.size(res))
                            {
                                var data1 = [{"data":100,"label":"No Data"}];
                            }
                            
                            var options = {
                                series: {
                                    pie: {
                                        show: true,
                                        innerRadius: 0.5
                                    }
                                }
                            };

                            var chart1 = $('.chart-donut-1');


                            if (chart1.length) {
                                $.plot(chart1, data1, options);
                            }
                        }
                    }
                );
            }

            var initPerOS = function(startdate, enddate){
            
                app.ajax(
                    app.vars.baseUrl + '/get-visit-per-location',
                    {startDate:startdate, endDate: enddate,_token: app.vars.csrf},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var res = response.data;
                            var data2 = [];
                            if(Object.size(res))
                            {
                                for(var i in res)
                                {
                                    data2.push({"data": res[i], "label": i});
                                }
                            }
                            else
                            {
                                data2 = [{"data":100,"label":"No Data"}];
                            }
                            
                            var options = {
                                series: {
                                    pie: {
                                        show: true,
                                        innerRadius: 0.5
                                    }
                                }
                            };
                            var chart2 = $('.chart-donut-2');
                            if (chart2.length) {
                                $.plot(chart2, data2, options);
                            }
                        }
                    }
                );
        }

        var initVisitByBeacon =  function(startdate, enddate, keyword, location)
        {
            var $tableVisit = $('#visit-beacon');
            
            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $tableVisit,
                {},
                {
                    colDefs: [
                        {
                            mData: 'bName'
                        },
                        {
                            mData: 'locName'
                        },
                        {
                            mData: 'count'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                    },
                    extraData: {
                        _object: 'Logs',
                        _filters: {
                            fromDate: startdate,
                            toDate : enddate,
                            keyword : keyword,
                            locationId: location
                        }
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            
                        },
                        drawCallback: function() {

                            searchVisit.on('click', function(e){
                                $tableVisit.fnFilter($(this).val());
                           });

                           locationSelect.on('change', function(e){
                                $tableVisit.fnFilter($('select[name="location"] option:selected').text());
                           });
                        },
                        onInitGetRow: function(wrap)
                        {
                        }
                    }
                }
            );
        }



        initPerDay(fromDate,toDate);
        initPerLocation(fromDate,toDate);
        initPerOS(fromDate,toDate);
        initVisitByBeacon(fromDate, toDate, '', '');
    }
}


    app.addCls('dashboard', instance);

})($, $.app);
