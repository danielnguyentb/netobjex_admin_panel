/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#group_management');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/group-management/save',
                        FORM: app.vars.baseUrl + '/group-management/form',
                        DELETE: app.vars.baseUrl + '/group-management/delete'
                    },
                    extraData: {
                        _object: 'GroupManage'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-eye icon_row edit"></div>';
                            var edit = '<div class="icon-pencil icon_row edit"></div>';
                            var del = '<div class="icon-trash icon_row delete"></div>';

                            if(roleUser == 'CustomerAdmin'){
                                actions = edit+del;
                            }

                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);


})($, $.app);
