/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#permissions');
            var classId  = $('.classId').val();
            var userId  = $('.userId').val();
            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'id'},

                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/user-permissions-by-class?classId='+classId+'&userId='+userId,
                        POST: app.vars.baseUrl + '/permissions-admin/save',
                        FORM: app.vars.baseUrl + '/permissions-admin/form',
                        DELETE: app.vars.baseUrl + '/permissions-admin/delete'
                    },
                    extraData: {
                        _object: 'UserPermissionsByClass'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);


})($, $.app);


