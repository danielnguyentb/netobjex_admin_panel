/**
 * Created by chungdd on 12/14/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {
        var $table,
            dataTable,
            language;

        this.run = function() {

        };

        this.manager = function(lang) {
            language = lang;
            $table = $('#device-type-management');

            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData     : 'action',
                            searchable: false,
                            orderable : false,
                            className : 'row-action'
                        }
                    ],
                    endpoint : {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST  : app.vars.baseUrl + '/device-type-management/save',
                        FORM  : app.vars.baseUrl + '/device-type-management/form',
                        DELETE: app.vars.baseUrl + '/device-type-management/delete'
                    },
                    extraData: {
                        _object: 'DeviceTypes'
                    },
                    language : lang || {},
                    callbacks: {
                        onRowCallback: function (row, data) {
                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-list icon_row attribute icon-btn-black" title="Attribute"></div>' +
                                '<div class="icon-control-forward icon_row command icon-btn-black" title="Command"></div>' +
                                '<div class="icon-settings icon_row configuration icon-btn-black" title="Configuration"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                            $('.attribute', row).click(viewAttribute);
                            $('.command', row).click(viewCommand);
                            $('.configuration', row).click(viewConfiguration);
                        },
                        onEditSelectedRow: function () {
                            console.log('aaaa');
                        }
                    }
                }
            );
        };

        var removeRowHandler = function() {
            var $this = $(this);
            var $row = $this.closest('tr');
            if($row.length) $row.remove();
            return false;
        };

        var viewAttribute = function(e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingAttribute, 'attribute-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.attribute-content').html());
            $('td', $row).append($content);
            $('.device-type', $row).html(data.name);
            $('.buttonClose', $content).click(removeRowHandler);

            (new (app.getCls('dataTable'))()).render(
                $('.device-type-management-attribute', $row),
                $('.add-button-attr', $row),
                {
                    colDefs: [
                        {mData: 'id'},
                        {mData: 'name'},
                        {mData: 'controltype'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable: false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-type-management/' + data.id + '/attribute/save',
                        FORM: app.vars.baseUrl + '/device-type-management/attribute/form',
                        DELETE: app.vars.baseUrl + '/device-type-management/attribute/delete'
                    },
                    extraData: {
                        _object: 'DeviceTypeAttributes',
                        _where: {
                            devicetypeid: data.id
                        }
                    },
                    classes: {
                        handlerRow: {
                            edit: '.edit-attribute',
                            delete: '.delete-attribute'
                        }
                    },
                    language : {
                        addLoading: language.attributeAddLoading,
                        editLoading: language.attributeEditLoading
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-pencil icon_row edit-attribute icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete-attribute icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                        },
                        drawCallback: function() {
                            if(!$content.is(':visible')) {
                                $loader.fadeOut(
                                    400,
                                    function() {
                                        $content.fadeIn();
                                        $loader.remove();
                                    }
                                );
                            }
                        }
                    }
                }
            );
        };

        var viewCommand = function(e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingCommand, 'command-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.command-content').html());
            $('td', $row).append($content);
            $('.device-type', $row).html(data.name);
            $('.buttonClose', $content).click(removeRowHandler);

            var $tbCmd = $('.device-type-management-command', $row);
            var dataTableCmd = (new (app.getCls('dataTable'))());
            dataTableCmd.render(
                $tbCmd,
                $('.add-button-cmd', $row),
                {
                    colDefs: [
                        {mData: 'id'},
                        {mData: 'name'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable: false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-type-management/' + data.id + '/command/save',
                        FORM: app.vars.baseUrl + '/device-type-management/command/form',
                        DELETE: app.vars.baseUrl + '/device-type-management/command/delete'
                    },
                    extraData: {
                        _object: 'Commands',
                        _where: {
                            dtype: data.id
                        }
                    },
                    classes: {
                        handlerRow: {
                            edit: '.edit-command',
                            delete: '.delete-command'
                        }
                    },
                    language : {
                        addLoading: language.commandAddLoading,
                        editLoading: language.commandEditLoading
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-pencil icon_row edit-command icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-doc icon_row parameter icon-btn-black" title="Parameter"></div>' +
                                '<div class="icon-trash icon_row delete-command icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                            $('.parameter', row).click(viewParameter);
                        },
                        drawCallback: function() {
                            if(!$content.is(':visible')) {
                                $loader.fadeOut(
                                    400,
                                    function() {
                                        $content.fadeIn();
                                        $loader.remove();
                                    }
                                );
                            }
                        }
                    }
                }
            );
        };

        var viewParameter = function (e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var $selfTable = $this.closest('table').data('instance');
            var data = $selfTable.api().row($selfRow[0]).data();

            if (!data) return false;

            var $row = $selfTable.rowLoading(language.loadingParameter, 'parameter-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.parameter-content').html());
            $('td', $row).append($content);
            $('.device-type', $row).html(data.name);
            $('.buttonClose', $content).click(removeRowHandler);
            $('.chosen-select', $row).chosen({disable_search_threshold: 10});

            var $form = $('form', $row);
            app.parsley($form);
            $form.submit(addParamBtnClick);

            (new (app.getCls('dataTable'))()).render(
                $('.device-type-management-parameter', $row),
                null,
                {
                    colDefs: [
                        {mData: 'name'},
                        {mData: 'defaultvalue'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable: false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-type-management/' + data.id + '/parameter/save',
                        FORM: app.vars.baseUrl + '/device-type-management/parameter/form',
                        DELETE: app.vars.baseUrl + '/device-type-management/parameter/delete'
                    },
                    extraData: {
                        _object: 'Parameters',
                        _where: {
                            commandid: data.id
                        }
                    },
                    classes: {
                        handlerRow: {
                            edit: '.edit-parameter',
                            delete: '.delete-parameter'
                        }
                    },
                    language : {
                        addLoading: language.parameterAddLoading,
                        editLoading: language.parameterEditLoading
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-pencil icon_row edit-parameter icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete-parameter icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                        },
                        drawCallback: function() {

                            if(!$content.is(':visible')) {
                                $loader.fadeOut(
                                    400,
                                    function() {
                                        $content.fadeIn();
                                        $loader.remove();
                                    }
                                );
                            }
                        }
                    }
                }
            );
        };

        var addParamBtnClick = function (e) {
            var $this = $(this);
            var $btn = $('.add-button', $this);

            if (!$this.parsley('validate')) return false;

            var $selfRow = $btn.closest('tr'),
                $selfTable = $btn.closest('table').find('table.device-type-management-parameter').data('instance'),
                cmdId = $selfRow.prop('class').replace('parameter-', '');

            $btn.button('loading');

            app.ajax(
                app.vars.baseUrl + '/device-type-management/' + cmdId + '/parameter/save',
                $this.serialize(),
                'POST',
                'json',
                false,
                function (response) {
                    if (response.status) {
                        swal('Added!', response.message, 'success');

                        $selfTable.redraw();
                    } else {
                        sweetAlert('Oops...', response.message, 'error');
                    }
                },
                function () {
                    sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                },
                function () {
                    $this.trigger('reset');
                    $btn.button('reset');

                    setTimeout(function () {
                        swal.enableButtons();
                    }, 100);
                }
            );

            return false;
        };

        var viewConfiguration = function(e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingConfiguration, 'configuration-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.configuration-content').html());
            $('td', $row).append($content);
            $('.device-type', $row).html(data.name);
            $('.buttonClose', $content).click(removeRowHandler);

            (new (app.getCls('dataTable'))()).render(
                $('.device-type-management-configuration', $row),
                $('.add-button-config', $row),
                {
                    colDefs: [
                        {mData: 'id'},
                        {mData: 'variable'},
                        {mData: 'value'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable: false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-type-management/' + data.id + '/configuration/save',
                        FORM: app.vars.baseUrl + '/device-type-management/configuration/form',
                        DELETE: app.vars.baseUrl + '/device-type-management/configuration/delete'
                    },
                    extraData: {
                        _object: 'Configurations',
                        _where: {
                            devicetypeid: data.id
                        }
                    },
                    classes: {
                        handlerRow: {
                            edit: '.edit-configuration',
                            delete: '.delete-configuration'
                        }
                    },
                    language : {
                        addLoading: language.configurationAddLoading,
                        editLoading: language.configurationEditLoading
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-pencil icon_row edit-configuration icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete-configuration icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                        },
                        drawCallback: function() {
                            if(!$content.is(':visible')) {
                                $loader.fadeOut(
                                    400,
                                    function() {
                                        $content.fadeIn();
                                        $loader.remove();
                                    }
                                );
                            }
                        }
                    }
                }
            );
        };

    };

    app.addCls('deviceTypeManagement', instance);

})($, $.app);