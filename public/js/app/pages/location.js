/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#location');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'address'},
                        {mData: 'city'},
                        {mData: 'state'},
                        {mData: 'zip'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/location/save',
                        FORM: app.vars.baseUrl + '/location/form',
                        DELETE: app.vars.baseUrl + '/location/delete'
                    },
                    extraData: {
                        _object: 'Location'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);


})($, $.app);


var initialize = {
    start: function() {
        var lat = $('.gpslat').val();
        var lng = $('.gpslng').val();
        var title = $('.address').val();
        initialize.initMap({lat: lat, lng: lng}, title);
        initialize.stateChoice();
    },
    stateChoice: function() {

        setTimeout(function() {
            initialize.switchViewState($('.country').val());
        }, 1000)
        $('body').on('click', '.country-over li', function() {
            var index = $(this).attr('data-option-array-index');
            var country = $('.country option:eq('+index+')').attr('value');
            initialize.switchViewState(country);
        });
    },
    switchViewState: function( country){

        if( country == 'US'){
            $('.state-over .chosen-container').removeClass('hidden');
            $('.state-text').addClass('hidden');
            $('.state').val(1);
            return;
        }

        $('.state-over .chosen-container').addClass('hidden');
        $('.state-text').removeClass('hidden');
        $('.state').val(2);

    },
    initMap: function(position, title) {
        var lat = (position.lat == '')?40.7127837: Number(position.lat);
        var lng = (position.lng == '')?-74.0059413: Number(position.lng);
        var title = (title != '') ? title: 'New York' ;
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: lng},
            zoom: 15
        });

        var infowindow = new google.maps.InfoWindow({
            content: title
        });

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: title
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

    },
    getLocation: function() {
        var address = $('.address').val();
        var city = $('.city').val();
        var country = $('.country').val();

        $.ajax({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+' '+city+' '+country,
            data: {},
            type: "GET",
            dataType : "html",
            success: function( response ) {
                var data = JSON.parse(response);

                if(data.status !='OK'){
                    return false;
                }
                $('.gpslat').val(data.results[0].geometry.location.lat);
                $('.gpslng').val(data.results[0].geometry.location.lng);
                initialize.initMap(data.results[0].geometry.location, data.results[0].formatted_address);
            }, error: function( xhr, status, errorThrown ) {

            }
        });
    }
};





