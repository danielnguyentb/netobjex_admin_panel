/**
 * Created by chungdd on 01/08/16.
 */


(function($, app) {
    "use strict";

    var instance = new function () {

        this.run = function() {
            $('.chosen-select').chosen({disable_search_threshold: 10});

            bindEvents();
        };

        this.manager = function () {

        };

        var defaultCenterMapBeacon = new google.maps.LatLng(43.792892, -100.829931),
            defaultCenterMapDevice = new google.maps.LatLng(43.792892, -100.829931),
            defaultMarkersBeacon = [],
            defaultMarkersDevice = [];

        var ui = {
            defaultOpts       : '<select class="chosen-select input-md form-control form-format" id="{id}">' +
                                    '<option value="">*</option>' +
                                    '{data}' +
                                '</select>',
            emptySearchResults: '<tr><td colspan="7" align="center">No data to display</td></tr>',
            loading           : '<div class="panel-body loader-add hide loader-demo">' +
                                    '<h4>{text}</h4>' +
                                    '<div class="ball-pulse">' +
                                        '<div></div>' +
                                        '<div></div>' +
                                        '<div></div>' +
                                    '</div>' +
                                '</div>'
        };

        var bindEvents = function () {
            hideElements();
            changeResultsTab();
            beaconSearchByOnClick();
            deviceSearchByOnClick();
            uuidOnChange();
            majorIdOnChange();
            searchBeacon();
            searchDevice();
        };

        var changeResultsTab = function () {
            $('a[data-toggle="tab"][href$="-map"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr('href');

                if (target == '#b-map') {
                    initMap(defaultCenterMapBeacon, defaultMarkersBeacon, 'beacon-map-canvas');
                } else {
                    initMap(defaultCenterMapDevice, defaultMarkersDevice, 'device-map-canvas');
                }
            });
        };

        var hideElements = function () {
            $('.beacon-set-input-items').hide();
            $('.device-set-input-items').hide();
        };

        var beaconSearchByOnClick = function () {
            $('.beacon-search-by').click(function () {
                var $this = $(this),
                    group = $this.data('group');

                $('.B-input-items').hide();

                $('.' + group).show();
            });
        };

        var deviceSearchByOnClick = function () {
            $('.device-search-by').click(function () {
                var $this = $(this),
                    group = $this.data('group');

                $('.D-input-items').hide();

                $('.' + group).show();
            });
        };

        var uuidOnChange = function () {
            $('#uuid-select').change(function () {
                var $this = $(this),
                    uuid = $this.val(),
                    $majorId = $('#major-id-select'),
                    $minorId = $('#minor-id-select');

                if (uuid) {
                    app.ajax(
                        app.vars.baseUrl + '/super-admin/get-major-ids-by-uuid',
                        {uuid: uuid},
                        'POST',
                        'json',
                        true,
                        function (response) {
                            if (response.status) {
                                var majorHtml = [];

                                if (response.data.length) {
                                    $.each(response.data, function (i) {
                                        var majorId = response.data[i].majorid;

                                        majorHtml.push('<option value="' + majorId + '">' + majorId + '</option>');
                                    });
                                }

                                $majorId.parent().html(app.f(ui.defaultOpts, {id: 'major-id-select', data: majorHtml}));
                                $minorId.parent().html(app.f(ui.defaultOpts, {id: 'minor-id-select', data: ''}));
                                $('.chosen-select').chosen({disable_search_threshold: 10});

                                majorIdOnChange();
                            } else {
                                sweetAlert('Oops...', response.message, 'error');
                            }
                        },
                        function () {
                            sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                        },
                        function () {

                        }
                    );
                } else {
                    $majorId.parent().html(app.f(ui.defaultOpts, {id: 'major-id-select', data: ''}));
                    $minorId.parent().html(app.f(ui.defaultOpts, {id: 'minor-id-select', data: ''}));
                    $('.chosen-select').chosen({disable_search_threshold: 10});
                }
            });
        };

        var majorIdOnChange = function () {
            $('#major-id-select').change(function () {
                var $this = $(this),
                    majorId = $this.val(),
                    $uuid = $('#uuid-select'),
                    $minorId = $('#minor-id-select');

                if (majorId) {
                    app.ajax(
                        app.vars.baseUrl + '/super-admin/get-minor-ids-by-uuid-and-major-id',
                        {uuid: $uuid.val(), majorId: majorId},
                        'POST',
                        'json',
                        true,
                        function (response) {
                            if (response.status) {
                                var minorHtml = [];

                                if (response.data.length) {
                                    $.each(response.data, function (i) {
                                        var minorId = response.data[i].minorid;

                                        minorHtml.push('<option value="' + minorId + '">' + minorId + '</option>');
                                    });
                                }

                                $minorId.parent().html(app.f(ui.defaultOpts, {id: 'minor-id-select', data: minorHtml}));
                                $('.chosen-select').chosen({disable_search_threshold: 10});
                            } else {
                                sweetAlert('Oops...', response.message, 'error');
                            }
                        },
                        function () {
                            sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                        },
                        function () {

                        }
                    );
                } else {
                    $minorId.parent().html(app.f(ui.defaultOpts, {id: 'minor-id-select', data: ''}));
                    $('.chosen-select').chosen({disable_search_threshold: 10});
                }
            });
        };

        var searchBeacon = function () {
            $('#search-beacon').click(function () {
                $(this).button('loading');

                var searchBy = $('.beacon-search-by:checked').val();

                if (searchBy == 'beacon') {
                    var uuid = $('#uuid-select').val(),
                        majorId = $('#major-id-select').val(),
                        minorId = $('#minor-id-select').val(),
                        activeFlag = $('#only-active-beacon').prop('checked') ? true : false;

                    var params = {uuid: uuid, majorId: majorId, minorId: minorId, activeFlag: activeFlag};
                } else {
                    var beaconSetId = $('#beacon-set-id-select').val();

                    var params = {beaconSetId: beaconSetId};
                }

                params.searchBy = searchBy;

                app.ajax(
                    app.vars.baseUrl + '/super-admin/search-beacons',
                    params,
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var $beaconResults = $('#beacon-results tbody'),
                                resultsHtml = [];

                            if (response.data.dataTable.length) {
                                $.each(response.data.dataTable, function (i) {
                                    var data = response.data.dataTable[i];

                                    resultsHtml.push('<tr data-id="' + data.id + '">');
                                    resultsHtml.push('<td>' + (i + 1) + '</td>');
                                    resultsHtml.push('<td>' + data.uuidvalue + '</td>');
                                    resultsHtml.push('<td>' + data.majorid + '</td>');
                                    resultsHtml.push('<td>' + data.minorid + '</td>');
                                    resultsHtml.push('<td>' + data.activeflag + '</td>');
                                    resultsHtml.push('<td>' + data.groupid + '</td>');
                                    resultsHtml.push('<td><div class="icon-eye icon_row view"></div></td>');
                                    resultsHtml.push('</tr>');
                                });
                            } else {
                                resultsHtml.push(ui.emptySearchResults);
                            }

                            $beaconResults.html(resultsHtml.join(''));
                            viewBeacon($beaconResults);

                            var dataMaps = response.data.dataMaps;

                            defaultCenterMapBeacon = dataMaps.center;
                            defaultMarkersBeacon = dataMaps.markers;

                            initMap(dataMaps.center, dataMaps.markers, 'beacon-map-canvas');
                        } else {
                            sweetAlert('Oops...', response.message, 'error');
                        }
                    },
                    function () {
                        sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                    },
                    function () {
                        $('#search-beacon').button('reset');
                    }
                );
            });
        };

        var viewBeacon = function ($tbody) {
            $('.icon_row.view', $tbody).click(function () {
                var $this = $(this),
                    $selfRow = $this.closest('tr'),
                    beaconId = $selfRow.data('id'),
                    $table = $tbody.closest('table');

                if (beaconId == '') return false;

                $('.view-row-' + beaconId, $table).remove();

                var $rowView = rowLoading('Loading View Beacon', 'view-row-' + beaconId, $table);

                //Append to table
                $rowView.insertAfter($selfRow);
                var $loader = $('.loader-add', $rowView);

                app.ajax(
                    ['super-admin/get-beacon', '/', beaconId].join(''),
                    {},
                    'GET',
                    'html',
                    false,
                    function(res) {
                        var $content = $(res);

                        $('td', $rowView).append($content);

                        $loader.fadeOut(
                            400,
                            function() {
                                $content.fadeIn();
                            }
                        );
                    },
                    function () {

                    },
                    function () {
                        viewRowEvents();
                    }
                );
            });
        };

        var searchDevice = function () {
            $('#search-device').click(function () {
                $(this).button('loading');

                var searchBy = $('.device-search-by:checked').val();

                if (searchBy == 'device') {
                    var deviceTypeId = $('#device-type-id-select').val(),
                        locationId = $('#location-id-select').val();

                    var params = {deviceTypeId: deviceTypeId, locationId: locationId};
                } else {
                    var deviceSetId = $('#device-set-id-select').val();

                    var params = {deviceSetId: deviceSetId};
                }

                params.searchBy = searchBy;

                app.ajax(
                    app.vars.baseUrl + '/super-admin/search-devices',
                    params,
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var $deviceResults = $('#device-results tbody'),
                                resultsHtml = [];

                            if (response.data.dataTable.length) {
                                $.each(response.data.dataTable, function (i) {
                                    var data = response.data.dataTable[i];

                                    resultsHtml.push('<tr data-id="' + data.id + '">');
                                    resultsHtml.push('<td>' + (i + 1) + '</td>');
                                    resultsHtml.push('<td>' + data.id + '</td>');
                                    resultsHtml.push('<td>' + data.name + '</td>');
                                    resultsHtml.push('<td>' + data.type + '</td>');
                                    resultsHtml.push('<td>' + data.customerid + '</td>');
                                    resultsHtml.push('<td><div class="icon-eye icon_row view"></div></td>');
                                    resultsHtml.push('</tr>');
                                });
                            } else {
                                resultsHtml.push(ui.emptySearchResults);
                            }

                            $deviceResults.html(resultsHtml.join(''));
                            viewDevice($deviceResults);

                            var dataMaps = response.data.dataMaps;

                            defaultCenterMapDevice = dataMaps.center;
                            defaultMarkersDevice = dataMaps.markers;

                            initMap(dataMaps.center, dataMaps.markers, 'device-map-canvas');
                        } else {
                            sweetAlert('Oops...', response.message, 'error');
                        }
                    },
                    function () {
                        sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                    },
                    function () {
                        $('#search-device').button('reset');
                    }
                );
            });
        };

        var viewDevice = function ($tbody) {
            $('.icon_row.view', $tbody).click(function () {
                var $this = $(this),
                    $selfRow = $this.closest('tr'),
                    deviceId = $selfRow.data('id'),
                    $table = $tbody.closest('table');

                if (deviceId == '') return false;

                $('.view-row-' + deviceId, $table).remove();

                var $rowView = rowLoading('Loading View Device', 'view-row-' + deviceId, $table);

                //Append to table
                $rowView.insertAfter($selfRow);
                var $loader = $('.loader-add', $rowView);

                app.ajax(
                    ['super-admin/get-device', '/', deviceId].join(''),
                    {},
                    'GET',
                    'html',
                    false,
                    function(res) {
                        var $content = $(res);

                        $('td', $rowView).append($content);

                        $loader.fadeOut(
                            400,
                            function() {
                                $content.fadeIn();
                            }
                        );
                    },
                    function () {

                    },
                    function () {
                        viewRowEvents();
                    }
                );
            });
        };

        var viewRowEvents = function () {
            $('.buttonCancel').click(function () {
                $(this).closest('tr').remove();

                return false;
            });
        };

        var rowLoading = function (text, cls, $table) {
            var $loader = $(app.f(ui.loading, {text: text}));

            $loader.removeClass('hide');

            var numCols = $('th', $table).length;

            var $row = $('<tr role="row" class="' + cls + '"><td colspan="' + numCols + '"></td></tr>');
            $('td', $row).append($loader);

            return $row;
        };

        var initMap = function (center, markers, mapId) {
            markers = markers || [];

            var mapOpts = {
                    zoom: 3,
                    center: eval(center)
                },
                map = new google.maps.Map(document.getElementById(mapId), mapOpts);

            if (markers.length) {
                $.each (markers, function (i) {
                    setTimeout(function () {
                        new google.maps.Marker({
                            position: eval(markers[i]),
                            map: map,
                            draggable: false,
                            animation: google.maps.Animation.DROP
                        });
                    }, i * 200);
                });
            }
        };
    };

    app.addCls('SuperAdmin', instance);

})($, $.app);