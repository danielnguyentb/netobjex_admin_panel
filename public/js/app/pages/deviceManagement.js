/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#device');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'deviceid'},
                        {mData: 'type'},
                        {mData: 'parentid'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-management/save',
                        FORM: app.vars.baseUrl + '/device-management/form',
                        DELETE: app.vars.baseUrl + '/device-management/delete'
                    },
                    extraData: {
                        _object: 'DeviceManagement'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

    $(function() {
        $('body').on('click','.device-type-changed li.active-result',function(e){
            var deviceTypeId = $('.device-type-id').val();
            $.ajax({
                url: app.vars.baseUrl + '/device-management/attribute-by-devicetype/'+deviceTypeId,
                data: {},
                type: "GET",
                dataType : "html",
                success: function( response ) {
                    $('#attribute').html(response);
                }, error: function( xhr, status, errorThrown ) {

                }
            });
        });
        $('body').on('click', '.show_hide.required', function() {
            var required = $('#required_attributes');
            required.slideToggle();
        })
        $('body').on('click', '.show_hide.optional', function() {
            var option = $('#option_attributes');
            option.slideToggle();

        })
    });


})($, $.app);

function update(){
    var isError = 0;

    $.each( $('#attrOption .form-format'), function() {
        var val = $(this).val();

        if(val === '' || val === null){
            isError++;
            $(this).addClass('error');
        } else {
            $(this).removeClass('error');
        }
    });

    if( isError > 0) return;

    $('#option_attributes').hide(0);
    $( ".buttonUpdate" ).trigger( "click" );
}
