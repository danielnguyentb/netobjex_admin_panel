/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#deviceSetEditor');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/device-set-editor/save',
                        FORM: app.vars.baseUrl + '/device-set-editor/form',
                        DELETE: app.vars.baseUrl + '/device-set-editor/delete'
                    },
                    extraData: {
                        _object: 'DeviceSetEditor'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

    var filter = {
        type: 0,
        name: ''
    }

    $('body').on('click','.btn-search-device', function() {

        filter.type =  $('.filter-by').val();
        filter.name = (filter.type == 0) ? $('.device-name').val(): $('.locational-name').val();
        setAppear(0, '');

        $.ajax({
            url: app.vars.baseUrl + '/device-set-editor/search-device-by-something/'+filter.type+'/'+filter.name,
            data: {},
            type: "GET",
            dataType : "html",
            success: function( response ) {
                setAppear(1, response);
            }, error: function( xhr, status, errorThrown ) {

            }
        });
    })


    $('body').on('click', '#filterBy .active-result', function() {
        var filterByLocational = $('.filter-by-locational');
        var filterByName = $('.filter-by-name');

        if( filterByNameDisplay ){
            filterByName.addClass('hidden');
            filterByLocational.removeClass('hidden');
            filterByNameDisplay = false;
        } else {
            filterByLocational.addClass('hidden');
            filterByName.removeClass('hidden');
            filterByNameDisplay = true;
        }
    });

    function setAppear(type, html) {
        var loading = $('.device-loading');
        var tableBody = $('#deviceBelongDeviceset').find('table tbody');
        if(type == 0){
            tableBody.html(html);
            loading.removeClass('hidden');
            return;
        }
        loading.addClass('hidden');
        tableBody.html(html);
    }


})($, $.app);



