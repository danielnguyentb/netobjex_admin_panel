/**
 * Created by chungdd on 12/16/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {
        var $table,
            dataTable,
            language;

        this.run = function() {

        };

        this.manager = function(lang) {
            language = lang;
            $table = $('#ad-campaign');

            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'type'},
                        {
                            mData     : 'action',
                            searchable: false,
                            orderable : false,
                            className : 'row-action'
                        }
                    ],
                    endpoint : {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST  : app.vars.baseUrl + '/ad-campaign/save',
                        FORM  : app.vars.baseUrl + '/ad-campaign/form',
                        DELETE: app.vars.baseUrl + '/ad-campaign/delete'
                    },
                    extraData: {
                        _object: 'AdCampaigns'
                    },
                    language : lang || {},
                    callbacks: {
                        onRowCallback: function (row, data) {
                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                            '<div class="fa fa-file-text-o icon_row raw icon-btn-black" title="Raw Data"></div>' +
                                '<div class="icon-user-follow icon_row invite icon-btn-black" title="Invite Guests"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                            $('.raw', row).click(viewRawData);
                            $('.invite', row).click(viewInvite);
                        },
                        onAfterAddNewRow: function (row) {
                            runOnEvent(row);
                        },
                        onEditSelectedRow: function (row) {
                            runOnEvent(row);
                        }

                    }
                }
            );
        };

        var viewRawData = function(e)
        {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingRawData, 'raw-data-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.raw-data-content').html());
            $('td', $row).append($content);
            $('input[name="adCampaignId"]', $content).val(data.id);
            $('.buttonClose', $content).click(removeRowHandler);

            $loader.fadeOut(
                400,
                function() {
                    $content.fadeIn();
                    $loader.remove();
                    var today = new Date();
                    $('#start-date', $row).datepicker('setDate', new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7));
                    $('#end-date', $row).datepicker('setDate', new Date(today));
                    $('#start-date', $row).datepicker('update');
                    $('#end-date', $row).datepicker('update');

                    $content.on('click','.buttonCancel, .buttonClose', removeRowHandler);
                }
            );
        }
        var runOnEvent = function ($row) {
            var $runAll = $('input[type="radio"][class="select_all"]', $row),
                $runEach = $('input[type="checkbox"][class="day"]', $row);

            $runAll.click(function () {
                $runEach.prop('checked', false);
            });

            $runEach.click(function () {
                $runAll.prop('checked', false);
            });
        };

        var removeRowHandler = function () {
            var $this = $(this);
            var $row = $this.closest('tr');
            if($row.length) $row.remove();
            return false;
        };

        var viewInvite = function(e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingInviteGuests, 'invite-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.invite-content').html());
            $('td', $row).append($content);

            $('input[name="access_to_object"]', $row).val(data.id);
            $('input[name="name"]', $row).val(data.name);
            $('input[name="object_type"]', $row).val('AdCampaign');

            $('.buttonClose', $content).click(removeRowHandler);

            $loader.fadeOut(
                400,
                function() {
                    $content.fadeIn();
                    $loader.remove();
                }
            );

            var $form = $('form', $row);

            app.parsley($form);
            $form.submit(doInvite);
        };

        var doInvite = function () {
            var $this = $(this);
            var $btn = $('.buttonInvite', $this);

            if (!$this.parsley('validate')) return false;

            var $selfRow = $btn.closest('tr');

            $btn.button('loading');

            //app.ajax(
            //    app.vars.baseUrl + '/device-type-management/' + cmdId + '/parameter/save',
            //    $this.serialize(),
            //    'POST',
            //    'json',
            //    false,
            //    function (response) {
            //        if (response.status) {
            //            swal('Added!', response.message, 'success');
            //
            //            //$selfTable.redraw();
            //        } else {
            //            sweetAlert('Oops...', response.message, 'error');
            //        }
            //    },
            //    function () {
            //        sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
            //    },
            //    function () {
            //        $this.trigger('reset');
            //        $btn.button('reset');
            //
            //        setTimeout(function () {
            //            swal.enableButtons();
            //        }, 100);
            //    }
            //);

            return false;
        };
    };

    app.addCls('adCampaign', instance);

})($, $.app);