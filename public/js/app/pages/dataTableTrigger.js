/**
 * Created by AnhNguyen on 12/10/15.
 */

function _dataTableTrigger( config ) {

   if(!config.table || !config.colDefs || !config._object || !$.isArray(config.colDefs) || config.colDefs.length == 0) {
       return;
   }

    (function($, app) {
        "use strict";

        var instance = new function() {

            var $table,
                dataTable;
            var colDefs = [];
            var url = (config.url.GET)?config.url.GET:'/datatable';

            $.each( config.colDefs, function( key, value ) {
                colDefs.push({mData: value});
            });
            colDefs.push({
                mData: 'action',
                searchable: false,
                orderable : false,
                className: 'row-action'
            });


            $table = $('#'+config.table);
            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : colDefs,
                    endpoint: {
                        GET   : app.vars.baseUrl + url,
                        POST  : app.vars.baseUrl + config.url.POST,
                        FORM  : app.vars.baseUrl + config.url.FORM,
                        DELETE: app.vars.baseUrl + config.url.DELETE
                    },
                    extraData: {
                        _object: config._object
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

        app.addCls('dataTabletrigger', instance);

    })($, $.app);
}


