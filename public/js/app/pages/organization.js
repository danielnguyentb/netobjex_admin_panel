/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#organization-editor');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/organization-editor/save',
                        FORM: app.vars.baseUrl + '/organization-editor/form',
                        DELETE: app.vars.baseUrl + '/organization-editor/delete'
                    },
                    extraData: {
                        _object: 'Customers'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

})($, $.app);
