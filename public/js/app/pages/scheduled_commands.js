/**
 * Created by chungdd on 12/23/15.
 */


(function($, app) {
    "use strict";

    var instance = new function() {
        var $table,
            dataTable,
            language;

        var defaultCmdsOpts = '<select id="select-command" name="command" class="chosen-select input-md form-control" required><option value="">Select Command</option></select>';

        this.run = function() {

        };

        this.manager = function(lang) {
            language = lang;
            $table = $('#scheduled-commands');

            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData     : 'action',
                            searchable: false,
                            orderable : false,
                            className : 'row-action'
                        }
                    ],
                    endpoint : {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST  : app.vars.baseUrl + '/scheduled-commands/save',
                        FORM  : app.vars.baseUrl + '/scheduled-commands/form',
                        DELETE: app.vars.baseUrl + '/scheduled-commands/delete'
                    },
                    extraData: {
                        _object: 'ScheduledCommands'
                    },
                    language : lang || {},
                    callbacks: {
                        onRowCallback: function (row, data) {
                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-control-start icon_row run icon-btn-black" title="Run Command"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                            $('.run', row).click(viewRun);
                        },
                        onAfterAddNewRow: function (row) {
                            bindEvents(row);
                        },
                        onEditSelectedRow: function (row) {
                            bindEvents(row);
                        }

                    }
                }
            );
        };

        var bindEvents = function ($row) {
            $('#end-date-checkbox', $row).click(endDateCheckBox);
            $('#repeat-checkbox', $row).click(repeatCheckBox);

            $('.device-select', $row).change(getCmdList);
        };

        var getCmdList = function () {
            var $this = $(this),
                deviceId = $this.val(),
                $selfRow = $this.closest('tr');

            if (deviceId) {
                app.ajax(
                    app.vars.baseUrl + '/scheduled-commands/get-cmds-by-device',
                    {deviceId: deviceId},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {
                            var $selectCmd = $('select#select-command', $selfRow);

                            if (response.data.length) {
                                var cmdHtml = ['<select id="select-command" name="command" class="chosen-select input-md form-control" required><option value="">Select Command</option>'];

                                $.each(response.data, function (i) {
                                    var cmd = response.data[i];

                                    cmdHtml.push('<option value="' + cmd.id + '">' + cmd.name + '</option>');
                                });

                                cmdHtml.push('</select>');

                                $selectCmd.parent().html(cmdHtml.join(''));
                            } else {
                                $selectCmd.parent().html(defaultCmdsOpts);
                            }

                            $('select#select-command', $selfRow).chosen({disable_search_threshold: 10});

                            onSelectCmd($selfRow);
                        }
                    },
                    function () {
                        console.log('Failed');
                    }
                );
            } else {
                $('select#select-command', $selfRow).parent().html(defaultCmdsOpts);
                $('select#select-command', $selfRow).chosen({disable_search_threshold: 10});
            }

            $('.params-area', $selfRow).addClass('hide').find('.sub-form-group').html('');
        };

        var onSelectCmd = function ($row) {
            $('select#select-command', $row).change(getParamList);
        };

        var getParamList = function () {
            var $this = $(this),
                cmdId = $this.val(),
                $selfRow = $this.closest('tr');

            if (cmdId) {
                app.ajax(
                    app.vars.baseUrl + '/scheduled-commands/get-params-by-command',
                    {cmdId: cmdId},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        console.log(response);
                        var $paramsArea = $('.params-area', $selfRow),
                            paramHtml = [];

                        if (response.status) {
                            if (response.data.length) {
                                $.each(response.data, function (i) {
                                    var param = response.data[i];

                                    paramHtml.push('<label>' + param.name + '</label><br/>');
                                    paramHtml.push('<div>');
                                    paramHtml.push('<input type="text" name="param_' + param.id + '_' + param.defaultvalue + '" class="form-control" data-type="' + param.defaultvalue + '" />');
                                    paramHtml.push('<h5 class="error_message param_' + param.id + '_error"> </h5>');
                                    paramHtml.push('</div>');
                                });

                                $paramsArea.removeClass('hide');
                            } else {
                                $paramsArea.addClass('hide');
                            }

                            $paramsArea.find('.sub-form-group').html(paramHtml.join(''));
                        } else {
                            $paramsArea.addClass('hide').find('.sub-form-group').html(paramHtml.join(''));
                        }
                    },
                    function () {
                        console.log('Failed');
                    }
                );
            } else {
                $('.params-area', $selfRow).addClass('hide').find('.sub-form-group').html('');
            }
        };

        var endDateCheckBox = function () {
            var $endDate = $('input[type="text"][name="end_date"]');

            if ($(this).prop('checked')) $endDate.removeClass('hide');
            else $endDate.addClass('hide');
        };

        var repeatCheckBox = function () {
            var $repeatWrap = $('.time_lapse');

            if ($(this).prop('checked')) $repeatWrap.removeClass('hide');
            else $repeatWrap.addClass('hide');
        };

        var viewRun = function () {
            var $this = $(this);

            var $selfRow = $this.closest('tr'),
                scId = $selfRow.prop('id');

            app.ajax(
                app.vars.baseUrl + '/scheduled-commands/run-command',
                {scId: scId},
                'POST',
                'json',
                true,
                function (response) {
                    console.log(response);
                    if (response.status) {
                        swal('Success!', response.message, 'success');
                    } else {
                        sweetAlert('Oops...', response.message, 'error');
                    }
                },
                function () {
                    sweetAlert('Oops...', 'An error occurred while processing please try again later!', 'error');
                },
                function () {
                    setTimeout(function () {
                        swal.enableButtons();
                    }, 100);
                }
            );

            return false;
        };
    };

    app.addCls('scheduledCommands', instance);

})($, $.app);