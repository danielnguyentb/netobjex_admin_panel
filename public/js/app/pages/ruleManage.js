/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#rule');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'enterprisegatewayid'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/rule-management/save',
                        FORM: app.vars.baseUrl + '/rule-management/form',
                        DELETE: app.vars.baseUrl + '/rule-management/delete'
                    },
                    extraData: {
                        _object: 'RuleManage'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

    $(function() {

        $('body').on('click','.device-type-changed li.active-result',function(e){
            var deviceTypeId = $('.device-type-id').val();

            if(!confirm('If you change device type, all condition will need to be removed, Do you want change ?')){
                $('.device-type-id').val(deviceTypeId);
                return false;
            }

            attr = '';isLoadAttr = true;
            $('.condition-attr').html('');

            callAjax(app.vars.baseUrl + '/rule-management/get-device-editor-by-device/'+deviceTypeId, 'GET', {}, function(response) {
                $('.editor').html(response);
            });

            callAjax(app.vars.baseUrl + '/rule-management/get-attr-by-device/'+deviceTypeId, 'GET', {}, function(response) {
                attr = response;

                if(attr == ''){
                    $('.condition_button').hide();
                } else {
                    $('.condition_button').show();
                }
            });

            callAjax(app.vars.baseUrl + '/rule-management/get-command-by-device/'+deviceTypeId, 'GET', {}, function(response) {
                $('.command-drop').html(response);
            });
        });

        $('body').on('click','.condition_button',function(e){

            if(!isLoadAttr){
                var deviceTypeId = $('.device-type-id').val();
                callAjax(app.vars.baseUrl + '/rule-management/get-attr-by-device/'+deviceTypeId, 'GET', {}, function(response) {
                    isLoadAttr = true;
                    attr = response;
                    $('.condition-attr').append(attr);
                });
                return;
            }

            $('.condition-attr').append(attr);
        });

        $('body').on('click','.attr-delete',function(e){
            $(this).closest('tr').remove();
        });

        $('body').on('click', '.btn-view-add-route, .btn-cancel-route', function() {

            var routeView = $('.add-route-view');
            if( routeView.is(':visible')) {
                routeView.slideUp(500); return;
            }

            routeView.slideDown(500);
            $('#routeType').trigger('change')
            ;
        })

        $('body').on('change', '#routeType', function(e) {
            var key = $('#routeType').val();

            callAjax('/rule-management/get-route-by-type/'+key, 'GET', {}, function(response){
                $('#routeId').html(response);
                $('#routeId').trigger('change');
            })
        });

        $('body').on('change', '#routeId', function(e) {
            var id = $('#routeId').val();
            var type = $('#routeType').val();
            var deviceTypeId = $('.device-type-id').val();

            callAjax('rule-management/get-variable-map/', 'GET', {type: type, id: id, deviceTypeId: deviceTypeId}, function(response){
                $('.map-value').html(response); return;
            });
            return;
        });
    });

    $('body').on('click','.btn-save-route', function(){

        var $this = $(this);
        var $form = $this.closest('form');

        var formRequest = function() {
            swal.disableButtons();

            $.ajax({
                url        : '/rule-management/save-variable-map',
                type       : 'POST',
                data       : new FormData($form[0]),
                dataType   : 'json',
                cache      : false,
                contentType: false,
                processData: false,
                success    : function (response) {

                    if (response.status) {
                        swal('Added!', response.message, "success");
                    } else {
                        sweetAlert("Oops...", response.message, "error");
                    }

                    $('.btn-cancel-route').trigger('click');
                },
                error      : function () {
                    sweetAlert("Oops...", "An error occurred while processing please try again later!", "error");
                },
                complete   : function () {

                    setTimeout(function () {
                        swal.enableButtons();
                    }, 100);
                }
            });
        };

        swal({
            title: "Are you sure you want to add this entry?",
            text: "You can change the contents in the table if you want.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "green",
            confirmButtonText: "Yes, add it!",
            closeOnConfirm: false,
            showLoaderOnConfirm : true
        }, formRequest);


    })

    function callAjax(url,method, data, callback) {
         $.ajax({
             url: url,
             data: data,
             type: method,
             dataType : "html",
             success: function( response ) {
                 callback(response);
             }, error: function( xhr, status, errorThrown ) {

             }
         });
     }

})($, $.app);










