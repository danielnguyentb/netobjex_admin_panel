/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#beaconset');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'uuids'},
                        {mData: 'majorids'},
                        {mData: 'minorids'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/beacon-set-editor/save',
                        FORM: app.vars.baseUrl + '/beacon-set-editor/form',
                        DELETE: app.vars.baseUrl + '/beacon-set-editor/delete'
                    },
                    extraData: {
                        _object: 'BeaconSetEditor'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('beaconSetEditor', instance);
    $('body').on('btnAddUpdate.click', function(e) {
        beaconSet.valid();
    });

})($, $.app);

var beaconSet = {

    trigger: function(){
        beaconSet.uuidOnSelect();
        beaconSet.uuidsClick();
        beaconSet.search();
    },
    search: function(){
        $('.btn-search').click(function() {
            $('.loading-beacon-set').show();
            $.ajax({
                url: '/beacon-set-editor/get-by-beacon-set',
                data: $(".edit-form").serialize(),
                type: "POST",
                dataType : "html",
                success: function( response ) {
                    $('.loading-beacon-set').hide();
                   $('#beaconSearch tbody').html(response);
                }, error: function( xhr, status, errorThrown ) {

                }
            });
        });
    },
    uuidOnSelect: function() {

        $('.uuids-list').click(function(event){
            event.stopPropagation();
        });

        $('.uuids-list .uuid-item').click(function(event){
            if($(this).is(':checked')) {
                _selected ++;
            } else {
                _selected --;
            }

            if(_selected == _uuidLength) {
                $('.uuid-item-all').prop('checked', true);
            } else {
                $('.uuid-item-all').prop('checked', false);
            }
            beaconSet.setValueToUuid();
        });

        $('.uuids-list .uuid-item-all').click(function(event){
            if($(this).is(':checked')) {
                beaconSet.selectedProp(true);
                _selected = _uuidLength;
            } else {
                beaconSet.selectedProp(false);
                _selected = 0;
            }
            beaconSet.setValueToUuid();
        });

    },
    valid: function() {
        if(_selected == 0){
            $('.uuids-hidden').val('');
            $('.uuids').addClass('input-error');
        } else {
            $('.uuids-hidden').val('hasvalue');
            $('.uuids').removeClass('input-error');
        }
    },
    selectedProp: function(stt) {
        $('.uuid-item').each(function() {
            $(this).prop('checked', stt);
        });
    },
    setValueToUuid: function() {
        var valueView = 'All';

        if(_selected == 0){
            valueView = '';
        } else if(_selected == 1){
            $('.uuid-item').each(function() {
                if($(this).is(':checked')){
                    valueView = $(this).val();
                    return;
                }
            });

        } else if(_selected ==  _uuidLength) {
            var valueView = 'All';
            $('.uuid-item-all').prop('checked', true);
        } else {
            var valueView = 'Multiple';
        }
        $('.uuids').val(valueView);
    },
    uuidsClick: function() {

        $('.uuids').click(function(event) {
            event.stopPropagation();
            var uuid = $('.uuids-list');
            if(uuid.hasClass('uuid-hide') ){
                uuid.removeClass('uuid-hide'); return;
            }
            uuid.addClass('uuid-hide');
        });
        $(document).click(function(){
            $('.uuids-list').addClass('uuid-hide');
        });

    }
}



