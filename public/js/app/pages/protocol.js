/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#protocol-management');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/protocol-management/save',
                        FORM: app.vars.baseUrl + '/protocol-management/form',
                        DELETE: app.vars.baseUrl + '/protocol-management/delete'
                    },
                    extraData: {
                        _object: 'Protocols'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('protocolManage', instance);

})($, $.app);
