/**
 * Created by AnhNguyen on 12/10/15.
 */
var _responseType = 1;
(function($, app) {
    "use strict";

    var $table;

    $(function() {
        $('body').on('click', '.notification_button', function() {
            //addRowHandler($(this).attr('data-type'));
        })
    });

    var addRowHandler = function(responseType) {

        $table = $('#beaconNotifications');
        //Remove current form add
        $('.form-add-table', $table).remove();

        var $row = rowLoading('asdasd', 'form-add-table');

        //Append to table
        $row.insertBefore($('tbody tr:first', $table));
        var $loader = $('.loader-add', $row);

        app.ajax(
            '/beacon-notifications/form-type/'+responseType,
            {},
            'GET',
            'html',
            false,
            function(res) {

                var $content = $(res);
                $('td', $row).append($content);
                $loader.fadeOut(
                    400,
                    function() {
                        $content.fadeIn();
                    }
                );
            }
        );
    };

    var rowLoading = function(text, cls) {

        var $loader = $(app.f(ui.loading, {
            text: text
        }));

        $loader = $($loader.outerHTML());
        $loader.removeClass('hide');

        var numCols = $('th', $table).length;

        var $row = $("<tr role='row' class='" + cls + "'><td colspan=" + numCols + "></td></tr>");
        $('td', $row).append($loader);

        return $row;
    };


    var ui = {
        loading: '<div class="panel-body loader-add hide loader-demo">' +
        '<h4>{text}</h4>' +
        '<div class="ball-pulse">' +
        '<div></div>' +
        '<div></div>' +
        '<div></div>' +
        '</div>' +
        '</div>'
    };



    var instance = new function() {

        var $table,
            dataTable;


        this.run = function() {
            $table = $('#beaconNotifications');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.notification_button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'description'},
                        {mData: 'responsetype'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/beacon-notifications/save',
                        FORM: app.vars.baseUrl + '/beacon-notifications/form',
                        DELETE: app.vars.baseUrl + '/beacon-notifications/delete'
                    },
                    extraData: {
                        _object: 'ActionResponse'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    },
                    initComplete: {
                        initComplete: function() {
                            alert(31);
                        }
                    }
                }
            );
        };

    };

    app.addCls('beaconNotifications', instance);


})($, $.app);



