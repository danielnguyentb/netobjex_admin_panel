
(function($, app) {
    "use strict";
    
    var instance = new function() {
        this.run = function(){
           var wrap = $('.dashboard'),
               searchButton = $('.search', wrap);
               
           $('.campaigntype', wrap).chosen({disable_search_threshold: 4});
           $('.campaignSelect', wrap).chosen({disable_search_threshold: 4});
           $('#start-date', wrap).datepicker('setDate', new Date($('#start-date', wrap).val()));
           $('#end-date', wrap).datepicker('setDate', new Date($('#end-date', wrap).val()));
           $('#start-date', wrap).datepicker('update');
           $('#end-date', wrap).datepicker('update');

           searchButton.on('click', function(e){
                getFilter(e);
           });

           var campaignTypeChange = function(){
                $('.campaigntype').change(function(e){
                    var $this = $(this),
                        type = $this.val();

                    getCampaignByType(type);
                })
           };

            var getCampaignByType = function(type)
            {
                var type = type || $('.campaigntype', wrap).val();
                    
                app.ajax(
                    app.vars.baseUrl + '/get-campaign-by-type',
                    {type: type, _token: app.vars.csrf},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {

                            var campaign = $('select.campaignSelect');
                            if(response.data.length)
                            {
                                var campaignsHtml = ['<select multiple="multiple" class="chosen-select form-control campaignSelect">'];

                                $.each(response.data, function (i) {
                                    var campaign = response.data[i];

                                    campaignsHtml.push('<option value="' + campaign.id + '">' + campaign.name + '</option>');
                                });

                                campaignsHtml.push('</select>');

                                campaign.parent().html(campaignsHtml.join(''));
                                $('.campaignSelect', wrap).chosen({disable_search_threshold: 4});
                            }
                        }
                    }
                    );
            };

            var getFilter = function(e)
            {
                var campaign = jQuery('select[name="campaigns[]"]', wrap).val(),
                toDate = jQuery("#start-date",wrap).val(),
                fromDate = jQuery("#end-date",wrap).val();

                var fDate = new Date(fromDate).toISOString(),
                tDate = new Date(toDate).toISOString();

                var url = jQuery('#MyIFrame', wrap).attr('src'),
                matchFdate = url.match("from:'(.*)',");

                if(matchFdate && matchFdate.length>1)
                {
                    url = url.replace(matchFdate[1], fDate);
                }
                var matchTdate = url.match(",to:'(.*)'(.*)&");

                if(matchTdate && matchTdate.length>1)
                {
                    url = url.replace(matchTdate[1], tDate);
                }

                var matchDevice = url.match("query_string:(.*)analyze_wildcard:!t,query:'(.*)'"),
                // listDevice  = [];

                if(matchDevice && matchDevice.length>1)
                {
                    url = url.replace(matchDevice[matchDevice.length-1], listCampaign.length>0?listCampaign.join(' or '):'*');
                }

                jQuery('iframe#MyIFrame', wrap).attr('src',url);
            };

            getCampaignByType('');
            campaignTypeChange();
        }
    }


    app.addCls('dashboard', instance);

})($, $.app);
