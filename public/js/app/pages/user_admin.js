/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#user-admin');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {
                            mData: 'id',
                            searchable: false,
                            orderable : false,
                        },
                        {mData: 'email'},
                        {
                            mData: 'customerid',
                            searchable: false,
                            orderable: false
                        },
                        {
                            mData: 'created',
                            searchable: false,
                            orderable: false
                        },
                        {
                            mData: 'role',
                            searchable: false,
                            orderable: false
                        },
                        {
                            mData: 'disabled',
                            searchable: false,
                            orderable: false,
                            className:'row-status'
                        },
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/user-admin/save',
                        FORM: app.vars.baseUrl + '/user-admin/form',
                        DELETE: app.vars.baseUrl + '/user-admin/delete',
                    },
                    extraData: {
                        _object: 'Users'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';
                            $('.row-action', row).html(actions);

                            var status = '<select class="change_status"><option value="0">Enabled</option><option '+(data.disabled?"selected='selected'":'')+' value="1">Disabled</option></select>';
                            $('.row-status', row).html(status);
                            $('.change_status', row).change(changeStatus);
                        }
                    }
                }
            );
        };

        var changeStatus =function(e){
            var $this    = $(this),
                $selfRow = $(this).closest('tr'),
                $status   = $this.val(),
                $uid = $selfRow.attr('id');

            swal({
                title: "Do you wish change the status?",
                text: "You can change the contents in the table if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: "Yes, change it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm)
                {
                    $.app.ajax(
                        app.vars.baseUrl + '/user-admin/change-status', 
                        {id:$uid, status: $status},
                        'POST' ,
                        'JSON',
                        '',
                        function(response)
                        {
                            if(response.status)
                            {
                                swal('Changed!','Status has been changed', "success");
                            }
                            else
                            {
                                sweetAlert("Oops...", response.message, "error");
                                $table.api().draw(false);
                            }
                        });
                }
                else
                {
                    $table.api().draw(false);
                }
            });

        }

    };

    app.addCls('userManage', instance);

})($, $.app);
