/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#beacon');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'uuidvalue'},
                        {mData: 'majorid'},
                        {mData: 'minorid'},
                        {mData: 'activeflag'},
                        {mData: 'lost'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/beacon-management/save',
                        FORM: app.vars.baseUrl + '/beacon-management/form',
                        DELETE: app.vars.baseUrl + '/beacon-management/delete'
                    },
                    extraData: {
                        _object: 'BeaconManagement'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

})($, $.app);
