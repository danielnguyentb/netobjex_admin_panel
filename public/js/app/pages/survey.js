/**
 * Created by chungdd on 12/14/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {
        var $table,
            dataTable,
            language,
            $tableQuestion = {},
            _URL = window.URL || window.webkitURL;
        this.run = function() {
            
        };

        this.manager = function(lang) {
            language = lang;
            $table = $('#survey-management');

            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {
                            mData:'numofquestions',
                            orderable: false
                        },
                        {
                            mData:'startdate',
                            orderable: false
                        },
                        {
                            mData: 'enddate',
                            orderable: false
                        },
                        {
                            mData: 'published',
                            orderable: false
                        },
                        {
                            mData     : 'action',
                            searchable: false,
                            orderable : false,
                            className : 'row-action'
                        }
                    ],
                    endpoint : {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST  : app.vars.baseUrl + '/surveys/save',
                        FORM  : app.vars.baseUrl + '/surveys/form',
                        DELETE: app.vars.baseUrl + '/surveys/delete'
                    },
                    extraData: {
                        _object: 'Surveys'
                    },
                    language : lang || {},
                    callbacks: {
                        onRowCallback: function (row, data) {
                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-docs icon_row questions icon-btn-black" title="Questions"></div>' +
                                '<div class="icon-bar-chart icon_row results icon-btn-black" title="Results"></div>' +
                                '<div class="fa fa-file-text-o icon_row raw icon-btn-black" title="Raw Data"></div>' +
                                '<div class="icon-user-follow icon_row invite icon-btn-black" title="Invite Guest"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                            $('.invite', row).click(viewInvite);
                            $('.questions', row).click(viewQuestion);
                            $('.raw', row).click(viewRawData);
                            $('.results', row).click(viewResult);
                        },
                        onInitGetRow: function(row)
                        {
                            $('#start-date', row).datepicker('setDate', new Date($('#start-date').val()));
                            $('#end-date', row).datepicker('setDate', new Date($('#end-date').val()));
                            $('#start-date', row).datepicker('update');
                            $('#end-date', row).datepicker('update');
                            $('#allDay',row).change(function(){
                                if ($(this).is(':checked')) {
                                    $('#eachDay').find('input').each(function(){
                                        $(this).prop('checked',false);
                                    })
                                }
                            });

                            $('#eachDay', row).change(function(){     
                              var isRunAll = true; 
                              $(this).find('input').each(function(){
                                if(this.checked) isRunAll = false;     
                            })
                              if(!isRunAll) $('#allDay', row).prop('checked',false);
                              else{
                                  if($('#start-date', row).val()!='' && $('#end-date', row).val() !='')
                                    $('#allDay').prop('checked',true);
                                }
                            })
                        }
                    }
                }
            );
        };
        
        var viewRawData = function(e)
        {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingRawData, 'raw-data-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.raw-data-content').html());
            $('td', $row).append($content);
            $('input[name="surveyId"]', $content).val(data.id);
            $('.buttonClose', $content).click(removeRowHandler);

            $loader.fadeOut(
                400,
                function() {
                    $content.fadeIn();
                    $loader.remove();
                    var today = new Date();
                    $('#start-date', $row).datepicker('setDate', new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7));
                    $('#end-date', $row).datepicker('setDate', new Date(today));
                    $('#start-date', $row).datepicker('update');
                    $('#end-date', $row).datepicker('update');

                    $content.on('click','.buttonCancel, .buttonClose', removeRowHandler);
                }
            );
        }   

        var viewResult = function(e)
        {
           var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;
            $('.result-' + data.id, $table).remove();
            var $row = dataTable.rowLoading(language.loadingResult, 'result-' + data.id);
            $row.insertAfter($selfRow);
            
            app.ajax(
                [app.vars.baseUrl + '/surveys/result', '/', $selfRow.prop('id')].join(''),
                {},
                'GET',
                'html',
                false,
                function(res) {

                    var $content = $(res);
                    $('td', $row).append($content);

                    var $loader = $('.loader-demo', $row);
                    $loader.fadeOut(
                        400,
                        function() {
                            $content.fadeIn();
                        }
                    );
                    $content.on('click','.buttonCancel, .buttonClose', function(e){
                        $row.remove();
                        return false;
                    });
                    $('#startDate', $row).datepicker('setDate', new Date($('#startDate').val()));
                    $('#endDate', $row).datepicker('setDate', new Date($('#endDate').val()));
                    $('#startDate', $row).datepicker('update');
                    $('#endDate', $row).datepicker('update');

                    getSurveyAnalytic($selfRow.prop('id'), $('#startDate', $row).val(), $('#endDate', $row).val(), $('select[name="locationId"]', $row).val(), $row);

                    var values = $('.data-value');
                    $('#data-point').on('change',function(){
                      var point = this.value;
                      for(var i = 0;i<values.length;i++)
                      {
                        if(values[i].dataset.value == point)
                        {
                          values[i].style['display']='block';
                      }
                      else values[i].style['display']='none';
                      }
                    });

                    $row.on('click', '.buttonSearch',function(){
                        var startDate = $('#startDate', $row).val();
                        var endDate = $('#endDate', $row).val();
                        var locationId = $('select[name="locationId"]', $row).val();
                        getSurveyAnalytic($selfRow.prop('id'), startDate, endDate, locationId, $row);
                    });
                }
            );
          
        }

        var getSurveyAnalytic = function(surveyId, startDate, endDate, locationId, wrap)
        {
            app.ajax(
                app.vars.baseUrl + '/surveys/analytic',
                {'surveyId':surveyId, 'startDate':startDate, 'endDate':endDate, 'locationId':locationId},
                'GET',
                'json',
                false,
                function(res) {

                    if(typeof res.ansList!='undefined' && res.ansList)
                    {
                        var ansList = res.ansList;
                        var option = '';
                        if(ansList.toString()){
                            for(var i in ansList)
                            {
                                option +='<option value=\"'+i+'\">'+i+'</option>';
                            }
                        }
                        jQuery('#listOfQuest', wrap).html(option);
                        _renderQuestion(ansList, 0, wrap);
                        jQuery('#listOfQuest', wrap).change(function(){
                            _renderQuestion(ansList,jQuery(this).val(), wrap);
                        })
                    }
                    if(typeof res.rawData !='undefined' && res.rawData)
                    {
                        jQuery('#raw_data tbody', wrap).html(res.rawData);
                    }
                    if(typeof res.numOfRespondents !='undefined' && res.numOfRespondents)
                    {
                        jQuery('#numOfRespondent', wrap).val(res.numOfRespondents);
                    }
                    if(typeof res.numOfDropouts !='undefined' && res.numOfDropouts)
                    {
                        jQuery('#numOfDropout', wrap).val(res.numOfDropouts);
                    }
                }
            )

        }

        var _renderQuestion = function(data, ord, wrap)
        {
            if(data.toString())
            {
                for(var i in data)
                {
                    if(i == ord || typeof ord== "undefined")
                    {
                        var html ="";
                        for(var j=0;j<data[i].length;j++)
                        {
                            html    += "<tr>";
                            html    += "<td>"+data[i][j].answer+"</td>";
                            html    += "<td>"+data[i][j].count+"</td>";
                            html    += "</tr>";
                        }
                        jQuery('#ansRespo', wrap).html(html);
                        break;
                    }
                }
            }
            else  jQuery('#ansRespo', wrap).html('');
        }

        var viewQuestion = function(e)
        {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            $('.question-' + data.id, $table).remove();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingQuestion, 'question-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.question-wizard-content').html());
            $('td', $row).append($content);
            $('.survey-name', $row).html(data.name.replace(/[(].*[)]/,''));
            $('.buttonClose', $content).click(removeRowHandler);
            $tableQuestion = $('.survey-questions', $row);

            (new (app.getCls('dataTable'))()).render(
                $tableQuestion,
                $('.add-button-question', $row),
                {
                    colDefs: [
                        {
                            mData: 'question'
                        },
                        {
                            mData: 'numorder',
                            orderable: false,
                        },
                        {
                            mData: 'action',
                            // searchable: false,
                            orderable: false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET: app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/surveys/' + data.id + '/question/save',
                        FORM: app.vars.baseUrl + '/surveys/'+data.id+'/question/form',
                        DELETE: app.vars.baseUrl + '/surveys/'+data.id+'/question/delete'
                    },
                    extraData: {
                        _object: 'Questions',
                        _where: {
                            surveyid: data.id
                        }
                    },
                    classes: {
                        handlerRow: {
                            edit: '.edit-question',
                            delete: '.delete-question'
                        }
                    },
                    language : {
                        addLoading: language.questionAddLoading,
                        editLoading: language.questionEditLoading
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {
                            var actions = '<div class="icon-pencil icon_row edit-question icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete-question icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                        },
                        drawCallback: function() {
                            if(!$content.is(':visible')) {
                                $loader.fadeOut(
                                    400,
                                    function() {
                                        $content.fadeIn();
                                        $loader.remove();

                                    }
                                );
                            }
                            
                            $(".numberOrder",$content).on("keyup", function() {
                                var questionId = $(this).attr('questionId');
                                var curValue = $(this).attr('curValue');
                                
                                if(curValue != this.value)
                                    change_number_Order(this.value,curValue, questionId, data.id);
                            });
                        },
                        onInitGetRow: function(wrap)
                        {
                            var $tbody      = $('table tbody', wrap),
                                $answertype = $('select[name="answertype"]', wrap);
                            
                            $('#js-area', wrap).hide();
                            $('#answer-area', wrap).hide();

                            defineAnswerTypeEvent($answertype.val(), wrap);
                            

                            $answertype.on('change', function(){
                                defineAnswerTypeEvent($(this).val(), wrap);
                            })

                            wrap.on('click', '.delete', function(){
                                $(this).closest('tr').remove();
                            })

                            wrap.on('change', '#routing', function(){
                                defineRoutingEvent($(this).val(), wrap);                                
                            })

                            wrap.on('click', '.remove-question-image', function(){

                            })

                            defineRoutingEvent($('#routing', wrap).val(), wrap);
                            
                            wrap.on('click', 'table#answer .delete-answer', removeRowHandler);

                            wrap.on('click', '.add_answer', function(e){

                                var $numOfAns = $('table#answer tbody tr', wrap).length,
                                    $surveyId = $('input[name="surveyId"]', wrap).val(),
                                    $questionId = $('input[name="id"]', wrap).val(),
                                    $routingValue = $('select#routing', wrap).val(),
                                    $noRouting = $('input[name="noRouting"]', wrap).val();
                                var $html = '';
                                app.ajax(
                                    app.vars.baseUrl + '/surveys/question/get-routing',
                                    {'surveyId':$surveyId, 'questionId':$questionId, 'valueTypeNum':$numOfAns},
                                    'GET',
                                    'json',
                                    false,
                                    function(res) {

                                        $html += '<tr><td>';
                                        $html += '<input type="text" value="" name="answer[name]['+$numOfAns+']" required="required" style="width:100% !important" class="form-control form-format">';
                                        
                                        if(!$noRouting && res)
                                        {
                                            $html += '<div style="float: left; width: 200px; padding: 5px;'+($routingValue!=2?"display:none":"")+'" class="routingNextQuestion">';
                                            $html += '<label style="float:left;width:30%;">Routing </label>';
                                            $html += '<span style="width:80px;float:left;">';
                                            if(typeof res.data !='undefined')
                                            {
                                                $html += res.data;
                                            }

                                            $html += '</span></div>';
                                        }
                                        $html  += '</td>';
                                        $html  += '<td class=" row-action"><div title="Delete" class="icon-trash icon_row delete-answer icon-btn-red"></div></td>';
                                        $html  += '</tr>';
                                        
                                        $('table#answer tbody', wrap).append($html);

                                        wrap.on('click', 'table#answer .delete-answer', removeRowHandler);
                                    }
                                )
                            });

                            

                            initQuestionImage(wrap);
                            
                            initActionQuestionImage(wrap);

                            var surveyType = $('input[name="surveytype"]', wrap);

                            if(surveyType)
                            {
                                var numofpanels = $('select[name="numofpanels"]', wrap);
                                var indexPanels = numofpanels.val();
                                
                                if(numofpanels)
                                {
                                    ChangeAnswerPanel(numofpanels.val(),wrap);
                                    numofpanels.change(function(e){
                                        var val = $(this).val();
                                        swal({
                                            title: language.changenumpanel,
                                            text: language.changenumpanelwarn,
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonColor: "green",
                                            confirmButtonText: language.changeorderconfirm,
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if(isConfirm)
                                            {
                                                indexPanels = val;
                                                ChangeAnswerPanel(val, wrap);
                                                swal("Changed!", language.changenumpanelsucc, "success");
                                            }
                                            else
                                            {
                                                numofpanels.val(indexPanels).trigger('chosen:updated');
                                                return false;
                                            }
                                        });
                                    });
                                }
                            }
                            
                            $('.answer-image-panel', wrap).change(function(e){
                                validFileUpload(this, wrap, true);
                            });
                        }
                    }
                }
            );
        }

        var initQuestionImage = function(wrap){
            
            $('.question-image-panel', wrap).on('change',function(e)
            {
                validFileUpload(this, wrap, false);
            });
        }

        var initActionQuestionImage = function(wrap){
          $('.remove-question-image', wrap).on('click', function(e){
              if(confirm('Do you want remove question image?'))
              {
                var parentDiv = jQuery(this, wrap).closest('div');
                jQuery('img', parentDiv).attr('src', '/img/no_img.png');
                
                jQuery(this).remove();
                jQuery(this).next().val('');
                    
                $('input[name="questionimageid"]').filestyle('clear');

                initQuestionImage(wrap);
            }
        });
      }

        var ChangeAnswerPanel = function(numOfPanel, wrap)
        {
            var $numOfAnswer = jQuery('.answer-items', wrap).length;
            
            if(parseInt($numOfAnswer) < parseInt(numOfPanel))    
            {

                for(var $i = $numOfAnswer; $i < numOfPanel; $i++)
                {
                    AddRow($i, wrap);
                }
                
            }
            else
            {
                var $listOfAnswer = jQuery('.answer-items', wrap);
                
                for(var $i = $numOfAnswer-1; $i >= numOfPanel; $i--)
                {
                    jQuery($listOfAnswer[$i]).remove();
                }

            }
            if(numOfPanel == 2 || numOfPanel == 4)
            {
                $('select[name="answerimagesize"]', wrap).val(1).trigger('chosen:updated');
            }
            else if(numOfPanel == 3 || numOfPanel == 5 || numOfPanel == 6)
            {
                $('select[name="answerimagesize"]', wrap).val(2).trigger('chosen:updated');
            }

            $('.answer-image-panel', wrap).change(function(e){
                validFileUpload(this, wrap, true);
            });
        }

        var AddRow = function(order, wrap)
        {
            var $listRouting = $('input[name="list-order-question"]', wrap),
                $noRouting   = $('input[name="noRouting"]', wrap).val();
            var $html  = '<tr class="answer-items">';
                $html += '<td>';
                $html += '<input type="text" name="answer[name]['+order+']" class="form-control form-format" style="width:100% !important" required>';
            
            if(!$noRouting )                                                
            {
                $html += '<div style="float: left; width: 200px; padding: 5px;" class="routingNextQuestion">';
                $html += '<label style="float:left;width:30%;">'+language.routing+'</label>';
                $html += '<span style="width:80px;float:left;">';
                $html += '<select name="answer[order]['+order+']" class="chosen-select input-md form-control">';
                if($listRouting && $listRouting.val())
                {
                    var $lr = JSON.parse($listRouting.val());

                    for(var i in $lr)
                    {
                        $html += '<option value="'+i+'">'+($lr[i])+'</option>';
                    }
                }   
                $html += '</select></span></div>';
            }
            $html += '<div style="float: left; width: 100%; padding: 5px;" class="answer-image-area">';
            $html += '<div class="col-md-2"><label style="float:left;width:30%;">'+language.image+'</label></div>';
            $html += '<div class="col-md-5">';
            $html += '<input type="file" name="answer[image]['+order+']" class="form-control filestyle answer-image-panel" data-classbutton="btn btn-default" data-classinput="form-control inline">';
            $html += '</div><div class="col-md-5"><div class="col-md-2"><label style="float:left;width:30%;">'+language.preview+'</label></div>';
            $html += '<img src="'+APP_URL+'/img/no_img.png"" style="height:80px;float:right">';
            $html += '</div></div></tr></tr>';

            $('table.survey-question-answer tbody', wrap).append($html);
            $('.answer-image-panel', wrap).filestyle();
        }

        var validFileUpload = function(e, wrap, isUploadAnsImg){
            var file, img,
                maxWidth = '',
                maxHeihgt = '',
                curHtml = e.outerHTML;

                if(isUploadAnsImg)
                {
                    var questionImageSize = $('select[name="answerimagesize"] option:selected', wrap).text(),
                    dismension = questionImageSize.replace(' X ',' ');
                }
                else
                {
                   var questionImageSize = $('.questionimagesize', wrap).val(),
                       dismension = questionImageSize.replace('x',' ');
                }
                
                if(dismension)
                {
                    dismension = dismension.split(' ');
                    maxWidth = dismension[0];
                    maxHeihgt = dismension[1];
                }
                if ((file = e.files[0])) {
                    img = new Image();
                    img.onload = function() {
                      if(this.width != maxWidth || this.height != maxHeihgt)
                      {
                        sweetAlert("Oops...", 'not a valid image size: '+maxWidth+'x'+maxHeihgt, "error");

                        $(e).filestyle('clear');

                        return false;
                      }
                      else
                      {
                            readURL(e, isUploadAnsImg, wrap);
                      }
                    };
                    img.onerror = function() {
                      alert( "not a valid file: " + file.type);
                    };
                    img.src = _URL.createObjectURL(file);
                }
        }

        var readURL = function(input, isUploadAnsImg, wrap) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    var $parentDiv =  $(input).closest('div');
                    $('img', $parentDiv).attr('src', e.target.result);
                    $('.remove-question-image').remove();
                    $parentDiv.append('<span class="'+(!isUploadAnsImg?"remove-question-image":"remove-answer-image")+'" title="Remove Image">x</span>');
                    initActionQuestionImage(wrap);
                }

                reader.readAsDataURL(input.files[0]);
            }
          }

        var change_number_Order = function(number,curValue, questionId, surveyId) {
            swal({
                title: language.changetheorder,
                text: language.changeordertext,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: language.changeorderconfirm,
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm)
                {
                    $.app.ajax(
                        app.vars.baseUrl + '/surveys/change-number-order-question', 
                        {number: number, questionId: questionId, surveyId: surveyId},
                        'POST' ,
                        'JSON',
                        '',
                        function(response)
                        {
                            if(response.status)
                            {
                                swal('Changed!',language.changeordersuccess, "success");
                            }
                            else
                            {
                                sweetAlert("Oops...", response.message, "error");
                            }
                            $tableQuestion.api().draw(false);
                        });
                }
                else
                {
                    $tableQuestion.api().draw(false);
                }
            });
        }

        var viewInvite = function(e) {
            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var data = $table.api().row($selfRow[0]).data();
            if (!data) return false;

            var $row = dataTable.rowLoading(language.loadingInviteGuests, 'invite-' + data.id);
            $row.insertAfter($selfRow);

            var $loader = $('.loader-add', $row);
            var $content = $($('.invite-content').html());
            $('td', $row).append($content);

            $('input[name="access_to_object"]', $row).val(data.id);
            $('input[name="name"]', $row).val(data.name.replace(/[(].*[)]/,''));
            $('input[name="object_type"]', $row).val('Survey');

            var $form = $('form', $row);

            app.parsley($form);
            $form.submit(doInvite);

            $('.buttonClose', $content).click(removeRowHandler);

            $loader.fadeOut(
                400,
                function() {
                    $content.fadeIn();
                    $loader.remove(doInvite);
                }
            );
        };

        var doInvite = function () {
            var $this = $(this);
            var $btn = $('.add-button', $this);

            if (!$this.parsley('validate')) return false;
        };

        var removeRowHandler = function() {
            var $this = $(this);
            var $row = $this.closest('tr');
            if($row.length) $row.remove();
            return false;
        };

        var defineAnswerTypeEvent = function(val, wrap)
        {
            switch(val)
            {
                case '4':
                $('#answer-area', wrap).hide();
                $('#js-area', wrap).show();
                break;
                case '1':
                $('#answer-area', wrap).hide();
                $('#js-area', wrap).hide();
                break;
                default:
                $('#answer-area', wrap).show();
                $('#js-area', wrap).hide();
                break;
            }
        }

        var defineRoutingEvent = function(val, wrap)
        {
            var $table = $('table#answer', wrap),
                $conditionRouting = $('.routingNextQuestion', $table);
            
            if(val != 2)
            {
                if($conditionRouting) $conditionRouting.hide();
            }
            else
            {
                if($conditionRouting) $conditionRouting.show();
            }
        }

    };

    app.addCls('surveyManagement', instance);

})($, $.app);