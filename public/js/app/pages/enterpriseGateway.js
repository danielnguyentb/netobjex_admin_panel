/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable;

        this.run = function() {
            $table = $('#enterprise_gateway');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'etype'},
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/enterprise-gateway/save',
                        FORM: app.vars.baseUrl + '/enterprise-gateway/form',
                        DELETE: app.vars.baseUrl + '/enterprise-gateway/delete'
                    },
                    extraData: {
                        _object: 'Enterprise'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

})($, $.app);

$(function() {
    $('body').on('click', '.button_Add', function() {
        var isError = 0;

        $('.edit-form input[required=required]').each(function() {

            if($(this).val() == '') {
                $(this).addClass('error input-invalid input-error');
                isError++;
            } else {
                $(this).removeClass('error input-invalid input-error').addClass('valid input-valid');
            }

        });

        $('.edit-form input[type=url]').each(function() {
            var url = $(this).val().split('://');

            if( !url[1]) {
                isError++; return;
            }

            if(url[1].length == 1) {
                 isError++; return;
            }

            url[1] = url[1].replace('.','');
            var firstChart = url[1].charAt(0);
            var newUrl = '';

            for(var i=0; i< url[1].length ; i++){
                newUrl+=firstChart;
            }

            if(url[1] == newUrl) {
                $(this).addClass('error input-invalid input-error');
                isError++; return;
            } else {
                $(this).removeClass('error input-invalid input-error').addClass('valid input-valid');
                return;
            }

        });

        if(isError == 0) {
            $('.buttonAdd').trigger('click');
        }

    });



});
