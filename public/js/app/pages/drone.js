/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#projects');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        { mData: 'name' },
                        { mData: 'photos' },
                        { mData: 'camera' },
                        { mData: 'drone' },
                        { mData: 'mission' },
                        { mData: 'start_date' },
                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/location/save',
                        FORM: app.vars.baseUrl + '/location/form',
                        DELETE: app.vars.baseUrl + '/location/delete'
                    },
                    extraData: {
                        _object: 'Location'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('droneProject', instance);


})($, $.app);





