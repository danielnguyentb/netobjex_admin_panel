/**
 * Created by chungdd on 01/08/16.
 */


(function($, app) {
    "use strict";

    var instance = new function() {
        var $table,
            dataTable,
            language;

        this.run = function() {

        };

        this.manager = function(lang) {
            language = lang;
            $table = $('#messaging');

            dataTable = new (app.getCls('dataTable'))();
            dataTable.render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'messagetype'},
                        {
                            mData     : 'action',
                            searchable: false,
                            orderable : false,
                            className : 'row-action'
                        }
                    ],
                    endpoint : {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST  : app.vars.baseUrl + '/messaging/save',
                        FORM  : app.vars.baseUrl + '/messaging/form',
                        DELETE: app.vars.baseUrl + '/messaging/delete'
                    },
                    extraData: {
                        _object: 'Messages'
                    },
                    language : lang || {},
                    callbacks: {
                        onRowCallback: function (row, data) {
                            var actions = '<div class="icon-pencil icon_row edit icon-btn-orange" title="Edit"></div>' +
                                '<div class="icon-trash icon_row delete icon-btn-red" title="Delete"></div>';

                            $('.row-action', row).html(actions);
                        },
                        onAfterAddNewRow: function (row) {
                            bindEvents(row);
                        },
                        onEditSelectedRow: function (row) {
                            bindEvents(row);
                        }

                    }
                }
            );
        };

        var bindEvents = function ($row) {
            tinymce.init({
                selector: 'textarea#message-body-editor',
                theme: 'modern',
                init_instance_callback: function() {
                    tinymce.get('message-body-editor').setContent($('#message-body-content').val());
                }
            });

            $('.buttonAdd, .buttonUpdate', $row).click(beforeSubmit);

            $('.insert-var', $row).click(insertVar);
            $('.add-var', $row).click(addVar);
            $('.message-type-select', $row).change(changeMessageType);
        };

        var beforeSubmit = function () {
            var $this = $(this),
                $form = $this.closest('form'),
                varsList = [];

            $('#message-body-content', $form).text(tinymce.get('message-body-editor').getContent());

            $('input[name="vars[]"]', $form).each(function () {
                var $self = $(this),
                    varName = $self.val(),
                    varValue = $self.data('id');

                if (varValue) varsList.push({name: varName, id: varValue});
            });

            $('<input>').attr({
                type: 'hidden',
                name: 'varsList',
                value: JSON.stringify(varsList)
            }).appendTo($form);
        };

        var insertVar = function () {
            var $this = $(this),
                varValue = $this.parent().find('select.variable-drop option:selected').text();

            if (varValue) tinymce.get('message-body-editor').execCommand('insertHTML', false, varValue);
        };

        var changeMessageType = function () {
            var $this = $(this),
                $selfForm = $this.closest('form'),
                $emailField = $('.row.email-type', $selfForm);

            if ($this.val() == 1) {
                $emailField.removeClass('hide');
            } else {
                $emailField.addClass('hide');
            }
        };

        var addVar = function () {
            var $this = $(this),
                $selfRow = $this.closest('tr'),
                $tableVars = $selfRow.find('.table-vars');

            var varHtml = ['<tr>'];

            varHtml.push('<td><input type="text" name="vars[]" value=""></td>');
            varHtml.push('<td><div class="icon-pencil icon_row edit"></div> <div class="icon-trash icon_row delete"></div></td>');
            varHtml.push('</tr>');

            $tableVars.find('tbody').append(varHtml.join(''));

            bindEventsVarsForm($tableVars);
        };

        var bindEventsVarsForm = function ($selfTable) {
            $('.icon_row', $selfTable).click(function (e) {
                var $this = $(this),
                    $selfForm = $this.closest('form'),
                    $selfRow = $this.closest('tr'),
                    action = $this.hasClass('edit') ? 'edit' : 'delete',
                    varName = $selfRow.find('input[type="text"]').val().trim();

                if (action == 'delete') {
                    swal({
                        title: 'Are you sure?',
                        text: 'This variable will be deleted!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No',
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (!isConfirm) return false;

                        reDrawVars($selfTable, $selfForm, action, $selfRow);
                    });
                } else {
                    if (!varName) {
                        swal('Missing data!', 'Please input variable name!', 'error');

                        return false;
                    }

                    reDrawVars($selfTable, $selfForm, action, $selfRow);
                }
            });
        };

        var reDrawVars = function ($selfTable, $selfForm, action, $selfRow) {
            var selectVarsHtml = [],
                i = 1;

            if (action == 'delete') $selfRow.remove();

            $selfTable.find('input[type="text"]').each(function () {
                var $this = $(this),
                    optName = $this.val().trim(),
                    optValue = $this.data('id');

                if (optName) {
                    if (!optValue) {
                        optValue = 'vars[-]' + i + '[-]new';
                        i++;
                    }

                    selectVarsHtml.push('<option value="' + optValue + '">' + optName + '</option>');

                    $this.data('id', optValue).attr('data-id', optValue);
                }
            });

            $('.variable-drop', $selfForm).html(selectVarsHtml.join(''));

            if (action == 'delete') {
                swal('Deleted!', 'Variable has been deleted.', 'success');
            } else {
                swal('Added!', 'Variable has been added!', 'success');
            }
        };
    };

    app.addCls('Messaging', instance);

})($, $.app);