/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = new function() {

        var $table,
            dataTable

        this.run = function() {
            $table = $('#beaconCampaigns');

            dataTable = (new (app.getCls('dataTable'))()).render(
                $table,
                $('.add-button'),
                {
                    colDefs  : [
                        {mData: 'name'},
                        {mData: 'beaconName'},
                        {mData: 'startdate'},
                        {mData: 'enddate'},

                        {
                            mData: 'action',
                            searchable: false,
                            orderable : false,
                            className: 'row-action'
                        }
                    ],
                    endpoint: {
                        GET   : app.vars.baseUrl + '/datatable',
                        POST: app.vars.baseUrl + '/beacon-campaigns/save',
                        FORM: app.vars.baseUrl + '/beacon-campaigns/form',
                        DELETE: app.vars.baseUrl + '/beacon-campaigns/delete'
                    },
                    extraData: {
                        _object: 'BeaconActionMap'
                    },
                    callbacks: {
                        onRowCallback: function(row, data) {

                            var actions = '<div class="icon-pencil icon_row edit"></div>' +
                                '<div class="icon-trash icon_row delete"></div>';
                            $('.row-action', row).html(actions);
                            $('.edit', row).html();

                        }
                    }
                }
            );
        };

    };

    app.addCls('organizationManage', instance);

    $( "body" ).on( "btnAddUpdate.click", function() {

        $('.dropHere').each(function() {
            var _this = $(this);
            var name = _this.attr('data-key');

            _this.find('img.ui-draggable').each(function() {
                var id = $(this).attr('data-id');
                $('#'+name).val(id);
            });
        })

    });

    $("body").on('click', '.select_all', function() {
        $('.select_days').find('.day').each(function() {
            $(this).prop('checked', true);
        });

    });

    $('body').on('click', '.day', function() {
        assignAllDays();
    });

    var dayInMonth = {
        '01' : '31',
        '02' : '28',
        '03' : '31',
        '04' : '30',
        '05' : '31',
        '06' : '30',
        '07' : '31',
        '08' : '31',
        '09' : '30',
        '10' : '31',
        '11' : '30',
        '12' : '31'
    };

    var currentDate = new Date();
    var year = currentDate.getFullYear();
    var month = currentDate.getMonth()+1;
    month = (month <10 ) ? '0'+month : month;
    renderCalender(year+'-'+month+'-01', year+'-'+month+'-'+dayInMonth[month]);

    $('body').on('click', '.fc-left button', function() {
        var moment = $('#calendarView').fullCalendar('getDate');
        var currentDate = new Date(moment.format());
        var year = currentDate.getFullYear();
        var month = currentDate.getMonth()+1;
        month = (month <10 ) ? '0'+month : month;
        renderCalender(year+'-'+month+'-01', year+'-'+month+'-'+dayInMonth[month]);
    })

})($, $.app);

function renderCalender(sdate,edate) {

    getData('/beacon-campaigns/get-calender-data/'+sdate+'/'+edate,{}, 'GET', function(res) {

        var events = [];

        $.each(res, function(k, v){
            events.push(
                {
                    //id: v.id,
                    title: v.name,
                    start: v.startdate,
                    end: v.enddate
                }
            );
        })

        $('#calendarView').fullCalendar({
            theme: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: '2016-01-12',
            //editable: true,
            eventLimit: true,
            events: events
        });
    } );
}

function getData(url,data,method, callback) {
    $.ajax({
        url: url,
        data: data,
        type: method,
        dataType : "json",
        success: function( response ) {
            callback(response);
        }, error: function( xhr, status, errorThrown ) {

        }
    });
}

function assignAllDays() {
    var dayLenght = $('.select_days .day').length;
    var dayCheckLenght = $('.select_days input:checked').length;

    if(dayLenght == dayCheckLenght){
        $('.select_all').prop('checked', true);
    } else {
        $('.select_all').prop('checked', false);
    }
}

function resetIcon(icons) {
    $('.dropHere').each(function() {
        var name = $(this).attr('data-key');
        $('#'+name).val('');
        $(this).html('');
    });

    if(icons.length != 0){
        $('.dropHere').each(function() {
            var name = $(this).attr('data-key');
            $(this).append( icons[name] );
        });
    }
}


