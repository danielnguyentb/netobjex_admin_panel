/**
 * Created by AnhNguyen on 12/10/15.
 */

(function($, app) {
    "use strict";

    var instance = function() {

        var $table,
            $addBtn,
            self = this;

        var options = {
            object        : '',
            paging        : true,
            search        : true,
            sort          : true,
            extraData     : {},
            serverSide    : true,
            colDefs       : [],
            pageLength    : 10,
            data          : null,
            rowEvents     : {
                add   : true,
                edit  : true,
                delete: true
            },
            endpoint      : {
                GET    : null,
                POST   : null,
                DELETE : null,
                FORM   : null
            },
            pagingSettings: {
                "lengthMenu": [
                    [10, 25, 50, 1000000],
                    [10, 25, 50, "All"]
                ]
            },
            classes       : {
                loader    : {
                    add : '.loader-add',
                    edit: '.loader-edit'
                },
                handlerRow: {
                    edit  : '.edit',
                    delete: '.delete'
                }
            },
            language : {
                addLoading: 'Loading Add',
                editLoading: 'Loading Edit'
            },
            callbacks: {
                onSelectedRow    : function () {
                },
                onDeSelectedRow  : function () {
                },
                onAfterAddNewRow : function () {
                },
                onOpenSelectedRow: function () {
                },
                onEditSelectedRow: function () {
                },
                onAfterRemoveRow : function () {
                },
                onAfterUpdateRow : function () {
                },
                onRowCallback    : function () {
                },
                onImportCallback : function () {
                },
                onCtxMenuCallback: function () {
                },
                onHeaderCallback : function () {
                },
                onEditPopup      : function () {
                },
                drawCallback     : function () {
                },
                onInitComplete   : function () {
                }
            }
        };

        this.render = function(table, addBtn, opts) {

            if(!table.length || $.isEmptyObject(opts)) return false;

            $table = table;
            $addBtn = addBtn;
            options = $.extend(options, opts);

            $table = $table.dataTable(getOptions());
            $table.data('instance', self);
        };

        this.redraw = function() {
            $table.api().draw(false);
        };

        this.api = function() {
            return $table.api();
        };

        this.rowLoading = function(text, cls) {
            return rowLoading(text, cls);
        };

        var bindEvents = function() {

            if(options.rowEvents.edit) {
                $(options.classes.handlerRow.edit, $table).click(editRowHandler);
            }

            if(options.rowEvents.delete) {
                $(options.classes.handlerRow.delete, $table).click(removeRowHandler);
            }

            $table.on('error.dt', function(e, settings, techNote, message) {
                console.log(arguments);
            });
        };

        var rowLoading = function(text, cls) {

            var $loader = $(app.f(ui.loading, {
                text: text
            }));

            $loader = $($loader.outerHTML());
            $loader.removeClass('hide');

            var numCols = $('th', $table).length;

            var $row = $("<tr role='row' class='" + cls + "'><td colspan=" + numCols + "></td></tr>");
            $('td', $row).append($loader);

            return $row;
        };

        var addRowHandler = function(e) {

            //Remove current form add
            $('.form-add-table', $table).remove();

            var $row = rowLoading(options.language.addLoading, 'form-add-table');
            var $this = $(this);
            var cData = $this.data('custom');
            if(!$.isPlainObject(cData)) cData = {};

            //Append to table
            $row.insertBefore($('tbody tr:first', $table));
            var $loader = $('.loader-add', $row);

            app.ajax(
                options.endpoint.FORM,
                cData,
                'GET',
                'html',
                false,
                function(res) {

                    var $content = $(res);
                    $('td', $row).append($content);
                    $loader.fadeOut(
                        400,
                        function() {
                            $content.fadeIn();
                        }
                    );

                    eventFormRow($content);

                    if(options.callbacks.onAfterAddNewRow) options.callbacks.onAfterAddNewRow($row);
                }
            );
        };

        var removeRowHandler = function(e) {

            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var id = $selfRow.prop('id');
            if(id == '') return false;

            swal({
                title: "Are you sure you want to delete?",
                text: "You will not be able to recover this.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm : true
            }, function(){

                app.ajax(
                    [options.endpoint.DELETE, '/', id].join(''),
                    {},
                    'POST',
                    'json',
                    '',
                    function (response) {

                        if (response.status) {
                            swal('Deleted!', 'Your entry has been been deleted.', "success");
                            self.redraw();
                        }
                        else {
                            sweetAlert("Oops...", response.message, "error");
                        }
                    }
                );
            });
        };

        var editRowHandler = function(e) {

            var $this = $(this);
            var $selfRow = $this.closest('tr');
            var id = $selfRow.prop('id');
            if(id == '') return false;
            $('.edit-row-' + id, $table).remove();
            
            var $row = rowLoading(options.language.editLoading, 'edit-row-' + id);
            
            //Append to table
            $row.insertAfter($selfRow);
            var $loader = $('.loader-add', $row);

            app.ajax(
                [options.endpoint.FORM, '/', $selfRow.prop('id')].join(''),
                {},
                'GET',
                'html',
                false,
                function(res) {

                    var $content = $(res);
                    $('td', $row).append($content);
                    $loader.fadeOut(
                        400,
                        function() {
                            $content.fadeIn();
                        }
                    );

                    eventFormRow($content);

                    if(options.callbacks.onEditSelectedRow) options.callbacks.onEditSelectedRow($row);
                }
            );

            return false;
        };

        var eventFormRow = function(wrap) {

            if($('.chosen-select', wrap).length) {
                $('.chosen-select', wrap).chosen({disable_search_threshold: 10});
            }

            $(":input", wrap).inputmask();
            $('.row-form', wrap).parsley();

            if($('.filestyle', wrap).length) {
                $('.filestyle', wrap).filestyle();
            }

            wrap.on('click','.buttonCancel, .buttonClose', function(e){

                $(this).closest('tr').remove();
                return false;
            });

            if(options.callbacks.onInitGetRow) options.callbacks.onInitGetRow(wrap);

            wrap.on('click', '.buttonAdd, .buttonUpdate', formSubmit);
        };

        var formSubmit = function(e) {

            var $this = $(this);
            var $form = $this.closest('form');
            var id = $form.data('id');
            var addForm = id == '';
            $('body').trigger('btnAddUpdate.click');

            if($form.valid()) {

                var formRequest = function() {
                    swal.disableButtons();

                    $.ajax({
                        url        : addForm ? options.endpoint.POST : [options.endpoint.POST, '/', id].join(''),
                        type       : 'POST',
                        data       : new FormData($form[0]),
                        dataType   : 'json',
                        cache      : false,
                        contentType: false,
                        processData: false,
                        success    : function (response) {

                            if (response.status) {
                                swal(addForm ? 'Added!' : 'Updated!', response.message, "success");

                                self.redraw();
                            }
                            else {
                                sweetAlert("Oops...", response.message, "error");
                            }
                        },
                        error      : function () {
                            sweetAlert("Oops...", "An error occurred while processing please try again later!", "error");
                        },
                        complete   : function () {

                            setTimeout(function () {
                                swal.enableButtons();
                            }, 100);
                        }
                    });
                };

                if(addForm) {
                    swal({
                        title: "Are you sure you want to add this entry?",
                        text: "You can change the contents in the table if you want.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "green",
                        confirmButtonText: "Yes, add it!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm : true
                    }, formRequest);
                } else {
                    swal({
                        title: "Are you sure you want to update this entry?",
                        text: "You can change the contents in the table if you want.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "green",
                        confirmButtonText: "Yes, update it!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm : true
                    }, formRequest);
                }
            }

            $('input:invalid', $form).removeClass('input-valid').addClass('input-invalid');
            $('input:valid', $form).removeClass('input-invalid').removeClass('input-error').addClass('input-valid');
            $('input.error', $form).removeClass('input-valid').addClass('input-error');

            $('input:invalid', $form).siblings('.error_message').show();
            $('input:valid', $form).siblings('.error_message').hide();

            $('textarea:invalid', $form).removeClass('input-valid').addClass('input-invalid');
            $('textarea:valid', $form).removeClass('input-invalid').addClass('input-valid');

            $('textarea:invalid', $form).siblings('.error_message').show();
            $('textarea:valid', $form).siblings('.error_message').hide();

            return false;
        };

        var getOptions = function() {

            var opts = {
                autoWidth: false,
                processing: true,
                columns: options.colDefs,
                language: {},
                pageLength: options.pageLength,
                columnDefs: []
            };

            if (options.serverSide) {
                opts.serverSide = true;
                opts.ajax = {
                    url: options.endpoint.GET,
                    data: function (d) {
                        if (typeof options.extraData === 'function') options.extraData(d);
                        else $.extend(d, options.extraData);
                    }
                };
            } else {
                opts.data = settings.data;
            }

            if (options.paging) {
                $.extend(opts, options.pagingSettings);
            } else opts.paging = false;

            if (!options.sort) opts.ordering = false;

            if (!options.search) opts.searching = false;

            $.fn.dataTable.ext.errMode = 'throw';
            $.fn.dataTable.ext.internal._fnLog = function(settings, level, msg, tn) {

                if(!level) {
                    sweetAlert("Oops...", msg, "error");
                } else if ( window.console && console.log ) {
                    console.log( msg );
                }
            };

            opts.drawCallback = drawCallBack;
            opts.rowCallback = onRowCallback;
            opts.initComplete = initComplete;
            opts.fnPreDrawCallback = onPreDrawCallback;
            opts.dom =
                "<'row'<'col-xs-6'l><'col-xs-6'f>>"+
                "rt"+
                "<'row'<'col-xs-6'i><'col-xs-6'p>>";

            return opts;
        };

        var drawCallBack = function() {

            bindEvents();
            if(options.callbacks.drawCallback) options.callbacks.drawCallback($table);
        };

        var onRowCallback = function(row, data) {

            if(options.callbacks.onRowCallback) options.callbacks.onRowCallback(row, data);
        };

        var initComplete = function() {

            if(options.rowEvents.add && $addBtn && $addBtn.length) {
                $addBtn.click(addRowHandler);
            }
        };

        var onPreDrawCallback = function() {

        };

    };

    var ui = {
        loading: '<div class="panel-body loader-add hide loader-demo">' +
                    '<h4>{text}</h4>' +
                    '<div class="ball-pulse">' +
                        '<div></div>' +
                        '<div></div>' +
                        '<div></div>' +
                    '</div>' +
                '</div>'
    };

    app.addCls('dataTable', instance);

})($, $.app);