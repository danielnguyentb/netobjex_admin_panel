/**
 * Created by AnhNguyen on 12/8/15.
 */
(function ($, app) {
    "use strict";

    var login = new function () {

        this.run = function () {

            var $form = $('form');
            app.parsley($form);

            $form.submit(function (e) {

                var $btn = $('button[type="submit"]', $form);
                if(!$form.parsley('validate')) return false;
                $btn.button('loading');

                app.ajax(
                    app.vars.baseUrl + '/do-login',
                    $form.serialize(),
                    'POST',
                    'json',
                    true,
                    function (response) {
                        if (response.status) {

                            var backUrl = app.utils.getQueryParam('_b');
                            if (backUrl && backUrl != '') {
                                window.location.href = decodeURIComponent(backUrl);
                            } else {
                                window.location.href = app.vars.baseUrl + '/dashboard';
                            }
                        } else {
                            app.pushNoty('error', response.message);
                            $btn.button('reset');
                        }
                    }
                );

                return false;
            });
        };
    };

    app.addCls('loginInstance', login);

})($, $.app);