/**
 * Created by chungdd on 12/9/15.
 */
(function ($, app) {
    "use strict";

    var layout = new function () {
        this.run = function () {
            $('.chosen-select', '.navbar-right').chosen({disable_search_threshold: 10});

            var defaultUsersOpts = '<select id="userSelect" class="chosen-select input-md form-control"><option value="">Select User</option></select>';

            var userListChange = function () {
                $('select#userSelect').change(function (e) {
                    var selectedOrg = $('select#orgList').val(),
                        selectedUser = $(this).val();

                    if (selectedOrg != app.vars.curCustomerId || selectedUser != app.vars.curUserId) {
                        setSelectedUser(selectedOrg, selectedUser);
                    } else {
                        window.location.href = app.vars.baseUrl + '/dashboard';
                    }
                }).chosen({disable_search_threshold: 10});
            };

            var setSelectedUser = function (customerId, userId) {
                app.ajax(
                    app.vars.baseUrl + '/set-selected-org-user',
                    {customerId: customerId, userId: userId, _token: app.vars.csrf},
                    'POST',
                    'json',
                    true,
                    function (response) {
                        console.log(response);
                        window.location.href = app.vars.baseUrl + '/dashboard';
                    }
                );
            };

            var orgListChange = function () {
                $('select#orgList').change(function (e) {
                    var $this = $(this),
                        customerId = $this.val();

                    if (customerId) {
                        getUsersList(customerId);
                    } else {
                        //$('select#userSelect').parent().html(defaultUsersOpts);
                        setSelectedUser('', '');
                    }
                });
            };

            var getUsersList = function (customerId, hasLoader) {
                customerId = customerId || $('select#orgList').val();
                hasLoader = (typeof hasLoader === 'undefined' || hasLoader === '') ? true : hasLoader;

                app.ajax(
                    app.vars.baseUrl + '/get-users-by-customer',
                    {customerId: customerId, _token: app.vars.csrf},
                    'POST',
                    'json',
                    hasLoader,
                    function (response) {
                        if (response.status) {
                            var $selectUser = $('select#userSelect');

                            if (response.data.length) {
                                var usersHtml = ['<select id="userSelect" class="chosen-select input-md form-control"><option value="">Select User</option>'];

                                $.each(response.data, function (i) {
                                    var user = response.data[i],
                                        selected = user.id == app.vars.curUserId ? ' selected="selected"' : '';

                                    usersHtml.push('<option value="' + user.id + '"' + selected + '>' + user.firstname + (user.lastname ? ' ' + user.lastname : '') + ' (' + user.email + ')' + '</option>');
                                });

                                usersHtml.push('</select>');

                                $selectUser.parent().html(usersHtml.join(''));
                            } else {
                                $selectUser.parent().html(defaultUsersOpts);
                            }
                        }

                        userListChange();
                    }
                );
            };

            orgListChange();
            getUsersList('', false);
            userListChange();
        };
    };

    app.addCls('layoutInstance', layout);

})($, $.app);