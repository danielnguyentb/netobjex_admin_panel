function error_message() {
    $("<span class='append_message'>Invalid</span>").prependTo('.error_message');

    // ID
    $("<span class='append_error'> ID </span>").appendTo('.id_error');

    // Group Name
    $("<span class='append_error'> Group Name </span>").appendTo('.group_name_error');
    // Address
    $("<span class='append_error'> Address </span>").appendTo('.address_error');
    // City
    $("<span class='append_error'> City </span>").appendTo('.city_error');
    // Zip Code
    $("<span class='append_error'> Zip Code </span>").appendTo('.zip_code_error');
    // Phone Number
    $("<span class='append_error'> Phone Number </span>").appendTo('.phone_number_error');
    // Email
    $("<span class='append_error'> Email </span>").appendTo('.email_error');

    // Name
    $("<span class='append_error'> Name </span>").appendTo('.name_error');
    // Organization
    $("<span class='append_error'> Organization </span>").appendTo('.organization_error');
    // User ID
    $("<span class='append_error'> User ID </span>").appendTo('.user_id_error');
    // Password
    $("<span class='append_error'> Password </span>").appendTo('.password_error');
    // URL
    $("<span class='append_error'> URL </span>").appendTo('.url_error');

    // UUID
    $("<span class='append_error'> UUID </span>").appendTo('.uuid_error');
    // Major ID
    $("<span class='append_error'> Major ID </span>").appendTo('.major_id_error');
    // Minor ID
    $("<span class='append_error'> Minor ID </span>").appendTo('.minor_id_error');

    // Device ID
    $("<span class='append_error'> Device ID </span>").appendTo('.device_id_error');
    // Device Name
    $("<span class='append_error'> Device Name </span>").appendTo('.device_name_error');
    // Firmware Version ID
    $("<span class='append_error'> Firmware Version ID </span>").appendTo('.firmware_version_id_error');
    // Controller
    $("<span class='append_error'> Controller </span>").appendTo('.controller_error');

    // Latitude
    $("<span class='append_error'> Latitude </span>").appendTo('.latitude_error');
    // Longitude
    $("<span class='append_error'> Longitude </span>").appendTo('.longitude_error');

    // Interval
    $("<span class='append_error'> Interval Second(s) </span>").appendTo('.interval_error');

    // Queue Topic Name
    $("<span class='append_error'> Queue Topic name </span>").appendTo('.queue_topic_name_error');

    // Start Date
    $("<span class='append_error'> Start Date: should be lesser than end date </span>").appendTo('.start_date_error');
    // End Date
    $("<span class='append_error'> End Date: should be greater than start date </span>").appendTo('.end_date_error');
    // Execute Time
    $("<span class='append_error'> Execute Time </span>").appendTo('.execute_time_error');
    // Parameter
    $("<span class='append_error'> Parameter </span>").appendTo('.parameter_error');

    // Description
    $("<span class='append_error'> Description </span>").appendTo('.description_error');
    // Image
    $("<span class='append_error'> Image </span>").appendTo('.image_error');

    // Multiple Emails
    $("<span class='append_error'> Email(s) </span>").appendTo('.multiple_email_error');
    // Access to Object
    $("<span class='append_error'> Access to Object </span>").appendTo('.access_to_object_error');
    // Object Type
    $("<span class='append_error'> Object Type </span>").appendTo('.object_type_error');
    // Question
    $("<span class='append_error'> Question </span>").appendTo('.question_error');

    // Notification Text
    $("<span class='append_error'> Notification </span>").appendTo('.notification_error');

    // First Name
    $("<span class='append_error'> First Name </span>").appendTo('.first_name_error');
    // Last Name
    $("<span class='append_error'> Last Name </span>").appendTo('.last_name_error');
    // Old Password
    $("<span class='append_error'> - this is not your current password </span>").appendTo('.old_password_error');
    // Confirm Password
    $("<span class='append_error'> Password to Confirm </span>").appendTo('.confirm_password_error');

    // Device Type
    $("<span class='append_error'> Device Type </span>").appendTo('.device_type_error');
    // Queue Name
    $("<span class='append_error'> Queue Name </span>").appendTo('.queue_name_error');
    // Status Viewer
    $("<span class='append_error'> Status Viewer </span>").appendTo('.status_viewer_error');

    // Command Name
    $("<span class='append_error'> Command Name </span>").appendTo('.command_name_error');
    // Command String
    $("<span class='append_error'> Command String </span>").appendTo('.command_string_error');

    // Variable
    $("<span class='append_error'> Variable </span>").appendTo('.variable_error');
    // Value
    $("<span class='append_error'> Value </span>").appendTo('.value_error');

    // Protocol
    $("<span class='append_error'> Protocol </span>").appendTo('.protocol_error');
}

$(document).ready(function(){
    $('#add_form, #add_notification_form, #add_image_form, #add_url_form, #add_live_feed_form').click(function(){
        error_message();
    });

    $('table').on('click', ".edit, .status_view, .log, .command, .configuration, " +
        ".invite, .questions, .edit_question, .results, " +
        ".edit-notification, .edit-image, .edit-url, .edit-live-feed, " +
        "#command_form, .edit_command, .parameter, .edit_parameter, #configuration_form, .edit_configuration", function() {
        error_message();
    });

    $('.buttonSubmit').click(function(){
        if (!$('.append_message').length) {
            error_message();
        }
    });
});