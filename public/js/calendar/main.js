function main () {

    // ========== Previous and Next Week Arrows ========== //
    $('#prev_week_arrow > h4, #next_week_arrow > h4').mouseenter(function(){
        $(this).css('color', 'black');
        $(this).css('background-color', '#E7ECEF');
        $(this).css('cursor', 'pointer');
    });

    $('#prev_week_arrow > h4, #next_week_arrow > h4').mouseleave(function(){
        $(this).css('color', 'white');
        $(this).css('background-color', 'none');
    });

    var dateNumbers = [];
    var d = new Date();
    var currentMonth = d.getMonth() + 1;
    var currentYear = d.getFullYear();
    var currentDay = $('a.active').children('.date_number').html();
    var over30 = false;
    var below0 = false;
    var daysCrossingToNextMonth = 0;
    var daysCrossingToLastMonth = 0;
    var newDataDate;

    $('#next_week_arrow > h4').click(function(){
        $(".date_number").each(function( i, element ) {
            dateNumbers[i] = parseInt($(element).html());
            dateNumbers[i] += 7;

            over30 = false;
            if (daysCrossingToLastMonth > 0)
            {
                daysCrossingToLastMonth -= 1;
                if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                    if (dateNumbers[i] > 30) {
                        dateNumbers[i] -= 30;
                        over30 = true;
                    }
                }
                else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                    if (dateNumbers[i] > 28) {
                        dateNumbers[i] -= 28;
                        over30 = true;
                    }
                }
                else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                    if (dateNumbers[i] > 29) {
                        dateNumbers[i] -= 29;
                        over30 = true;
                    }
                }
                else {
                    if (dateNumbers[i] > 31) {
                        dateNumbers[i] -= 31;
                        over30 = true;

                        if (currentMonth == 12) {
                            newDataDate = currentYear + 1;
                        }
                    }
                }
            }
            else {
                if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                    if (dateNumbers[i] > 30) {
                        dateNumbers[i] -= 30;
                        over30 = true;
                        daysCrossingToNextMonth += 1;
                    }
                }
                else if (currentMonth == 2 && currentYear % 4 != 0) {
                    if (dateNumbers[i] > 28) {
                        dateNumbers[i] -= 28;
                        over30 = true;
                        daysCrossingToNextMonth += 1;
                    }
                }
                else if (currentMonth == 2 && currentYear % 4 == 0) {
                    if (dateNumbers[i] > 29) {
                        dateNumbers[i] -= 29;
                        over30 = true;
                        daysCrossingToNextMonth += 1;
                    }
                }
                else {
                    if (dateNumbers[i] > 31) {
                        dateNumbers[i] -= 31;
                        over30 = true;
                        daysCrossingToNextMonth += 1;

                        if (daysCrossingToLastMonth != 0)
                            daysCrossingToLastMonth -= 1;

                        if (currentMonth == 12) {
                            newDataDate = currentYear + 1;
                        }
                    }
                }
            }

            $(element).html(dateNumbers[i]);

            if (daysCrossingToNextMonth == 7) {
                currentMonth += 1;
            }

            // Crossing to Next Year
            if (currentMonth > 12 && daysCrossingToNextMonth != 0) {
                currentMonth = 1;
                currentYear += 1;
                $('#year').html(currentYear);
            }

            /*
            if (over30 && (daysCrossingToNextMonth > 0 && daysCrossingToNextMonth < 7)) {
                if (currentMonth + 1 > 12) {
                    newDataDate += " 01";
                }
                else {
                    newDataDate += "" + (((currentMonth + 1 < 10) ? "0" : "") + (currentMonth + 1));
                }
            }
            else {
                newDataDate += "" + ((currentMonth < 10) ? "0" : "") + currentMonth;
            }
            */

            if (daysCrossingToNextMonth == 7) {
                daysCrossingToNextMonth = 0;
            }

            /*
            newDataDate += "" + (((dateNumbers[i] < 10) ? "0" : "") + dateNumbers[i]);
            $(element).parent().attr('data-date', newDataDate);
            */
        });


        switch(currentMonth) {
            case 1:
                $('#month').html('January');
                break;
            case 2:
                $('#month').html('February');
                break;
            case 3:
                $('#month').html('March');
                break;
            case 4:
                $('#month').html('April');
                break;
            case 5:
                $('#month').html('May');
                break;
            case 6:
                $('#month').html('June');
                break;
            case 7:
                $('#month').html('July');
                break;
            case 8:
                $('#month').html('August');
                break;
            case 9:
                $('#month').html('September');
                break;
            case 10:
                $('#month').html('October');
                break;
            case 11:
                $('#month').html('November');
                break;
            case 12:
                $('#month').html('December');
                break;
        }

        if (daysCrossingToNextMonth != 0) {
            if ($("a.active").children('.date_number').html() <= 6) {
                switch(currentMonth + 1) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
        }
    });

    $('#prev_week_arrow > h4').click(function(){

        $(".date_number").each(function( i, element ) {
            dateNumbers[i] = parseInt($(element).html());
            dateNumbers[i] -= 7;

            below0 = false;
            if (daysCrossingToNextMonth > 0)
            {
                if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 30;
                        below0 = true;
                        daysCrossingToNextMonth -= 1;
                    }
                }
                else if (currentMonth == 2 && currentYear % 4 != 0) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 28;
                        below0 = true;
                        daysCrossingToNextMonth -= 1;
                    }
                }
                else if (currentMonth == 2 && currentYear % 4 == 0) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 29;
                        below0 = true;
                        daysCrossingToNextMonth -= 1;
                    }
                }
                else {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 31;
                        below0 = true;
                        daysCrossingToNextMonth -= 1;
                    }
                }
            }
            else {
                if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 30;
                        below0 = true;
                        daysCrossingToLastMonth += 1;
                    }
                }
                else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 28;
                        below0 = true;
                        daysCrossingToLastMonth += 1;
                    }
                }
                else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 29;
                        below0 = true;
                        daysCrossingToLastMonth += 1;
                    }
                }
                else {
                    if (dateNumbers[i] <= 0) {
                        dateNumbers[i] += 31;
                        below0 = true;
                        daysCrossingToLastMonth += 1;

                        if (currentMonth == 1) {
                            newDataDate = currentYear - 1;
                        }
                    }
                }
            }

            $(element).html(dateNumbers[i]);

            if (daysCrossingToLastMonth == 7) {
                currentMonth -= 1;
            }

            // Crossing to Last Year
            if (currentMonth == 0 && daysCrossingToLastMonth != 0) {
                currentMonth = 12;
                currentYear -= 1;
                $('#year').html(currentYear);
            }

            if (daysCrossingToLastMonth == 7) {
                daysCrossingToLastMonth = 0;
            }

            /*
            newDataDate += " " + (((dateNumbers[i] < 10) ? "0" : "") + dateNumbers[i]);
            $(element).parent().attr('data-date', newDataDate);
            */
        })

        /*
        var tempCounter = 0;
        $(".date_number").each(function( i, element ) {
            dateNumbers[i] = parseInt($(element).html());
            dateNumbers[i] -= 7;

            if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                if (dateNumbers[i] <= 0) {
                    dateNumbers[i] += 30;
                    daysCrossingToLastMonth += 1;
                }

                if (dateNumbers[i] == 0) {
                    dateNumbers[i] = 30;
                }
            }
            else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                if (dateNumbers[i] <= 0) {
                    dateNumbers[i] += 28;
                    daysCrossingToLastMonth += 1;
                }

                if (dateNumbers[i] == 0) {
                    dateNumbers[i] = 28;
                }
            }
            else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                if (dateNumbers[i] <= 0) {
                    dateNumbers[i] += 29;
                    daysCrossingToLastMonth += 1;
                }

                if (dateNumbers[i] == 0) {
                    dateNumbers[i] = 29;
                }
            }
            else {
                if (dateNumbers[i] <= 0) {
                    dateNumbers[i] += 31;
                    daysCrossingToLastMonth += 1;
                }

                if (dateNumbers[i] == 0) {
                    dateNumbers[i] = 31;
                }
            }

            $(element).html(dateNumbers[i]);
        });

        if (daysCrossingToLastMonth == 7) {
            below0 = true;
        }

        if (below0) {
            currentMonth -= 1;
            below0 = false;
            daysCrossingToLastMonth = 0;
        }

        if (currentMonth <= 0) {
            currentMonth = 12;
            currentYear -= 1;
            $('#year').html(currentYear);
        }

        if (currentMonth == 1 && daysCrossingToLastMonth < 7) {
            tempCounter = daysCrossingToLastMonth;

            $('a.epg-reload').each(function(i, element) {
                $(element).attr('data-date', ((tempCounter > 0) ? currentYear - 1: currentYear) +
                    ((tempCounter > 0) ? "12" : "01") +
                    (((dateNumbers[i] < 10) ? "0" : "") + dateNumbers[i]));

                tempCounter -= 1;
            });
        }
        else {
            tempCounter = daysCrossingToLastMonth;

            if (tempCounter == 0) {
                $("a.epg-reload").each(function (i, element) {
                    $(element).attr('data-date', currentYear +
                        (((currentMonth < 10) ? "0" : "") + currentMonth) +
                        (((dateNumbers[i] < 10) ? "0" : "") + dateNumbers[i]));
                });
            }
            else {
                $("a.epg-reload").each(function (i, element) {
                    newDataDate = "" + currentYear;

                    if (tempCounter < 7 && tempCounter > 0) {
                        newDataDate += "" + (((currentMonth - 1 < 10) ? "0" : "") + (currentMonth - 1));
                    }
                    else {
                        newDataDate += "" + (((currentMonth < 10) ? "0" : "") + currentMonth);
                    }

                    newDataDate += "" + (((dateNumbers[i] < 10) ? "0" : "") + dateNumbers[i]);

                    $(element).attr('data-date', newDataDate);

                    tempCounter -= 1;
                });
            }
        }
        */

        switch(currentMonth) {
            case 1:
                $('#month').html('January');
                break;
            case 2:
                $('#month').html('February');
                break;
            case 3:
                $('#month').html('March');
                break;
            case 4:
                $('#month').html('April');
                break;
            case 5:
                $('#month').html('May');
                break;
            case 6:
                $('#month').html('June');
                break;
            case 7:
                $('#month').html('July');
                break;
            case 8:
                $('#month').html('August');
                break;
            case 9:
                $('#month').html('September');
                break;
            case 10:
                $('#month').html('October');
                break;
            case 11:
                $('#month').html('November');
                break;
            case 12:
                $('#month').html('December');
                break;
        }

        if (daysCrossingToLastMonth != 0) {
            if ($("a.active").children('.date_number').html() >= 25) {
                switch(currentMonth - 1) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
        }
    });
    // ========== Previous and Next Week Arrows ========== //

    // ========== Datepicker ========== //
    var datepicker;
    var month;
    var day;
    var year;
    var dateMet;
    var dayMet;
    var monthMet;
    var yearMet;
    var monthChanged = false;
    var withinRange = false;

    $("#datepicker").on("input", function() {
        var datepicker = $("#datepicker").val().split("-");
        year = parseInt(datepicker[0]);
        month = parseInt(datepicker[1]);
        day = parseInt(datepicker[2]);
        if (month != undefined && day != undefined && year != undefined) {
            dateMet = false;

            if (currentYear < year) {
                while (!dateMet) {
                    $(".date_number").each(function (i, element) {
                        dateNumbers[i] = parseInt($(element).html());
                        dateNumbers[i] += 7;

                        over30 = false;

                        if (daysCrossingToLastMonth > 0)
                        {
                            daysCrossingToLastMonth -= 1;
                            if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                if (dateNumbers[i] > 30) {
                                    dateNumbers[i] -= 30;
                                    over30 = true;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] > 28) {
                                    dateNumbers[i] -= 28;
                                    over30 = true;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] > 29) {
                                    dateNumbers[i] -= 29;
                                    over30 = true;
                                }
                            }
                            else {
                                if (dateNumbers[i] > 31) {
                                    dateNumbers[i] -= 31;
                                    over30 = true;

                                    if (currentMonth == 12) {
                                        newDataDate = currentYear + 1;
                                    }
                                }
                            }
                        }
                        else {
                            if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                if (dateNumbers[i] > 30) {
                                    dateNumbers[i] -= 30;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] > 28) {
                                    dateNumbers[i] -= 28;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] > 29) {
                                    dateNumbers[i] -= 29;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] > 31) {
                                    dateNumbers[i] -= 31;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                        }

                        $(element).html(dateNumbers[i]);

                        if (daysCrossingToNextMonth == 7) {
                            currentMonth += 1;
                        }

                        // Crossing to Next Year
                        if (currentMonth > 12 && daysCrossingToNextMonth != 0) {
                            currentMonth = 1;
                            currentYear += 1;
                            $('#year').html(currentYear);
                        }

                        if (daysCrossingToNextMonth == 7) {
                            daysCrossingToNextMonth = 0;
                        }
                    });

                    if (daysCrossingToNextMonth > 0){
                        if (currentMonth + 1 == month || (currentMonth == 12 && month == 1)) {

                            if (day <= 6) {
                                currentMonth += 1;
                                monthChanged = true;
                            }

                            if (currentMonth > 12) {
                                currentMonth = 1;
                                currentYear += 1;
                            }
                        }
                    }

                    $(".date_number").each(function (i, element) {
                        if ($(element).html() == day && currentMonth == month && currentYear == year) {
                            dateMet = true;
                        }
                    });
                }

                switch(month) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    case 12:
                        $('#month').html('December');
                        break;
                }

                if (monthChanged) {
                    currentMonth -= 1;
                    monthChanged = false;

                    if (currentMonth <= 0)
                        currentMonth = 12;
                }
            }
            else if (currentYear > year) {
                while (!dateMet) {
                    $(".date_number").each(function (i, element) {
                        dateNumbers[i] = parseInt($(element).html());
                        dateNumbers[i] -= 7;

                        below0 = false;

                        if (daysCrossingToNextMonth > 0)
                        {
                            if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 30;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 28;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 29;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 31;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                        }
                        else {
                            if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 30;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 28;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 29;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 31;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                        }

                        $(element).html(dateNumbers[i]);

                        if (daysCrossingToLastMonth == 7) {
                            currentMonth -= 1;
                        }

                        // Crossing to Last Year
                        if (currentMonth <= 0 && daysCrossingToLastMonth != 0) {
                            currentMonth = 12;
                            currentYear -= 1;
                        }

                        if (daysCrossingToLastMonth == 7) {
                            daysCrossingToLastMonth = 0;
                        }
                    });

                    if (daysCrossingToLastMonth > 0){
                        if (currentMonth - 1 == month || (currentMonth == 1 && month == 12)) {
                            if (day >= 25) {
                                currentMonth -= 1;
                                monthChanged = true;
                            }

                            if (currentMonth <= 0) {
                                currentMonth = 12;
                                currentYear -= 1;
                            }
                        }
                    }

                    $(".date_number").each(function (i, element) {
                        if ($(element).html() == day && currentMonth == month && currentYear == year) {
                            dateMet = true;
                        }
                    });
                }

                switch(month) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    case 12:
                        $('#month').html('December');
                        break;
                }

                if (monthChanged) {
                    currentMonth += 1;
                    monthChanged = false;

                    if (currentMonth <= 0)
                        currentMonth = 12;
                }
            }
            else if (currentMonth < month || (currentMonth == month && daysCrossingToLastMonth != 0)) {
                while (!dateMet) {
                    $(".date_number").each(function (i, element) {
                        dateNumbers[i] = parseInt($(element).html());
                        dateNumbers[i] += 7;

                        over30 = false;

                        if (daysCrossingToLastMonth > 0) {
                            daysCrossingToLastMonth -= 1;
                            if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                if (dateNumbers[i] > 30) {
                                    dateNumbers[i] -= 30;
                                    over30 = true;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] > 28) {
                                    dateNumbers[i] -= 28;
                                    over30 = true;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] > 29) {
                                    dateNumbers[i] -= 29;
                                    over30 = true;
                                }
                            }
                            else {
                                if (dateNumbers[i] > 31) {
                                    dateNumbers[i] -= 31;
                                    over30 = true;
                                }
                            }
                        }
                        else {
                            if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                if (dateNumbers[i] > 30) {
                                    dateNumbers[i] -= 30;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] > 28) {
                                    dateNumbers[i] -= 28;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] > 29) {
                                    dateNumbers[i] -= 29;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] > 31) {
                                    dateNumbers[i] -= 31;
                                    over30 = true;
                                    daysCrossingToNextMonth += 1;
                                }
                            }
                        }

                        $(element).html(dateNumbers[i]);

                        if (daysCrossingToNextMonth == 7) {
                            currentMonth += 1;
                            daysCrossingToNextMonth = 0;
                        }
                    });

                    if (daysCrossingToNextMonth > 0) {
                        if (currentMonth + 1 == month) {
                            if (day <= 6) {
                                currentMonth += 1;
                                monthChanged = true;
                            }
                        }
                    }

                    $(".date_number").each(function (i, element) {
                        if ($(element).html() == day && currentMonth == month) {
                            dateMet = true;
                        }
                    });

                    if (monthChanged) {
                        currentMonth -= 1;
                        monthChanged = false;

                        if (currentMonth <= 0)
                            currentMonth = 12;
                    }
                }

                switch (month) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    case 12:
                        $('#month').html('December');
                        break;
                }
            }
            else if (currentMonth > month || (currentMonth == month && daysCrossingToNextMonth != 0)) {
                while (!dateMet) {
                    $(".date_number").each(function (i, element) {
                        dateNumbers[i] = parseInt($(element).html());
                        dateNumbers[i] -= 7;

                        below0 = false;

                        if (daysCrossingToNextMonth > 0)
                        {
                            if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 30;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 28;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else if (currentMonth == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 29;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 31;
                                    below0 = true;
                                    daysCrossingToNextMonth -= 1;
                                }
                            }
                        }
                        else {
                            if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 30;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 28;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 29;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                            else {
                                if (dateNumbers[i] <= 0) {
                                    dateNumbers[i] += 31;
                                    below0 = true;
                                    daysCrossingToLastMonth += 1;
                                }
                            }
                        }

                        $(element).html(dateNumbers[i]);

                        if (daysCrossingToLastMonth == 7) {
                            currentMonth -= 1;
                            daysCrossingToLastMonth = 0;
                        }
                    });

                    if (daysCrossingToLastMonth > 0){
                        if (currentMonth - 1 == month) {
                            if (day >= 25) {
                                currentMonth -= 1;
                                monthChanged = true;
                            }

                            if (currentMonth <= 0) {
                                currentMonth = 12;
                                currentYear -= 1;
                            }
                        }
                    }

                    $(".date_number").each(function (i, element) {
                        if ($(element).html() == day && currentMonth == month) {
                            dateMet = true;
                        }
                    });

                    if (monthChanged) {
                        currentMonth += 1;
                        monthChanged = false;

                        if (currentMonth <= 0)
                            currentMonth = 12;
                    }
                }

                switch(month) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    case 12:
                        $('#month').html('December');
                        break;
                }
            }
            else {

                withinRange = false;
                $(".date_number").each(function (i, element) {
                    if ($(element).html() == day) {
                        withinRange = true;
                        return false;
                    }
                });

                if (!withinRange) {
                    if (currentDay < day) {
                        while (!dateMet) {
                            $(".date_number").each(function (i, element) {
                                dateNumbers[i] = parseInt($(element).html());
                                dateNumbers[i] += 7;

                                over30 = false;

                                if (daysCrossingToLastMonth > 0)
                                {
                                    daysCrossingToLastMonth -= 1;
                                    if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                        if (dateNumbers[i] > 30) {
                                            dateNumbers[i] -= 30;
                                            over30 = true;
                                        }
                                    }
                                    else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                        if (dateNumbers[i] > 28) {
                                            dateNumbers[i] -= 28;
                                            over30 = true;
                                        }
                                    }
                                    else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                        if (dateNumbers[i] > 29) {
                                            dateNumbers[i] -= 29;
                                            over30 = true;
                                        }
                                    }
                                    else {
                                        if (dateNumbers[i] > 31) {
                                            dateNumbers[i] -= 31;
                                            over30 = true;

                                            if (currentMonth == 12) {
                                                newDataDate = currentYear + 1;
                                            }
                                        }
                                    }
                                }
                                else {
                                    if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                        if (dateNumbers[i] > 30) {
                                            dateNumbers[i] -= 30;
                                            over30 = true;
                                            daysCrossingToNextMonth += 1;
                                        }
                                    }
                                    else if (currentMonth == 2 && currentYear % 4 != 0) {
                                        if (dateNumbers[i] > 28) {
                                            dateNumbers[i] -= 28;
                                            over30 = true;
                                            daysCrossingToNextMonth += 1;
                                        }
                                    }
                                    else if (currentMonth == 2 && currentYear % 4 == 0) {
                                        if (dateNumbers[i] > 29) {
                                            dateNumbers[i] -= 29;
                                            over30 = true;
                                            daysCrossingToNextMonth += 1;
                                        }
                                    }
                                    else {
                                        if (dateNumbers[i] > 31) {
                                            dateNumbers[i] -= 31;
                                            over30 = true;
                                            daysCrossingToNextMonth += 1;
                                        }
                                    }
                                }

                                $(element).html(dateNumbers[i]);
                            });

                            $(".date_number").each(function (i, element) {
                                if ($(element).html() == day) {
                                    dateMet = true;
                                }
                            });
                        }
                    }
                    else if (currentDay > day) {
                        while (!dateMet) {
                            $(".date_number").each(function (i, element) {
                                dateNumbers[i] = parseInt($(element).html());
                                dateNumbers[i] -= 7;

                                below0 = false;

                                if (daysCrossingToNextMonth > 0)
                                {
                                    if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 30;
                                            below0 = true;
                                            daysCrossingToNextMonth -= 1;
                                        }
                                    }
                                    else if (currentMonth == 2 && currentYear % 4 != 0) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 28;
                                            below0 = true;
                                            daysCrossingToNextMonth -= 1;
                                        }
                                    }
                                    else if (currentMonth == 2 && currentYear % 4 == 0) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 29;
                                            below0 = true;
                                            daysCrossingToNextMonth -= 1;
                                        }
                                    }
                                    else {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 31;
                                            below0 = true;
                                            daysCrossingToNextMonth -= 1;
                                        }
                                    }
                                }
                                else {
                                    if (currentMonth - 1 == 4 || currentMonth - 1 == 6 || currentMonth - 1 == 9 || currentMonth - 1 == 11) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 30;
                                            below0 = true;
                                            daysCrossingToLastMonth += 1;
                                        }
                                    }
                                    else if (currentMonth - 1 == 2 && currentYear % 4 != 0) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 28;
                                            below0 = true;
                                            daysCrossingToLastMonth += 1;
                                        }
                                    }
                                    else if (currentMonth - 1 == 2 && currentYear % 4 == 0) {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 29;
                                            below0 = true;
                                            daysCrossingToLastMonth += 1;
                                        }
                                    }
                                    else {
                                        if (dateNumbers[i] <= 0) {
                                            dateNumbers[i] += 31;
                                            below0 = true;
                                            daysCrossingToLastMonth += 1;
                                        }
                                    }
                                }

                                $(element).html(dateNumbers[i]);
                            });

                            $(".date_number").each(function (i, element) {
                                if ($(element).html() == day) {
                                    dateMet = true;
                                }
                            });
                        }
                    }
                }
            }

            $('#year').html(year);

            $('a.epg-reload').removeClass('active');
            $(".date_number").each(function (i, element) {
                if ($(element).html() == day) {
                    $(element).parent().addClass('active');
                    currentDay = day;
                    return false;
                }
            });
        }
    });
    // ========== Datepicker ========== //

    // ========== Weekpicker ========== //
    $('a.epg-reload').click(function(){
        if (daysCrossingToLastMonth != 0) {
            if ($(this).children('.date_number').html() >= 25) {
                switch(currentMonth - 1) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
            else {
                switch(currentMonth) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
        }

        if (daysCrossingToNextMonth != 0) {
            if ($(this).children('.date_number').html() <= 6) {
                switch(currentMonth + 1) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
            else {
                switch(currentMonth) {
                    case 1:
                        $('#month').html('January');
                        break;
                    case 2:
                        $('#month').html('February');
                        break;
                    case 3:
                        $('#month').html('March');
                        break;
                    case 4:
                        $('#month').html('April');
                        break;
                    case 5:
                        $('#month').html('May');
                        break;
                    case 6:
                        $('#month').html('June');
                        break;
                    case 7:
                        $('#month').html('July');
                        break;
                    case 8:
                        $('#month').html('August');
                        break;
                    case 9:
                        $('#month').html('September');
                        break;
                    case 10:
                        $('#month').html('October');
                        break;
                    case 11:
                        $('#month').html('November');
                        break;
                    default:
                        $('#month').html('December');
                        break;
                }
            }
        }
    });

    // ========== Remove Duplicate Select ========== //
    $('#add_form').click(function(){
        $("div.chosen-container-single-nosearch").not(':last').hide();
        $("div.chosen-container-single-nosearch").not(':first').show();
        $("div.chosen-container-single-nosearch:gt(2)").hide();
    });

    $('table').on('click', '.edit, .view', function(){
        $("div.chosen-container-single-nosearch").not(':last').hide();
        $("div.chosen-container-single-nosearch").not(':first').show();
        $("div.chosen-container-single-nosearch:gt(2)").hide();
    });
    // ========== Remove Duplicate Select ========== //

    Date.prototype.stdTimezoneOffset = function () {
        var jan = new Date(this.getFullYear(), 0, 1);
        var jul = new Date(this.getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    };

    Date.prototype.dst = function () {
        return this.getTimezoneOffset() < this.stdTimezoneOffset();
    };

    var beinsport = {};
    beinsport = {
        dom: {
            daysListTabs: $('#bein_root .days_list a'),
            programmeListTabs: $('#bein_root .tabs_options > a'),
            paginationTabs: $('.pagination_list a'),
            dropDownList: $('#bein_root .dropbdown_list'),
            timeLineSliderContainer: $('#bein_root .time_line_slider')
        },
        sliders: {
            miniSliders: [],
            slick: null
        },
        vars: {
            currentLang: $('.current_lang').val(),
            currentRegion: $('.current_region').val(),
            baseUrl: $('.baseUrl').val(),
            isMobile: navigator.userAgent.match(/iPad|iPhone|iPod|Android|Samsung/ig),
            flag: true
        },
        init: function () {
            // calling the functions bellow
            this.tabsActions();
            this.progsMiniSlider();
            this.styledDropDown();
            this.timeLineSlider();
            this.progressBar();
            this.tweetsStyler();
            $(window).on('resize', this.progsMiniSlider);
        },
        tabsActions: function () {
            beinsport.dom.daysListTabs.on('click', function () {
                beinsport.dom.daysListTabs.removeClass('active');
                $(this).addClass('active');
                return false;
            });

            beinsport.dom.paginationTabs.on('click', function () {
                beinsport.dom.paginationTabs.removeClass('active');
                $(this).addClass('active');
                return false;
            })
        },
        styledDropDown: function () {
            noResultsText = "";
            switch (beinsport.vars.currentLang) {
                case "ar":
                    noResultsText = "????? ?? ??? ?????!";
                    break;
                case "fr":
                    noResultsText = "Oops, rien trouv�!";
                    break;
                case "en":
                    noResultsText = "Oops, nothing found !";
                    break;
            }
            beinsport.dom.dropDownList.chosen({
                disable_search_threshold: 50,
                no_results_text: noResultsText
            });
        },
        tweetsStyler: function () {
            if ($('.prog_tweet_area').length) {
                var checkExist = setInterval(function () {
                    $('.prog_tweet_area > iframe').contents().find('body').append('<style>.EmbeddedTweet-tweet{padding:10px !important;width: 96%;}.EmbeddedTweet{border-radius: 0 !important;border-color:#dbdbdb;max-width: 100%;min-height:139px;}.Tweet-text{font-family:arial !important;font-size:12px !important;}.Tweet-header{margin-bottom:7px !important;}.Tweet-metadata{font-size:12px !important;}.Tweet-actions{margin-top: 0px !important;}</style>');
                    clearInterval(checkExist);
                    $('.prog_tweet_area').show();
                }, 600);
            }
        },
        progsMiniSlider: function () {
            i = 0;
            $('.prog_items_cover').each(function () {
                beinsport.sliders.miniSliders[i] = $(this).progsMiniSlider();
                i++;
            })
        },
        progressBar: function () {
            $('.prog_period').prograssBar();
        },
        timeLineSlider: function () {
            var dt = new Date();
            var start_slide, start_hour;
            $('#bein_root .time_slide').each(function (index) {
                var slide_hour = $.trim($(this).data('hour'));
                var sys_hour;
                if (dt.getHours() < 10) {
                    sys_hour = $.trim('0' + dt.getHours() + ':00');
                } else {
                    sys_hour = $.trim(dt.getHours() + ':00');
                }

                if (slide_hour == sys_hour) {
                    start_slide = index;
                    start_hour = slide_hour;
                    console.log("current slide now : ", slide_hour, " ", index);
                    $(this).addClass('now_hour').text($('.now_text').text());

                }
            });
            beinsport.dom.timeLineSliderContainer.on('init', function (event, slick) {
                beinsport.slideToTime(start_hour);
            });
            beinsport.sliders.slick = beinsport.dom.timeLineSliderContainer.slick({
                initialSlide: start_slide,
                slidesToShow: 7,
                slidesToScroll: 1,
                dots: false,
                centerMode: true,
                focusOnSelect: true,
                mobileFirst: true,
                responsive: false,
                draggable: false,
                refresh: true,
                variableWidth: true,
                appendArrows: $('.time_line_arrows'),
                prevArrow: '<button type="button" class="slick-prev prev_slide"> < </button>',
                nextArrow: '<button type="button" class="slick-next next_slide"> > </button>'
            });

            // $('.change_initial_slide').on('click',function(){
            // 	beinsport.dom.timeLineSliderContainer.slickSetOption("initialSlide", 10,true);

            // })


        },
        searchByDay: function (date_search) {
            console.log(123);
            console.log('baseUrl', this.vars);

            var today = new Date();
            var isdst = today.dst();
            var tzo = today.getTimezoneOffset();

            $("#widget-loading").show({
                duration: 400,
                easing: "swing"
            });
            var url = "server/ajax_calendar/9-11-2015.json" ; 
            console.log("this.vars.baseUrl" + this.vars.baseUrl);
            $.ajax({
                //url: this.vars.baseUrl + "views/epg",
                url : url,
                type: "GET",
                data: {
                    date: date_search,
                    lang: beinsport.vars.currentLang,
                    region: beinsport.vars.currentRegion,
                    tzo: tzo,
                    isdst: isdst,
                    list: 1
                },
                success: function (data, textStatus, jqXHR) {

                    $("#widget-loading").hide({
                        duration: 400,
                        easing: "swing"
                    });

                    //************** this part should be done from the server   ****************
                    var len = $.map(data, function(n, i) { return i; }).length;
                     console.log( len );
                     var string = '<ul class="prog_list"><input type="hidden" value="2015-09-11" class="current_date">' ; 
                    for (var i = 1 ; i <= len ; i++ ){
                          var obj2 =  data['m'+i]; 
                         
                       string = string + ' <li class=""><div class="channel_visu red_bg"><a href="#"><span class="logo_chan with_play"><div class="calendarMachine"><b>M1</b></div><br></span></a></div><div class="programmes_items"><div class="progs_wrapper"><div class="prog_items_cover clearfix" style="margin-left: -880px; width: 7497px;">';
                            var len2 = $.map(obj2, function(n, i) { return i; }).length;
                        for (var j = 1 ; j <= len2 ; j++ ){
                            string = string + ' <div data-position="1" data-old-start-time="' + obj2['time-'+j].old_start_time + '" data-old-end-time="' + obj2['time-'+j].old_end_time + '" data-start-time="' + obj2['time-'+j].start_time  + '" data-end-time="' + obj2['time-'+j].end_time  + '" data-genre="' + obj2['time-'+j].data_genre  + '" class="prog_item" style="width: 441px;"><a href="#"><span class="top_infos"><span class="prog_visu"><img src="img/logo-single.png" alt="" style="background-color: #00AEEF;"></span><span class="prog_infos"><span title="' + obj2['time-'+j].span_title + '" class="prog_title">' + obj2['time-'+j].prog_title + '</span><br><span class="prog_type">' + obj2['time-'+j].prog_type + '</span></span></span><span class="bottom_infos"><span class="prog_status"></span><span data-old-start-time="' + obj2['time-'+j].old_start_time + '" data-old-end-time="' + obj2['time-'+j].old_end_time + '" data-start-time="' + obj2['time-'+j].start_time  + '" data-end-time="' + obj2['time-'+j].end_time  + '" class="prog_period"><span class="prog_hour">' + obj2['time-'+j].prog_hour_begin  + '</span><span class="prog_period_mask"><span style="width: 100%; background-color: rgb(2, 134, 183);">&nbsp;</span></span><span class="prog_hour">' + obj2['time-'+j].prog_hour_end  + '</span></span></span></a><span class="trending_tweets_symbole"></span></div>';
                        }
                        string = string +  '</div><a href="#" class="slide_next"><span class="arrows_symbole"></span><span class="next_link_lbl">Next</span></a></div></div></li>'; 
                    }
                    console.log(string);
                    string = string +  '</ul>'; 
                    // **************************************************************************



                    $('ul.prog_list').replaceWith(string);
                    
                    //data - response from server
                   // $('ul.prog_list').replaceWith(data);
                    currentTime = beinsport.sliders.slick.slick('slickCurrentSlide');
                    beinsport.progressBar();
                    beinsport.progsMiniSlider();
                    beinsport.slideToTime(currentTime);
                    //$("#loading_dialog").loading("loadStop");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error from the server");
                    console.log(jqXHR);
                }
            })
        },
        filterProgramByTime: function (currentTime) {
            currentTime = $('.current_date').val()+' '+currentTime;
            //currentTime = moment(currentTime, 'YYYY-MM-DD h:mma');

            $(".prog_list li").each(function (progSliderIndex) {
                var selfSlider = this;
                //currentTime = moment(currentTime, 'h:mma');
                currentTime = moment(currentTime, 'YYYY-MM-DD h:mma');
                $(this).find(".prog_item").each(function (index) {
                    var program = this;
                    var startTime = $(program).data("start-time");
                    var endTime = $(program).data("end-time");
                    startTime = moment(startTime);
                    endTime = moment(endTime);
                    if (startTime.isSame(currentTime) || (currentTime.isAfter(startTime)) && endTime.isAfter(currentTime)) {
                        beinsport.sliders.miniSliders[progSliderIndex].slideTo(index + 1);
                        return false;
                    }
                });
            });
        },
        slideToTime: function (start_hour) {
            setTimeout(function () {
                for (i = 0; i < beinsport.sliders.miniSliders.length; i++) {
                    console.log(start_hour);
                    beinsport.filterProgramByTime(start_hour);
                }
            }, 500);
        }
    };

//call init function on document ready
    $(function () {
        beinsport.init();

        // Time slider filter
        $('#bein_root .time_line_slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            currentSlideItem = $(slick.$slides[currentSlide]);
            currentSlideTime = currentSlideItem.data("hour");
            beinsport.filterProgramByTime(currentSlideTime);
        });

        $("#hour-filter").on("change", function () {
            currentSlideTime = $(this).val();
            beinsport.filterProgramByTime(currentSlideTime);
            beinsport.sliders.slick.slick('slickGoTo', parseInt(currentSlideTime));
        });

        $("#select_filtre").on("change", function () {
            $("#widget-loading").show({
                duration: 400,
                easing: "swing"
            });
            var filtred = $(this).filterData($(".prog_list"));
            if (filtred) {
                $(".tweets_list").promise().done(function (resolved) {
                    $("#widget-loading").hide({
                        duration: 400,
                        easing: "swing"
                    });
                });

            }
        });

        //$(".epg-reload").on('click', function () {
        //
        //    var monthname = $("#month").html();
        //    var year = $("#year").html();
        //    var day = $(this).find('.date_number').html();
        //    var monthNumber = ["january", "february", "march","april","may","june","july","august","september","october","november","december"].indexOf(monthname.toLowerCase()) + 1;
        //    if (monthNumber.toString().length == 1) {
        //        monthNumber = "0" + monthNumber;
        //    }
        //    var date = year + monthNumber + day;
        //    $(this).data("date",date);
        //    var date = $(this).data("date");
        //    console.log(date);
        //    beinsport.searchByDay(date);
        //});

    });
}

$(document).ready(function(){

    main();
    


});