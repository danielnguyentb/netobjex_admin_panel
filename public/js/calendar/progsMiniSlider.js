(function ($) {

    $.fn.progsMiniSlider = function (options) {
        var mainDiv = $(this);
        var progsCount = $(this).find('.prog_item').length;
        var progsWrapperWidth = $('.progs_wrapper').width();
        var progItemsWidth = parseInt(progsWrapperWidth / 2);
        var progsCover = progsCount * progItemsWidth;
        var moveLeft = 0;
        var nextClickCount = 0;
        var currentSlide = 1;

        $(this).css({width: progsCover});
        $(this).find('.prog_item').css({width: progItemsWidth});


        $(this).next('.slide_next').on('click', function () {
            slideNext();
            return false;
        });

        $('.progs_wrapper').hover(function () {
            $(this).find('.slide_next').addClass('active');
        }, function () {
            $(this).find('.slide_next').removeClass('active');
        });

        var slideTo = function (index) {
            if (index == 0) {
                index == 1;
            }
            if (index >= progsCount) {
                return false;
            }
            var moveLeft = 0;
            moveLeft -= (index - 1) * progItemsWidth;
            if (moveLeft > 0) {
                moveLeft = 0;
            }
            if ($('.ar_version').length) {
                mainDiv.css({marginRight: moveLeft});
            }
            mainDiv.css({marginLeft: moveLeft});
            currentSlide = index;
            return true;

        };

        var slideNext = function () {
            currentSlide++;
            if (!slideTo(currentSlide)) {
                currentSlide--;
            }
        };

        var restart = function () {
            slideTo(0);
            progsCount = mainDiv.find('.prog_item:visible').length;
        };

        return {
            elem : mainDiv,
            progsCount: progsCount,
            progsWrapperWidth: progsWrapperWidth,
            progItemsWidth: progItemsWidth,
            progsCover: progsCover,
            moveLeft: moveLeft,
            currentSlide: nextClickCount,
            slideTo: slideTo,
            slideNext: slideNext,
            restart: restart
        };
    };

}(jQuery));