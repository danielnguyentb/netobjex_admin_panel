(function ($) {

    $.fn.prograssBar = function (options) {
        $(this).each(function () {
            var start_time = moment($(this).data("start-time")).unix();
            var end_time = moment($(this).data("end-time")).unix();

            var interval = end_time - start_time

            var now = moment().unix();
            var passedTime = now - start_time;
            var percent = Math.min(Math.max(Math.round((passedTime / interval) * 100), 0), 100);

            var progressBar = $(this).find('.prog_period_mask span');
            progressBar.css("width", percent + "%");
        });
    };

}(jQuery));