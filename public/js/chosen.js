function chosen_update() {
    $('.chosen-select').chosen({disable_search_threshold: 10});
    //$('.hour_select, .minute_select').chosen({disable_search_threshold: 1});
};

$(document).ready(function() {
    $('.mobile_select').chosen({disable_search_threshold: 1});
    chosen_update();
});
