$(document).ready(function() {

    // Adding Table Rows
    var add_form = $('.add-content').html();
    var edit_form = $('.edit-content').html();
    var view_form = $('.view-content').html();
    var inlineOpen = false;
    var rowIndex = null;
    var currentIndex = 0;
    var numCols;
    var editData = null;
    if ( $( "table" ).length ) {
        numCols = $(".table").find('tr')[0].cells.length;
    }
    // Timer
    var timeoutLoad = null;
    var timeoutMain = null;

    $('table').on('mouseleave', '.view, .status_view, .edit, .log, .run, .delete, ' +
        '.command, .configuration, .invite, .questions, .edit_question, .results, ' +
        '.edit-notification, .edit-image, .edit-url, .edit-live-feed, ' +
        '.view-notification, .view-image, .view-url, .view-live-feed, ' +
        '.parameter, .edit_command, .edit_parameter, .edit_configuration', function(){
        $(this).css('color', 'gray');
    });

    /********** Help **********/
    $('.panel-heading').on('mouseenter', '.help', function(){
        $(this).css('color', 'gray');
    });

    $('.panel-heading').on('mouseleave', '.help', function(){
        $(this).css('color', 'black');
    });

    /* ========================= FORMS ========================= */

    /********** Add Form **********/
    $("#add_form").on('click', function(){
        //if (rowIndex != currentIndex) {
            $("table tbody tr:eq(" + rowIndex + ")").remove();

            $("<tr role='row'><td colspan=" + numCols + ">" + add_form + "</td></tr>").insertBefore($('tbody tr:first').closest('tr'));

            if (timeoutLoad) {
                clearTimeout(timeoutLoad); //cancel the previous timer.
                timeoutLoad = null;
            }

            if (timeoutMain) {
                clearTimeout(timeoutMain); //cancel the previous timer.
                timeoutMain = null;
            }

            timeoutLoad = setTimeout(function(){
                $('.loader-demo').fadeOut(2000);
            }, 2000);

            timeoutMain = setTimeout(function(){
                $('.main-content').fadeIn(2000);
            }, 4000);

            rowIndex = 0;
            currentIndex = 0;
            $('form.add-form').parsley();
            chosen_update();
            maskedInput_update();
            
        //}
    });

    /********** Edit Form **********/
    mouseHoverIcon('.edit', 'orange', 'Edit');

    $('table#user-admin').on('change', '.change_status',function(){
        $formType = $(this).data('table'),
        $uid      = $(this).closest('tr').attr('id'),
        $status   = $(this).val();
        
        swal({
                title: "Do you wish change the status?",
                text: "You can change the contents in the table if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: "Yes, change it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if(isConfirm && $formType)
                {
                    $.app.ajax(
                        '/'+$formType+'/change-status', 
                        {id:$uid, status: $status},
                        'POST' ,
                        'JSON',
                        '',
                        function(response)
                        {
                            if(response.status)
                            {
                                swal('Changed!','Status has been changed', "success");
                                RefreshTable('#'+$formType,'/'+$formType+'/renderTable');
                            }
                            else
                            {
                                sweetAlert("Oops...", response.message, "error");
                            }
                        });
                }
                else
                {
                    RefreshTable('#'+$formType,'/'+$formType+'/renderTable');
                }
            });

    });

    $('table').on('click', '.edit', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        
        
        $dataTypeName = $(this).attr('data-dataTable');
        
        if($dataTypeName)
        {
            $rowId = $(this).closest('tr').attr("id");
            $tr = $(this).closest('tr');
            
            $.get('/'+$dataTypeName+'/get',
                {id : $rowId}).
                done(function(response)
                {
                    
                    $("<tr id='edit_row'><td colspan=" + numCols + "></td></tr>").insertAfter($tr);                
                    $('#edit_row>td').html(response.toString());

                    if (timeoutLoad) {
                        clearTimeout(timeoutLoad); //cancel the previous timer.
                        timeoutLoad = null;
                    }

                    if (timeoutMain) {
                        clearTimeout(timeoutMain); //cancel the previous timer.
                        timeoutMain = null;
                    }

                    timeoutLoad = setTimeout(function(){
                        $('.loader-demo').fadeOut(2000);
                    }, 2000);

                    timeoutMain = setTimeout(function(){
                        $('.main-content').fadeIn(2000);
                    }, 4000);

                    rowIndex = $(this).closest('tr').next().index();
                    if (rowIndex < 0) {
                        rowIndex = $('.table tbody tr:last').index();
                    }

                    $('form.edit-form').parsley();

                    chosen_update();
                    maskedInput_update();
                });
        }
        else
        {
            $("<tr><td colspan=" + numCols + ">" + edit_form + "</td></tr>").insertAfter($(this).closest('tr'));

            if (timeoutLoad) {
                clearTimeout(timeoutLoad); //cancel the previous timer.
                timeoutLoad = null;
            }

            if (timeoutMain) {
                clearTimeout(timeoutMain); //cancel the previous timer.
                timeoutMain = null;
            }

            timeoutLoad = setTimeout(function(){
                $('.loader-demo').fadeOut(2000);
            }, 2000);

            timeoutMain = setTimeout(function(){
                $('.main-content').fadeIn(2000);
            }, 4000);

            rowIndex = $(this).closest('tr').next().index();
            if (rowIndex < 0) {
                rowIndex = $('.table tbody tr:last').index();
            }

            chosen_update();
            maskedInput_update();
        }
    });
    /********** View Form **********/
    mouseHoverIcon('.view', 'blue', 'View');

    $('table').on('click', '.view', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + view_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }

        chosen_update();
        maskedInput_update();
    });

    /********** Status Viewer Form **********/
    var status_view_form = $('.status-view-content').html();

    mouseHoverIcon('.status_view', 'blue', 'Status View');

    $('table').on('click', '.status_view', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + status_view_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }

        chosen_update();
        maskedInput_update();
    });

    /********** Log Form **********/
    var log_form = $('.log-content').html();

    mouseHoverIcon('.log', 'green', 'Log');

    $('table').on('click', '.log', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + log_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }

        chosen_update();
        maskedInput_update();
    });

    /********** Run **********/
    mouseHoverIcon('.run', 'red', 'Run Command');

    /* ========================= BUTTON SET ========================= */
    $('table').on('mouseenter', '.buttonAdd, .buttonUpdate, .buttonCancel, .buttonClose, ' +
        '.buttonInvite, .buttonSave', function(){
        $(this).css('background-color', 'yellow');
        $(this).css('color', 'black');
    });

    $('.buttonSubmit').mouseenter(function(){
        $(this).css('background-color', 'yellow');
        $(this).css('color', 'black');
    });

    $('.buttonSubmit').mouseleave(function(){
        $(this).css('background-color', '#656565');
        $(this).css('color', 'white');
    });

    /********** Submitting **********/
    $('#add-form').submit(function(){
    });

    /********** Add Button **********/
    mouseLeaveButton('.buttonAdd', '#5D9CEC');

    $('table').on('click', '.buttonAdd', function(){
        $formType = $(this).data('form');
        $form = $(this).closest('form');
        if ($(".add-form").valid()) {
            swal({
                title: "Are you sure you want to add this entry?",
                text: "You can change the contents in the table if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: "Yes, add it!",
                closeOnConfirm: false
            }, function () {
                if($formType)
                {
                    $.ajax({
                        url        : '/' + $formType + '/save',
                        type       : 'POST',
                        data       : new FormData($form[0]),
                        dataType   : 'json',
                        async      : false,
                        cache      : false,
                        contentType: false,
                        processData: false,
                        success    : function (response) {
                            if (response.status) {
                                swal('Added!', response.message, "success");
                                RefreshTable('#' + $formType, '/' + $formType + '/renderTable');
                            }
                            else {
                                sweetAlert("Oops...", response.message, "error");
                            }
                        }
                    });
                }
                else
                    swal("congratulations!", "Your entry has been add into the table.", "success");
            });
        }
        $('tr input:invalid').css('border', '1px solid red');
        $('tr input:valid').css('border', '1px solid green');

        $('tr input:invalid').siblings('.error_message').show();
        $('tr input:valid').siblings('.error_message').hide();

        $('tr textarea:invalid').css('border', '1px solid red');
        $('tr textarea:valid').css('border', '1px solid green');

        $('tr textarea:invalid').siblings('.error_message').show();
        $('tr textarea:valid').siblings('.error_message').hide();

        event.preventDefault();
    });

    /********** Update Button **********/
    mouseLeaveButton('.buttonUpdate', 'green');

    $('table').on('click', '.buttonUpdate', function(){
        $formType = $(this).data('form');
        $form = $(this).closest('form');

        if ($(".edit-form").valid()) {
            swal({
                title: "Are you sure you want to update this entry?",
                text: "You can change the contents in the table if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: "Yes, add it!",
                closeOnConfirm: false
            }, function () {
                if($formType)
                {
                    $.ajax({
                        url        : '/' + $formType + '/save',
                        type       : 'POST',
                        data       : new FormData($form[0]),
                        dataType   : 'json',
                        async      : false,
                        cache      : false,
                        contentType: false,
                        processData: false,
                        success    : function (response) {
                            if (response.status) {
                                swal('Updated!', response.message, "success");
                                RefreshTable('#' + $formType, '/' + $formType + '/renderTable');
                            }
                            else {
                                sweetAlert("Oops...", response.message, "error");
                            }
                        }
                    });
                }
                else
                    swal("congratulations!", "Your entry has been updated into the table.", "success");
            });
        }

        $('tr input:invalid').css('border', '1px solid red');
        $('tr input:valid').css('border', '1px solid green');

        $('tr input:invalid').siblings('.error_message').show();
        $('tr input:valid').siblings('.error_message').hide();

        $('tr textarea:invalid').css('border', '1px solid red');
        $('tr textarea:valid').css('border', '1px solid green');

        $('tr textarea:invalid').siblings('.error_message').show();
        $('tr textarea:valid').siblings('.error_message').hide();

        // event.preventDefault();
    });

    /********** Delete Button **********/
    mouseHoverIcon('.delete', 'red', 'Delete');

    $('table').on('click', '.delete', function(){
        var deleteThis = $(this).closest('tr'),
            $formType = $(this).attr('data-dataTable');
        
        swal({
            title: "Are you sure you want to delete?",
            text: "You will not be able to recover this.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function(){
            if($formType)
                {
                    $.app.ajax(
                        '/'+$formType+'/delete',
                        {
                            id: deleteThis.attr('id')
                        },
                        'POST',
                        'json',
                        '',
                        function(response)
                        {
                            if(response.status)
                            {
                                swal('Deleted!','Your entry has been been deleted.', "success");
                                RefreshTable('#'+$formType,'/'+$formType+'/renderTable');
                            }
                            else
                            {
                                sweetAlert("Oops...", response.message, "error");
                            }
                        }
                    );
                }
                else
                {
                    swal("Deleted!", "Your entry has been been deleted.", "success");
                    deleteThis.remove();
                }
        });
    });

    /********** Close Button **********/
    mouseLeaveButton('.buttonClose', 'red');

    /********** Cancel Button **********/
    mouseLeaveButton('.buttonCancel', 'red');

    /********** Invite Button **********/
    mouseLeaveButton('.buttonInvite', 'purple');

    $('table').on('click', '.buttonInvite', function(){
        if ($(".invite-form").valid()) {
            swal({
                title: "Are you sure you want to invite this person?",
                text: "You will not be able to cancel this request.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, invite this person!",
                closeOnConfirm: false
            }, function () {
                swal("Sent!", "Your invitation has been sent.", "success");
            });
        }

        $('tr input:invalid').css('border', '1px solid red');
        $('tr input:valid').css('border', '1px solid green');

        $('tr input:invalid').siblings('.error_message').show();
        $('tr input:valid').siblings('.error_message').hide();

        event.preventDefault();
    });

    /********** Submit Button **********/
    mouseLeaveButton('.buttonSubmit', '#656565');

    $('.buttonSubmit').click(function(){
        if ($(".account-form").valid()) {
            swal({
                title: "Are you sure you want to make these changes?",
                text: "You can still change the contents again if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, update it!",
                closeOnConfirm: false
            }, function () {
                swal("Updated!", "Your entry has been been updated.", "success");
            });
        }

        $('input:invalid').css('border', '1px solid red');
        $('input:valid').css('border', '1px solid green');

        $('input:invalid').siblings('.error_message').show();
        $('input:valid').siblings('.error_message').hide();
    });

    /********** Save Button **********/
    mouseLeaveButton('.buttonSave', 'orange');

    $('table').on('click', '.buttonSave', function(){
        if ($(".edit-form").valid()) {
            swal({
                title: "Are you sure you want to save this question?",
                text: "You can still change the contents again if you want.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "green",
                confirmButtonText: "Yes, update it!",
                closeOnConfirm: false
            }, function () {
                swal("Updated!", "Your question has been been updated.", "success");
            });
        }

        $('tr input:invalid').css('border', '1px solid red');
        $('tr input:valid').css('border', '1px solid green');

        $('tr input:invalid').siblings('.error_message').show();
        $('tr input:valid').siblings('.error_message').hide();

        event.preventDefault();
    });

    /********** Cancel/Close Button **********/
    $('table').on('click','.buttonCancel, .buttonClose', function(){
        $(this).closest('tr').remove();
        rowIndex = null;
        nestedTableRowIndex = null;
        commandIndex = null;

        event.preventDefault();
    });

    /* ========================= DEVICE ADMIN ========================= */

    // --------------- Device Management --------------- //

    /********** Show/Hide Toggle **********/
    mouseHoverIcon('.required', '#2C5AB3', 'Show/Hide this attribute');
    mouseHoverIcon('.optional', '#2C5AB3', 'Show/Hide this attribute');

    $('table').on('mouseleave', '.required, .optional', function(){
        $(this).css('cursor', 'auto');
        $(this).css('color', 'blue');
    });

    /********** Required/Optional Attributes **********/
    $('table').on('click', '.required', function(){
        $('#required_attributes').toggle();
    });

    $('table').on('click', '.optional', function(){
        $('#optional_attributes').toggle();
    });

    mouseHoverIcon('#delete_selected', 'red', 'Delete selected entries');

    $('table').on('mouseleave', '#delete_selected', function(){
        $(this).css('color', 'blue');
        $(this).css('text-decoration', 'none');
        $(this).css('cursor', 'auto');
    });

    /*************** Location ***************/
    $('.panel-heading').on('mouseenter', '.fullscreen-map', function(){
        $(this).css('color', 'gray');
        $(this).css('cursor', 'pointer');
        $(this).attr('title', 'Click for a fullscreen map')
    });

    $('.panel-heading').on('mouseleave', '.fullscreen-map', function(){
        $(this).css('color', 'black');
    });

    /********** Modal Google Map **********/
    var address;
    var city;
    var state;
    var zip_code;

    function initialize(setAddress, getID) {
        var latitude;
        var longitude;

        var geocoder = new google.maps.Geocoder();
        var getAddress = address + ", " + city + ", " + state + " " + zip_code;

        geocoder.geocode( { 'address': setAddress}, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                latitude = results[0].geometry.location.lat();
                longitude = results[0].geometry.location.lng();
            }

            var mapOptions = {
                center: new google.maps.LatLng(latitude, longitude),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude)
            });

            $('#lat_value').remove();
            $('#lng_value').remove();

            $( "<h4 class='lat' id='lat_value'>" + latitude.toFixed(6) + '&deg;' + "</h4>" ).appendTo( ".lat" );
            $( "<h4 class='lng' id='lng_value'>" + longitude.toFixed(6) + '&deg;' + "</h4>" ).appendTo( ".lng" );

            marker.setMap(map);

        });
    }

    $('#myModalLarge').on('shown.bs.modal', function() {
        initialize("276 N TUSTIN ST, ORANGE, CA 92867", "mapCanvas");
        google.maps.event.trigger(map, "resize");
    });

    $('.search_button_modal').click(function(){
        address = $('.search_address').val();
        initialize(address, "mapCanvas");
        google.maps.event.trigger(map, "resize");
    });

    // --------------- Rule Management --------------- //

    /********* Add Condition **********/
    var condition_form = $("<tr>" +
                                "<td style='width: 10%'>" + "<input type='checkbox' class='checkbox_condition'>" + "</td>" +
                                "<td style='width: 20%'>" + "<input type='text' name='attribute' required='required' class='form-control' style='width: 80px'>" + "</td>" +
                                "<td style='width: 20%'>" + "<select class='chosen-select input-md form-control'><option>" + "Int" + "</option><option>" + "Float" + "</option></select>" + "</td>" +
                                "<td style='width: 20%'>" + "<input type='number' name='value' required='required' class='form-control' style='width: 80px'>" + "</td>" +
                                "<td style='width: 20%'>" + "<select class='chosen-select input-md form-control'><option>" + "And" + "</option><option>" + "Or" + "</option></select>" + "</td>" +
                                "<td style='width: 10%'>" + "<div class='icon-trash icon_row delete'></div>" + "</td>" +
                           "</tr>").html();

    $('table').on('click', '.condition_button', function(){
        $("<tr>" + condition_form + "</tr>").appendTo($(".attribute tbody"));
        chosen_update();
    });

    /********* Condition Checkboxes **********/
    $('table').on('click', '.checkbox_all_conditions', function() {

        if ($('.checkbox_all_conditions').is(':checked')) {
            $('.checkbox_condition').prop('checked', true);
        }
        else {
            $('.checkbox_condition').prop('checked', false);
        }
    });

    // --------------- Scheduled Commands --------------- //

    /********** Time Checkbox **********/
     $('table').on('click', '.end_date', function(){
        $("#end_date_text").toggle(this.checked);
    });

     $('table').on('click', '.repeat_task', function(){
        $(".time_lapse").toggle(this.checked);
    });

    /* ========================= MARKETING ========================= */

    // --------------- Surveys --------------- //
    var question_form = $('.question-wizard-content').html();

    /********** Questions **********/
    mouseHoverIcon('.questions', 'green', 'Questions');

    $('table').on('click', '.questions', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + question_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }

        chosen_update();
        maskedInput_update();
    });

    $('li').on('click', '#next', function(){
        console.log("Clicked!");
    });

    /********** Edit Question **********/
    var edit_question_form = $('.edit-question-content').html();
    var nestedTableRowIndex = null;

    mouseHoverIcon('.edit_question', 'orange', 'Edit Question');

    $('table').on('click', '.edit_question', function(){
        $("#questions tbody tr:eq(" + nestedTableRowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + edit_question_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        nestedTableRowIndex = $(this).closest('tr').next().index();
        if (nestedTableRowIndex < 0) {
            nestedTableRowIndex = $('#questions tbody tr:last').index();
        }

        chosen_update();
        maskedInput_update();
    });

    /********** Add Answer **********/
    var answer_form = $("<tr>" +
        "<td style='width: 90%'>" + "<input type='text' name='required' required='required' class='form-control' style='width: 630px'>" + "</td>" +
        "<td style='width: 10%'>" + "<div class='icon-trash icon_row delete'></div>" + "</td>" +
        "</tr>").html();

    $('table').on('click', '.add_answer', function(){
        console.log("here add_answer");
        $("<tr>" + answer_form + "</tr>").appendTo($("#answer tbody"));
        chosen_update();
    });
    $('.add_answer').click(function(){
        console.log("here add_answer");
        $("<tr>" + answer_form + "</tr>").appendTo($("#answer tbody"));
        chosen_update();
    });

    /********** Checkbox Days **********/
    $('table').on('click', '.select_all', function(){
        $('.day').attr('checked', false);
    });

    $('table').on('click', '.day', function(){
        $('.select_all').attr('checked', false);

        if ($("table input[class=day]:checked").length == 0) {
            $('.select_all').prop('checked', true);
        }
    });

    /********** Results **********/
    var results_form = $('.results-content').html();

    mouseHoverIcon('.results', 'red', 'Results');

    $('table').on('click', '.results', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + results_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }

        chosen_update();
    });


    // --------------- Ad Campaigns --------------- //

    /********** Invite **********/
    var invite_form = $('.invite-content').html();

    mouseHoverIcon('.invite', 'purple', 'Invite Guest');

    $('table').on('click', '.invite', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + invite_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    // --------------- Beacon Notification Buttons --------------- //

    /********** Notification **********/

    /***** Add *****/
    var add_notification_form = $('.add-beacon-notification').html();

    $("#add_notification_form").on('click',function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr role='row'><td colspan=" + numCols + ">" + add_notification_form + "</td></tr>").insertBefore($('tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function () {
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function () {
            $('.main-content').fadeIn(1000);
        }, 4000);

        rowIndex = 0;
    });

    /***** Edit *****/
    var edit_notification_form = $('.edit-beacon-notification').html();

    mouseHoverIcon('.edit-notification', 'orange', 'Edit');

    $('table').on('click', '.edit-notification', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + edit_notification_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(1000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    /***** View *****/
    var view_notification_form = $('.view-beacon-notification').html();

    mouseHoverIcon('.view-notification', 'blue', 'View');

    $('table').on('click', '.view-notification', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + view_notification_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }
    });

    /********** Image **********/

    /***** Add *****/
    var add_image = $('.add-beacon-image').html();

    $("#add_image_form").on('click',function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr role='row'><td colspan=" + numCols + ">" + add_image + "</td></tr>").insertBefore($('tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function () {
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function () {
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = 0;
    });

    /***** Edit *****/
    var edit_image_form = $('.edit-beacon-image').html();

    mouseHoverIcon('.edit-image', 'orange', 'Edit');

    $('table').on('click', '.edit-image', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + edit_image_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(1000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    /***** View *****/
    var view_image_form = $('.view-beacon-image').html();

    mouseHoverIcon('.view-image', 'blue', 'View');

    $('table').on('click', '.view-image', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + view_image_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }
    });

    /********** URL **********/

    /***** Add *****/
    var add_url = $('.add-beacon-url').html();

    $("#add_url_form").on('click',function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr role='row'><td colspan=" + numCols + ">" + add_url + "</td></tr>").insertBefore($('tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function () {
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function () {
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = 0;

        chosen_update();
        maskedInput_update();
    });

    /***** Edit *****/
    var edit_url_form = $('.edit-beacon-url').html();

    mouseHoverIcon('.edit-url', 'orange', 'Edit');

    $('table').on('click', '.edit-url', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + edit_url_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(1000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    /***** View *****/
    var view_url_form = $('.view-beacon-url').html();

    mouseHoverIcon('.view-url', 'blue', 'View');

    $('table').on('click', '.view-url', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + view_url_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }
    });

    /********** Live Feed **********/

    /***** Add *****/
    var add_live_feed = $('.add-beacon-live-feed').html();

    $("#add_live_feed_form").on('click',function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr role='row'><td colspan=" + numCols + ">" + add_live_feed + "</td></tr>").insertBefore($('tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function () {
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function () {
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = 0;

        chosen_update();
    });

    /***** Edit *****/
    var edit_live_feed_form = $('.edit-beacon-live-feed').html();

    mouseHoverIcon('.edit-live-feed', 'orange', 'Edit');

    $('table').on('click', '.edit-live-feed', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + edit_live_feed_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(1000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }

        chosen_update();
    });

    /***** View *****/
    var view_live_feed_form = $('.view-beacon-live-feed').html();

    mouseHoverIcon('.view-live-feed', 'blue', 'View');

    $('table').on('click', '.view-live-feed', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + view_live_feed_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tr:last').index();
        }
    });

    /* ========================= SUPER ADMIN ========================= */

    // --------------- Device Type Management --------------- //

    /********** Command **********/
    var command_form = $('.command-content').html();

    mouseHoverIcon('.command', 'black', 'Command');

    $('table').on('click', '.command', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + command_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    /***** Command Table *****/
    var commandIndex = null;

    mouseHoverIcon('.parameter', 'black', 'Parameter');

    /* Add */
    var add_command_form = $('.add-command-content').html();

    $("table").on('click', '#command_form', function(){
        $("#device_type_management_command tbody tr:eq(" + commandIndex + ")").remove();

        numCols = $("#device_type_management_command").find('tr')[0].cells.length;
        $("<tr role='row'><td colspan=" + numCols + ">" + add_command_form + "</td></tr>").insertBefore($('#device_type_management_command tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function () {
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function () {
            $('.main-content').fadeIn(2000);
        }, 4000);

        commandIndex = 0;

        chosen_update();
    });

    /* Edit */
    var edit_command_form = $('.edit-command-content').html();

    mouseHoverIcon('.edit_command', 'orange', 'Edit');

    $('table').on('click', '.edit_command', function(){
        $("#device_type_management_command tbody tr:eq(" + commandIndex + ")").remove();

        numCols = $("#device_type_management_command").find('tr')[0].cells.length;
        $("<tr><td colspan=" + numCols + ">" + edit_command_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        commandIndex = $(this).closest('tr').next().index();
        if (commandIndex < 0) {
            commandIndex = $('#device_type_management_command tbody tr:last').index();
        }

        chosen_update();
    });

    /* Parameter */
    var parameter_form = $('.parameter-content').html();

    $('table').on('click', '.parameter', function(){
        $("#device_type_management_command tbody tr:eq(" + commandIndex + ")").remove();

        numCols = $("#device_type_management_command").find('tr')[0].cells.length;
        $("<tr><td colspan=" + numCols + ">" + parameter_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        commandIndex = $(this).closest('tr').next().index();
        if (commandIndex < 0) {
            commandIndex = $('#device_type_management_command tbody tr:last').index();
        }

        chosen_update();
    });

    /***** Parameter Table *****/
    var parameterIndex = null;

    /* Edit Parameter */
    var edit_parameter_form = $('.edit-parameter-content').html();

    mouseHoverIcon('.edit_parameter', 'orange', 'Edit');

    $('table').on('click', '.edit_parameter', function(){
        $("#device_type_management_parameter tbody tr:eq(" + parameterIndex + ")").remove();

        numCols = $("#device_type_management_parameter").find('tr')[0].cells.length;
        $("<tr><td colspan=" + numCols + ">" + edit_parameter_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        parameterIndex = $(this).closest('tr').next().index();
        if (parameterIndex < 0) {
            parameterIndex = $('#device_type_management_parameter tbody tr:last').index();
        }

        chosen_update();
    });

    /********** Add Parameter **********/

    $('table').on('click', '#parameter_form', function(){

        var add_parameter_form  = $("<tr>" +
            "<td style='width: 50%'>" + $('#parameter_input').val() + "</td>" +
            "<td style='width: 25%'>" + $('#type_select').val() + "</td>" +
            "<td style='width: 25%'>" + "<div class='icon-pencil icon_row edit_parameter'></div><div class='icon-trash icon_row delete'></div>" + "</td>" +
            "</tr>").html();

        $("<tr>" + add_parameter_form + "</tr>").appendTo($("#device_type_management_parameter tbody"));
        chosen_update();
    });


    /********** Configuration **********/
    var configuration_form = $('.configuration-content').html();

    mouseHoverIcon('.configuration', 'black', 'Configuration');

    $('table').on('click', '.configuration', function(){
        $("table tbody tr:eq(" + rowIndex + ")").remove();
        $("<tr><td colspan=" + numCols + ">" + configuration_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex = $(this).closest('tr').next().index();
        if (rowIndex < 0) {
            rowIndex = $('.table tbody tr:last').index();
        }
    });

    /********** Configuration Table **********/
    var configurationIndex = null;

    /***** Add *****/
    var add_value_form = $('.add-value-content').html();

    $("table").on('click', '#configuration_form', function(){
        $("#device_type_management_configuration tbody tr:eq(" + configurationIndex + ")").remove();

        numCols = $("#device_type_management_configuration").find('tr')[0].cells.length;
        $("<tr role='row'><td colspan=" + numCols + ">" + add_value_form + "</td></tr>").insertBefore($('#device_type_management_configuration tbody tr:first').closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        configurationIndex = 0;
    });

    /***** Edit *****/
    var edit_value_form = $('.edit-value-content').html();

    mouseHoverIcon('.edit_configuration', 'orange', 'Edit');

    $('table').on('click', '.edit_configuration', function(){
        $("#device_type_management_configuration tbody tr:eq(" + configurationIndex + ")").remove();

        numCols = $("#device_type_management_configuration").find('tr')[0].cells.length;
        $("<tr><td colspan=" + numCols + ">" + edit_value_form + "</td></tr>").insertAfter($(this).closest('tr'));

        if (timeoutLoad) {
            clearTimeout(timeoutLoad); //cancel the previous timer.
            timeoutLoad = null;
        }

        if (timeoutMain) {
            clearTimeout(timeoutMain); //cancel the previous timer.
            timeoutMain = null;
        }

        timeoutLoad = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        configurationIndex = $(this).closest('tr').next().index();
        if (configurationIndex < 0) {
            configurationIndex = $('#device_type_management_configuration tbody tr:last').index();
        }
    });
});

function mouseHoverIcon(icon, color, title) {
    $('table').on('mouseenter', icon, function(){
        $(this).css('color', color);
        $(this).css('cursor', 'pointer');
        $(this).attr('title', title);
    });
}

function mouseLeaveButton(button, backgroundColor) {
    $('table').on('mouseleave', button, function(){
        $(this).css('background-color', backgroundColor);
        $(this).css('color', 'white');
    });
}

function RefreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
}


/*
function add_an_entry(rowIndex, add_button, numCols, form, table, timeoutLoad, timeoutMain) {

    $(add_button).on('click', function(){

        $(table + " tbody tr:eq(" + rowIndex.value + ")").remove();
        $("<tr role='row'><td colspan=" + numCols + ">" + form + "</td></tr>").insertBefore($(table + ' tbody tr:first').closest('tr'));

        if (timeoutLoad.value) {
            clearTimeout(timeoutLoad.value); //cancel the previous timer.
            timeoutLoad.value = null;
        }

        if (timeoutMain.value) {
            clearTimeout(timeoutMain.value); //cancel the previous timer.
            timeoutMain.value = null;
        }

        timeoutLoad.value = setTimeout(function(){
            $('.loader-demo').fadeOut(2000);
        }, 2000);

        timeoutMain.value = setTimeout(function(){
            $('.main-content').fadeIn(2000);
        }, 4000);

        rowIndex.value = 0;

        chosen_update();
        maskedInput_update();
    });
}
*/