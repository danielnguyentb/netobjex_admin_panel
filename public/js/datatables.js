// Demo datatables
// -----------------------------------

(function(window, document, $, undefined){

    $(function(){
        console.log('kaka');
        //
        // AJAX
        //

        $('#beacon_visits').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/datatable.json',
            aoColumns: [
                { mData: 'beacon' },
                { mData: 'location' },
                { mData: 'visits' }
            ]
        });

        $('#group_management').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/group_management.json',
            aoColumns: [
                { mData: 'group_name' },
                { mData: 'action'}
            ]
        });

        //$('#enterprise_gateway').dataTable({
        //    'paging':   true,  // Table pagination
        //    'ordering': true,  // Column ordering
        //    'info':     true,  // Bottom left status text
        //    'ajax':  '/enterprise-gateway/renderTable',
        //    aoColumns: [
        //        { mData: 'name' },
        //        { mData: 'type' },
        //        { mData: 'action'}
        //    ]
        //});

        $('#beacon').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            //sAjaxSource: 'server/beacon.json',
            'ajax':  '/beacon-management/renderTable',
            aoColumns: [
                { mData: 'name' },
                { mData: 'uuid' },
                { mData: 'major_id' },
                { mData: 'minor_id' },
                { mData: 'status' },
                { mData: 'lost' },
                { mData: 'action'}
            ]
        });

        $('#device').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/device.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#beaconset').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beaconset.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'id' },
                { mData: 'major_id' },
                { mData: 'minor_id' },
                { mData: 'action'}
            ]
        });

        $('#location').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/location.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'address' },
                { mData: 'city' },
                { mData: 'state' },
                { mData: 'zip' },
                { mData: 'action'}
            ]
        });

        $('#feed').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/feed.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'url' },
                { mData: 'type' },
                { mData: 'user_id' },
                { mData: 'interval' },
                { mData: 'action'}
            ]
        });

        $('#attribute').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/attribute.json',
            aoColumns: [
                { mData: 'checkbox' },
                { mData: 'attribute' },
                { mData: 'operator' },
                { mData: 'value' },
                { mData: 'and/or' },
                { mData: 'action'}
            ]
        });

        $('#rule').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/rule.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'enterprise_gateway' },
                { mData: 'action'}
            ]
        });

        $('#scheduled_commands').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/scheduled_commands.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#survey').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/surveys.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'num_of_questions' },
                { mData: 'start_date' },
                { mData: 'end_date' },
                { mData: 'published' },
                { mData: 'action'}
            ]
        });

        $('#ad_campaign').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/ad_campaigns.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'type' },
                { mData: 'action'}
            ]
        });

        $('#beacon_campaigns').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beacon_campaigns.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'beacon' },
                { mData: 'start_date' },
                { mData: 'end_date' },
                { mData: 'action'}
            ]
        });

        $('#beacon_notifications').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beacon_notifications.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'description' },
                { mData: 'notification_type' },
                { mData: 'action'}
            ]
        });

        $('#user-admin').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: '/user-admin/renderTable',
            aoColumns: [
                { mData: 'id' },
                { mData: 'email' },
                { mData: 'organization' },
                { mData: 'signup_date' },
                { mData: 'role' },
                { mData: 'status' },
                { mData: 'action'}
            ]
        });

        //$('#organization-editor').dataTable({
        //    paging:   true,  // Table pagination
        //    ordering: true,  // Column ordering
        //    info:     true,  // Bottom left status text
        //    ajax: '/organization-editor/renderTable',
        //    aoColumns: [
        //        { mData: 'name' },
        //        { mData: 'action'}
        //    ]
        //});

        $('#device_type_management').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/device_type_management.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#protocol-management').dataTable({
            'paging':   true,
            'ordering': true,   
            'info':     true,
            'ajax':  '/protocol-management/renderTable',
            'aoColumns': [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });
    });

})(window, document, window.jQuery);
