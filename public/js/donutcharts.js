// DASHBOARD - CHART DONUT
// -----------------------------------
(function(window, document, $, undefined){

    $(function(){

        // Chart 1 (Visits by OS)
        var data1 = [
            { "color" : "#39C558",
                "data" : 60,
                "label" : "Android"
            },
            { "color" : "#00b4ff",
                "data" : 90,
                "label" : "iOS"
            },
            { "color" : "#FFBE41",
                "data" : 20,
                "label" : "Windows"
            }
        ];

        // Chart 2 (Visits by Location)
        var data2 = [
            { "color" : "#39C558",
                "data" : 60,
                "label" : "Los Angeles"
            },
            { "color" : "#00b4ff",
                "data" : 90,
                "label" : "San Francisco"
            },
            { "color" : "#FFBE41",
                "data" : 50,
                "label" : "New York"
            },
            { "color" : "#ff3e43",
                "data" : 80,
                "label" : "Singapore"
            },
            { "color" : "#937fc7",
                "data" : 116,
                "label" : "Israel"
            }
        ];

        var options = {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.5 // This makes the donut shape
                }
            }
        };

        var chart1 = $('.chart-donut-1');
        var chart2 = $('.chart-donut-2');

        if (chart1.length) {
            $.plot(chart1, data1, options);
        }

        if (chart2.length) {
            $.plot(chart2, data2, options);
        }

    });

})(window, document, window.jQuery);