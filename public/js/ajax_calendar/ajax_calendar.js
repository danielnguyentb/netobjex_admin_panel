$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();

    var monthKeys = {
        January : '01',
        February : '02',
        March : '03',
        April : '04',
        May : '05',
        June : '06',
        July : '07',
        August : '08',
        September : '09',
        October : '10',
        November : '11',
        December : '12'
    }

    dd = (dd < 10) ? '0'+dd: dd;
    mm = (mm < 10) ? '0'+mm: mm;
    today = yyyy+'-'+mm+'-'+dd;

    callAjax('/beacon-campaigns/get-calender-data/'+today, 'GET', {}, function(data) {
        genCaleder(data);
        beinsport.init();
    } );

    $(".epg-reload").on('click', function () {
        var yearMotn = $('#current_month');
        var year = yearMotn.find('#year').text();
        var month = yearMotn.find('#month').text();
        var day = $(this).find('.date_number').text();
        day = (day.length ==1) ?'0'+day:day;
        var atDay = year+'-'+monthKeys[month]+'-'+day;
        callAjax('/beacon-campaigns/get-calender-data/'+atDay, 'GET', {}, function(data) {
            genCaleder(data);
            beinsport.init();
        } );
    });
});

function genCaleder( data ) {
    var len = $.map(data, function(n, i) { return i; }).length;
    var string = '<ul class="prog_list"><input type="hidden" value="2015-09-11" class="current_date">' ;
    for (var i = 1 ; i <= len ; i++ ){
        var obj2 =  data['m'+i];
        //console.log(obj2);
        string = string + ' <li class=""><div class="channel_visu red_bg"><a href="#"><span class="logo_chan with_play"><div class="calendarMachine"><b>M1</b></div><br></span></a></div><div class="programmes_items"><div class="progs_wrapper"><div class="prog_items_cover clearfix" style="margin-left: 0px; width: 7497px;">';
        var len2 = $.map(obj2, function(n, i) { return i; }).length;
        for (var j = 1 ; j <= len2 ; j++ ){
            string = string + ' <div data-position="1" data-old-start-time="' + obj2['time-'+j].old_start_time + '" data-old-end-time="' + obj2['time-'+j].old_end_time + '" data-start-time="' + obj2['time-'+j].start_time  + '" data-end-time="' + obj2['time-'+j].end_time  + '" data-genre="' + obj2['time-'+j].data_genre  + '" class="prog_item" style="width: 441px;"><a href="#"><span class="top_infos"><span class="prog_visu"><img src="img/logo-single.png" alt="" style="background-color: #00AEEF;"></span><span class="prog_infos"><span title="' + obj2['time-'+j].span_title + '" class="prog_title">' + obj2['time-'+j].prog_title + '</span><br><span class="prog_type">' + obj2['time-'+j].prog_type + '</span></span></span><span class="bottom_infos"><span class="prog_status"></span><span data-old-start-time="' + obj2['time-'+j].old_start_time + '" data-old-end-time="' + obj2['time-'+j].old_end_time + '" data-start-time="' + obj2['time-'+j].start_time  + '" data-end-time="' + obj2['time-'+j].end_time  + '" class="prog_period"><span class="prog_hour">' + obj2['time-'+j].prog_hour_begin  + '</span><span class="prog_period_mask"><span style="width: 100%; background-color: rgb(2, 134, 183);">&nbsp;</span></span><span class="prog_hour">' + obj2['time-'+j].prog_hour_end  + '</span></span></span></a><span class="trending_tweets_symbole"></span></div>';
            //string = string + ' <div data-positi data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00" data-start-time="2015-09-11 03:00:00" data-end-time="2015-09-11 04:00:00" data-genre="Sports, non-event" class="prog_item"  441px;"><a href="#"><span class="top_infos"><span class="prog_visu"><img src="img/logo-single.png" alt=""  #00AEEF;"></span><span class="prog_infos"><span title="Real Madrid TV" class="prog_title">Advertisement 1</span><br><span class="prog_type">Chase, BOA</span></span></span><span class="bottom_infos"><span class="prog_status"></span><span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00" data-start-time="2015-09-11 03:00:00" data-end-time="2015-09-11 04:00:00" class="prog_period"><span class="prog_hour">03:00</span><span class="prog_period_mask"><span  100%; background-color: rgb(2, 134, 183);">&nbsp;</span></span><span class="prog_hour">04:00</span></span></span></a><span class="trending_tweets_symbole"></span></div>';
            // console.log(obj2['time-'+j]);
        }
        string = string +  '</div><a href="#" class="slide_next"><span class="arrows_symbole"></span><span class="next_link_lbl">Next</span></a></div></div></li>';
        //console.log(data['m'+i]) ;
    }
    string = string +  '</ul>';
    $('ul.prog_list').replaceWith(string);
    //$('ul.prog_list').html(string);
}

function callAjax(url,method, data, callback) {
    $.ajax({
        url: url,
        data: data,
        type: method,
        dataType : "json",
        success: function( response ) {
            callback(response);
        }, error: function( xhr, status, errorThrown ) {

        }
    });
}

var beinsport = {};
beinsport = {
    dom: {
        daysListTabs: $('#bein_root .days_list a'),
        programmeListTabs: $('#bein_root .tabs_options > a'),
        paginationTabs: $('.pagination_list a'),
        dropDownList: $('#bein_root .dropbdown_list'),
        timeLineSliderContainer: $('#bein_root .time_line_slider')
    },
    sliders: {
        miniSliders: [],
        slick: null
    },
    vars: {
        currentLang: $('.current_lang').val(),
        currentRegion: $('.current_region').val(),
        baseUrl: $('.baseUrl').val(),
        isMobile: navigator.userAgent.match(/iPad|iPhone|iPod|Android|Samsung/ig),
        flag: true
    },
    init: function () {
        // calling the functions bellow
        this.tabsActions();
        this.progsMiniSlider();
        this.styledDropDown();
        this.timeLineSlider();
        this.progressBar();
        this.tweetsStyler();
        $(window).on('resize', this.progsMiniSlider);
    },
    tabsActions: function () {
        beinsport.dom.daysListTabs.on('click', function () {
            beinsport.dom.daysListTabs.removeClass('active');
            $(this).addClass('active');
            return false;
        });

        beinsport.dom.paginationTabs.on('click', function () {
            beinsport.dom.paginationTabs.removeClass('active');
            $(this).addClass('active');
            return false;
        })
    },
    styledDropDown: function () {
        noResultsText = "";
        switch (beinsport.vars.currentLang) {
            case "ar":
                noResultsText = "????? ?? ??? ?????!";
                break;
            case "fr":
                noResultsText = "Oops, rien trouv�!";
                break;
            case "en":
                noResultsText = "Oops, nothing found !";
                break;
        }
        //beinsport.dom.dropDownList.chosen({
        //    disable_search_threshold: 50,
        //    no_results_text: noResultsText
        //});
    },
    tweetsStyler: function () {
        if ($('.prog_tweet_area').length) {
            var checkExist = setInterval(function () {
                $('.prog_tweet_area > iframe').contents().find('body').append('<style>.EmbeddedTweet-tweet{padding:10px !important;width: 96%;}.EmbeddedTweet{border-radius: 0 !important;border-color:#dbdbdb;max-width: 100%;min-height:139px;}.Tweet-text{font-family:arial !important;font-size:12px !important;}.Tweet-header{margin-bottom:7px !important;}.Tweet-metadata{font-size:12px !important;}.Tweet-actions{margin-top: 0px !important;}</style>');
                clearInterval(checkExist);
                $('.prog_tweet_area').show();
            }, 600);
        }
    },
    progsMiniSlider: function () {
        i = 0;
        $('.prog_items_cover').each(function () {
            beinsport.sliders.miniSliders[i] = $(this).progsMiniSlider();
            i++;
        })
    },
    progressBar: function () {
        $('.prog_period').prograssBar();
    },
    timeLineSlider: function () {
        var dt = new Date();
        var start_slide, start_hour;
        $('#bein_root .time_slide').each(function (index) {
            var slide_hour = $.trim($(this).data('hour'));
            var sys_hour;
            if (dt.getHours() < 10) {
                sys_hour = $.trim('0' + dt.getHours() + ':00');
            } else {
                sys_hour = $.trim(dt.getHours() + ':00');
            }

            if (slide_hour == sys_hour) {
                start_slide = index;
                start_hour = slide_hour;
                console.log("current slide now : ", slide_hour, " ", index);
                $(this).addClass('now_hour').text($('.now_text').text());

            }
        });
        beinsport.dom.timeLineSliderContainer.on('init', function (event, slick) {
            beinsport.slideToTime(start_hour);
        });

    },
    searchByDay: function (date_search) {
        console.log(123);
        console.log('baseUrl', this.vars);

        var today = new Date();
        var isdst = today.dst();
        var tzo = today.getTimezoneOffset();

        $("#widget-loading").show({
            duration: 400,
            easing: "swing"
        });
        var url = "server/ajax_calendar/9-11-2015.json" ;
        console.log("this.vars.baseUrl" + this.vars.baseUrl);
        callAjax('/beacon-campaigns/get-calender-data/2016-01-18', 'GET', {}, function(data) {
            genCaleder(data);
        } );
    },
    filterProgramByTime: function (currentTime) {
        currentTime = $('.current_date').val()+' '+currentTime;
        //currentTime = moment(currentTime, 'YYYY-MM-DD h:mma');

        $(".prog_list li").each(function (progSliderIndex) {
            var selfSlider = this;
            //currentTime = moment(currentTime, 'h:mma');
            currentTime = moment(currentTime, 'YYYY-MM-DD h:mma');
            $(this).find(".prog_item").each(function (index) {
                var program = this;
                var startTime = $(program).data("start-time");
                var endTime = $(program).data("end-time");
                startTime = moment(startTime);
                endTime = moment(endTime);
                if (startTime.isSame(currentTime) || (currentTime.isAfter(startTime)) && endTime.isAfter(currentTime)) {
                    beinsport.sliders.miniSliders[progSliderIndex].slideTo(index + 1);
                    return false;
                }
            });
        });
    },
    slideToTime: function (start_hour) {
        setTimeout(function () {
            for (i = 0; i < beinsport.sliders.miniSliders.length; i++) {
                console.log(start_hour);
                beinsport.filterProgramByTime(start_hour);
            }
        }, 500);
    }
};

