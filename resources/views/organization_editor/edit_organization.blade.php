<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Organization') : trans('core.Edit Organization') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}

            @if($form == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11" style="margin-left: -3px;">
                                {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                <h5 class="error_message id_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Organization Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11" style="margin-left: -3px;">
                                {!! Form::text('organization_name', $name, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message organization_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Address1') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('address1', $address1, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message address_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Address2') !!} </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('address2', $address2, ['class' => 'form-control form-format']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.City') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('city', $city, ['class' => 'form-control form-format', 'required']) !!}
                            <h5 class="error_message city_error"> </h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Zip Code') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('zip_code', $zip, ['class' => 'form-control form-format', 'required']) !!}
                            <h5 class="error_message zip_code_error" style="margin-left: -37px"> </h5>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Country') !!} </label>
                            </div>
                            <div class="col-md-10" style="width: 72%">
                                {!! Form::select('country', $countriesList, $country, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.State') !!} </label>
                            </div>
                            <div class="col-md-10" style="width: 72%">
                                {!! Form::select('state', $statesList, $state, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Email') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::email('email', $email, ['class' => 'form-control form-format', 'required', 'data-parsley-type' => 'email']) !!}
                                <h5 class="error_message email_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-2">
                            <label> {!! trans('core.Phone') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::tel('phone', $phone, ['class' => 'form-control form-format', 'required', 'data-inputmask' => '"mask": "999-999-9999"', 'placeholder' => '000-000-0000']) !!}
                            <h5 class="error_message phone_number_error"> </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Image') !!}</label>
                            </div>
                            <div class="col-md-11" style="margin-left: -3px;">
                                {!! Form::file('image', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                <h5 class="error_message image_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label>&nbsp;</label>
                            </div>
                            <div class="col-md-11" style="margin-left: -3px;">
                                {!! $logofileid != '' ? Html::image(genDownloadUrl($logofileid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($form == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                            <label>{!! trans('core.Dashboard URL') !!}</label>
                            </div>
                            <div class="col-md-11" style="margin-left: -3px;">
                                <!-- {!! $logofileid != '' ? Html::image(genDownloadUrl($logofileid), '', ['style' => 'height: 100px; width: auto;']) : '' !!} -->
                                {!! Form::textarea('kibanaurl', $kibanaurl, ['class'=>'form-control textarea', 'style' => 'width: 90%;']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>