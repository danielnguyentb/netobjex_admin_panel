<div class="main-content ">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.Update DeviceSet') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> Role<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-5">
                                    <select class="chosen-select input-md form-control form-format">
                                            <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> Class <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-5">
                                    <select class="chosen-select input-md form-control form-format">
                                            <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> Access Control <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <label class="checkbox-inline c-checkbox">
                                    <input type="checkbox" value="option1" id="inlineCheckbox10">
                                    <span class="fa fa-check"></span>Edit</label>
                                    <label class="checkbox-inline c-checkbox">
                                    <input type="checkbox" value="option1" id="inlineCheckbox20">
                                    <span class="fa fa-check"></span>View</label>
                                    <label class="checkbox-inline c-checkbox">
                                    <input type="checkbox" value="option1" id="inlineCheckbox30">
                                    <span class="fa fa-check"></span>Delete</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> Default Access to Objects <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <label class="radio-inline c-radio">
                                         <input type="radio" checked="" value="option1" name="access" >
                                         <span class="fa fa-circle"></span>All</label>
                                      <label class="radio-inline c-radio">
                                         <input type="radio" value="option2" name="access">
                                         <span class="fa fa-circle"></span>None</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) required </h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

