@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')

    <div class="content-wrapper">
        <h3> Privilege Admin </h3>
        <div class="container-fluid">
             <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">Group Admin</a></li>
                    <li class="active">Permissions Admin</li>
                </ul>

            </div>
            <div class="row">
              <div class="col-md-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="pt-5"> Class </label>
                            </div>
                            <div class="col-md-8">
                                <select  class="chosen-select form-control form-format classId" name="classId" style="min-width: 100%">
                                    @foreach($classes as $k=>$v)
                                        <option value="{!! $v->id!!}">{!! $v->name !!} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="pt-5"> User </label>
                            </div>
                            <div class="col-md-8">
                                <select class="chosen-select  form-control form-format userId" name="userId" style="min-width: 100%">
                                    @foreach($users as $k=>$v)
                                        <option value="{!! $v->id!!}">{!! $v->firstname !!} {!! $v->lastname!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pt-5" >
                    <button type="button" class="mb-sm btn btn-primary add-button" style="float:left;">
                    <b>Show Permissions</b>
                </button>
                </div>

            </div>

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Permissions Admin
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="permissions" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:70%">Name</th>
                                        <th style="width:10%">Edit</th>
                                        <th style="width:10%">View</th>
                                        <th style="width:10%">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->
        </div>
    </div>

@stop


@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/permissions_admin.js') !!}
@stop
