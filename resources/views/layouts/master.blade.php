<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App + jQuery">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <base href="{!! URL::to('') !!}">
    <title>{!! trans('core.NetObjex') . (!empty($title) ? " | $title" : '') !!}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{!! asset('favicons') !!}/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{!! asset('favicons') !!}/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{!! asset('favicons') !!}/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{!! asset('favicons') !!}/apple-touch-icon-76x76.png">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="{!! asset('favicons') !!}/manifest.json">
    <link rel="mask-icon" href="{!! asset('favicons') !!}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    {!! Html::style('vendor/fontawesome/css/font-awesome.min.css') !!}
    <!-- SIMPLE LINE ICONS-->
    {!! Html::style('vendor/simple-line-icons/css/simple-line-icons.css') !!}
    <!-- ANIMATE.CSS-->
    {!! Html::style('vendor/animate.css/animate.min.css') !!}
    <!-- WHIRL (spinners)-->
    {!! Html::style('vendor/whirl/dist/whirl.css') !!}
    {!! Html::style('vendor/nprogress/nprogress.css') !!}
    {!! Html::style('vendor/sweetalert/dist/sweetalert.css') !!}
    <!-- =============== PAGE VENDOR STYLES ===============-->
    @section('pageCss')
    @show
    <!-- =============== BOOTSTRAP STYLES ===============-->
    {!! Html::style('css/bootstrap.css', array('id' => 'bscss')) !!}
    <!-- =============== APP STYLES ===============-->
    {!! Html::style('css/app.css', array('id' => 'maincss')) !!}
    {!! Html::style('css/blockUI.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('css/app_custom.css') !!}

    <!-- JQUERY GROWL -->
    {!! Html::style('css/jquery.growl/jquery.growl.css') !!}
    <!-- JQUERY-->
    {!! Html::script('vendor/jquery/dist/jquery.js') !!}
</head>

<body>

<div class="wrapper">
    <!-------------------- TOP NAVBAR -------------------->
    <header class="topnavbar-wrapper">
        <!-- START Top Navbar-->
        <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a href="{!! URL::to('') !!}" class="navbar-brand">
                    <div class="brand-logo">
                        <img src="{!! URL::to('img/logo.png') !!}" alt="App Logo" class="img-responsive">
                    </div>
                    <div class="brand-logo-collapsed">
                        <img src="{!! URL::to('img/logo-single.png') !!}" alt="App Logo" class="img-responsive">
                    </div>
                </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
                <!-- START Left navbar-->
                <ul class="nav navbar-nav">
                    <li>
                        <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                        <a href="javascript:void(0)" data-toggle-state="aside-collapsed" class="hidden-xs">
                            <em class="fa fa-navicon"></em>
                        </a>
                        <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                        <a href="javascript:void(0)" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                            <em class="fa fa-navicon"></em>
                        </a>
                    </li>
                    <!-- START User avatar toggle-->
                    <li>
                        <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                        <a id="user-block-toggle" href="#user-block" data-toggle="collapse">
                            <em class="icon-user"></em>
                        </a>
                    </li>
                    <!-- END User avatar toggle-->
                    <li>
                        <a href="{!! action('UserController@logout') !!}">
                            <div class="logout">
                                <em class="icon-logout"></em> {!! trans('core.Logout') !!}
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- END Left navbar-->
                <!-- START Right Navbar-->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <div class="row">
                            <div class="col-md-5" style="width: 28%">
                                <h5> {!! trans('core.On Behalf of Organization:') !!} </h5>
                            </div>
                            <div class="col-md-7 behalf_select">
                                {!! $orgListHtml !!}
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row" style="margin-right: 30px">
                            <div class="col-md-2" style="width: 70px">
                                <h5> {!! trans('core.User') !!}: </h5>
                            </div>
                            <div class="col-md-10 behalf_select">
                                <select id="userSelect" class="chosen-select input-md form-control">
                                    <option value="">Select User</option>
                                </select>
                            </div>
                        </div>
                    </li>
                    <!-- Fullscreen (only desktops)-->
                    <li class="visible-lg">
                        <a href="#" data-toggle-fullscreen="">
                            <em class="fa fa-expand"></em>
                        </a>
                    </li>
                </ul>
                <!-- END Right Navbar-->
            </div>
            <!-- END Nav wrapper-->
            <!-- START Search form-->
            <form role="search" action="search.html" class="navbar-form">
                <div class="form-group has-feedback">
                    <input type="text" placeholder="{!! trans('core.Type and hit enter ...') !!}" class="form-control">
                    <div data-search-dismiss="" class="fa fa-times form-control-feedback"></div>
                </div>
                <button type="submit" class="hidden btn btn-default">{!! trans('core.Submit') !!}</button>
            </form>
            <!-- END Search form-->
        </nav>
        <!-- END Top Navbar-->
    </header>
    <!--------------------/ TOP NAVBAR -------------------->

    <!-------------------- SIDEBAR -------------------->
    <aside class="aside">
        <!-- START Sidebar (left)-->
        <div class="aside-inner">
            <nav data-sidebar-anyclick-close="" class="sidebar">
                <!-- START sidebar nav-->
                <ul class="nav">
                    <!--------------- USER INFO --------------->
                    <li class="has-user-block">
                        <div id="user-block" class="collapse">
                            <div class="item user-block">
                                <!-- User picture-->
                                <div class="user-block-picture">
                                    <div class="user-block-status">
                                        <img src="{!! URL::to('img/user/02.jpg') !!}" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                                        <div class="circle circle-success circle-lg"></div>
                                    </div>
                                </div>
                                <!-- Name and Job-->
                                <div class="user-block-info">
                                    <span class="user-block-name">{!! trans('core.Hello, :name', ['name' => 'Mike']) !!}</span>
                                    <span class="user-block-role">{!! trans('core.Designer') !!}</span>
                                </div>
                                <div id="mobile_form">
                                    <div id="mobile_behalf">
                                        <h5> {!! trans('core.On Behalf of Organization:') !!} </h5>
                                        <select class="mobile_select chosen-select input-md form-control">
                                            <option>{!! trans('core.Demo Organization') !!}</option>
                                            <option>{!! trans('core.GoCharge') !!}</option>
                                        </select>
                                    </div>
                                    <div id="mobile_user">
                                        <h5> User: </h5>
                                        <select class="mobile_select chosen-select input-md form-control">
                                            <option>kevin@ocwebkings.com</option>
                                            <option>socialcontact69@gmail.com</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!---------------/ USER INFO --------------->
                    <!--------------- Iterates over all sidebar items --------------->
                    <li class="nav-heading ">
                        <span data-localize="sidebar.heading.HEADER">{!! trans('core.Main Navigation') !!}</span>
                    </li>

                    {!! $sideBarHtml !!}

                    <!---------------/ Iterates over all sidebar items --------------->
                </ul>
                <!-- END sidebar nav-->
            </nav>
        </div>
        <!-- END Sidebar (left)-->
    </aside>
    <!--------------------/ SIDEBAR -------------------->

    <!-------------------- SIDEBAR -------------------->
    <aside class="offsidebar hide">
        <!-- START Off Sidebar (right)-->
        <nav>
            <div role="tabpanel">
                <!-- Nav tabs-->
                <ul role="tablist" class="nav nav-tabs nav-justified">
                    <li role="presentation" class="active">
                        <a href="#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab">
                            <em class="icon-equalizer fa-lg"></em>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab">
                            <em class="icon-users fa-lg"></em>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes-->
                <div class="tab-content">
                    <div id="app-settings" role="tabpanel" class="tab-pane fade in active">
                        <h3 class="text-center text-thin">{!! trans('core.Settings') !!}</h3>
                        <div class="p">
                            <h4 class="text-muted text-thin">{!! trans('core.Themes') !!}</h4>
                            <div class="table-grid mb">
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-a.css">
                                            <input type="radio" name="setting-theme" checked="checked">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-info"></span>
                                       <span class="color bg-info-light"></span>
                                    </span>
                                            <span class="color bg-white"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-b.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-green"></span>
                                       <span class="color bg-green-light"></span>
                                    </span>
                                            <span class="color bg-white"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-c.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-purple"></span>
                                       <span class="color bg-purple-light"></span>
                                    </span>
                                            <span class="color bg-white"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-d.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-danger"></span>
                                       <span class="color bg-danger-light"></span>
                                    </span>
                                            <span class="color bg-white"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="table-grid mb">
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-e.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-info-dark"></span>
                                       <span class="color bg-info"></span>
                                    </span>
                                            <span class="color bg-gray-dark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-f.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-green-dark"></span>
                                       <span class="color bg-green"></span>
                                    </span>
                                            <span class="color bg-gray-dark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-g.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-purple-dark"></span>
                                       <span class="color bg-purple"></span>
                                    </span>
                                            <span class="color bg-gray-dark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col mb">
                                    <div class="setting-color">
                                        <label data-load-css="css/theme-h.css">
                                            <input type="radio" name="setting-theme">
                                            <span class="icon-check"></span>
                                    <span class="split">
                                       <span class="color bg-danger-dark"></span>
                                       <span class="color bg-danger"></span>
                                    </span>
                                            <span class="color bg-gray-dark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p">
                            <h4 class="text-muted text-thin">{!! trans('core.Layout') !!}</h4>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.Fixed') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-fixed" type="checkbox" data-toggle-state="layout-fixed">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.Boxed') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-boxed" type="checkbox" data-toggle-state="layout-boxed">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.RTL') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-rtl" type="checkbox">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="p">
                            <h4 class="text-muted text-thin">{!! trans('core.Aside') !!}</h4>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.Collapsed') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-collapsed" type="checkbox" data-toggle-state="aside-collapsed">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.Float') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-float" type="checkbox" data-toggle-state="aside-float">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">
                                <p class="pull-left">{!! trans('core.Hover') !!}</p>
                                <div class="pull-right">
                                    <label class="switch">
                                        <input id="chk-hover" type="checkbox" data-toggle-state="aside-hover">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="app-chat" role="tabpanel" class="tab-pane fade">
                        <h3 class="text-center text-thin">{!! trans('core.Connections') !!}</h3>
                        <ul class="nav">
                            <!-- START list title-->
                            <li class="p">
                                <small class="text-muted">{!! trans('core.ONLINE') !!}</small>
                            </li>
                            <!-- END list title-->
                            <li>
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-success circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/05.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>{!! trans('core.Juan Sims') !!}</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Designeer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-success circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/06.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Maureen Jenkins</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Designeer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-danger circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/07.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Billie Dunn</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Designeer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-warning circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/08.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Tomothy Roberts</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Designer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                            </li>
                            <!-- START list title-->
                            <li class="p">
                                <small class="text-muted">{!! trans('core.OFFLINE') !!}</small>
                            </li>
                            <!-- END list title-->
                            <li>
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/09.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Lawrence Robinson</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Developer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                                <!-- START User status-->
                                <a href="#" class="media-box p mt0">
                              <span class="pull-right">
                                 <span class="circle circle-lg"></span>
                              </span>
                              <span class="pull-left">
                                 <!-- Contact avatar-->
                                 <img src="{!! URL::to('img/user/10.jpg') !!}" alt="Image" class="media-box-object img-circle thumb48">
                              </span>
                                    <!-- Contact info-->
                              <span class="media-box-body">
                                 <span class="media-box-heading">
                                    <strong>Tyrone Owens</strong>
                                    <br>
                                    <small class="text-muted">{!! trans('core.Designer') !!}</small>
                                 </span>
                              </span>
                                </a>
                                <!-- END User status-->
                            </li>
                            <li>
                                <div class="p-lg text-center">
                                    <!-- Optional link to list more users-->
                                    <a href="#" title="See more contacts" class="btn btn-purple btn-sm">
                                        <strong>{!! trans('core.Load more..') !!}</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <!-- Extra items-->
                        <div class="p">
                            <p>
                                <small class="text-muted">{!! trans('core.Tasks completion') !!}</small>
                            </p>
                            <div class="progress progress-xs m0">
                                <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-success progress-80">
                                    <span class="sr-only">{!! trans('core.:percent% Complete', ['percent' => 80]) !!}</span>
                                </div>
                            </div>
                        </div>
                        <div class="p">
                            <p>
                                <small class="text-muted">{!! trans('core.Upload quota') !!}</small>
                            </p>
                            <div class="progress progress-xs m0">
                                <div role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-warning progress-40">
                                    <span class="sr-only">{!! trans('core.:percent% Complete', ['percent' => 40]) !!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- END Off Sidebar (right)-->
    </aside>
    <!--------------------/ SIDEBAR -------------------->

    <!-------------------- MAIN SECTION -------------------->
    <!-- Main section-->
    <section>
        <!-- Page content-->
        @yield('content')
    </section>
    <!--------------------/ MAIN SECTION -------------------->

    <!-- Page footer-->
    <footer>
        <span>{!! trans('core.Copyright &copy; :year :company LLC, All Right Reserved', ['year' => 2015, 'company' => 'NetObjex']) !!}</span>
    </footer>

</div>

<!----- Content ----->
@section('form')
@show


<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
{!! Html::script('vendor/modernizr/modernizr.js') !!}
<!-- BOOTSTRAP-->
{!! Html::script('vendor/bootstrap/dist/js/bootstrap.js') !!}
<!-- STORAGE API-->
{!! Html::script('vendor/jQuery-Storage-API/jquery.storageapi.js') !!}
<!-- JQUERY EASING-->
{!! Html::script('vendor/jquery.easing/js/jquery.easing.js') !!}
<!-- ANIMO-->
{!! Html::script('vendor/animo.js/animo.js') !!}
<!-- SLIMSCROLL-->
{!! Html::script('vendor/slimScroll/jquery.slimscroll.min.js') !!}
<!-- SCREENFULL-->
{!! Html::script('vendor/screenfull/dist/screenfull.js') !!}
<!-- LOCALIZE-->
{!! Html::script('vendor/jquery-localize-i18n/dist/jquery.localize.js') !!}
<!-- RTL demo-->
{!! Html::script('js/demo/demo-rtl.js') !!}
{!! Html::script('vendor/nprogress/nprogress.js') !!}

<!-- GROWL -->
{!! Html::script('js/jquery.growl/jquery.growl.js') !!}
<!-- BLOCKUI -->
{!! Html::script('js/jquery.blockUI.js') !!}

<!-- BASE APP -->
{!! Html::script('js/app/base.app.js') !!}
{!! Html::script('js/app/common.js') !!}
<!-- =============== PAGE VENDOR SCRIPTS ===============-->
@yield('custom_scripts')
@section('pageJs')
@show
<!-- =============== APP SCRIPTS ===============-->
<!-- {!! Html::script('js/linegraphs.js') !!} -->
<!-- {!! Html::script('js/donutcharts.js') !!} -->
{!! Html::script('js/app.js') !!}
{!! Html::script('js/maskedinput.js') !!}
{!! Html::script('js/app/app.js') !!}

<script type="text/javascript">
    $.app.init({
        baseUrl: '{!! url() !!}',
        csrf   : '{!! csrf_token() !!}',
        curCustomerId: '{!! $curCustomerId !!}',
        curUserId: '{!! $curUserId !!}'
    });
</script>

</body>

</html>