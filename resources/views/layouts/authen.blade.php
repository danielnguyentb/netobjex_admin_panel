<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App + jQuery">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <base href="{!! URL::to('') !!}">
    <title>{!! trans('core.NetObjex') . (!empty($title) ? " | $title" : '') !!}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{!! asset('favicons') !!}/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{!! asset('favicons') !!}/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{!! asset('favicons') !!}/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{!! asset('favicons') !!}/apple-touch-icon-76x76.png">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="{!! asset('favicons') !!}/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="{!! asset('favicons') !!}/manifest.json">
    <link rel="mask-icon" href="{!! asset('favicons') !!}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.min.css">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <!-- ANIMATE.CSS-->
    <link rel="stylesheet" href="vendor/animate.css/animate.min.css">
    <!-- WHIRL (spinners)-->
    <link rel="stylesheet" href="vendor/whirl/dist/whirl.css">
    <!-- =============== PAGE VENDOR STYLES ===============-->
    <!-- WEATHER ICONS-->
    <link rel="stylesheet" href="vendor/weather-icons/css/weather-icons.min.css">
    <!-- DATATABLES-->
    <link rel="stylesheet" href="vendor/datatables-colvis/css/dataTables.colVis.css">
    <link rel="stylesheet" href="vendor/datatable-bootstrap/css/dataTables.bootstrap.css">
    <!-- CHOSEN-->
    <link rel="stylesheet" href="vendor/chosen_v1.2.0/chosen.min.css">
    {!! Html::style('vendor/nprogress/nprogress.css') !!}
    {!! Html::style('vendor/sweetalert/dist/sweetalert.css') !!}
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="css/bootstrap.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="css/app.css" id="maincss">
    <link rel="stylesheet" href="css/custom.css">
    <!-- JQUERY GROWL -->
    <link rel="stylesheet" href="css/jquery.growl/jquery.growl.css">
</head>

<body>
<div class="wrapper">
    <div class="block-center mt-xl wd-xl">
        <!-- START panel-->
        @yield('content')
        <!-- END panel-->
        <div class="p-lg text-center">
            <span>&copy;</span>
            <span>2015</span>
            <span>-</span>
            <span>netObjex LLC, All Right Reserved</span>
        </div>
    </div>
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->
<script src="vendor/modernizr/modernizr.js"></script>
<!-- JQUERY-->
<script src="vendor/jquery/dist/jquery.js"></script>
<!-- BOOTSTRAP-->
<script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
<!-- STORAGE API-->
<script src="vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
<!-- JQUERY EASING-->
<script src="vendor/jquery.easing/js/jquery.easing.js"></script>
<!-- ANIMO-->
<script src="vendor/animo.js/animo.js"></script>
<!-- SLIMSCROLL-->
<script src="vendor/slimScroll/jquery.slimscroll.min.js"></script>
<!-- SCREENFULL-->
<script src="vendor/screenfull/dist/screenfull.js"></script>
<!-- LOCALIZE-->
<script src="vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>
<!-- RTL demo-->
<script src="js/demo/demo-rtl.js"></script>
<!-- JQUERY GROWL -->
<script src="js/jquery.growl/jquery.growl.js"></script>
<!-- BLOCKUI -->
{!! Html::script('js/jquery.blockUI.js') !!}
{!! Html::script('vendor/nprogress/nprogress.js') !!}

<!-- BASE APP -->
{!! Html::script('js/app/base.app.js') !!}
{!! Html::script('js/app/common.js') !!}

<!-- CUSTOM SCRIPTS -->
@yield('custom_scripts')

<script type="text/javascript">
    $.app.init({
        baseUrl: '{!! url() !!}'
    });
</script>
</body>

</html>