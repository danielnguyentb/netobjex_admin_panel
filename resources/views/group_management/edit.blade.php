<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.View Group') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Group Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    @if($role == 'CustomerAdmin')
                                        <input type="text" value="{!! $name !!}" name="name" class="form-control form-format" required>
                                    @else
                                        <input type="text" value="{!! $name !!}" class="form-control form-format" readonly>
                                    @endif

                                    <h5 class="error_message group_name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4" style="margin-right: -39px">
                                    <label> {!! trans('core.Parent Group') !!} </label>
                                </div>
                                <div class="col-md-8">
                                    @if($role == 'CustomerAdmin')
                                        <select name="parentid" value="{!! $parentid !!}" class="chosen-select input-md form-control form-format">
                                            <option value="{!! $parentId !!}"> ----</option>
                                             @foreach($parents as $k => $v)
                                                @if($v->id == $parentid)
                                                    <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                                @else
                                                    <option value="{!! $v->id !!}" >{!! $v->name !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select value="{!! $parentid !!}" class="chosen-select input-md form-control form-format" disabled>
                                            <option value="{!! $parentId !!}"> ----</option>
                                             @foreach($parents as $k => $v)
                                                @if($v->id == $parentid)
                                                    <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                                @else
                                                    <option value="{!! $v->id !!}" >{!! $v->name !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label> {!! trans('core.Address1') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                @if($role == 'CustomerAdmin')
                                    <input type="text" value="{!! $address1 !!}" name="address1" class="form-control form-format" required>
                                @else
                                    <input type="text" value="{!! $address1 !!}" class="form-control form-format" readonly>
                                @endif

                                <h5 class="error_message address_error"> </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Address2') !!} </label>
                                </div>
                                <div class="col-md-9">
                                @if($role == 'CustomerAdmin')
                                     <input type="text" value="{!! $address2 !!}" name="address2" class="form-control form-format">
                                @else
                                     <input type="text" value="{!! $address2 !!}"  class="form-control form-format" readonly>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.City') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    @if($role == 'CustomerAdmin')
                                        <input type="text" value="{!! $city !!}" name="city" class="form-control form-format" required>
                                    @else
                                        <input type="text" value="{!! $city !!}" class="form-control form-format" readonly>
                                    @endif

                                    <h5 class="error_message city_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Zip Code') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                @if($role == 'CustomerAdmin')
                                    <input type="text" value="{!! $zip !!}" name="zip" data-inputmask="'mask':'99999-99'" class="form-control form-format" required>
                                @else
                                    <input type="text" value="{!! $zip !!}" data-inputmask="'mask':'99999-99'" class="form-control form-format" readonly>
                                @endif

                                    <h5 class="error_message zip_code_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Country') !!} </label>
                                </div>
                                <div class="col-md-7">
                                    @if($role == 'CustomerAdmin')
                                        <select name="country" value="{!! $country !!}" class="chosen-select input-md form-control form-format">
                                            @foreach($countriesList as $k => $v)
                                                @if($k == $country)
                                                    <option value="{!! $k !!}" selected="selected">{!! $v !!}</option>
                                                @else
                                                    <option value="{!! $k !!}" >{!! $v !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select value="{!! $country !!}" class="chosen-select input-md form-control form-format" disabled>
                                            @foreach($countriesList as $k => $v)
                                                @if($k == $country)
                                                    <option value="{!! $k !!}" selected="selected">{!! $v !!}</option>
                                                @else
                                                    <option value="{!! $k !!}" >{!! $v !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.State') !!} </label>
                                </div>
                                <div class="col-md-7">
                                    @if($role == 'CustomerAdmin')
                                        <select name="state" value="{!! $state !!}" class="chosen-select input-md form-control form-format">
                                            @foreach($statesList as $k => $v)
                                                @if($k == $state)
                                                    <option value="{!! $k !!}" selected="selected">{!! $v !!}</option>
                                                @else
                                                    <option value="{!! $k !!}" >{!! $v !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select value="{!! $state !!}" class="chosen-select input-md form-control form-format" disabled>
                                            @foreach($statesList as $k => $v)
                                                @if($k == $state)
                                                    <option value="{!! $k !!}" selected="selected">{!! $v !!}</option>
                                                @else
                                                    <option value="{!! $k !!}" >{!! $v !!}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Phone') !!} <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    @if($role == 'CustomerAdmin')
                                        <input type="text" value="{!! $phone !!}" pattern="^\d{3}-\d{3}-\d{4}$" data-inputmask="'mask': '999-999-9999'"
                                         placeholder="000-000-0000" name="phone" class="form-control form-format" required>
                                    @else
                                        <input type="text" value="{!! $phone !!}" pattern="^\d{3}-\d{3}-\d{4}$" data-inputmask="'mask': '999-999-9999'"
                                         placeholder="000-000-0000" class="form-control form-format" readonly>
                                    @endif

                                    <h5 class="error_message phone_number_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Email') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    @if($role == 'CustomerAdmin')
                                        <input type="email" value="{!! $email !!}" name="email" data-parsley-type="email" class="form-control form-format" required>
                                    @else
                                        <input type="email" value="{!! $email !!}" class="form-control form-format" readonly>
                                    @endif
                                    <h5 class="error_message email_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($role == 'CustomerAdmin')
                                    @if($form == 'edit')
                                        <input type="submit" class="btn btn-sm btn-default buttonUpdate" value="{!! trans('core.Update') !!}">
                                    @else
                                        <input type="submit" class="btn btn-sm btn-default buttonAdd" value="{!! trans('core.Add') !!}">
                                    @endif

                                    <button type="button" class="btn btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                                @else
                                    <button type="button" class="btn btn-default buttonCancel"> {!! trans('core.Close') !!}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>



