@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    {!! Html::style('vendor/datatables-colvis/css/dataTables.colVis.css') !!}
    {!! Html::style('vendor/datatable-bootstrap/css/dataTables.bootstrap.css') !!}
    <!-- CHOSEN-->
    {!! Html::style('vendor/chosen_v1.2.0/chosen.min.css') !!}
    <!-- Loaders.css-->
    {!! Html::style('vendor/loaders.css/loaders.css') !!}
    <!-- Spinkit-->
    {!! Html::style('vendor/spinkit/css/spinkit.css') !!}
    <!-- SWEET ALERT -->
    {!! Html::style('vendor/sweetalert/dist/sweetalert.css') !!}
    <!-- DATEPICKER -->
    {!! Html::style('js/datepicker/jquery-1.10.2.js') !!}
    {!! Html::style('js/datepicker/jquery-ui.js') !!}

    <!-- AJAX POST -->
    {!! Html::script('js/ajax_calendar/jquery.min.js') !!}
    {{--{!! Html::script('js/ajax_calendar/ajax_calendar.js') !!}--}}
    <!-- CALENDAR -->
    {!! Html::style('css/fullcalendar/cupertino/jquery-ui.min.css') !!}
    {!! Html::style('css/fullcalendar/fullcalendar.css') !!}

    <!-- bxSlider CSS file -->

    <style type="text/css">
        #scrollbar-piece{
            width: 20px;
            height: 40px;
            background-color: grey
        }
        .dropHere {
            width:290px;
            height:180px;
            border: dotted 1px black;
            color: red;
            position: relative;
        }
        .dropHere img{
            width: 39px;

        }
        .ui-state-active{
            background-color: rgba(234,255,84,1);
            opacity:0.6;
        }
        .nav-map ul {
            list-style: none;
        // background-color: #444;
            text-align: center;
        //padding: 0;
        //margin: 0;
        }
        .nav-map li {
            font-family: 'Oswald', sans-serif;
            font-size: 1.2em;
            line-height: 40px;
        //    height: 40px;
            border-bottom: 1px solid #888;
        }
        .sec2{
            margin-top: 250px;
        }
        .icon{
            margin: 10px;
            padding: 20px;
        }
        .fc-toolbar .fc-right{
            display: none!important;
        }

        @media screen and (min-width: 600px) {
            .nav-map li {
            //width: 120px;
                border-bottom: none;
                height: 50px;
                line-height: 50px;
                font-size: 1.4em;
            }

            /* Option 1 - Display Inline */
            .nav-map li {
                display: inline-block;
                margin-right: -4px;
            }
        }
        .vertical-text {
            transform: rotate(90deg);
            float: left;
            font-size: 20px;
        }
        .map-table{
        // background-color: khaki;
        //background-image: url('img/map1.jpeg') ;
            border:solid 1px grey;
        }
        .td-vertical{
            height: 100px;
            border:solid 1px grey;
        }
        .center{
            text-align: center;
        }

    </style>
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Beacon Campaigns') !!} </h3>
        <div class="container-fluid">
            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Marketing') !!}</a></li>
                    <li class="active">{!! trans('core.Beacon Campaigns') !!}</li>
                </ul>
            </div>
            <!--/ Button -->

            <!-- START panel tab-->
            <div class="row">
                <div class="col-lg-12">
                    <div role="tabpanel" class="panel">
                        <!-- Nav tabs-->
                        <ul role="tablist" class="nav nav-tabs nav-justified">
                            <li role="presentation" class="active">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                    {!! trans('core.Campaign Calendar') !!}
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    {!! trans('core.Campaigns') !!}
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes-->
                        <div class="tab-content p0">
                            <div id="home" role="tabpanel" class="tab-pane">
                                <!-- Button -->
                                <div class="row">
                                    <button type="button" class="mb-sm btn btn-primary add-button" id="add_form">
                                        <b>{!! trans('core.Add Campaign') !!}</b>
                                    </button>
                                </div>
                                <!--/ Button -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        {!! trans('core.Beacon Campaigns') !!}
                                        <div class='icon-question help'
                                             data-container="body"
                                             data-toggle="popover"
                                             data-placement="left"
                                             data-trigger="hover"
                                             data-content="{!! trans('core.Vivamus sagittis lacus vel augue laoreet rutrum faucibus.') !!}">
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="beaconCampaigns" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th style="width:20%">{!! trans('core.Name') !!}</th>
                                                <th style="width:20%">{!! trans('core.Beacons') !!}</th>
                                                <th style="width:20%">{!! trans('core.Start Date') !!}</th>
                                                <th style="width:20%">{!! trans('core.End Date') !!}</th>
                                                <th style="width:20%">{!! trans('core.Action') !!}</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                @if($auth)
                                <div  class="nav-map">
                                    <ul>
                                    @foreach($icons as $k=>$v)
                                        <li class="icon" >
                                            <img src="{!! $v->iconurl !!}" class="dragIcon" data-id="{!! $v->iconid !!}" style="width: 39px; height: 38px"/>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                                <div class="nav">

                                </div>


                                <table class="map-table table">

                                    <tr>
                                        <td ></td>
                                        <td colspan="3" class="area1 center">{!! trans('core.area') !!} 1</td>
                                        <td colspan="3" class="area2 center">{!! trans('core.area') !!} 2</td>
                                        <td colspan="3" class="area3 center">{!! trans('core.area') !!} 3</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" class="td-vertical"><div class="vertical-text" >{!! trans('core.Enter') !!}</div></td>
                                        <td colspan="3" rowspan="2" ><div id="section-1" class="dropHere" data-key="enterimmedresponseid" data-set="area1"></div></td>
                                        <td colspan="3" rowspan="2"><div id="section-1" class="dropHere" data-key="enternearresponseid" data-set="area2"></div></td>
                                        <td colspan="3" rowspan="2"><div id="section-1" class="dropHere" data-key="enterfarresponseid" data-set="area3"></div></td>

                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" class="td-vertical"><div class="vertical-text">{!! trans('core.Exit') !!}</div></td>
                                        <td colspan="3" rowspan="2"><div id="section-1" class="dropHere" data-key="exitimmedresponseid" data-set="area1"></div></td>
                                        <td colspan="3" rowspan="2"><div id="section-1" class="dropHere" data-key="exitnearresponseid"  data-set="area2"></div></td>
                                        <td colspan="3" rowspan="2"><div id="section-1" class="dropHere" data-key="exitfarresponseid" data-set="area3"></div></td>

                                    </tr>
                                    <tr>
                                    </tr>
                                </table>
                                @endif
                            </div>
                            <div id="profile" role="tabpanel" class="tab-pane active">
                                <div id='calendarView' style="margin-top:25px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop



@section('pageJs')
        <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- FILESTYLE-->
    @include('includes.js.filestyle')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    <!-- CALENDAR -->
    @include('includes.js.calendar')

    <!-- slider -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <span class="now_text hidden">Now</span>
    <script type="text/javascript">
        var scripts = [
            'js/calendar/helper.js',
            'js/calendar/plugins.js',
            'js/calendar/progsMiniSlider.js',
            'js/calendar/progressbar.js',
            'js/calendar/filter.js',
            'js/calendar/main.js'
        ]
        for(i = 0; i< scripts.length; i++){
            var s = document.createElement("script");
            s.async = false;
            s.src = scripts[i];
            document.head.appendChild(s);
        }

    </script>
    <!-- drag_drop -->
    @include('includes.js.drag_drop')
    {!! Html::script('js/calendar/custom-calendar.js') !!}
    {!! Html::script('js/fullcalendar/moment.min.js') !!}
    {!! Html::script('js/fullcalendar/fullcalendar.min.js') !!}
    @parent
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/beaconCampaign.js') !!}

@stop
