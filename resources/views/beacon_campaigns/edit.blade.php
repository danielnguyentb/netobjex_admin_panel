<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                @if($form == 'add')
                    <h3> {!! trans('core.Add Campaign') !!} </h3>
                @else
                    <h3> {!! trans('core.Edit Campaign') !!} </h3>
                @endif

            </div>
        </div>
    </div>
    <div class="content-body">
         {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="name" value="{!! $name !!}" class="form-control form-format" required>
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.Select Beacon') !!} </label>
                            </div>
                            <div class="col-md-9">
                                <select required name="beaconid[]" multiple class="chosen-select form-control">
                                    @foreach($beacons as $k=>$v)
                                        @if( in_array($v->id, $beaconArr) )
                                            <option value="{!! $v->id !!}" selected>{!! $v->name!!}</option>
                                        @else
                                            <option value="{!! $v->id !!}">{!! $v->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.Select BeaconSet') !!} </label>
                            </div>
                            <div class="col-md-9">
                                <select name="beaconsetid[]" required multiple class="chosen-select form-control">
                                    @foreach($beaconSets as $k=>$v)
                                        @if( in_array($v->id, $beaconSetArr) )
                                            <option value="{!! $v->id !!}" selected>{!! $v->name!!}</option>
                                        @else
                                            <option value="{!! $v->id !!}">{!! $v->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> Start Date<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" value="{!! $startdate !!}" name="startdate" class="form-control form-format" required>
                                <h5 class="error_message start_date_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.End Date') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" value="{!! $enddate !!}" name="enddate" class="form-control form-format" required>
                                <h5 class="error_message end_date_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label> {!! trans('core.Run On') !!}<span class="asterisk">*</span> </label>
                            </div>
                        </div>
                        <div class="row run_on">
                            <div class="col-md-12">
                                <div class="radio">
                                    <label>
                                        @if($eachday === true)
                                             <input class="select_all" type="radio" name="eachday"  checked="checked" >
                                        @else
                                            <input class="select_all" type="radio" name="eachday"  >
                                        @endif
                                        {!! trans('core.All days within start/end timeframe') !!}
                                    </label>
                                    <br/>
                                    <label>{!! trans('core.or') !!}</label>
                                    <br/>
                                    <label>{!! trans('core.repeat every') !!}</label>
                                    <br/>
                                    <div class="checkbox select_days" style="width: 100%">
                                        <label>
                                            @if($eachmonday === true)
                                                <input name="eachmonday" class="day" type="checkbox" value="Mon" checked> {!! trans('core.Monday') !!}
                                            @else
                                                <input name="eachmonday" class="day" type="checkbox" value="Mon"> {!! trans('core.Monday') !!}
                                            @endif
                                        </label>
                                        <label>
                                        @if($eachtuesday === true)
                                            <input name="eachtuesday" class="day" type="checkbox" value="Tues" checked> {!! trans('core.Tuesday') !!}
                                        @else
                                            <input name="eachtuesday" class="day" type="checkbox" value="Tues"> {!! trans('core.Tuesday') !!}
                                        @endif

                                        </label>
                                        <label>
                                            @if($eachwednesday === true)
                                                <input name="eachwednesday" class="day" type="checkbox" value="Wed" checked> {!! trans('core.Wednesday') !!}
                                            @else
                                                <input name="eachwednesday" class="day" type="checkbox" value="Wed"> {!! trans('core.Wednesday') !!}
                                            @endif
                                        </label>
                                        <label>
                                            @if($eachthirsday === true)
                                                <input name="eachthirsday" class="day" type="checkbox" value="Thur" checked> {!! trans('core.Thursday') !!}
                                            @else
                                                <input name="eachthirsday" class="day" type="checkbox" value="Thur"> {!! trans('core.Thursday') !!}
                                            @endif

                                        </label>
                                        <label>
                                            @if($eachfriday === true)
                                                <input name="eachfriday" class="day" type="checkbox" value="Fri" checked> {!! trans('core.Friday') !!}
                                            @else
                                                <input name="eachfriday" class="day" type="checkbox" value="Fri"> {!! trans('core.Friday') !!}
                                            @endif
                                        </label>
                                        <label>
                                            @if($eachsaturday === true)
                                                <input name="eachsaturday" class="day" type="checkbox" value="Sat" checked> {!! trans('core.Saturday') !!}
                                            @else
                                                <input name="eachsaturday" class="day" type="checkbox" value="Sat"> {!! trans('core.Saturday') !!}
                                            @endif

                                        </label>
                                        <label>
                                            @if($eachsunday === true)
                                                <input name="eachsunday" class="day" type="checkbox" value="Sun" checked> {!! trans('core.Sunday') !!}
                                            @else
                                                <input name="eachsunday" class="day" type="checkbox" value="Sun"> {!! trans('core.Sunday') !!}
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 hour">
                                <label> {!! trans('core.Start Hour') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                <select name="startHour" class="chosen-select input-md form-control form-format">
                                    @foreach($hours as $k=>$v)
                                        @if($v == $startHour)
                                            <option value="{!! $v!!}" selected> {!! $v !!}</option>
                                        @else
                                            <option value="{!! $v!!}"> {!! $v !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 minutes">
                                <label> {!! trans('core.Minutes') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                <select name="startMinute" class="chosen-select input-md form-control form-format">
                                    @foreach($minutes as $k=>$v)
                                        @if($v == $startMinute)
                                            <option value="{!! $v!!}" selected> {!! $v !!}</option>
                                        @else
                                            <option value="{!! $v!!}"> {!! $v !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 ampm">
                                <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                            </div>
                            <div class="col-md-2 chosen" style="padding-right:55px">
                                <select name="startApm" class="chosen-select input-md form-control form-format">
                                    @if($startApm == 'AM')
                                        <option value="AM" selected>{!! trans('core.AM') !!}</option>
                                        <option value="PM">{!! trans('core.PM') !!}</option>
                                    @else
                                        <option value="AM">{!! trans('core.AM') !!}</option>
                                        <option value="PM" selected>{!! trans('core.PM') !!}</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 hour">
                                <label> {!! trans('core.End Hour') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                <select name="endHour" class="chosen-select input-md form-control form-format">
                                    @foreach($hours as $k=>$v)
                                        @if($endHour == $v)
                                            <option value="{!! $v!!}" selected> {!! $v !!}</option>
                                        @else
                                            <option value="{!! $v!!}"> {!! $v !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 minutes">
                                <label> {!! trans('core.Minutes') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                <select name="endMinute" class="chosen-select input-md form-control form-format">
                                    @foreach($minutes as $k=>$v)
                                        @if($endMinute == $v)
                                            <option value="{!! $v!!}" selected> {!! $v !!}</option>
                                        @else
                                            <option value="{!! $v!!}"> {!! $v !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 ampm">
                                <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                            </div>
                            <div class="col-md-2 chosen" style="padding-right:55px">
                                <select name="endApm" class="chosen-select input-md form-control form-format">
                                    @if($endApm == 'AM')
                                        <option value="AM" selected>{!! trans('core.AM') !!}</option>
                                        <option value="PM">{!! trans('core.PM') !!}</option>
                                    @else
                                        <option value="AM">{!! trans('core.AM') !!}</option>
                                        <option value="PM" selected>{!! trans('core.PM') !!}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Time Zone') !!} </label>
                            </div>
                            <div class="col-md-10">
                                <select name="timezoneid" class="chosen-select form-control" required>
                                    @foreach($timezonesList as $k =>$v)
                                        @if($k == $timezoneid)
                                            <option value="{!! $k !!}" selected>{!! $v !!}</option>
                                        @else
                                            <option value="{!! $k !!}" >{!! $v !!}</option>
                                        @endif;
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label> {!! trans('core.Background Color') !!} </label>
                            </div>
                            <div class="col-md-6">
                                <!--<div style="background-color: #1B65B1; border: 1px solid black; width: 20px; height: 20px;"></div>-->
                                <div id="colorSelectorBackground">
                                    <input name="backcolor" class="jscolor" value="{!! $backcolor !!}" style="width: 50px; height: 30px; border: 2px solid rgb(0, 0, 0); background-color: #{!! $backcolor !!}">
                                    {{--<div style="width: 30px; height: 30px; border: 2px solid rgb(0, 0, 0); background-color: rgb(212, 21, 212);"></div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label> {!! trans('core.Text Color') !!} </label>
                            </div>
                            <div class="col-md-8">
                                <!--<div style="background-color: red; border: 1px solid black; width: 20px; height: 20px;"></div>-->
                                <div id="colorSelectorForeground">
                                     <input class="jscolor" name="textcolor" value="{!! $textcolor !!}" style="width: 50px; height: 30px; border: 2px solid rgb(0, 0, 0); background-color: #{!! $textcolor !!}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}

                            <input type="hidden" value="" name="enternearresponseid" id="enternearresponseid">
                            <input type="hidden" value="" name="enterfarresponseid" id="enterfarresponseid">
                            <input type="hidden" value="" name="enterimmedresponseid" id="enterimmedresponseid">
                            <input type="hidden" value="" name="exitnearresponseid" id="exitnearresponseid">
                            <input type="hidden" value="" name="exitfarresponseid" id="exitfarresponseid">
                            <input type="hidden" value="" name="exitimmedresponseid" id="exitimmedresponseid">
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<script src="/js/colorpicker/jscolor.js"></script>
<script type="text/javascript">
   var icons = {
        enternearresponseid: "{!! $enternearresponseid !!}",
        enterfarresponseid: "{!! $enterfarresponseid !!}",
        enterimmedresponseid: "{!! $enterimmedresponseid !!}",
        exitnearresponseid: "{!! $exitnearresponseid !!}",
        exitfarresponseid: "{!! $exitfarresponseid !!}",
        exitimmedresponseid: "{!! $exitimmedresponseid !!}"
   };
   assignAllDays();
   resetIcon(icons);

</script>

