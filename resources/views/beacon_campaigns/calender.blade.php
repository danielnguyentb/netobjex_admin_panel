<li class="">
    <div class="channel_visu red_bg">
        <a href="#">
                <span class="logo_chan with_play">
                    <div class="calendarMachine">
                        <b>M1</b>
                    </div><br>
                </span>
        </a>
    </div>
    <!---------- Item List ---------->
    <div class="programmes_items">
        <div class="progs_wrapper">
            <div class="prog_items_cover clearfix" >
                <!-- Advertisement 1 -->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 03:00:00" data-end-time="2015-09-11 04:00:00" data-genre="Sports, non-event"
                     class="prog_item" style="width: 452px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="{!! trans('core.Real Madrid TV') !!}" class="prog_title">
                                        {!! trans('core.Advertisement') !!} 1
                                    </span><br>
                                    <span class="prog_type">
                                        {!! trans('core.Chase, BOA') !!}
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 03:00:00"
                                      data-end-time="2015-09-11 04:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        03:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                        04:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 2 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 04:00:00" data-end-time="2015-09-11 05:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 452px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        {!! trans('core.Advertisement') !!} 2
                                    </span><br>
                                    <span class="prog_type">
                                        {!! trans('core.Sports, non-event') !!}
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 04:00:00"
                                      data-end-time="2015-09-11 05:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        04:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         05:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 3 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 05:00:00" data-end-time="2015-09-11 06:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 3
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 05:00:00"
                                      data-end-time="2015-09-11 06:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        05:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         06:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 4 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 06:00:00" data-end-time="2015-09-11 07:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 4
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 06:00:00"
                                      data-end-time="2015-09-11 07:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        06:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         07:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 5 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 07:00:00" data-end-time="2015-09-11 08:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 5
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 07:00:00"
                                      data-end-time="2015-09-11 08:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        07:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         08:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 6 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 08:00:00" data-end-time="2015-09-11 09:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 6
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 08:00:00"
                                      data-end-time="2015-09-11 09:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        08:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         09:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 7 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 09:00:00" data-end-time="2015-09-11 10:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 7
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 09:00:00"
                                      data-end-time="2015-09-11 10:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        09:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         10:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 8 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 10:00:00" data-end-time="2015-09-11 11:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 8
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00"
                                      data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 10:00:00"
                                      data-end-time="2015-09-11 11:00:00" class="prog_period">
                                    <span class="prog_hour">
                                        10:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                        11:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 9 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 11:00:00" data-end-time="2015-09-11 12:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 9
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                            <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 11:00:00" data-end-time="2015-09-11 12:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        11:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         12:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 10 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 12:00:00" data-end-time="2015-09-11 13:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 10
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 12:00:00" data-end-time="2015-09-11 13:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        12:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         13:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 11 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 13:00:00" data-end-time="2015-09-11 14:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 11
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 13:00:00" data-end-time="2015-09-11 14:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        13:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         14:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 12 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 14:00:00" data-end-time="2015-09-11 15:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 12
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 14:00:00" data-end-time="2015-09-11 15:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        14:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         15:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 13 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 15:00:00" data-end-time="2015-09-11 16:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 13
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                            <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 15:00:00" data-end-time="2015-09-11 16:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        15:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         16:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 14 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 16:00:00" data-end-time="2015-09-11 17:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 14
                                    </span><br>
                                    <span class="prog_type">
                                        Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 16:00:00" data-end-time="2015-09-11 17:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        16:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         17:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 15 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 17:00:00" data-end-time="2015-09-11 18:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 15
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                                <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 17:00:00" data-end-time="2015-09-11 18:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        17:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         18:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 16 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 18:00:00" data-end-time="2015-09-11 19:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 16
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                            <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 18:00:00" data-end-time="2015-09-11 19:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        18:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                    </span>
                                    <span class="prog_hour">
                                         19:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
                <!----- Advertisement 17 ----->
                <div data-position="1" data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                     data-start-time="2015-09-11 19:00:00" data-end-time="2015-09-11 20:00:00"
                     data-genre="Sports, non-event" class="prog_item" style="width: 419px;">
                    <a href="#">
                            <span class="top_infos">
                                <span class="prog_visu">
                                    <img src="img/logo-single.png" alt="" style="background-color: #00AEEF;">
                                </span>
                                <span class="prog_infos">
                                    <span title="Real Madrid TV" class="prog_title">
                                        Advertisement 17
                                    </span><br>
                                    <span class="prog_type">
                                    Sports, non-event
                                    </span>
                                    <!--<span class="score_results">
                                        score 120 : 112
                                    </span>-->
                                </span>
                            </span>
                            <span class="bottom_infos">
                            <span class="prog_status"></span>
                                <span data-old-start-time="2015-09-11 22:00:00" data-old-end-time="2015-09-11 23:00:00"
                                      data-start-time="2015-09-11 19:00:00" data-end-time="2015-09-11 10:00:00"
                                      class="prog_period">
                                    <span class="prog_hour">
                                        19:00
                                    </span>
                                    <span class="prog_period_mask">
                                        <span style="width: 0%; background-color: #0286B7">&nbsp;</span>
                                     </span>
                                    <span class="prog_hour">
                                         20:00
                                    </span>
                                </span>
                            </span>
                    </a>
                    <span class="trending_tweets_symbole"></span>
                </div>
            </div>
            <a href="#" class="slide_next">
                <span class="arrows_symbole"></span>
                <span class="next_link_lbl">{!! trans('core.Next') !!}</span>
            </a>
        </div>
    </div>
</li>
<!--------------- M2 --------------->

