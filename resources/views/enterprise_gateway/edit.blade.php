<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.Edit Enterprise Gateway') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">

        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}

            <div class="row">

                 <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span>  </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('name',$name,['class'=>'form-control form-format', 'required']) !!}
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.URL') !!}<span class="asterisk">*</span>  </label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::url('url',$url,['class'=>'form-control form-format', 'placeholder' => 'include https://', 'required']) !!}

                                <h5 class="error_message url_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3" style="margin-right: -39px">
                                <label> {!! trans('core.User ID') !!}<span class="asterisk">*</span>  </label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('username',$username,['class'=>'form-control form-format', 'required']) !!}
                                <h5 class="error_message user_id_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.Password') !!}<span class="asterisk">*</span>  </label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::input('password', 'password', $password, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message password_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-1">
                            <label> {!! trans('core.Type') !!} </label>
                        </div>
                        <div class="col-md-4">
                            <select class="chosen-select input-md form-control form-format" name="etype">
                                @foreach ($etypes as $ety)
                                    <option {!! !($ety[0] == $etype) ?: 'selected="selected"' !!}>{!! $ety[1] !!}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h5>(<span class="asterisk">*</span>) {!! trans('core.required') !!}</h5>
                </div>
            </div>
            <div class="row">
                <div class="buttonAdd"></div>
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default button_Add']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>