<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                 @if($form == 'add')
                    <h3> {!! trans('core.Add Beacon') !!} </h3>
                @else
                    <h3> {!! trans('core.Edit Beacon') !!} </h3>
                @endif
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">
                <div class="row">

                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label> {!! trans('core.UUID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="uuidvalue" value="{!! $uuidvalue !!}" class="form-control form-format" required>
                                <h5 class="error_message uuid_error"> </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Major ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9" style="margin-left: -38px">
                                    <input name="majorid" value="{!! $majorid !!}" type="text" class="form-control form-format" required>
                                    <h5 class="error_message major_id_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" name="name" value="{!! $name !!}" class="form-control form-format" required>
                                    <h5 class="error_message name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Minor ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9" style="margin-left: -38px">
                                    <input type="text" name="minorid" value="{!! $minorid !!}" class="form-control form-format" required>
                                    <h5 class="error_message minor_id_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Location') !!} </label>
                                </div>
                                <div class="col-md-8">
                                    <select name="locationid" class="chosen-select input-md form-control form-format">
                                       @foreach ($locations as $k => $v)
                                           @if($v->id == $locationid)
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                       @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Group') !!} </label>
                                </div>
                                <div class="col-md-8">
                                    <select name="groupid" class="chosen-select input-md form-control form-format">
                                        @foreach ($groups as $k => $v)
                                            @if($v->id == $groupid)
                                                 <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                            @else
                                                <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label> {!! trans('core.Description') !!} </label>
                                </div>
                                <div class="col-md-11">
                                    <textarea name="description" class="description" placeholder="Type something..." >{!! $description !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label> {!! trans('core.Lost beacon') !!} </label>
                                </div>
                                <div class="col-md-11">
                                    @if($lost == true)
                                        <input type="checkbox" name="lost" checked/>
                                    @else
                                        <input type="checkbox" name="lost" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
