<div class="main-content ">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                @if($form == 'edit')
                    <h3> {!! trans('core.Update Privilege Admin') !!} </h3>
                @else
                    <h3> {!! trans('core.Add Privilege Admin') !!} </h3>
                @endif

            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Role') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-5">
                                    <select name="rid" class="chosen-select input-md form-control form-format">
                                        @foreach($roles as $k=>$v)
                                            @if($v->id == $rid)
                                                <option value="{!! $v->id!!}" selected>{!! $v->name!!}</option>
                                            @else
                                               <option value="{!! $v->id!!}" >{!! $v->name!!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Class') !!} <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-5">
                                    <select name="object" class="chosen-select input-md form-control form-format">
                                            @foreach($classes as $k=>$v)
                                                @if($v->id == $object)
                                                    <option value="{!! $v->id!!}" selected>{!! $v->name!!}</option>
                                                @else
                                                   <option value="{!! $v->id!!}" >{!! $v->name!!}</option>
                                                @endif
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>  {!! trans('core.Access Control') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <label class="checkbox-inline c-checkbox">
                                    @if($e == true)
                                        <input type="checkbox" name="e" checked id="inlineCheckbox10">
                                    @else
                                        <input type="checkbox" name="e" value="{!! $e !!}" id="inlineCheckbox10">
                                    @endif
                                    <span class="fa fa-check"></span>Edit</label>

                                    <label class="checkbox-inline c-checkbox">
                                    @if($vvv == true)
                                        <input type="checkbox" name="v" checked id="inlineCheckbox20">
                                    @else
                                        <input type="checkbox" name="v"  id="inlineCheckbox20">
                                    @endif
                                    <span class="fa fa-check"></span>View</label>

                                    <label class="checkbox-inline c-checkbox">
                                    @if($d == true)
                                        <input type="checkbox" name="d" checked id="inlineCheckbox30">
                                    @else
                                        <input type="checkbox" name="d" id="inlineCheckbox30">
                                    @endif

                                    <span class="fa fa-check"></span>Delete</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Default Access to Objects') !!}  <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <label class="radio-inline c-radio">
                                        @if($access == true)
                                             <input type="radio" checked="" value="all" name="access" >
                                             <span class="fa fa-circle"></span>All</label>
                                             <label class="radio-inline c-radio">
                                             <input type="radio" value="none" name="access">
                                             <span class="fa fa-circle"></span>None</label>
                                        @else
                                             <input type="radio"  value=all name="access" >
                                             <span class="fa fa-circle"></span>All</label>
                                             <label class="radio-inline c-radio">
                                             <input type="radio" checked="" value="none" name="access">
                                             <span class="fa fa-circle"></span>None</label>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) required </h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

