@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Asset Management') !!} </h3>
        <div class="container-fluid">
            <!-- Button -->

            <div class="row beacon_statistics" style="padding-left:20px;">
                <button type="button" class="mb-sm btn btn-primary">
                    <div class="icon-screen-smartphone icons beacon_statistics_content"> </div>
                    <b class="beacon_statistics_content">{!! trans('core.Device') !!}</b>
                    <br/>
                    <span>{!! $devicesCount !!}</span>
                </button>
                <button type="button" class="mb-sm btn btn-primary">
                    <div class="icon-screen-smartphone beacon_statistics_content"> </div>
                    <b class="beacon_statistics_content">{!! trans('core.DeviceSet') !!}</b>
                    <br/>
                    <span>{!! $deviceSetsCount !!}</span>
                </button>
                <button type="button" class="mb-sm btn btn-primary">
                    <div class="fa fa-wifi beacon_statistics_content"></div>
                    <b class="beacon_statistics_content">{!! trans('core.Beacon') !!}</b>
                    <br/>
                    <span>{!! $beaconsCount !!}</span>
                </button>
                <button type="button" class="mb-sm btn btn-primary">
                    <div class="fa fa-futbol-o  beacon_statistics_content"></div>
                    <b class="beacon_statistics_content">{!! trans('core.BeaconSet') !!}</b>
                    <br/>
                    <span>{!! $beaconSetsCount !!}</span>
                </button>
                <button type="button" class="mb-sm btn btn-primary">
                    <div class="icon-map beacon_statistics_content"></div>
                    <b class="beacon_statistics_content">{!! trans('core.Locations') !!}</b>
                    <br/>
                    <span>{!! $locationsCount !!}</span>
                </button>
            </div>
            <!--/ Button -->

            <div class="row" style="margin-top:20px;">
                <div class="col-md-12">
                    <div role="tabpanel" class="panel">
                        <!-- Nav tabs-->
                        <ul role="tablist" class="nav nav-tabs nav-justified">
                            <li role="presentation" class="active">
                                <a href="#beacon_search" aria-controls="home" role="tab" data-toggle="tab">
                                    {!! trans('core.Beacon Search') !!}
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#device_search" aria-controls="profile" role="tab" data-toggle="tab">
                                    {!! trans('core.Device Search') !!}
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes-->
                        <div class="tab-content p0">
                            <div id="beacon_search" role="tabpanel" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label> {!! trans('core.Search by') !!}: </label>
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="radio-inline c-radio">
                                                        <input type="radio" checked="" value="beacon" name="beacon_search_by" class="beacon-search-by" data-group="beacon-input-items">
                                                        <span class="fa fa-circle"></span>{!! trans('core.Beacon') !!}
                                                    </label>
                                                    <label class="radio-inline c-radio">
                                                        <input type="radio" value="beaconSet" name="beacon_search_by" class="beacon-search-by" data-group="beacon-set-input-items">
                                                        <span class="fa fa-circle"></span>{!! trans('core.BeaconSet') !!}
                                                    </label>
                                                </div>
                                                <div class="col-md-4 B-input-items beacon-input-items">
                                                    <label class="checkbox-inline c-checkbox">
                                                        <input type="checkbox" value="1" id="only-active-beacon" checked>
                                                        <span class="fa fa-check"></span> {!! trans('core.Show only Active Beacons') !!}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row B-input-items beacon-input-items">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label> {!! trans('core.UUID') !!}: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::select('uuid', $UUIDsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'uuid-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row B-input-items beacon-input-items">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label> {!! trans('core.Major ID') !!}: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::select('majorId', $majorIdsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'major-id-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row B-input-items beacon-input-items">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label> {!! trans('core.Minor ID') !!}: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::select('minorId', $minorIdsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'minor-id-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row B-input-items beacon-set-input-items">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label> {!! trans('core.BeaconSet') !!}: </label>
                                                </div>
                                                <div class="col-md-9">
                                                    {!! Form::select('beaconSetId', $beaconSetsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'beacon-set-id-select']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6" style="padding-bottom:15px;">
                                        <div class="row">
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="submit" value="Search"
                                                       class="btn btn-sm btn-default buttonAdd"
                                                       id="search-beacon"
                                                       style="background-color: rgb(93, 156, 236); color: white;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:20px;">
                                    <div class="col-md-12">
                                        <div role="tabpanel" class="panel">
                                            <!-- Nav tabs-->
                                            <ul role="tablist" class="nav nav-tabs nav-justified">
                                                <li role="presentation" class="active">
                                                    <a href="#b-results" aria-controls="home" role="tab"
                                                       data-toggle="tab">
                                                        {!! trans('core.Search Results') !!}
                                                    </a>
                                                </li>
                                                <li role="presentation">
                                                    <a href="#b-map" aria-controls="profile" role="tab"
                                                       data-toggle="tab">
                                                        {!! trans('core.Map') !!}
                                                    </a>
                                                </li>
                                            </ul>

                                            <!-- Tab panes-->
                                            <div class="tab-content p0">
                                                <div id="b-results" role="tabpanel" class="tab-pane active">
                                                    <div class="row">
                                                        <div class="table-responsive">
                                                            <table id="beacon-results" class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width:5%">{!! trans('core.No') !!}</th>
                                                                        <th style="width:20%">{!! trans('core.UUID') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.MajorID') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.MinorID') !!}</th>
                                                                        <th style="width:10%">{!! trans('core.Active') !!}</th>
                                                                        <th style="width:20%">{!! trans('core.Organization') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="7" align="center">{!! trans('core.No data to display') !!}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="b-map" role="tabpanel" class="tab-pane">
                                                    <div role="form">
                                                        <!-- Map -->
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="beacon-map-canvas" style="margin: 0 auto; width: 100%; height: 400px;"></div>
                                                            </div>
                                                        </div>
                                                        <!--/ Map -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="device_search" role="tabpanel" class="tab-pane">
                                <div role="form">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label> {!! trans('core.Search by') !!}: </label>
                                                    </div>

                                                    <div class="col-md-5">
                                                        <label class="radio-inline c-radio">
                                                            <input type="radio" checked="" value="device" name="device_search_by" class="device-search-by" data-group="device-input-items">
                                                            <span class="fa fa-circle"></span>{!! trans('core.Device') !!}
                                                        </label>
                                                        <label class="radio-inline c-radio">
                                                            <input type="radio" value="deviceSet" name="device_search_by" class="device-search-by" data-group="device-set-input-items">
                                                            <span class="fa fa-circle"></span>{!! trans('core.DeviceSet') !!}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row D-input-items device-input-items">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label> {!! trans('core.Device Type') !!}: </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::select('device_type', $deviceTypesList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'device-type-id-select']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row D-input-items device-input-items">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label> {!! trans('core.Location') !!}: </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::select('location', $locationsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'location-id-select']) !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row D-input-items device-set-input-items">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <label> {!! trans('core.DeviceSet') !!}: </label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::select('deviceSetId', $deviceSetsList, '', ['class' => 'chosen-select input-md form-control form-format', 'id' => 'device-set-id-select']) !!}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6" style="padding-bottom:15px;">
                                            <div class="row">
                                                <div class="col-md-3">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="submit" value="Search"
                                                           class="btn btn-sm btn-default buttonAdd"
                                                           id="search-device"
                                                           style="background-color: rgb(93, 156, 236); color: white;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin-top:20px;">
                                        <div class="col-md-12">
                                            <div role="tabpanel" class="panel">
                                                <!-- Nav tabs-->
                                                <ul role="tablist" class="nav nav-tabs nav-justified">
                                                    <li role="presentation" class="active">
                                                        <a href="#d-results" aria-controls="home"
                                                           role="tab" data-toggle="tab">
                                                            {!! trans('core.Search Results') !!}
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#d-map" aria-controls="profile" role="tab"
                                                           data-toggle="tab">
                                                            {!! trans('core.Map') !!}
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes-->
                                                <div class="tab-content p0">
                                                    <div id="d-results" role="tabpanel"
                                                         class="tab-pane active">
                                                        <div class="row">
                                                            <div class="table-responsive">
                                                                <table id="device-results" class="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:5%">{!! trans('core.No') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.ID') !!}</th>
                                                                        <th style="width:30%">{!! trans('core.Name') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.Type') !!}</th>
                                                                        <th style="width:20%">{!! trans('core.Organization') !!}</th>
                                                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="7" align="center">{!! trans('core.No data to display') !!}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="d-map" role="tabpanel" class="tab-pane">
                                                        <div role="form">

                                                            <!-- Map -->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div id="device-map-canvas" style="margin: 0 auto; width: 100%; height: 400px;"></div>
                                                                </div>
                                                            </div>
                                                            <!--/ Map -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('form')
    <div id="myModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 id="myModalLabel" class="modal-title">{!! trans('core.Modal title') !!}</h4>
                </div>
                <div class="modal-body">
                    <!-- START panel-->
                    <div class="panel panel-default">
                        <div class="panel-heading">{!! trans('core.Stacked form') !!}</div>
                        <div class="panel-body">
                            <form role="form">
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label> {!! trans('core.Username') !!} </label>
                                    <input type="username" placeholder="Enter username" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.First Name') !!} </label>
                                    <input type="first_name" placeholder="First Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.Last Name') !!} </label>
                                    <input type="last_name" placeholder="Last Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.Email') !!} </label>
                                    <input type="email" placeholder="Enter email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>{!! trans('core.Simple wysiwyg') !!}</label>
                                    <div>
                                        <div data-role="editor-toolbar" data-target="#editor" class="btn-toolbar btn-editor">
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Font" class="btn btn-default">
                                                    <em class="fa fa-font"></em><b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" data-edit="fontName Arial" style="font-family:'Arial'">{!! trans('core.Arial') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontName Sans" style="font-family:'Sans'">{!! trans('core.Sans') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontName Serif" style="font-family:'Serif'">{!! trans('core.Serif') !!}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Font Size" class="btn btn-default">
                                                    <em class="fa fa-text-height"></em>&nbsp;<b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" data-edit="fontSize 5" style="font-size:24px">{!! trans('core.Huge') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontSize 3" style="font-size:18px">{!! trans('core.Normal') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontSize 1" style="font-size:14px">{!! trans('core.Small') !!}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a data-edit="bold" data-toggle="tooltip" title="Bold (Ctrl/Cmd+B)" class="btn btn-default">
                                                    <em class="fa fa-bold"></em>
                                                </a>
                                                <a data-edit="italic" data-toggle="tooltip" title="Italic (Ctrl/Cmd+I)" class="btn btn-default">
                                                    <em class="fa fa-italic"></em>
                                                </a>
                                                <a data-edit="strikethrough" data-toggle="tooltip" title="Strikethrough" class="btn btn-default">
                                                    <em class="fa fa-strikethrough"></em>
                                                </a>
                                                <a data-edit="underline" data-toggle="tooltip" title="Underline (Ctrl/Cmd+U)" class="btn btn-default">
                                                    <em class="fa fa-underline"></em>
                                                </a>
                                            </div>
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Hyperlink" class="btn btn-default">
                                                    <em class="fa fa-link"></em>
                                                </a>
                                                <div class="dropdown-menu">
                                                    <div class="input-group ml-xs mr-xs">
                                                        <input id="LinkInput" placeholder="{!! trans('core.URL') !!}" type="text" data-edit="createLink" class="form-control input-sm">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-sm btn-default">{!! trans('core.Add') !!}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a data-edit="unlink" data-toggle="tooltip" title="{!! trans('core.Remove Hyperlink') !!}" class="btn btn-default">
                                                    <em class="fa fa-cut"></em>
                                                </a>
                                            </div>
                                            <div class="btn-group pull-left">
                                                <a data-edit="undo" data-toggle="tooltip" title="{!! trans('core.Undo (Ctrl/Cmd+Z)') !!}" class="btn btn-default">
                                                    <em class="fa fa-undo"></em>
                                                </a>
                                                <a data-edit="redo" data-toggle="tooltip" title="{!! trans('core.Redo (Ctrl/Cmd+Y)') !!}" class="btn btn-default">
                                                    <em class="fa fa-repeat"></em>
                                                </a>
                                            </div>
                                        </div>
                                        <div style="overflow:scroll; height:250px;max-height:250px" class="form-control wysiwyg mt-lg">{!! trans('core.Type something ...') !!}</div>
                                    </div>
                                </div>
                                <br/>
                                <button type="add" class="btn btn-sm btn-default"> Add </button>
                            </form>
                        </div>
                    </div>
                    <!-- END panel-->
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default"> Cancel </button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent

    {!! Html::script('https://maps.googleapis.com/maps/api/js') !!}
    {!! Html::script('js/app/pages/asset_management.js') !!}
@stop
