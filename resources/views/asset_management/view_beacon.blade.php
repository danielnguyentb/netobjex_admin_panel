<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.View Beacon') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach ($beacon as $b)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans("core.{$b['label']}") !!}</label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('', $b['value'], ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-12">
                <div class="row buttonSet">
                    <div class="col-md-12">
                        {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>