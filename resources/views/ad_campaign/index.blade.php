@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')

    {!! Html::style('css/bootstrap-datepicker.css') !!}
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Ad Campaign') !!} </h3>
        <div class="container-fluid">

            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Marketing') !!}</a></li>
                    <li class="active">{!! trans('core.Ad Campaign') !!}</li>
                </ul>
                @if ($isSelectedOrg)
                    {!! Form::button('<b>' . trans('core.Add Ad Campaign') . '</b>', ['class' => 'mb-sm btn btn-primary add-button', 'id' => 'add_form']) !!}
                @endif
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! trans('core.Ad Campaigns') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="ad-campaign" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:40%">{!! trans('core.Name') !!}</th>
                                    <th style="width:45%">{!! trans('core.Type') !!}</th>
                                    <th style="width:15%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->

        </div>
    </div>
@stop

@section('form')
<div class="raw-data-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Export Raw Data') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
            {!! Form::open(['role' => 'form', 'action'=>'AdCampaignController@export','class' => 'edit-form row-form','data-1-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'id'=>'adcampaign-raw-form']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Start Date') !!}</label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('startdate', null, ['class'=>'form-control form-format', 'required', 'id'=>'start-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.End Date') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('enddate', null, ['class'=>'form-control form-format', 'required', 'id'=>'end-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-default buttonAdd buttonExport" value="{!! trans('core.Export') !!}">

                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="adCampaignId">
                    {!! Form::close()!!}
            </div>
        </div>
        
    </div>
    <div class="invite-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Invite Guest Users') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::open(['role' => 'form', 'class' => 'invite-form', 'novalidate']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group invite-form">
                                <div class='row'>
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Enter email addresses') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('emails', '', ['required', 'class' => 'form-control form-format']) !!}
                                        <p style="color: red;"> {!! trans('core.For multiple invitees , please use a comma separate list of email addresses') !!} </p>
                                        <h5 class="error_message emails_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Access to Object') !!}</label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('access_to_object', '', ['disabled', 'class' => 'form-control form-format']) !!}
                                        <h5 class="error_message access_to_object_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Name') !!}</label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('name', '', ['disabled', 'class' => 'form-control form-format']) !!}
                                        <h5 class="error_message name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Object Type') !!}</label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('object_type', '', ['disabled', 'class' => 'form-control form-format']) !!}
                                        <h5 class="error_message object_type_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    {!! Form::submit(trans('core.Invite'), ['class' => 'btn btn-sm btn-default buttonInvite']) !!}
                                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonClose']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- FILESTYLE-->
    @include('includes.js.filestyle')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent

    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/bootstrap-datepicker.js') !!}
    {!! Html::script('js/app/pages/ad_campaign.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $.app.getCls('adCampaign').manager({
                addLoading: '{!! trans('core.Loading Add Ad Campaign') !!}',
                editLoading: '{!! trans('core.Loading Edit Ad Campaign') !!}',
                loadingInviteGuests: '{!! trans('core.Loading Invite Guests') !!}'
            });
        });
    </script>
@stop
