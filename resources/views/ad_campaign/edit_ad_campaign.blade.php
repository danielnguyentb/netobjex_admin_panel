<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Ad Campaign') : trans('core.Edit Ad Campaign') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            @if ($form == 'edit')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                    <h5 class="error_message idShow_error"></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('name', $name, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message name_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Campaign Type') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10" style="width: 35%">
                                {!! Form::select('type', $campaignTypesList, $type, ['class' => 'chosen-select input-md form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Campaign URL') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::url('url', $url, ['class' => 'form-control form-format', 'required', 'placeholder' => 'include https://']) !!}
                                <h5 class="error_message url_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Select Device') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::select('devices[]', $devicesList, $devices, ['class' => 'chosen-select form-control', 'required', 'multiple']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <h5 class="at_least_one_device"> {!! trans('core.Select at least one device') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Select DeviceSet') !!} </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::select('deviceSets[]', $deviceSetsList, $deviceSets, ['class' => 'chosen-select form-control', 'multiple']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <h5 class="at_least_one_device"> {!! trans('core.Select at least one device set') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label> {!! trans('core.Start Date') !!} </label>
                            </div>
                            <div class="col-md-8">
                                {!! Form::text('start_date', $startDate, ['data-inputmask' => "'mask': '99/99/9999'", 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control form-format']) !!}
                                <h5 class="error_message start_date_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label> {!! trans('core.End Date') !!} </label>
                            </div>
                            <div class="col-md-9">
                                {!! Form::text('end_date', $endDate, ['data-inputmask' => "'mask': '99/99/9999'", 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control form-format']) !!}
                                <h5 class="error_message end_date_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label> {!! trans('core.Run On') !!}</label>
                            </div>
                        </div>
                        <div class="row run_on">
                            <div class="col-md-12">
                                <div class="radio">
                                    <label>
                                        {!! Form::radio('eachDay', 1, $eachDay, ['class' => 'select_all']) !!}
                                        {!! trans('core.All days within start/end timeframe') !!}
                                    </label>
                                    <br/>
                                    <label>{!! trans('core.or') !!}</label>
                                    <br/>
                                    <label>{!! trans('core.repeat every') !!}</label>
                                    <br/>

                                    <div class="checkbox select_days" style="width: 100%">
                                        <label>
                                            {!! Form::checkbox('eachMonday', 1, $eachMonday, ['class' => 'day']) !!}
                                            {!! trans('core.Monday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachTuesday', 1, $eachTuesday, ['class' => 'day']) !!}
                                            {!! trans('core.Tuesday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachWednesday', 1, $eachWednesday, ['class' => 'day']) !!}
                                            {!! trans('core.Wednesday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachThursday', 1, $eachThursday, ['class' => 'day']) !!}
                                            {!! trans('core.Thursday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachFriday', 1, $eachFriday, ['class' => 'day']) !!}
                                            {!! trans('core.Friday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachSaturday', 1, $eachSaturday, ['class' => 'day']) !!}
                                            {!! trans('core.Saturday') !!}
                                        </label>
                                        <label>
                                            {!! Form::checkbox('eachSunday', 1, $eachSunday, ['class' => 'day']) !!}
                                            {!! trans('core.Sunday') !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 hour">
                                <label> {!! trans('core.Start Hour') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                {!! Form::select('start_hour', $hoursList, $startHour, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                            <div class="col-md-1 minutes">
                                <label> {!! trans('core.Minutes') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                {!! Form::select('start_minute', $minutesList, $startMinute, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                            <div class="col-md-1 ampm">
                                <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                            </div>
                            <div class="col-md-2 chosen" style="padding-right:60px">
                                {!! Form::select('start_period', $periodsList, $startPeriod, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 hour">
                                <label> {!! trans('core.End Hour') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                {!! Form::select('end_hour', $hoursList, $endHour, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                            <div class="col-md-1 minutes">
                                <label> {!! trans('core.Minutes') !!} </label>
                            </div>
                            <div class="col-md-2 chosen">
                                {!! Form::select('end_minute', $minutesList, $endMinute, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                            <div class="col-md-1 ampm">
                                <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                            </div>
                            <div class="col-md-2 chosen" style="padding-right:60px">
                                {!! Form::select('end_period', $periodsList, $endPeriod, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Time Zone') !!} </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::select('timezone', $timezonesList, $timeZoneId, ['class' => 'chosen-select form-control', 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $labelImgStyle = 'style="margin-top: 35px;"'; ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2" {!! $labelImgStyle !!}>
                                <label> {!! trans('core.Campaign Background Image') !!}</label>
                            </div>
                            <div class="col-md-6" {!! $labelImgStyle !!}>
                                {!! Form::file('backimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                <h5 class="error_message image_error"> </h5>
                            </div>
                            <div class="col-md-4">
                                {!! $backimageid != '' ? Html::image(genDownloadUrl($backimageid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2" {!! $labelImgStyle !!}>
                                <label> {!! trans('core.Logo Image') !!}</label>
                            </div>
                            <div class="col-md-6" {!! $labelImgStyle !!}>
                                {!! Form::file('logoimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                <h5 class="error_message image_error"> </h5>
                            </div>
                            <div class="col-md-4">
                                {!! $logoimageid != '' ? Html::image(genDownloadUrl($logoimageid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2" {!! $labelImgStyle !!}>
                                <label> {!! trans('core.Sponsor Image') !!}</label>
                            </div>
                            <div class="col-md-6" {!! $labelImgStyle !!}>
                                {!! Form::file('sponsorimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                <h5 class="error_message image_error"> </h5>
                            </div>
                            <div class="col-md-4">
                                {!! $sponsorimageid != '' ? Html::image(genDownloadUrl($sponsorimageid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if ($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>