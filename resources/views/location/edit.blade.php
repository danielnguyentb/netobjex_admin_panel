    <!-- Map -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {!! trans('core.Google Map') !!}
                <div class="icon-size-fullscreen fullscreen-map"
                     data-toggle="modal"
                     data-target="#myModalLarge">
                </div>
            </div>
            <div class="panel-body">
                 <div id="map" class="gmap" ></div>
            </div>
        </div>
    </div>
</div>

<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                 @if($form == 'add')
                    <h3> {!! trans('core.Add Location') !!} </h3>
                 @else
                    <h3> {!! trans('core.Edit Location') !!} </h3>
                 @endif
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $name !!}" name="name" class="form-control form-format" required>
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Address1') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $address !!}" onblur="initialize.getLocation()" name="address" id="address" class="form-control form-format address" required>
                                <h5 class="error_message address_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Address2') !!} </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $address2 !!}" name="address2" class="form-control form-format">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.City') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $city !!}" onblur="initialize.getLocation()" name="city"  class="form-control form-format city" required>
                                <h5 class="error_message city_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3" style="margin-right: -39px">
                                <label> {!! trans('core.Zip Code') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" value="{!! $zip !!}"  name="zip" class="form-control form-format" data-inputmask="'mask':'99999-99'" required >
                                <h5 class="error_message zip_code_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Country') !!} </label>
                            </div>
                            <div class="col-md-8 country-over">
                                <select class="chosen-select input-md form-control form-format country" name="country" class="country" onchange="initialize.getLocation()">
                                    @foreach($countriesList as $k => $v)
                                        @if($k == $country)
                                            <option value="{!! $k !!}" selected="selected">{!! $v!!}</option>
                                        @else
                                            <option value="{!! $k !!}">{!! $v!!}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.State') !!} </label>
                            </div>
                            <div class="col-md-8 state-over">
                                <select class="chosen-select input-md form-control form-format state-drop " name="state-drop">
                                    @foreach($statesList as $k => $v)
                                        <option value="{!! $k !!}" selected="selected">{!! $v!!}</option>
                                        @if($k == $state)
                                            <option value="{!! $k !!}" selected="selected">{!! $v!!}</option>
                                        @else
                                            <option value="{!! $k !!}">{!! $v!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                 <input type="text" value="{!! $state !!}" name="state-text"  class="form-control form-format state-text hidden" >
                                 <input type="hidden" value="1" name="state" class="form-control form-format state" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.GPS Latitude') !!}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" value="{!! $gpslat !!}" name="gpslat" class="form-control form-format gpslat" >
                                <h5 class="error_message latitude_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.GPS Longitude') !!}</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" value="{!! $gpslng !!}" name="gpslng" class="form-control form-format gpslng" >
                                <h5 class="error_message longitude_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Description') !!} </label>
                            </div>
                            <div class="col-md-11">
                                <textarea class="description" name="description" placeholder="{!! trans('core.Type something...') !!}">{!! $description !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">initialize.start();</script>



