<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Messaging') : trans('core.Edit Messaging') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            @if ($form == 'edit')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-11">
                                    {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                    <h5 class="error_message idShow_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('name', $name, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Type') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('type', $messageTypesList, $messagetype, ['class' => 'chosen-select form-control message-type-select', 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row email-type{!! $messagetype == 1 ? '' : ' hide' !!}">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.From') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('from', $mfrom, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message from_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row email-type{!! $messagetype == 1 ? '' : ' hide' !!}">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.To') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('to', $mto, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message to_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row email-type{!! $messagetype == 1 ? '' : ' hide' !!}">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Reply-to') !!} </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('reply_to', $replyto, ['class' => 'form-control form-format']) !!}
                                <h5 class="error_message reply_to_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row email-type{!! $messagetype == 1 ? '' : ' hide' !!}">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Subject') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('subject', $subject, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message subject_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label>  </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::select('variables', $variablesList, NULL, ['class' => 'variable-drop input-md form-control form-format']) !!}

                                <a href="javascript:void(0)" class="btn btn-sm btn-default insert-var" style="background:#5d9cec; color:#FFF;" >{!! trans('core.Insert Variable') !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {{--<h2>This Section is for Text Editor. Please use a suitable editor for your need</h2>--}}
                    <div class="row">
                        <div class="col-md-1">
                            <label>{!! trans('core.Message Body') !!} <span class="asterisk">*</span> </label>
                        </div>

                        <textarea id="message-body-editor" title=""></textarea>
                        <textarea class="hide" id="message-body-content" title="" name="message_body">{!! $body !!}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="padding:20px 0;">
                    <div class="panel-heading">
                        <div class="panel-title">{!! trans('core.Variables') !!}
                            <a style="background: #5d9cec; color: #FFF; float: right;" class="btn btn-sm btn-default add-var" href="javascript:void(0)">
                                {!! trans('core.Add Variable') !!}
                            </a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-vars">
                            <thead>
                                <tr>
                                    <th style="width:70%">{!! trans('core.Name') !!}</th>
                                    <th style="width:30%">{!! trans('core.Action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($variablesList)
                                    @foreach ($variablesList as $k => $v)
                                        <tr>
                                            <td>
                                                {!! Form::text('vars[]', $v, ['data-id' => $k]) !!}
                                            </td>
                                            <td><div class='icon-pencil icon_row edit'></div> <div class='icon-trash icon_row delete'></div></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if ($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>