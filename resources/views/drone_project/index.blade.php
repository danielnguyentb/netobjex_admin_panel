@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
            <h3>Project Management</h3>
            <div class="container-fluid">
                <!-- Button -->
                <div class="row">
                    <ul class="breadcrumb">
                        <li><a href="#">Project Management</a></li>
                        <li class="active">Drone Project </li>
                    </ul>
                    <button type="button" class="mb-sm btn btn-primary add-button" id="add_form">
                        <b>Add Project</b>
                    </button>
                </div>
                <!--/ Button -->

                <!-- Datatable -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Projects
                                <div class='icon-question help'
                                     data-container="body"
                                     data-toggle="popover"
                                     data-placement="left"
                                     data-trigger="hover"
                                     data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table id="projects" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:16%">Name</th>
                                            <th style="width:5%"><i class="icon-picture icons" style="font-size:20px;"></i></th>
                                            <th style="width:15%"><i class="icon-camera icons" style="font-size:20px;"></i></th>
                                            <th style="width:15%"><i class="icon-rocket icons" style="font-size:20px;"></i></th>
                                            <th style="width:15%">Mission</th>
                                            <th style="width:10%">Start Date</th>
                                            <th style="width:24%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Datatable -->

            </div>
        </div>
@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent

    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/drone.js') !!}

@stop
