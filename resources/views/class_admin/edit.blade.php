<div class="main-content ">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                @if($form == 'add')
                    <h3> {!! trans('core.Add Class') !!} </h3>
                @else
                    <h3> {!! trans('core.Update Class') !!} </h3>
                @endif

            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" value="{!! $name !!}" name="name" class="form-control form-format" required>
                                    <h5 class="error_message name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Tag') !!} <span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <select name="class" class="form-control form-format" required>
                                        @foreach($tags as $k=>$v)
                                            @if($v == $class)
                                                <option value="{!! $v !!}" selected>{!! $v !!}</option>
                                            @else
                                                <option value="{!! $v !!}">{!! $v !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    {{--<input type="text" value="{!! $class !!}" name="class" class="form-control form-format" >--}}
                                    <h5 class="error_message name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) required </h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

