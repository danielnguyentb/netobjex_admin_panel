@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Protocol Management') !!} </h3>
        <div class="container-fluid">

            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Super Admin') !!}</a></li>
                    <li class="active">{!! trans('core.Protocol Management') !!}</li>
                </ul>
                <button type="button" class="mb-sm btn btn-primary add-button" id="add_form">
                    <b>{!! trans('core.Add Protocol') !!}</b>
                </button>
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">{!! trans('core.Protocols') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="protocol-management" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:85%">{!! trans('core.Name') !!}</th>
                                    <th style="width:15%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->

        </div>
    </div>
    
@stop
@section('custom_scripts')
    <script type="text/javascript">
        @if (!empty($response))
            @if ($response['status'])
                $.growl.notice({message: '{!! trans('core.$response["message"]') !!}'});
            @else
                $.growl.error({message: '{!! $response['message'] !!}'});
            @endif
        @endif
    </script>
@stop
@section('pageJs')
    <!-- SPARKLINE
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent

     {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/protocol.js') !!}
@stop
