        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                    <h3> {!! ($form == 'add') ? trans('core.Add Protocol') : trans('core.Edit Protocol') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- <form role="form" class="edit-form"> -->
                <!-- <form method="POST" action="http://netobjex.com/protocol-management/edit" accept-charset="UTF-8" role="form" class="edit-form"><input name="_token" type="hidden" value="Nd2B2St0OseT8X3nejN78jKE0q7FeZBJOElG743Y"> -->
                {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-1">
                                <label> Protocol<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                <!-- <input type="text" name="protocol" class="form-control form-format" required> -->
                                <!-- <input class="form-control form-format" required="required" name="name" type="text" value="MQTTsfdddddddddddddddddddddddddddddddddddddddddddd"> -->
                                {!! Form::text('name',$name,['class'=>'form-control form-format', 'required']) !!}
                                <h5 class="error_message protocol_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h5> (<span class="asterisk">*</span>) required</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                <!-- <input type="button" class="btn btn-sm btn-default buttonUpdate" data-form="protocol" value="Update"> -->
                                <!-- <button type="button" class="btn btn-sm btn-default buttonUpdate" data-form="protocol-management"> {!! trans('core.Update')!!} </button>
                                <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel')!!} </button> -->
                                 @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::hidden('id',$id,['class'=>'form-control form-format']) !!}
            
                {!! Form::close() !!}
            </div>
        </div>