<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.Edit BeaconSet') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
         {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $name !!}" name="name" class="form-control form-format" required>
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.UUID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control form-format uuids" value="" style="background: #ffffff" readonly="readonly" required>
                                <input type="text" class="uuids-hidden" style="opacity: 0; position: absolute;" required />
                                <ul class="uuids-list uuid-hide">
                                    @if(sizeof($services) !=0 )
                                        <li><input type="checkbox" value="*" class="uuid-item-all" name="uuidAll"/>&nbsp;&nbsp;All</li>
                                        @foreach($services as $k=>$v)
                                        <li>
                                            @if(in_array($v->uuidvalue, $uuidArr))
                                                 <input type="checkbox" class="uuid-item uuid-item-single" name="uuidItem[]" value="{!! $v->uuidvalue !!}" checked/>&nbsp;
                                            @else
                                                 <input type="checkbox" class="uuid-item uuid-item-single" name="uuidItem[]" value="{!! $v->uuidvalue !!}"/>&nbsp;
                                            @endif
                                            {!! $v->uuidvalue !!}
                                            (@foreach($v->beaconnames as $k1=>$v1)
                                                {!! $v1 !!},
                                            @endforeach)

                                        </li>
                                        @endforeach
                                    @endif

                                </ul>
                                <h5 class="error_message uuid_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Major ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="majorids" value="{!! $majorids !!}" class="form-control form-format" required>
                                <h5 class="error_message major_id_error"> </h5>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Minor ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" value="{!! $minorids !!}" name="minorids" class="form-control form-format" required>
                                <h5 class="error_message minor_id_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            @if($form == 'update')
            <div class="row" style="margin: 20px 0;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="javascript:" class="btn btn-sm btn-default btn-search">Search</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            Beacon list
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="panel panel-default">
                                <div class="table-responsive">

                                    <table id="beaconSearch" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width:21.25%">Name</th>
                                            <th style="width:21.25%">UUID</th>
                                            <th style="width:21.25%">Major ID</th>
                                            <th >Minor ID</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                    <div class="panel-body loader-add loader-demo loading-beacon-set" style="display: none"><h4>{!! trans('Loading beacons') !!}</h4><div class="ball-pulse"><div></div><div></div><div></div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                            @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    var _selected = '{!! $sizeUuid!!}';
    var _uuidLength = $('.uuid-item-single').length;
    beaconSet.setValueToUuid();
    beaconSet.trigger();
 </script>
