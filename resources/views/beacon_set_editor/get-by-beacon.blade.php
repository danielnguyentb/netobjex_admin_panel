@if(sizeof($data) == 0)
    <tr>
        <td colspan="4">{!! trans('core.No data to display') !!}</td>
    </tr>
@else
    @foreach($data as $k => $v)
        <tr>
            <td >{!! $v->name !!}</td>
            <td >{!! $v->id !!}</td>
            <td >{!! $v->majorid !!}</td>
            <td >{!! $v->minorid !!}</td>
        </tr>
    @endforeach
@endif
