<div class="table-responsive">
    <table class="table table-striped attribute" id="ruleCondition">
        <thead>
        <tr>
             <th style="width:10%" colspan="2">{!! trans('core.Variable') !!}</th>
             <th style="width:20%">{!! trans('core.Mapping Value') !!}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($variable as $k => $v)
                <tr class="variableMappingRow">
                    <td>
                        <input  class="variable form-control form-format" name="variable[]" value="{!! $v->name !!}" type="text" readonly/>
                        <input type="hidden" name="variable-value[]" value="{!! $v->id !!}" />
                    </td>
                    <td>
                        <select name="mapping[]" class="mapping form-control form-format">
                            @foreach($mapping as $k=>$v)
                                <option value="{!! $v !!}">{!! $v !!}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>