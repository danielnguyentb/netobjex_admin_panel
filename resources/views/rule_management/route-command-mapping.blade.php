<div class="table-responsive">
    <table class="table table-striped attribute" id="ruleCondition">
        <thead>
        <tr>
            <th style="width:10%" colspan="2">{!! trans('core.Variable') !!}</th>
            <th style="width:20%">{!! trans('core.Mapping Value') !!}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($variable as $k => $v)
                <tr class="variableMappingRow">
                    <td style="min-width: 130px;"><label>Parameter {!! ($k+1) !!} : </label></td>
                    <td>
                        <input  class="variable form-control form-format" name="defaultvalue[]" value="{!! $v->name !!}" type="text" readonly/>
                        <input type="hidden" name="variable-value[]" value="{!! $v->id !!}" />
                        <input type="hidden" name="scheduledcommandid[]" value="{!! $routId !!}" />
                    </td>
                    <td>
                       <input type="text" class="form-control form-format" name="mapping[]" value="{!! $v->defaultvalue !!}"/>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>