<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                @if($form == 'edit')
                     <h3> {!! trans('core.Edit Rule') !!} </h3>
                @else
                     <h3> {!! trans('core.Add Rule') !!} </h3>
                @endif
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input value="{!! $name !!}" type="text" name="name" class="form-control form-format" required>
                                    <h5 class="error_message name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Device Type') !!} </label>
                                </div>
                                <div class="col-md-4 device-type-changed">
                                    <select name="devicetypeid" class="chosen-select input-md form-control form-format device-type-id ">
                                        @foreach($types as $k=>$v)
                                            @if($v->id == $devicetypeid)
                                                 <option  value="{!! $v->id!!}" selected>{!! $v->name !!}</option>
                                            @else
                                                 <option value="{!! $v->id!!}">{!! $v->name !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        @if($active == true)
                                            <input type="checkbox" name="active" checked> {!! trans('core.Rule in Effect') !!}
                                        @else
                                            <input type="checkbox" name="active"> {!! trans('core.Rule in Effect') !!}
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Device Set') !!}</label>
                                </div>
                                <div class="col-md-4">
                                    <select name="devicesetid" class="input-md form-control form-format editor">
                                        <option value="">-- Select Device set  --</option>
                                        @foreach($deviceSetEditors as $k=>$v)
                                            @if($v->id == $devicesetid)
                                                 <option  value="{!! $v->id!!}" selected>{!! $v->name !!}</option>
                                            @else
                                                 <option value="{!! $v->id!!}">{!! $v->name !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Queue Topic Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input value="{!! $queuetopicname !!}" name="queuetopicname" type="text" name="queue_topic_name" class="form-control form-format" required>
                                    <h5 class="error_message queue_topic_name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label> {!! trans('core.Interval Time') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                @if($interval == '')
                                    <input type="number" name="interval" class="form-control form-format" required>
                                @else
                                    <input type="number" value="{!! $interval !!}" name="interval" class="form-control form-format" required>
                                @endif
                                <h5 class="error_message interval_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            @if($deviceAttr != false)
                            <button type="button" class="mb-sm btn btn-primary condition_button pull-right">
                                <b>{!! trans('core.Add Condition') !!}</b>
                            </button>
                            @endif
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-striped attribute" id="ruleCondition">
                                    <thead>
                                    <tr>
                                        <th style="width:10%"><input type="checkbox"></th>
                                        <th style="width:20%">{!! trans('core.Attribute') !!}</th>
                                        <th style="width:20%">{!! trans('core.Operator') !!}</th>
                                        <th style="width:20%">{!! trans('core.Value') !!}</th>
                                        <th style="width:20%">{!! trans('core.And/Or') !!}</th>
                                        <th style="width:10%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody class="condition-attr">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding:15px 30px 5px!important;">
                    <button type="button" class="mb-sm btn btn-primary pull-right btn-view-add-route"> <b>{!! trans('core.Add router') !!}</b> </button>
                </div>

                <div class="add-route-view">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>  {!! trans('core.Route type') !!}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <select  class="input-md form-control form-format" name="routeType" id="routeType" >
                                            <option value="1">Enterprise Gateway</option>
                                            <option value="2">Messaging</option>
                                            <option value="3">Command</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Route') !!}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <select  class="input-md form-control form-format" name="routeId" id="routeId"></select>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row map-value"></div>
                    <div class="row text-right" style="padding: 15px 20px 15px;">
                        <span class="mb-sm btn btn-cancel btn-cancel-route">{!! trans('core.Cancel') !!}</span>
                        <span class="mb-sm btn btn-primary btn-save-route"> {!! trans('core.Add') !!}</span>
                    </div>
                </div>

                <div class="row" style="padding: 20px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="table">
                                    <table class="table table-striped attribute" id="ruleCondition">
                                        <thead>
                                        <tr>
                                            <th style="width:50">{!! trans('core.Route type') !!}</th>
                                            <th style="width:50%">{!! trans('core.Route') !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody >
                                            <tr>
                                                <td><label> {!! trans('core.Enterprise Gateway') !!} </label></td>
                                                <td>
                                                    <select name="enterprisegatewayid" class="chosen-select input-md form-control form-format">
                                                        <option value="">-- Select Enterprise --</option>
                                                        @foreach($EGs as $k=>$v)
                                                            @if($v->id == $enterprisegatewayid)
                                                                 <option value="{!! $v->id !!}" selected>{!! $v->name !!}</option>
                                                            @else
                                                                 <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label>  {!! trans('core.Messaging') !!}</label></td>
                                                <td>
                                                    <select name="messageid" class="chosen-select input-md form-control form-format">
                                                        <option value="">-- Select Messaging --</option>
                                                        @foreach($messages as $k=>$v)
                                                            @if($v->id == $messageid)
                                                                 <option value="{!! $v->id !!}" selected>{!! $v->name !!}</option>
                                                            @else
                                                                 <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label> {!! trans('core.Command') !!} </label></td>
                                                <td>
                                                    <select name="commandid" class="command-drop input-md form-control form-format">
                                                        <option value="">-- Select Command --</option>
                                                        @foreach($commands as $k=>$v)
                                                            @if($v->id == $commandid)
                                                                 <option value="{!! $v->id !!}" selected>{!! $v->name !!}</option>
                                                            @else
                                                                 <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         {!! Form::close() !!}
    </div>

</div>






<script type="text/javascript">
    var filterByNameDisplay = true;
    var attr = ''; var isLoadAttr = false;
</script>
@if($form == 'edit')
    <script type="text/javascript">
        _dataTableTrigger({
            table: 'ruleCondition',
            colDefs: ['checkbox','devicetypeattributeid', 'operand', 'value', 'isand'],
            _object: 'RuleCondition',
            url: {GET: '', POST: '', FORM: '', DELETE: '/rule-management/delete-condition'}
        })
    </script>
@endif

