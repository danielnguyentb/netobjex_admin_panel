<tr>
    <td style="width:10%"><input type="checkbox"></td>
    <td style="width:20%">
        <select name="attribute[]">
        @foreach($data as $k=>$v)
            <option value="{!! $v->id!!}">{!! $v->name!!}</option>
        @endforeach
        </select>
    </td>
    <td style="width:20%">
        <select name="operand[]" >
            @foreach($operators['valKey'] as $k=>$v)
                <option value="{!! $k !!}">{!! $v!!}</option>
            @endforeach
        </select>
    </td>
    <td style="width:20%"><input type="text" name="value[]" required/></td>
    <td style="width:20%">
        <select name="isand[]" id="">
            <option value="true">And</option>
            <option value="false">Or</option>
        </select>
    </td>
    <td style="width:10%"><div class="icon-trash icon_row attr-delete"></div></td>
</tr>
