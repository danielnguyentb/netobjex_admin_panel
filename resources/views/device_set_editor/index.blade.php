@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.DeviceSet Editor') !!} </h3>
        <div class="container-fluid">

            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Asset Management') !!}</a></li>
                    <li class="active">{!! trans('core.DeviceSet Editor') !!}</li>
                </ul>
                @if($existCustomerId)
                    <button type="button" class="mb-sm btn btn-primary add-button" id="add_form">
                        <b>{!! trans('core.Add DeviceSet') !!}</b>
                    </button>
                @endif
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! trans('core.DeviceSet') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="deviceSetEditor" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:21.25%">{!! trans('core.Name') !!}</th>
                                    <th style="width:15%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->
            <div class='panel-body loader-add hide loader-demo'>
                <h4> {!! trans('core.Loading View Device set editor') !!} </h4>
                <div class='ball-pulse'>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>

            <div class='panel-body loader-edit hide loader-demo'>
                <h4> {!! trans('core.Loading View Device set editor') !!} </h4>
                <div class='ball-pulse'>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>

        </div>
    </div>
@stop


@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/deviceSetEditor.js') !!}
    {!! Html::script('js/app/pages/dataTableTrigger.js') !!}
@stop
