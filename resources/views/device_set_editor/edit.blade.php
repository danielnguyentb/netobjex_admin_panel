<div class="main-content device-set-editor">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.Update DeviceSet') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">

                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.DeviceSet Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-4">
                                    <input value="{!! $name !!}" type="text" name="name" class="form-control input_full" required>
                                    <h5 class="error_message device_id_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.DeviceSet Type') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-4">
                                    <select name="devicetypeid" class="chosen-select input-md form-control ">
                                        @foreach ($types as $k => $v)
                                           @if($v->id == $devicetypeid )
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row filter-by-name">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> Name </label>
                                </div>
                                <div class="col-md-4" >
                                    <input type="text" name="device_name" class="device-name form-control form-format input_full ">
                                    <h5 class="error_message device_name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row filter-by-locational hidden">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Location') !!} </label>
                                </div>
                                <div class="col-md-4">
                                    <select class="chosen-select input-md form-control form-format locational-name">
                                        @foreach ($locations as $k => $v)
                                            <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                        @endforeach
                                    </select>
                                    <h5 class="error_message device_name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.By') !!} </label>
                                </div>
                                <div class="col-md-4" id="filterBy">
                                    <select class="chosen-select filter-by input-md form-control form-format">
                                        <option value="0">Name</option>
                                        <option value="1">Location</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>  </label>
                                </div>
                                <div class="col-md-4">
                                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-search-device " style="background:#5d9cec; color:#FFF;" >Search </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h5>(<span class="asterisk">*</span>) required</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="padding:20px 0;">
                        <div class="table-responsive">

                            <div id="deviceBelongDeviceset">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:10%">#</th>
                                        <th style="width:30%">{!! trans('core.Name') !!}</th>
                                        <th style="width:30%"> {!! trans('core.Device ID') !!}</th>
                                        <th style="width:20%">{!! trans('core.Location') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div class="panel-body loader-demo device-loading hidden" style="">
                                    <h4> {!! trans('core.Device loading') !!} </h4>
                                    <div class="ball-pulse">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="col-md-12">
                            <h5>{!! trans('core.Current device') !!}</h5>
                     </div>
                </div>

                <div class="row" id="option_attributes">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="table-responsive">
                                <table id="deviceSetCurrent" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:30%">{!! trans('core.Name') !!}</th>
                                        <th style="width:30%"> {!! trans('core.Device ID') !!}</th>
                                        <th style="width:20%">{!! trans('core.Location') !!}</th>
                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    var filterByNameDisplay = true;
</script>
@if($form == 'edit')
    <script type="text/javascript">
        _dataTableTrigger({
            table: 'deviceSetCurrent',
            colDefs: ['name', 'deviceid', 'location.name' ],
            _object: 'DeviceSetdevice',
            url: {GET: '', POST: '', FORM: '', DELETE: '/device-set-editor/delete-device-set-device'}
        })
    </script>
@endif
