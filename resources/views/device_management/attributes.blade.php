<div class="row">
    <div class="col-md-12">
        <h4> {!! trans('core.Required Attributes') !!} <span class="show_hide required">{!! trans('core.Show/Hide') !!}</span></h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="required_attributes">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="width:20%">{!! trans('core.Attribute') !!}</th>
                        <th style="width:80%">{!! trans('core.Value') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($required as $k=>$v)
                          <tr>
                             <td style="width:30%">
                                 <div class="form-group">
                                     <label class="attribute_label"> {!! $v['name'] !!}<span class="asterisk">*</span> </label>
                                 </div>
                             </td>
                             <td style="width:70%">
                                 <div class="form-group">
                                     <input type="text" name="attrrequiredvalue[]" class="form-control form-format" required>
                                     <input type="hidden" name="attrrequiredname[]" class="form-control form-format" value="{!! $v['name'] !!}">
                                     <input type="hidden" name="attrrequiredid[]" class="form-control form-format" value="{!! $v['id'] !!}">
                                     <h5 class="error_message firmware_version_id_error"> </h5>
                                 </div>
                             </td>
                         </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
