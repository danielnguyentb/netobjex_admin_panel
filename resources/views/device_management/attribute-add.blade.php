 {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'id' => 'attrOption', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => '']) !!}
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label>Attribute<span class="asterisk">*</span> </label>
                    </div>
                    <div class="col-md-9">
                          <input type="text" name="name" class="form-control form-format" required>
                        <h5 class="error_message device_id_error"> </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="col-md-12 col-lg-12">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <label> Name<span class="asterisk">*</span> </label>
                    </div>
                    <div class="col-md-9">
                          <input type="text" name="value" class="form-control form-format" required>
                        <h5 class="error_message device_id_error"> </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row buttonSet">
                <div class="col-md-12">
                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                </div>
            </div>
        </div>
    </div>

{!! Form::close() !!}