<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! trans('core.Update Device') !!} </h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Device ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    @if($form == 'update')
                                        <input type="text" name="deviceid" value="{!! $deviceid !!}" class="form-control form-format" required readonly>
                                    @else
                                        <input type="text" name="deviceid" value="" class="form-control form-format" required>
                                    @endif
                                    <h5 class="error_message device_id_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> {!! trans('core.Device Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" value="{!! $name !!}" name="name" class="form-control form-format" required>
                                    <h5 class="error_message device_name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <label> {!! trans('core.Device Type') !!} </label>
                                </div>
                                <div class="col-md-9 col-lg-9 device-type-changed">
                                    <select name="type" class="chosen-select device-type-id input-md form-control form-format">
                                        @foreach ($types as $k => $v)
                                           @if($v->id == $type )
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <label> {!! trans('core.Parent Device') !!} </label>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <select name="parentid" class="chosen-select input-md form-control form-format">
                                        <option value="">Select Device Type...</option>
                                         @foreach ($parents as $k => $v)
                                           @if($v->id == $parentid)
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <label> {!! trans('core.Location') !!} </label>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <select name="locationid" class="chosen-select input-md form-control form-format">
                                         @foreach ($locations as $k => $v)
                                           @if($v->id == $locationid)
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <label> {!! trans('core.Group') !!} </label>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <select name="customerid" class="chosen-select input-md form-control form-format">
                                         @foreach ($groups as $k => $v)
                                           @if($v->id == $customerid)
                                               <option value="{!! $v->id !!}" selected="selected">{!! $v->name !!}</option>
                                           @else
                                               <option value="{!! $v->id !!}">{!! $v->name !!}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <label> {!! trans('core.Tags') !!} </label>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <textarea  name="tags" id="" cols="30" rows="10">{!! $tags !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="attribute">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="col-md-6 col-lg-6">
                                <h4> {!! trans('core.Required Attributes') !!} <span class="show_hide required">{!! trans('core.Show/Hide') !!}</span></h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="required_attributes">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width:20%">{!! trans('core.Attribute') !!}</th>
                                            <th style="width:80%">{!! trans('core.Value') !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($required as $k=>$v)
                                             <tr>
                                                <td style="width:30%">
                                                    <div class="form-group">
                                                        <label class="attribute_label"> {!! $v['name'] !!}<span class="asterisk">*</span> </label>
                                                    </div>
                                                </td>
                                                <td style="width:70%">
                                                    <div class="form-group">
                                                        <input type="text" name="attrrequiredvalue[]" value="{!! $v['value'] !!}" class="form-control form-format" required>
                                                        <input type="hidden" name="attrrequiredname[]" class="form-control form-format" value="{!! $v['name'] !!}">
                                                        <input type="hidden" name="attrrequiredid[]" class="form-control form-format" value="{!! $v['id'] !!}">
                                                        <h5 class="error_message firmware_version_id_error"> </h5>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($form == 'update')
                <div class="row" style="padding-top: 10px;">
                    <div class="col-md-12">
                        <div class="col-lg-6 col-md-6 text-left"> <h4> {!! trans('core.Optional Attributes') !!} <span class="show_hide optional">{!! trans('core.Show/Hide') !!}</span></h4></div>
                        <div class="col-lg-6 col-md-6 text-right">
                            <button type="button" class="mb-sm btn btn-primary add-button-option" id="add_form">
                                 <b>{!! trans('core.Add') !!}</b>
                             </button>
                         </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="row " id="option_attributes">
                    <div class="col-md-12">
                        <!-- Datatable -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="table-responsive">
                                        <table id="deviceOptionAttribute" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th style="width:10%"><input class="day" type="checkbox" value="Mon"></th>
                                                <th style="width:40%">{!! trans('core.Attribute') !!}</th>
                                                <th style="width:35%">{!! trans('core.Value') !!}</th>
                                                <th >{!! trans('core.Action') !!}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Datatable -->
                    </div>
                </div>
                <script type="text/javascript">
                    callBackDataTable.build();
                </script>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <h5>(<span class="asterisk">*</span>) {!! trans('core.required') !!}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12" >
                                <div id="readyForPost"></div>
                                <div class="buttonUpdate" ></div>
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default', 'style' => 'background: green; color: #ffffff', 'onclick' => 'update()' ]) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

