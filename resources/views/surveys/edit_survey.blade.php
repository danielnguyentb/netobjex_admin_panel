<style>
    .form-group .bootstrap-filestyle{
        float:left;
    }

</style>
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h4>{!! trans('core.Edit Survey') !!}</h4> -->
                        <h4> {!! ($form == 'add') ? trans('core.Add Survey') : trans('core.Edit Survey') !!} </h4>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- <form role="form" class="edit-form"> -->
                {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form','data-1-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id, 'id'=>'survey-form']) !!}
                    @if($form =='edit')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('idShow',$id,['class'=>'form-control form-format','required', 'readonly', 'disabled']) !!}
                                        <h5 class="error_message id_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <!-- <input type="text" name="name" class="form-control form-format" required> -->
                                        {!! Form::text('name',$name,['class'=>'form-control form-format', 'required']) !!}
                                        <h5 class="error_message name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Survey Type') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <!-- <input type="text" name="name" class="form-control form-format" required> -->
                                        {!! Form::select('surveytype',['3'=>'TEXT', '2'=>'480x75 PANEL', '1'=>'450x370 PANEL'], $surveytype,['class'=>'form-control chosen-select', 'required']) !!}
                                        <h5 class="error_message name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Select Device') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::select('devices[]', $listDevice, $devices,['class'=>'chosen-select form-control','multiple'=>'multiple','id'=>'select-device', 'required']) !!}
                                    </div>
                                    <div class="row">
                                        <h5 class="at_least_one_device"> {!! trans('core.Select at least one device') !!} </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Select DeviceSet') !!}</label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::select('deviceSets[]', $listDeviceSet, $deviceSets,['class'=>'chosen-select form-control','multiple'=>'multiple','id'=>'select-deviceset']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Description') !!} </label>
                                    </div>
                                    <div class="col-md-10">
                                        <!-- <textarea class="description" placeholder="{!! trans('core.Type something...') !!}" required></textarea> -->
                                        {!! Form::textarea('description', $description, ['class'=>'description', 'placeholder'=>'Type something...']) !!}
                                        <h5 class="error_message description_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Start Date') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('startdate', $startdate, ['class'=>'form-control form-format', 'required', 'id'=>'start-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.End Date') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('enddate', $enddate, ['class'=>'form-control form-format', 'required', 'id'=>'end-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label> {!! trans('core.Run On') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                </div>
                                <div class="row run_on">
                                    <div class="col-md-12">
                                        <div class="radio">
                                            <label>
                                                <!-- <input class="select_all" type="radio" name="optionsRadios" value="option2" checked> -->
                                                {!! Form::radio('eachday', '1', $eachday, ['class'=>'select_all','id'=>'allDay']) !!}
                                                {!! trans('core.All days within start/end timeframe') !!}
                                            </label>
                                            <br/>
                                            <label>{!! trans('core.or') !!}</label>
                                            <br/>
                                            <label>{!! trans('core.repeat every') !!}</label>
                                            <br/>
                                            <div class="checkbox select_days" style="width: 100%" id="eachDay">
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Mon"> {!! trans('core.Monday') !!} -->
                                                    {!! Form::checkbox('eachmonday', '1', $eachmonday,['class' => 'day']) !!}{!! trans('core.Monday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Tues"> {!! trans('core.Tuesday') !!} -->
                                                    {!! Form::checkbox('eachtuesday', '1', $eachtuesday,['class' => 'day']) !!}{!! trans('core.Tuesday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Wed"> {!! trans('core.Wednesday') !!} -->
                                                    {!! Form::checkbox('eachwednesday', '1', $eachwednesday,['class'=> 'day']) !!}{!! trans('core.Wednesday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Thur"> {!! trans('core.Thursday') !!} -->
                                                    {!! Form::checkbox('eachthirsday', '1', $eachthirsday,['class' => 'day']) !!}{!! trans('core.Thursday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Fri"> {!! trans('core.Friday') !!} -->
                                                    {!! Form::checkbox('eachfriday', '1', $eachfriday,['class' => 'day']) !!}{!! trans('core.Friday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Sat"> {!! trans('core.Saturday') !!} -->
                                                    {!! Form::checkbox('eachsaturday', '1', $eachsaturday,['class' => 'day']) !!}{!! trans('core.Saturday') !!}
                                                </label>
                                                <label>
                                                    <!-- <input class="day" type="checkbox" value="Sun"> {!! trans('core.Sunday') !!} -->
                                                    {!! Form::checkbox('eachsunday', '1', $eachsunday,['class' => 'day']) !!}{!! trans('core.Sunday') !!}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 hour">
                                        <label> {!! trans('core.Start Hour') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen">
                                        {!! Form::select('starthour', $listHour, $starthour, ['class'=>'hour_select chosen-select input-md form-control form-format']) !!}
                                    </div>
                                    <div class="col-md-1 minutes">
                                        <label> {!! trans('core.Minutes') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen">
                                        {!! Form::select('startminute', $listMinutes, $startminute, ['class'=>'minute_select chosen-select input-md form-control form-format']) !!}
                                    </div>
                                    <div class="col-md-1 ampm">
                                        <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen" style="padding-right:60px">
                                        <!-- <select class="chosen-select input-md form-control form-format">
                                            <option>{!! trans('core.AM') !!}</option>
                                            <option>{!! trans('core.PM') !!}</option>
                                        </select> -->
                                        {!! Form::select('start_period', $listUtc, $startutc, ['class'=>'chosen-select input-md form-control form-format']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 hour">
                                        <label> {!! trans('core.End Hour') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen">
                                        {!! Form::select('endhour', $listHour, $endhour, ['class'=>'hour_select chosen-select input-md form-control form-format']) !!}
                                    </div>
                                    <div class="col-md-1 minutes">
                                        <label> {!! trans('core.Minutes') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen">
                                      {!! Form::select('endminute', $listMinutes, $endminute, ['class'=>'minute_select chosen-select input-md form-control form-format']) !!}  
                                    </div>
                                    <div class="col-md-1 ampm">
                                        <label> {!! trans('core.AM') !!}/{!! trans('core.PM') !!} </label>
                                    </div>
                                    <div class="col-md-2 chosen" style="padding-right:60px">
                                        {!! Form::select('end_period', $listUtc, $endutc, ['class'=>'chosen-select input-md form-control form-format']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Time Zone') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="file" name="background_image" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle" required> -->
                                        {!! Form::select('timezone', $listTimezone, $timezoneid,['class'=>'chosen-select input-md form-control form-format']) !!}

                                        <h5 class="error_message image_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Campaign Background Image') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="file" name="background_image" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle" required> -->
                                        {!! Form::file('backimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                        {!! Html::image($backimageid, '', ['style' => 'height:80px;float:right']) !!}
                                        <h5 class="error_message image_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Logo Image') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="file" name="logo_image" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle" required> -->
                                        {!! Form::file('logoimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}

                                        {!! Html::image($logoimageid, '', ['style' => 'height:80px;float:right']) !!}
                                        <h5 class="error_message image_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Sponsor Image') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="file" name="logo_image" data-classbutton="btn btn-default" data-classinput="form-control inline" class="form-control filestyle" required> -->
                                        {!! Form::file('sponsorimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                        
                                        {!! Html::image($sponsorimageid, '', ['style' => 'height:80px;float:right']) !!}
                                        <h5 class="error_message image_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                   <!--  <input type="submit" class="btn btn-sm btn-default buttonUpdate" value="{!! trans('core.Update') !!}">
                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button> -->
                                    @if($form == 'add')
                                        {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                                    @else
                                        {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                                    @endif
                                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- </form> -->
                {!! Form::hidden('id', $id) !!}
                {!! Form::close() !!}
            </div>
        </div>