
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h4>{!! trans('core.Edit Question') !!}</h4> -->
                        <h4> {!! ($form == 'add') ? trans('core.Add Question') : trans('core.Edit Question') !!} </h4>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- <form role="form" class="edit-form"> -->
                {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form','data-1-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id, 'id'=>'question-form']) !!}
                @if($form =='edit')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <!-- <input type="text" name="id" class="form-control form-format" required> -->
                                        {!! Form::text('idShow',$id,['class'=>'form-control form-format','required', 'readonly', 'disabled']) !!}
                                        <h5 class="error_message id_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Question') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('question', $question, ['class'=>'form-control form-format', 'required']) !!}
                                        <h5 class="error_message question_error"> {!! trans('core.This is not a valid question') !!} </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Answer Type') !!} </label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::select('answertype', $listSurveyAnswerType, $answertype, ['class'=>'chosen-select input-md form-control', 'required']) !!}
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Image') !!} {!! trans('core.(optional)') !!} </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::file('questionimageid', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                        {!! Html::image($questionimageid, '', ['style' => 'height:80px;float:right']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Routing') !!} </label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! $select_routing !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row" id="js-area">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Script') !!} </label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::textarea('javascript', $javascript, ['class'=>'script', 'placeholder'=>'Type something...']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="answer-area">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <button type="button" class="mb-sm btn btn-primary add-button add_answer">
                                <b>{!! trans('core.Add Answer') !!}</b>
                            </button> -->
                            {!! Form::button(trans('core.Add Answer'), ['class'=>'mb-sm btn btn-primary add-button add_answer']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="answer" class="table survey-question-answer table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:90%">{!! trans('core.Answer(s)') !!}</th>
                                        <th style="width:10%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($answers as $k=>$v)
                                         <tr>
                                            <td>
                                                {!! Form::text('answer[name]['.$v["id"].']', $v['name'], ['class'=>'form-control form-format', 'style'=>'width:100% !important', 'required']) !!}
                                                @if (!$noRouting)
                                                    <div style="float: left; width: 200px; padding: 5px;" class="routingNextQuestion">
                                                    <label style="float:left;width:30%;">Routing </label>
                                                    <span style="width:80px;float:left;">
                                                        {!! Form::select('answer[order]['.$v["id"].']', $nextquestions,$v['nextquestionid'],['class'=>'chosen-select input-md form-control']) !!}
                                                    </span>
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="row-action"><div title="Delete" class='icon-trash icon_row delete-answer'></div></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    @if($form == 'add')
                                        {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                                    @else
                                        {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                                    @endif
                                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- </form> -->
                {!! Form::hidden('id', $id) !!}
                {!! Form::hidden('noRouting', $noRouting) !!}
                {!! Form::hidden('surveyId', $surveyId) !!}
                {!! Form::close() !!}
            </div>
        </div>