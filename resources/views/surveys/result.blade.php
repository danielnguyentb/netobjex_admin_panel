<div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Survey Results') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <form role="form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label> {!! trans('core.Survey ID') !!}<span class="asterisk">*</span> </label>
                                        </div>
                                        <div class="col-md-10">
                                            <!-- <input type="text" name="surveyId" required="required" class="form-control form-format"> -->
                                            {!! Form::text('surveyId', $surveyId, ['class'=>'form-control form-format', 'required']) !!}
                                            <h5 class="error_message id_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Survey Name') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <!-- <input type="text" name="surveyName" required="required" class="form-control form-format"> -->
                                        {!! Form::text('surveyName', $surveyName, ['class'=>'form-control form-format', 'required']) !!}
                                        <h5 class="error_message name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div role="tabpanel" class="panel">
                                <!-- Nav tabs-->
                                <ul role="tablist" class="nav nav-tabs nav-justified">
                                    <li role="presentation" class="active">
                                        <a href="#home-{!! $surveyId !!}" aria-controls="home-{!! $surveyId !!}" role="tab" data-toggle="tab">
                                            {!! trans('core.Raw Data') !!}
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#profile-{!! $surveyId !!}" aria-controls="profile-{!! $surveyId !!}" role="tab" data-toggle="tab">
                                            {!! trans('core.Analytics') !!}
                                        </a>
                                    </li>
                                </ul>
                                <!-- Tab panes-->
                                <div class="tab-content p0">
                                    <div id="home-{!! $surveyId !!}" role="tabpanel" class="tab-pane active">
                                        <div id="raw_data">
                                        <table class="table table-striped no-footer">
                                            <thead>
                                                <th style="width:50%;">{!! trans('core.Phone') !!}#</th>
                                                <th>{!! trans('core.Date Time') !!}</th>
                                            </thead>
                                            <tbody>
                                                @if (!empty($results))
                                                    @foreach ($results as $k=>$v)
                                                        <tr><td>{{$v->mobile}}</td><td>{{$v->created}}</td></tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    <div id="profile-{!! $surveyId !!}" role="tabpanel" class="tab-pane">
                                        <div role="form" id="analytics_form">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class='row'>
                                                        <div class="col-md-12">
                                                            <h4> {!! trans('core.Filter') !!} </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label> {!! trans('core.Start Date') !!} </label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <!-- <input type="text" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" name="required" required="required" class="form-control form-format"> -->
                                                                {!! Form::text('startDate', $startDate, ['class'=>'form-control form-format', 'required', 'id'=>'startDate' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label> {!! trans('core.End Date') !!} </label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <!-- <input type="text" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" name="required" required="required" class="form-control form-format"> -->
                                                                {!! Form::text('endDate', $endDate, ['class'=>'form-control form-format', 'required', 'id'=>'endDate' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3" style="padding-right:0px;">
                                                                <label> {!! trans('core.By Location:') !!} </label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <!-- <input type="text" name="required" required="required" class="form-control"
                                                                       style="margin-left: -39px; width: 738px"> -->
                                                                       {!! Form::select('locationId', $locationList,null,['class'=>'chosen-select input-md form-control form-format']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row buttonSet">
                                                        <div class="col-md-12">
                                                            <!-- <button type="button" class="btn btn-sm btn-default buttonAdd">Search</button> -->
                                                            {!! Form::button(trans('core.Search'), ['class' => 'btn btn-sm btn-default buttonSearch buttonAdd action-btn']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class='row'>
                                                        <div class="col-md-12">
                                                            <h4> {!! trans('core.General Statistics') !!} </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label> {!! trans('core.Data Point') !!} </label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <!-- <input type="text" name="required" required="required" class="form-control form-format"> -->
                                                                {!! Form::select('dataPoint', ['1'=>'Number of respondents','2'=>'Number of dropouts'],null,['class'=>'chosen-select input-md form-control form-format', 'id'=>'data-point']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label> {!! trans('core.Value') !!} </label>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <!-- <input type="text" name="required" required="required" class="form-control form-format"> -->
                                                                {!! Form::text('', 0,['class'=>'form-control form-format data-value', 'id'=>'numOfRespondent','data-value'=>'1', 'disabled'=>'disabled']) !!}
                                                                {!! Form::text('', 0,['class'=>'form-control form-format data-value', 'id'=>'numOfDropout', 'data-value'=>'2', 'disabled'=>'disabled','style'=>'display:none']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class='row'>
                                                        <div class="col-md-12">
                                                            <h4> {!! trans('core.Question Statistics') !!} </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label> {!! trans('core.Question') !!} </label>
                                                            </div>
                                                            <div class="col-md-9 question" >
                                                                <!-- <select class="chosen-select input-md form-control form-format">
                                                                    <option>{!! trans('core.In selecting a credit card, are you primarily interested in:') !!}</option>
                                                                    <option>{!! trans('core.With whom do you have your primary banking relationship?') !!}</option>
                                                                    <option>{!! trans('core.What credit card(s) do you use?') !!}</option>
                                                                </select> -->
                                                                <select class="chosen-select input-md form-control form-format" id="listOfQuest">
                                        
                                                                 </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive">
                                                                    <table id="question_stats" class="table table-striped">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width:75%">{!! trans('core.Name') !!}</th>
                                                                            <th style="width:25%">{!! trans('core.Number of Respondents') !!}</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody id="ansRespo">
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>