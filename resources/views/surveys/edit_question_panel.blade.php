
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-6">
                        <!-- <h4>{!! trans('core.Edit Question') !!}</h4> -->
                        <h4> {!! ($form == 'add') ? trans('core.Add Question') : trans('core.Edit Question') !!} </h4>
                        
                    </div>
                    <div class="col-md-6" style="padding-top:20px; padding-bottom:6px;">
                        <button class="btn btn-success" data-target="#previewModal" data-toggle="modal" type="button" style="float:right; margin-right:5px;">Preview Final Deck</button>
                        <button class="btn btn-success" data-target="#deckModal" data-toggle="modal" type="button" style="float:right; margin-right:5px;">Deck Layout(s) </button>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- <form role="form" class="edit-form"> -->
                {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form','data-1-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id, 'id'=>'question-form']) !!}
                @if($form =='edit')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="id" class="form-control form-format" required> -->
                                        {!! Form::text('idShow',$id,['class'=>'form-control form-format','required', 'readonly', 'disabled']) !!}
                                        <h5 class="error_message id_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>{!! trans('core.Question') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-9">
                                     <!-- <textarea class="textarea" placeholder="Type something..."></textarea> -->
                                     {!! Form::textarea('question',$question, ['class'=>'textarea','placeholder'=>'Type something...']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                            <div class="col-md-3">
                                <label>{!! trans('core.Answer Type')!!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                    <label class="radio-inline c-radio">
                                         <!-- <input type="radio" name="radio" value="option1" checked=""> -->
                                         {!! Form::radio('answertype', 3, true) !!}
                                         <span class="fa fa-circle"></span>{!! trans('core.Radio Button')!!}</label>
                                </div>
                              </div>
                              <div class="row" style="padding-top:30px;">  
                                <div class="col-md-3">
                                <label>{!! trans('core.Number of Panels') !!} <span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                 <!-- <select class="chosen-select input-md form-control form-format" style="width: 60px; display: none;">
                                            <option>Select</option>
                                </select> -->
                                @if($surveytype==2)
                                        {!! Form::select('numofpanels', ['2'=> '2'], null, ['class'=>'chosen-select input-md form-control', 'required']) !!}
                                    @elseif($surveytype ==1)
                                        {!! Form::select('numofpanels', ['2'=> '2', '3'=> '3', '4'=> '4', '5'=> '5', '6'=> '6'], $numofpanels, ['class'=>'chosen-select input-md form-control', 'required']) !!}
                                    @endif
                                <!-- <div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 0px;" title=""><a class="chosen-single" tabindex="-1"><span>Select</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" readonly=""></div><ul class="chosen-results"></ul></div></div> -->
                                </div>
                                </div>
                                
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-2">
                                    @if($surveytype==2)
                                        <label> {!! trans('core.Question Image') !!} (300x75)</label>
                                        {!! Form::hidden('','300x75',['class'=>'questionimagesize']) !!}
                                    @elseif($surveytype ==1)
                                        <label> {!! trans('core.Question Image') !!} (450x225) </label>
                                        {!! Form::hidden('','450x225',['class'=>'questionimagesize']) !!}
                                    @endif
                                    </div>
                                    <div class="col-xs-10">
                                        {!! Form::file('questionimageid', ['class' => 'form-control filestyle question-image-panel', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                        {!! Html::image($questionimageid, '', ['style' => 'padding-top:8px;float:left;']) !!}
                                        @if($hasQuestionImage)
                                            <span class="remove-question-image" title="Remove Image">x</span>
                                            {!! Form::hidden('questionimageurl', $questionimageid) !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Answer Image Size') !!} <span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                    @if($surveytype==2)
                                        {!! Form::select('answerimagesize', ['5'=> '90 X 75'], null, ['class'=>'chosen-select input-md form-control', 'required']) !!}
                                    @elseif($surveytype ==1)
                                        {!! Form::select('answerimagesize', ['1'=> '222 X 225', '2'=> '147 X 150'], $answerimagesize, ['class'=>'chosen-select input-md form-control', 'required']) !!}
                                    @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Routing') !!} </label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! $select_routing !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div id="answer-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="answer" class="table survey-question-answer table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:90%">{!! trans('core.Answer(s)') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($answers as $k=>$v)
                                         <tr class="answer-items">
                                            <td>
                                                {!! Form::text('answer[name]['.$v["id"].']', $v['name'], ['class'=>'form-control form-format', 'style'=>'width:100% !important']) !!}
                                                @if (!$noRouting)
                                                    <div style="float: left; width: 200px; padding: 5px;" class="routingNextQuestion">
                                                    <label style="float:left;width:30%;">{!! trans('core.Routing') !!}</label>
                                                    <span style="width:80px;float:left;">
                                                        {!! Form::select('answer[order]['.$v["id"].']', $nextquestions,$v['nextquestionid'],['class'=>'chosen-select input-md form-control']) !!}
                                                    </span>
                                                    </div>
                                                @endif
                                                <div style="float: left; width: 100%; padding: 5px;" class="answer-image-area">
                                                    <div class="col-md-2"><label style="float:left;width:30%;">{!! trans('core.Image') !!} </label></div>
                                                    <div class="col-md-5">
                                                        {!! Form::file('answer[image]['.$v["id"].']', ['class' => 'form-control filestyle answer-image-panel', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="col-md-2"><label style="float:left;width:30%;">{!! trans('core.Preview') !!} </label></div>
                                                        {!! Html::image($v['answerimageid'], '', ['style' => 'height:80px;float:right']) !!}
                                                        @if($v['isAnswerimage'])
                                                            <span class="remove-answer-image" title="Remove Image">x</span>
                                                            {!! Form::hidden('answerImage['.$v["id"].']', $v['answerimageid']) !!}
                                                        @endif
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    @if($form == 'add')
                                        {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                                    @else
                                        {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                                    @endif
                                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- </form> -->
                {!! Form::hidden('id', $id) !!}
                {!! Form::hidden('noRouting', $noRouting) !!}
                {!! Form::hidden('surveytype', $surveyId) !!}
                {!! Form::hidden('surveyId', $surveyId) !!}
                {!! Form::hidden('list-order-question', $nextquestions?json_encode($nextquestions):'') !!}
                {!! Form::close() !!}
            </div>
  
        </div>
        
<!-- <div class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="deckModal" style="display: none;">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
               </button>
               <h4 class="modal-title" id="myModalLabel">Deck Layout(s)</h4>
            </div>
            <div class="modal-body"><img src="img/decks.jpg" style="width:100%;"></div>
            <div class="clearfix"></div>
            
         </div>
      </div>
   </div> -->

<!-- <div class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="previewModal" >
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
               </button>
               <h4 class="modal-title" id="myModalLabel">Preview Final Deck</h4>
            </div>
            <div class="modal-body">
               
               <div class="qus_ans_wrap">
                <div class="col-md-12 question_banner">
                 <figure><img src="img/q1_banner.png"></figure>
                 <h2><span class="span">What is your favorite summer vacation spot?</span></h2>
                </div>
                <ul class="answers four_choice">
                 <li class="answer_block"><img src="img/ans1.jpg"> </li>
                 <li class="answer_block"><img src="img/ans2.jpg"> </li>
                </ul>
               </div>
            
            </div>
            <div class="clearfix"></div>
            
         </div>
      </div>
   </div>  -->

   <script type="text/javascript">
    var APP_URL = {!! json_encode(url('/')) !!};
    // $('.modal').hide();
</script>