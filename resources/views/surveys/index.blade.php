@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')

    <!-- {!! Html::style('css/bootstrap-multiselect.css') !!} -->
    {!! Html::style('css/survey.css') !!}
    {!! Html::style('css/bootstrap-datepicker.css') !!}
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Surveys') !!} </h3>
        <div class="container-fluid">

            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Marketing') !!}</a></li>
                    <li class="active">{!! trans('core.Surveys') !!}</li>
                </ul>
                @if($existCustomerId)
                <button type="button" class="mb-sm btn btn-primary add-button" id="add_form" data-toggle="modal" data-target=".bs-example-modal-lg">
                    <b>{!! trans('core.Add Survey') !!}</b>
                </button>
                @endif
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! trans('core.Surveys') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="survey-management" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:16%">{!! trans('core.Name') !!}</th>
                                    <th style="width:16%">{!! trans('core.Number of Questions') !!}</th>
                                    <th style="width:16%">{!! trans('core.Start Date') !!}</th>
                                    <th style="width:16%">{!! trans('core.End Date') !!}</th>
                                    <th style="width:16%">{!! trans('core.Published') !!}?</th>
                                    <th style="width:20%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->

        </div>
    </div>
@stop

@section('form')
    <div class="raw-data-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Export Raw Data') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
            {!! Form::open(['role' => 'form', 'action'=>'SurveysController@export','class' => 'edit-form row-form','data-1-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'id'=>'survey-raw-form']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Start Date') !!}</label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('startdate', null, ['class'=>'form-control form-format', 'required', 'id'=>'start-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.End Date') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('enddate', null, ['class'=>'form-control form-format', 'required', 'id'=>'end-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-default buttonAdd buttonExport" value="{!! trans('core.Export') !!}">

                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="surveyId">
                    {!! Form::close()!!}
            </div>
        </div>
        
    </div>
    <div class="invite-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Invite Guest Users') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <form role="form" class="invite-form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class='row'>
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Enter email addresses') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="emails" class="form-control" style="width: 760px" required>
                                        <p style="color: red;"> {!! trans('core.For multiple invitees , please use a comma separate list of email addresses') !!} </p>
                                        <h5 class="error_message multiple_email_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Access to Object') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="access_to_object" class="form-control form-format" required>
                                        <h5 class="error_message access_to_object_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" name="name" class="form-control form-format" required>
                                            <h5 class="error_message"> {!! trans('core.This is not a valid name') !!} </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Object Type') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="object_type" class="form-control form-format" required>
                                        <h5 class="error_message object_type_error"> {!! trans('core.This is not a valid type') !!} </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-default buttonInvite" value="{!! trans('core.Invite') !!}">
                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- <div class="results-content">
        <div class='panel-body loader-demo'>
            <h4> {!! trans('core.Loading Survey Results') !!} </h4>
            <div class='ball-pulse'>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        
    </div> -->

    
    <div class="question-wizard-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Question') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body survey-body">
                <div class="panel-heading">{!! trans('core.Survey')!!}: <span class="survey-name"></span>
                    {!! Form::button('<b>' . trans('core.Add Question') . '</b>', ['class' => 'mb-sm btn btn-primary add-button-question action-btn', 'id' => 'question_form']) !!}
                </div>
                <div class="panel-body">

                    <!-- <form id="question_form" action="#"> -->
                        <div>
                            <fieldset>
                                <div class="panel panel-default">
                                    <div class="panel-heading">{!! trans('core.Questions') !!}</div>
                                    <div class="table-responsive">
                                        <table id="questions" class="survey-questions table table-striped">
                                            <thead>
                                            <tr>
                                                <th style="width:75%">{!! trans('core.Question') !!}</th>
                                                <th style="width:10%">{!! trans('core.Order') !!}</th>
                                                <th style="width:15%">{!! trans('core.Action') !!}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    <div class="row">
                    <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-default buttonClose" style="color: white; background-color: red;"> {!! trans('core.Cancel') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/bootstrap-select.js') !!}
    {!! Html::script('js/bootstrap-datepicker.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    <!-- {!! Html::script('js/formValidation.min.js') !!} -->
    {!! Html::script('js/app/pages/survey.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $.app.getCls('surveyManagement').manager({
                addLoading: '{!! trans('core.Loading Add Survey') !!}',
                editLoading: '{!! trans('core.Loading Edit Survey') !!}',
                loadingQuestion: '{!! trans('core.Loading Question') !!}',
                questionAddLoading: '{!! trans('core.Loading Add Question') !!}',
                questionEditLoading: '{!! trans('core.Loading Edit Question') !!}',
                loadingResult: '{!! trans('core.Loading Result') !!}',
                loadingInvite: '{!! trans('core.Loading Guests Invite') !!}',
                loadingRawData:'{!! trans('core.Loading Raw Data') !!}',
                routing:'{!! trans('core.Routing') !!}',
                preview:'{!! trans('core.Preview') !!}',
                image:'{!! trans('core.Image') !!}',
                changetheorder:'{!! trans('core.Do you wish change the order?') !!}',
                changeordertext:'{!! trans('core.Changing order of questions will cause all Conditional routing logic to be cleared, and Automatic routing will be used instead') !!}',
                changeorderconfirm:'{!! trans('core.Yes, change it!') !!}',
                changeordersuccess: '{!! trans('core.Question order has been changed') !!}',
                changenumpanel:'{!! trans('core.Do you wish change the number?') !!}',
                changenumpanelwarn:'{!! trans('core.Changing num of panel will cause all Answers image to be cleared') !!}',
                changenumpanelsucc: '{!! trans('core.Num of panel has been changed.') !!}'
            });
        });
    </script>
@stop
