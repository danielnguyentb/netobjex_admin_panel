<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Device Type') : trans('core.Edit Device Type') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            @if ($form == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                <h5 class="error_message idShow_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Device Type') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('device_type', $name, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message device_type_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Protocol') !!} </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::select('protocol', $protocolsList, $protocolid, ['class' => 'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Queue Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('queue_name', $queuename, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message queue_name_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Status Viewer') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('status_viewer', $statusviewer, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message status_viewer_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Image') !!}</label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::file('image', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                <h5 class="error_message image_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                            </div>
                            <div class="col-md-10">
                                {!! $imageid != '' ? Html::image(genDownloadUrl($imageid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if ($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}

    </div>
</div>