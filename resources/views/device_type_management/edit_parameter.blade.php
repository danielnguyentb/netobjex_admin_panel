<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Parameter') : trans('core.Edit Parameter') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-parameter-form row-form', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
        @if ($form == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                <h5 class="error_message idShow_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Parameter') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('parameter', $name, ['class' => 'form-control form-format', 'required']) !!}
                            <h5 class="error_message command_name_error"></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Data Type') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-3">
                            {!! Form::select('data_type', $dataTypesList, $defaultvalue, ['class' => 'chosen-select input-md form-control form-format']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row buttonSet">
                    <div class="col-md-12">
                        @if ($form == 'add')
                            {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                        @else
                            {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                        @endif
                        {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>