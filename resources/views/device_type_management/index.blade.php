@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Device Type Management') !!} </h3>
        <div class="container-fluid">
            <!-- Button -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Super Admin') !!}</a></li>
                    <li class="active">{!! trans('core.Device Type Management') !!}</li>
                </ul>
                <button type="button" class="mb-sm btn btn-primary add-button" id="add_form" data-custom='{"type":"123"}'>
                    <b>{!! trans('core.Add Device Type') !!}</b>
                </button>
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! trans('core.Device Types') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="device-type-management" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:80%">{!! trans('core.Name') !!}</th>
                                    <th style="width:20%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->
        </div>
    </div>
@stop

@section('form')
    <div class="attribute-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Attribute') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::button('<b>' . trans('core.Add Attribute') . '</b>', ['class' => 'mb-sm btn btn-primary add-button-attr', 'id' => 'attribute_form']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {!! trans('core.Device Type: ') !!} <span class="device-type"></span>
                                <div class='icon-question help'
                                     data-container="body"
                                     data-toggle="popover"
                                     data-placement="left"
                                     data-trigger="hover"
                                     data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="device-type-management-attribute table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:15%">{!! trans('core.ID') !!}</th>
                                        <th style="width:15%">{!! trans('core.Name') !!}</th>
                                        <th style="width:15%">{!! trans('core.Control Type') !!}</th>
                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-default buttonClose"> {!! trans('core.Close') !!} </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="command-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Command') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::button('<b>' . trans('core.Add Command') . '</b>', ['class' => 'mb-sm btn btn-primary add-button-cmd action-btn', 'id' => 'command_form']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {!! trans('core.Device Type: ') !!} <span class="device-type"></span>
                                <div class="icon-question help"
                                     data-container="body"
                                     data-toggle="popover"
                                     data-placement="left"
                                     data-trigger="hover"
                                     data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="device-type-management-command table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:65%">{!! trans('core.ID') !!}</th>
                                        <th style="width:20%">{!! trans('core.Command') !!}</th>
                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-default buttonClose"> {!! trans('core.Close') !!} </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="configuration-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Configuration') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::button('<b>' . trans('core.Add Value') . '</b>', ['class' => 'mb-sm btn btn-primary add-button-config', 'id' => 'configuration_form']) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {!! trans('core.Device Type: ') !!} <span class="device-type"></span>
                                <div class='icon-question help'
                                     data-container="body"
                                     data-toggle="popover"
                                     data-placement="left"
                                     data-trigger="hover"
                                     data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="device-type-management-configuration table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width:15%">{!! trans('core.ID') !!}</th>
                                        <th style="width:15%">{!! trans('core.Variable') !!}</th>
                                        <th style="width:15%">{!! trans('core.Value') !!}</th>
                                        <th style="width:15%">{!! trans('core.Action') !!}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-default buttonClose"> {!! trans('core.Close') !!} </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="parameter-content">
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <h3> {!! trans('core.Parameter') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::open(['role' => 'form', 'novalidate' => 'novalidate']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.New Parameter') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10">
                                        {!! Form::text('parameter', '', ['class' => 'form-control form-format', 'required' => TRUE]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Data Type') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::select('data_type', $paramDataTypes, NULL, ['class' => 'chosen-select input-md form-control form-format']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {!! Form::submit(trans('core.Add Parameter'), ['class' => 'mb-sm btn btn-primary add-button']) !!}
                    </div>
                {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {!! trans('core.Parameters') !!}
                                    <!--<div class='icon-question help'
                                         data-container="body"
                                         data-toggle="popover"
                                         data-placement="left"
                                         data-trigger="hover"
                                         data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                                    </div>-->
                                </div>
                                <div class="table-responsive">
                                    <table class="device-type-management-parameter table table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width:60%">{!! trans('core.Name') !!}</th>
                                            <th style="width:25%">{!! trans('core.Data Type') !!}</th>
                                            <th style="width:15%">{!! trans('core.Action') !!}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-sm btn-default buttonClose"> {!! trans('core.Close') !!} </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@stop

@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent

    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/device_type.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $.app.getCls('deviceTypeManagement').manager({
                addLoading: '{!! trans('core.Loading Add Device Type') !!}',
                editLoading: '{!! trans('core.Loading Edit Device Type') !!}',
                loadingCommand: '{!! trans('core.Loading Command') !!}',
                commandAddLoading: '{!! trans('core.Loading Add Command') !!}',
                commandEditLoading: '{!! trans('core.Loading Edit Command') !!}',
                loadingConfiguration: '{!! trans('core.Loading Configuration') !!}',
                configurationAddLoading: '{!! trans('core.Loading Add Variable') !!}',
                configurationEditLoading: '{!! trans('core.Loading Edit Variable') !!}',
                loadingParameter: '{!! trans('core.Loading Parameter') !!}',
                parameterAddLoading: '{!! trans('core.Loading Add Parameter') !!}',
                parameterEditLoading: '{!! trans('core.Loading Edit Parameter') !!}',
                loadingAttribute: '{!! trans('core.Loading Attribute') !!}',
                attributeAddLoading: '{!! trans('core.Loading Add Attribute') !!}',
                attributeEditLoading: '{!! trans('core.Loading Edit Attribute') !!}'
            });
        });
    </script>
@stop
