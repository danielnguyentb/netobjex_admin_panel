<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Attribute') : trans('core.Edit Attribute') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-attribute-form row-form', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
        @if ($form == 'edit')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                <h5 class="error_message idShow_error"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::text('name', $name, ['class' => 'form-control form-format', 'required']) !!}
                            <h5 class="error_message name_error"></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label> {!! trans('core.Control Type') !!}<span class="asterisk">*</span> </label>
                        </div>
                        <div class="col-md-10">
                            {!! Form::select('control_type', $controlTypesList, $controltype, ['class' => 'chosen-select input-sm form-control form-format', 'required']) !!}
                            <h5 class="error_message control_type_error"></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row buttonSet">
                    <div class="col-md-12">
                        @if ($form == 'add')
                            {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                        @else
                            {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                        @endif
                        {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>