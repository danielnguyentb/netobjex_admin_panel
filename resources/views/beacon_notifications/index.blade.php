@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> {!! trans('core.Beacon Notifications') !!} </h3>
        <div class="container-fluid">

            <!-- Statement -->
            <div class="row">
                <ul class="breadcrumb">
                    <li><a href="#">{!! trans('core.Device Admin') !!}</a></li>
                    <li class="active">{!! trans('core.Beacon Notifications') !!}</li>
                </ul>
                <h4 id="notification"> <b>{!! trans('core.To add notification, please select from one of the notification types below') !!}</b> </h4>
            </div>
            <!--/ Statement -->

            <!-- Button -->
            <div class="row notification_buttons">
                <button type="button" class="mb-sm btn btn-primary notification_button" data-custom='{"type":"1"}' id="add_notification_form">
                    <div class="icon-bubbles notification_content"></div>
                    <b class="notification_content">{!! trans('core.Notification') !!}</b>
                </button>
                <button type="button" class="mb-sm btn btn-primary notification_button" data-custom='{"type":"6"}'  id="add_image_form">
                    <div class="icon-picture notification_content"></div>
                    <b class="notification_content">{!! trans('core.Image') !!}</b>
                </button>
                <button type="button" class="mb-sm btn btn-primary notification_button" data-custom='{"type":"7"}' id="add_url_form">
                    <div class="icon-globe notification_content"></div>
                    <b class="notification_content">{!! trans('core.URL') !!}</b>
                </button>
                <button type="button" class="mb-sm btn btn-primary notification_button" data-custom='{"type":"8"}' id="add_live_feed_form">
                    <div class="icon-feed notification_content"></div>
                    <b class="notification_content">{!! trans('core.Live Feed') !!}</b>
                </button>
            </div>
            <!--/ Button -->

            <!-- Datatable -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! trans('core.Beacon Notifications') !!}
                            <div class='icon-question help'
                                 data-container="body"
                                 data-toggle="popover"
                                 data-placement="left"
                                 data-trigger="hover"
                                 data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="beaconNotifications" class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="width:15%">{!! trans('core.Name') !!}</th>
                                    <th style="width:55%">{!! trans('core.Description') !!}</th>
                                    <th style="width:15%">{!! trans('core.Notification Type') !!}</th>
                                    <th style="width:15%">{!! trans('core.Action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Datatable -->

        </div>
    </div>
@stop



@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- WYSIWYG-->
    @include('includes.js.wysisyg')
    <!-- FILESTYLE-->
    @include('includes.js.filestyle')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/beaconNotifications.js') !!}
@stop
