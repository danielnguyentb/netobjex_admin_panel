<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                @if($form == 'edit')
                    <h3> {!! trans('core.Edit URL') !!} </h3>
                @else
                    <h3> {!! trans('core.Add URL') !!} </h3>
                @endif
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="name" value="{!! $name !!}" class="form-control form-format" required>
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Description') !!} </label>
                            </div>
                            <div class="col-md-10">
                                <textarea class="description" style="width: 100%" name="description" placeholder="Type something...">{!! $description !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.URL') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                <input type="url" name="contenturl" value="{!! $contenturl !!}" placeholder="include https://" class="form-control form-format" required>
                                <h5 class="error_message url_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label> {!! trans('core.Icon') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-10">
                                @if($form == 'edit')
                                     {!! Form::file('image', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline']) !!}
                                @else
                                     {!! Form::file('image', ['class' => 'form-control filestyle', 'data-classbutton' => 'btn btn-default', 'data-classinput' => 'form-control inline', 'required']) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($iconid != '')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>&nbsp;</label>
                                </div>
                                <div class="col-md-10">
                                    {!! $iconid != '' ? Html::image(genDownloadUrl($iconid), '', ['style' => 'height: 100px; width: auto;']) : '' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            <input type="hidden" name="type" value="{!! $type !!}"/>
                            @if($form == 'add')
                                <input type="submit" class="btn btn-sm btn-default buttonAdd" value="{!! trans('core.Add') !!}">
                            @else
                                <input type="submit" class="btn btn-sm btn-default buttonUpdate" value="{!! trans('core.Update') !!}">
                            @endif
                                <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

