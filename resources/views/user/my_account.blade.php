@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
@stop
@section('content')
    <div class="content-wrapper">
        <h3> My Account </h3>
        <div class="container-fluid">
            <div class="row">
                <ul class="breadcrumb">
                    <li class="active">{!! trans('core.My Account') !!}</li>
                </ul>
            </div>
            <div class="row edit_form">
                <div class="col-lg-12">
                    <!-- START panel-->
                    <div class="panel panel-default">
                        <div class="panel-heading form_title"> {!! trans('core.My Account') !!} </div>
                        <div class="panel-body">
                            {!! Form::open(['role' => 'form', 'data-parsley-validate' ] ) !!}
                                <h3> {!! trans('core.User Information') !!} </h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.First name') !!}</label>
                                            {!! Form::text('firstname', $firstname, ['class' => 'form-control', 'placeholder' => 'First name', 'required', 'id' => 'inputFirstName' ]) !!}
                                            <h5 class="error_message first_name_error"> </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Last name') !!}</label>
                                            
                                            {!! Form::text('lastname', $lastname, ['class' => 'form-control', 'placeholder' => 'Last name', 'required', 'id' => 'inputLastName' ]) !!}
                                            <h5 class="error_message last_name_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Email') !!}</label>
                                            {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Email address', 'required', 'autofocus', 'id' => 'inputEmail' ]) !!}
                                            <h5 class="error_message email_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                                <h3> Address </h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Address1') !!}</label>
                                            {!! Form::text('address1', $address1, ['class' => 'form-control', 'placeholder' => 'Address1', 'required', 'id' => 'inputAddress1' ]) !!}
                                            <h5 class="error_message address_error"> </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Address2') !!}</label>
                                            {!! Form::text('address2', $address2, ['class' => 'form-control', 'placeholder' => 'Address2', 'id' => 'inputAddress2' ]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.State') !!}</label>
                                            
                                                {!! Form::select('state', $states, $state_selected,['class'=>'chosen-select input-md form-control form-format']) !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Country') !!}</label>
                                                {!! Form::select('country', $countries, $country_selected, ['class'=>'chosen-select input-md form-control form-format']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.City') !!}</label>
                                            {!! Form::text('city', $city, ['class' => 'form-control', 'placeholder' => 'City', 'id' => 'inputCity' ]) !!}
                                            <h5 class="error_message city_error"> </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Zip Code') !!}</label>
                                            {!! Form::text('zip', $zip, ['class' => 'form-control', 'placeholder' => '', 'id' => 'inputZip' ]) !!}
                                            <h5 class="error_message zip_code_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                                <h3>{!! trans('core.Reset Password') !!} </h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Old Password') !!}</label>
                                            {!! Form::password('old_password', [
                                                'class'                         => 'form-control',
                                                'placeholder'                   => 'Current Password',
                                                'id'                            => 'old-password',
                                                'data-parsley-required-message' => 'Password is required',
                                                'data-parsley-trigger'          => 'change focusout'
                                            ]) !!}
                                            <h5 class="error_message old_password_error"> </h5>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.New Password') !!}</label>
                                            {!! Form::password('new_password', [
                                                'class'                         => 'form-control',
                                                'placeholder'                   => 'New Password',
                                                'id'                            => 'id-password',
                                                'data-parsley-trigger'          => 'change focusout'
                                            ]) !!}
                                            <h5 class="error_message password_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{!! trans('core.Confirm Password') !!}</label>
                                            {!! Form::password('confirmPassword', [
                                                'class'                         => 'form-control',
                                                'placeholder'                   => 'Password confirmation',
                                                'id'                            => 'confirm-password',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-equalto'          => '#id-password',
                                                'data-parsley-equalto-message'  => 'Not same as New Password',
                                            ]) !!}
                                            <h5 class="error_message confirm_password_error"> </h5>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-sm btn-default buttonSubmit" value="Submit">
                            {!! Form::close() !!}
                            <script type="text/javascript">
                                window.ParsleyConfig = {
                                    errorsWrapper: '<div></div>',
                                    errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
                                };
                               
                            </script>
                        </div>
                    </div>
                </div>
                <!-- END panel-->
            </div>
        </div>
    </div>
@stop
@section('custom_scripts')
    <script type="text/javascript">
        @if (!empty($response))
            @if ($response['status'])
                $.growl.notice({message: '{!! trans('core.Account has been saved') !!}'});
            @else
                $.growl.error({message: '{!! $response['message'] !!}'});
            @endif
        @endif
    </script>
@stop
@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
@stop
