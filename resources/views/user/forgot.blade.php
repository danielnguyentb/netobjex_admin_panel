@extends('layouts.authen')

@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="img/logo.png" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">{!! trans('core.PASSWORD RESET') !!}</p>
            <!-- <form role="form"> -->
            {!! Form::open(['role' => 'form']) !!}
                <p class="text-center">{!! trans('core.Fill with your mail to receive instructions on how to reset your password.') !!}</p>
                <div class="form-group has-feedback">
                    <label for="resetInputEmail1" class="text-muted">{!! trans('core.Email address') !!}</label>
                    <!-- <input id="resetInputEmail1" type="email" placeholder="Enter email" autocomplete="off" class="form-control"> -->
                    {!! Form::input('email', 'email', '', ['placeholder' => trans('core.Enter email'), 'autocomplete' => 'off', 'required' => FALSE, 'class' => 'form-control']) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                    <div class="pull-left" style="margin: 5px 0;"><a href="/" class="text-muted" style="font-size:13px;">&larr; {!! trans('core.Back to Login') !!}</a>
                </div>
                <button type="submit" class="btn btn-danger btn-block">{!! trans('core.Reset') !!}</button>
            <!-- </form> -->
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('custom_scripts')
    <script type="text/javascript">
        @if (!empty($response))
            @if ($response['status'])
                $.growl.notice({message: '{!! trans('core.Success! A link to reset password has been sent to your e-mail address. Please check your inbox') !!}'});
            @else
                $.growl.error({message: '{!! $response['message'] !!}'});
            @endif
        @endif
    </script>
@stop
