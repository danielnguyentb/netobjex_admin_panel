@extends('layouts.authen')

@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="img/logo.png" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">{!! trans('core.CONFIRM PASSWORD') !!}</p>
                <div class="form-group has-feedback">
                    <?php 
                        if (!empty($response) && !$response['status'])
                        {
                            echo '<p class="text-center">'.$response['message'].'</p>';
                        }
                    ?>
                    <div><a href="/">{!! trans("core.Click Here") !!} </a> {!! trans("core.to login")!!}</div>
                </div>
        </div>
    </div>
@stop