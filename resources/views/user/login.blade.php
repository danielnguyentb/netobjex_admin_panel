@extends('layouts.authen')

@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="{!! url() !!}">
                <img src="{!! url() !!}/img/logo.png" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">{!! trans('core.SIGN IN TO CONTINUE.') !!}</p>
            {!! Form::open(['role' => 'form', 'novalidate' => 'novalidate']) !!}
                <div class="form-group has-feedback">
                    {!! Form::input('email', 'email', '', ['placeholder' => trans('core.Enter email'), 'autocomplete' => 'off', 'required' => TRUE, 'class' => 'form-control']) !!}
                    <span class="fa fa-envelope form-control-feedback text-muted"></span>
                </div>
                <div class="form-group has-feedback">
                    {!! Form::input('password', 'password', '', ['placeholder' => trans('core.Password'), 'autocomplete' => 'off', 'required' => TRUE, 'class' => 'form-control']) !!}
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                </div>
                <div class="clearfix">
                    <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                            <input type="checkbox" value="1" name="remember"{!! $isRemember ? ' checked="checked"' : '' !!}>
                            <span class="fa fa-check"></span><i style="font-size:13px;">{!! trans('core.Remember Me') !!}</i></label>
                    </div>
                    <div class="pull-right"><a href="/forgot" class="text-muted" style="font-size:13px;">{!! trans('core.Forgot your password?') !!}</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-primary mt-lg">{!! trans('core.Login') !!}</button>
            {!! Form::close() !!}
                    <!-- <p class="pt-lg text-center">Need to Signup?</p><a href="register.html" class="btn btn-block btn-default">Register Now</a>-->
        </div>
    </div>
@stop

@section('custom_scripts')
    @include('includes.js.parsley')
    {!! Html::script('js/app/users/login.js') !!}
@stop
