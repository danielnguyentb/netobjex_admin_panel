@extends('layouts.authen')

@section('content')
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="img/logo.png" alt="Image" class="block-center img-rounded">
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">{!! trans('core.CONFIRM PASSWORD') !!}</p>
                <div class="form-group has-feedback">
                <label for="resetInputEmail1" class="text-muted">{!! trans('core.Your password:') !!}</label>
                    <?php 
                        if ($response['status'])
                        {
                            echo '<input type="text" autocomplete="off" class="form-control" value="'.$response['password'].'">';
                        }

                    ?>
                    <div><a href="/">{!! trans("core.Click Here") !!} </a> {!! trans("core.to login")!!}</div>
                </div>
        </div>
    </div>
@stop