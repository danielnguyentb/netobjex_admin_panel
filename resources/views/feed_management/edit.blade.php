<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                  @if($form == 'add')
                        <h3> {!! trans('core.Add Feed') !!} </h3>
                  @else
                    <h3> {!! trans('core.Edit Feed') !!} </h3>
                  @endif
            </div>
        </div>
    </div>
    <div class="content-body">
         {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" name="name" value="{!! $name !!}"  class="form-control form-format" required>
                                    <h5 class="error_message name_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label> {!! trans('core.Type') !!} </label>
                                </div>
                                <div class="col-md-4">
                                    <select class="chosen-select input-md form-control form-format" name="type">
                                        @foreach($types as $k => $v)
                                            @if($k == $type)
                                                 <option value="{!! $k!!}" selected="selected">{!! $v!!}</option>
                                            @else
                                                 <option value="{!! $k !!}">{!! $v !!}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.URL') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input type="url" name="url" value="{!! $url !!}"  placeholder="include https://" class="form-control form-format" required>
                                    <h5 class="error_message url_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-3" style="margin-right: -39px">
                                <label> {!! trans('core.User ID') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" value="{!! $userid !!}" name="userid" class="form-control form-format" required>
                                <h5 class="error_message user_id_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label> {!! trans('core.Password') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-10">
                                    <input type="password" value="{!! $password !!}"  name="password" class="form-control form-format" required>
                                    <h5 class="error_message password_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label> {!! trans('core.Feed Pull Interval (in seconds)') !!}<span class="asterisk">*</span> </label>
                                    <br/>
                                    <input type="number" name="interval" value="{!! $interval !!}" class="form-control form-format" required>
                                    <h5 class="error_message interval_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row buttonSet">
                            <div class="col-md-12">
                                @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd']) !!}
                                @else
                                    {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate']) !!}
                                @endif
                                {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         {!! Form::close() !!}
    </div>
</div>

{{--<div class="edit-content">--}}
{{--<div class='panel-body loader-demo'>--}}
    {{--<h4> {!! trans('core.Loading Edit Feed') !!} </h4>--}}
    {{--<div class='ball-pulse'>--}}
        {{--<div></div>--}}
        {{--<div></div>--}}
        {{--<div></div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="main-content">--}}
    {{--<div class="content-header">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<h3> {!! trans('core.Edit Feed') !!} </h3>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="content-body">--}}
        {{--<form role="form" class="form-horizontal edit-form">--}}
            {{--<div class="container-fluid">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" name="id" class="form-control form-format" required>--}}
                                    {{--<h5 class="error_message id_error"> </h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" name="name" class="form-control form-format" required>--}}
                                    {{--<h5 class="error_message name_error"> </h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label> {!! trans('core.Type') !!} </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<select class="chosen-select input-md form-control form-format">--}}
                                        {{--<option selected="selected">{!! trans('core.FaceBook') !!}</option>--}}
                                        {{--<option>{!! trans('core.LinkedIn') !!}</option>--}}
                                        {{--<option>{!! trans('core.RSS') !!}</option>--}}
                                        {{--<option>{!! trans('core.Twitter') !!}</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.URL') !!}<span class="asterisk">*</span> </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="url" name="url" placeholder="include https://" class="form-control form-format" required>--}}
                                    {{--<h5 class="error_message url_error"> </h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-3" style="margin-right: -39px">--}}
                                {{--<label> {!! trans('core.User ID') !!}<span class="asterisk">*</span> </label>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9">--}}
                                {{--<input type="text" name="user_id" class="form-control form-format" required>--}}
                                {{--<h5 class="error_message user_id_error"> </h5>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.Password') !!}<span class="asterisk">*</span> </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="password" name="password" class="form-control form-format" required>--}}
                                    {{--<h5 class="error_message password_error"> </h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<label> {!! trans('core.Feed Pull Interval (in seconds)') !!}<span class="asterisk">*</span> </label>--}}
                                    {{--<br/>--}}
                                    {{--<input type="number" name="interval" class="form-control form-format" required>--}}
                                    {{--<h5 class="error_message interval_error"> </h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="row buttonSet">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<input type="submit" class="btn btn-sm btn-default buttonUpdate" value="{!! trans('core.Update') !!}">--}}
                                {{--<button type="button" class="btn btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="view-content">--}}
{{--<div class='panel-body loader-demo'>--}}
    {{--<h4> {!! trans('core.Loading View Feed') !!} </h4>--}}
    {{--<div class='ball-pulse'>--}}
        {{--<div></div>--}}
        {{--<div></div>--}}
        {{--<div></div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="main-content">--}}
    {{--<div class="content-header">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<h3> {!! trans('core.View Feed') !!} </h3>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="content-body">--}}
        {{--<form role="form" class="form-horizontal">--}}
            {{--<div class="container-fluid">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" value="ID" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.Name') !!} </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" value="{!! trans('core.Name') !!}" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-1">--}}
                                    {{--<label> {!! trans('core.Type') !!} </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<input type="text" value="{!! trans('core.FaceBook') !!}" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.URL') !!} </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="url" value="https://www.google.com" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-3" style="margin-right: -38px">--}}
                                {{--<label> {!! trans('core.User ID') !!} </label>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-9">--}}
                                {{--<input type="text" value="{!! trans('core.User ID') !!}" class="form-control form-format" style="background-color: white" readonly>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<label> {!! trans('core.Password') !!} </label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="password" value="{!! trans('core.Password') !!}" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<label> {!! trans('core.Feed Pull Interval (in seconds)') !!} </label>--}}
                                    {{--<br/>--}}
                                    {{--<input type="number" value="0" class="form-control form-format" style="background-color: white" readonly>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="row buttonSet">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<button type="button" class="btn btn-default buttonClose"> {!! trans('core.Close') !!} </button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}
{{--</div>--}}
{{--</div>--}}
