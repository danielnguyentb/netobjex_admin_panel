@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')

    {!! Html::style('css/bootstrap-datepicker.css') !!}
@stop
@section('content')
    <div class="content-wrapper">
        <h3>
            Dashboard
            <small data-localize="dashboard.WELCOME"></small>
        </h3>

        <div class="container-fluid dashboard">
            <div class="row">
                <ul class="breadcrumb">
                    <li class="active">{!! trans('core.Dashboard') !!}</li>
                </ul>
            </div>
            <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Start Date') !!}</label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('startdate', $startdate, ['class'=>'form-control form-format', 'required', 'id'=>'start-date-dashboard' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.End Date') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('enddate', $enddate, ['class'=>'form-control form-format', 'required', 'id'=>'end-date-dashboard' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="mb-sm btn btn-primary add-button search" data-toggle="modal" data-target=".bs-example-modal-lg">
                            <b>{!! trans('core.Search') !!}</b>
                        </button>
                    </div>
            <!-- Line Graph -->
            <div class="row">
                <div class="col-lg-12">
                    <div id="panelChart4" class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">{!! trans('core.Unique Views') !!}</div>
                        </div>
                        <div class="panel-body">
                            <div class="chart-line flot-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Line Graph -->

            <!-- Donut Charts -->
            <div class="row">
                <div class="col-md-6">
                    <div id="panelChart6" class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">{!! trans('core.Visits by OS') !!}</div>
                        </div>
                        <div class="panel-body">
                            <div class="chart-donut-1 flot-chart"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="panelChart6" class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">{!! trans('core.Visits by Location') !!}</div>
                        </div>
                        <div class="panel-body">
                            <div class="chart-donut-2 flot-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Donut Charts-->

            <!-- Datatable -->
            <div class="row">
            <div class="col-lg-12">
                  <div class="panel panel-default">
                     <div class="panel-heading">Visits by Beacon</div>
                     <div class="container-fluid" style="padding-left:25px; padding-top:16px;">
                     <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                       <input type="text" name="keyword" class="form-control form-format " placeholder="Enter beacon name to search">
                                        <h5 class="error_message name_error"> </h5>
                                    </div>
                                    <div class="col-md-3">
                                         <input type="submit" class="btn btn-sm btn-default buttonAdd search-visit" value="Search">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading" style="color:#23b7e5 ; font-size:18px; padding-left:0px;">Advanced Filter</div>
                        </div>
                        
                    </div>
                    
                    <div class="row" style="padding-bottom:25px;">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-3" style="padding-left:0px; padding-top:6px;">
                                <label>Locations </label>
                            </div>
                            <div class="col-md-9">
                                <!-- <select class="chosen-select input-md form-control form-format">
                                            <option>All</option>
                                </select> -->
                                {!! Form::select('location', $locations, null, ['class'=>'chosen-select input-md form-control form-format']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                    
                    </div>
                     
                     <div class="table-responsive">
    <div id="beacon_visits_wrapper" class="dataTables_wrapper form-inline no-footer">
        <table id="visit-beacon" class="table table-striped">
            <thead>
                <tr role="row">
                    <th style="width: 30%;">Beacon</th>
                    <th style="width: 40%;">Location</th>
                    <th style="width: 30%;">Visits</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
                  </div>
               </div>
            </div>

        </div>
    </div>
@stop

@section('form')
    <div id="myModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 id="myModalLabel" class="modal-title">{!! trans('core.Modal title') !!}</h4>
                </div>
                <div class="modal-body">
                    <!-- START panel-->
                    <div class="panel panel-default">
                        <div class="panel-heading">{!! trans('core.Stacked form') !!}</div>
                        <div class="panel-body">
                            <form role="form">
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label> {!! trans('core.Username') !!} </label>
                                    <input type="text" placeholder="Enter username" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.First Name') !!} </label>
                                    <input type="text" placeholder="First Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.Last Name') !!} </label>
                                    <input type="text" placeholder="Last Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> {!! trans('core.Email') !!} </label>
                                    <input type="email" placeholder="Enter email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>{!! trans('core.Simple wysiwyg') !!}</label>
                                    <div>
                                        <div data-role="editor-toolbar" data-target="#editor" class="btn-toolbar btn-editor">
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Font" class="btn btn-default">
                                                    <em class="fa fa-font"></em><b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" data-edit="fontName Arial" style="font-family:'Arial'">{!! trans('core.Arial') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontName Sans" style="font-family:'Sans'">{!! trans('core.Sans') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontName Serif" style="font-family:'Serif'">{!! trans('core.Serif') !!}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Font Size" class="btn btn-default">
                                                    <em class="fa fa-text-height"></em>&nbsp;<b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="" data-edit="fontSize 5" style="font-size:24px">{!! trans('core.Huge') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontSize 3" style="font-size:18px">{!! trans('core.Normal') !!}</a>
                                                    </li>
                                                    <li><a href="" data-edit="fontSize 1" style="font-size:14px">{!! trans('core.Small') !!}</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a data-edit="bold" data-toggle="tooltip" title="Bold (Ctrl/Cmd+B)" class="btn btn-default">
                                                    <em class="fa fa-bold"></em>
                                                </a>
                                                <a data-edit="italic" data-toggle="tooltip" title="Italic (Ctrl/Cmd+I)" class="btn btn-default">
                                                    <em class="fa fa-italic"></em>
                                                </a>
                                                <a data-edit="strikethrough" data-toggle="tooltip" title="Strikethrough" class="btn btn-default">
                                                    <em class="fa fa-strikethrough"></em>
                                                </a>
                                                <a data-edit="underline" data-toggle="tooltip" title="Underline (Ctrl/Cmd+U)" class="btn btn-default">
                                                    <em class="fa fa-underline"></em>
                                                </a>
                                            </div>
                                            <div class="btn-group dropdown">
                                                <a data-toggle="dropdown" title="Hyperlink" class="btn btn-default">
                                                    <em class="fa fa-link"></em>
                                                </a>
                                                <div class="dropdown-menu">
                                                    <div class="input-group ml-xs mr-xs">
                                                        <input id="LinkInput" placeholder="URL" type="text" data-edit="createLink" class="form-control input-sm">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-sm btn-default">{!! trans('core.Add') !!}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a data-edit="unlink" data-toggle="tooltip" title="{!! trans('core.Remove Hyperlink') !!}" class="btn btn-default">
                                                    <em class="fa fa-cut"></em>
                                                </a>
                                            </div>
                                            <div class="btn-group pull-left">
                                                <a data-edit="undo" data-toggle="tooltip" title="{!! trans('core.Undo (Ctrl/Cmd+Z)') !!}" class="btn btn-default">
                                                    <em class="fa fa-undo"></em>
                                                </a>
                                                <a data-edit="redo" data-toggle="tooltip" title="{!! trans('core.Redo (Ctrl/Cmd+Y)') !!}" class="btn btn-default">
                                                    <em class="fa fa-repeat"></em>
                                                </a>
                                            </div>
                                        </div>
                                        <div style="overflow:scroll; height:250px;max-height:250px" class="form-control wysiwyg mt-lg">{!! trans('core.Type something ...') !!}</div>
                                    </div>
                                </div>
                                <br/>
                                <button type="submit" class="btn btn-sm btn-default"> {!! trans('core.Add') !!} </button>
                            </form>
                        </div>
                    </div>
                    <!-- END panel-->
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default"> {!! trans('core.Cancel') !!} </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('js/bootstrap-datepicker.js') !!}
    {!! Html::script('js/app/datatables.js') !!}
    {!! Html::script('js/app/pages/dashboard-chart.js') !!}
@stop