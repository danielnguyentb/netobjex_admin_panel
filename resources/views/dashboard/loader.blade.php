@extends('layouts.master')
@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')

    {!! Html::style('css/bootstrap-datepicker.css') !!}
@stop
@section('content')
    <div class="content-wrapper">
        <h3>
            Dashboard
            <small data-localize="dashboard.WELCOME"></small>
        </h3>

        <div class="container-fluid dashboard">
            <div class="row">
                <ul class="breadcrumb">
                    <li class="active">{!! trans('core.Dashboard') !!}</li>
                </ul>
            </div>
            <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Campaign Type') !!}</label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::select('campaigntype', ['Advertising'=>'Advertising', 'Survey'=>'Survey'],null, ['class'=>'chosen-select form-control campaigntype', 'required']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Campaign') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        <!-- {!! Form::select('campaign', $campaigns, null, ['class'=>'chosen-select form-control', 'required', 'multiple'=>'multiple','id'=>'campaign']) !!} -->
                                        <!-- {!! Form::select('campaigns[]', $campaigns, null,['class'=>'chosen-select form-control','id'=>'select-campaign', 'multiple'=>'multiple', 'required']) !!} -->
                                        <select multiple="multiple" class="chosen-select form-control campaignSelect">
                                        </select>
                                        
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {!! trans('core.Start Date') !!}</label>
                                    </div>
                                    <div class="col-md-8">
                                        <!-- <input type="text" name="start_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('startdate', $startdate, ['class'=>'form-control form-format', 'required', 'id'=>'start-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message start_date_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.End Date') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="end_date" data-inputmask="'mask': '99/99/9999'" placeholder="mm/dd/yyyy" class="form-control form-format" required> -->
                                        {!! Form::text('enddate', $enddate, ['class'=>'form-control form-format', 'required', 'id'=>'end-date' , 'data-date-format'=> 'mm/dd/yyyy']) !!}
                                        <h5 class="error_message end_date_error">  </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="mb-sm btn btn-primary add-button search" data-toggle="modal" data-target=".bs-example-modal-lg">
                            <b>{!! trans('core.Search') !!}</b>
                        </button>
                    </div>
            <div class="row">
                <!-- <ul class="breadcrumb">
                    <li class="active">{!! trans('core.Dashboard') !!}</li>
                </ul> -->
                <iframe id="MyIFrame" frameborder="0" src="{{$kibanaurl}}" scrolling="auto" style="width:100%;height:1270px;" >
                </iframe>
            </div>
        </div>
    </div>
@stop

@section('pageJs')
    <!-- SPARKLINE-->
    @include('includes.js.sparkline')
    <!-- FLOT CHART-->
    @include('includes.js.flot_chart')
    <!-- CLASSY LOADER-->
    @include('includes.js.classy_loader')
    <!-- MOMENT JS-->
    @include('includes.js.moment_js')
    <!-- DATATABLES-->
    @include('includes.js.datatables')
    <!-- CHOSEN-->
    @include('includes.js.chosen')
    <!-- INPUT MASK -->
    @include('includes.js.input_mask')
    <!-- PARSLEY-->
    @include('includes.js.parsley')
    <!-- SWEET ALERT-->
    @include('includes.js.sweet_alert')
    <!-- VALIDATION-->
    @include('includes.js.validation')
    @parent
    {!! Html::script('js/bootstrap-datepicker.js') !!}
    {!! Html::script('vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') !!}
    {!! Html::script('js/app/pages/dashboard.js') !!}
@stop