<div class="main-content">
    <div class="content-header">
        <div class="row">
            <div class="col-md-12">
                <h3> {!! ($form == 'add') ? trans('core.Add Scheduled Command') : trans('core.Edit Scheduled Command') !!}</h3>
            </div>
        </div>
    </div>
    <div class="content-body">
        {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
            @if ($form == 'edit')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label> {!! trans('core.ID') !!}<span class="asterisk">*</span> </label>
                                </div>
                                <div class="col-md-11">
                                    {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!}
                                    <h5 class="error_message idShow_error"> </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Name') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-11">
                                {!! Form::text('name', $name, ['class' => 'form-control form-format', 'required']) !!}
                                <h5 class="error_message name_error"> </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <label> {!! trans('core.Device') !!}<span class="asterisk">*</span> </label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('device', $devicesList, $deviceid, ['class' => 'chosen-select form-control device-select', 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" style="padding-left: 13px;">
                <div class="row">
                    <div class="col-md-12">
                        <label> {!! trans('core.Schedule') !!}<span class="asterisk">*</span> </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="sub-form-group">
                            <!-- Start Date -->
                            <div class="row">
                                <div class="col-md-2" style="margin-right: -30px">
                                    <label> {!! trans('core.Start Date') !!} </label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::text('start_date', $startdate, ['data-inputmask' => "'mask': '99/99/9999'", 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control form-format', 'required']) !!}
                                    <h5 class="error_message start_date_error"> </h5>
                                </div>
                            </div>
                            <br/>
                            <!-- End Date -->
                            {!! Form::checkbox('show_end_date', 1, $showEndDate, ['id' => 'end-date-checkbox']) !!}
                            <label> {!! trans('core.End Date') !!} </label>
                            {!! Form::text('end_date', $enddate, ['data-inputmask' => "'mask': '99/99/9999'", 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control form-format' . ($showEndDate ? '' : ' hide')]) !!}
                            <br/>
                            <!-- Repeat Task -->
                            {!! Form::checkbox('show_repeat', 1, $repeat, ['id' => 'repeat-checkbox']) !!}
                            <label> Repeat Task </label>
                            <br/>
                            <div class="time_lapse{!! $repeat ? '' : ' hide' !!}">
                                <label> Every </label>
                                <div class="time">
                                    {!! Form::select('every_num', $repeatEveryNumList, $everynum, ['class' => 'chosen-select input-md form-control']) !!}
                                </div>
                                <div class="time">
                                    {!! Form::select('repeat_period', $repeatPeriodsList, $repeatperiod, ['class' => 'chosen-select input-md form-control']) !!}
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-2" style="margin-right: -30px">
                                    <label> {!! trans('core.Execute time') !!} </label>
                                </div>
                                <div class="col-md-10">
                                    {!! Form::text('exec_time', $exectime, ['data-inputmask' => "'mask': '99/99/9999 99:99'", 'placeholder' => 'mm/dd/yyyy 00:00', 'class' => 'form-control form-format']) !!}
                                    <h5 class="error_message exec_time_error"> </h5>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-3" style="width: 17%">
                            <label> {!! trans('core.Command to Execute') !!} </label>
                        </div>
                        <div class="col-md-2">
                            {!!
                                Form::select(
                                    'command',
                                    array_merge(['' => 'Select Command'], $commandsList),
                                    $commandid,
                                    [
                                        'id'    => 'select-command',
                                        'class' => 'chosen-select input-md form-control',
                                        'required'
                                    ]
                                )
                            !!}
                        </div>
                    </div>
                </div>
            </div>


            <div class="params-area row{!! !empty($allCmdParams) ? '' : ' hide' !!}">
                <div class="col-md-6">
                    <div class="form-group" style="padding-left: 13px;">
                        <label> {!! trans('core.Parameter Values') !!}<span class="asterisk">*</span> </label>
                        <br/>

                        <div class="sub-form-group">
                        @if (!empty($allCmdParams))
                            @foreach ($allCmdParams as $cp)

                                <label> {!! $cp->name !!} </label>
                                <br/>
                                <div>
                                    {!! Form::text(("param_{$cp->id}_{$cp->defaultvalue}"), $cp->value, ['class' => 'form-control', 'data-type' => $cp->defaultvalue]) !!}
                                    <h5 class="error_message {!! "param_{$cp->id}_error" !!}"> </h5>
                                </div>

                            @endforeach
                        @endif
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row buttonSet">
                        <div class="col-md-12">
                            @if ($form == 'add')
                                {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                            @else
                                {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                            @endif
                            {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $id) !!}
        {!! Form::close() !!}
    </div>
</div>