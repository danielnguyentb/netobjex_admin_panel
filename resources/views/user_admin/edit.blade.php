@section('pageCss')
    <!-- WEATHER ICONS-->
    @include('includes.css.weather_icons')
    <!-- DATATABLES-->
    @include('includes.css.datatables')
    <!-- CHOSEN-->
    @include('includes.css.chosen')
    <!-- SWEET ALERT -->
    @include('includes.css.sweet_alert')
    <!-- Loaders.css-->
    @include('includes.css.loaders')
    <!-- Spinkit-->
    @include('includes.css.spinkit')
@stop
        <div class="main-content">
            <div class="content-header">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h3> {!! trans('core.Edit User') !!} </h3> -->
                        <h3> {!! ($form == 'add') ? trans('core.Add User') : trans('core.Edit User') !!} </h3>
                    </div>
                </div>
            </div>
            <div class="content-body">
                {!! Form::open(['role' => 'form', 'class' => 'edit-form row-form','data-parsley-validate', 'enctype' => 'multipart/form-data', 'method' => 'POST', 'accept-charset' => 'utf-8', 'data-id' => $id]) !!}
                    @if($form == 'edit')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.ID') !!}</label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="id" class="form-control form-format" required> -->
                                        {!! Form::text('idShow',$id,['class'=>'form-control form-format','required', 'readonly', 'disabled']) !!}
                                        <!-- {!! Form::text('idShow', $id, ['class' => 'form-control form-format', 'required', 'readonly', 'disabled']) !!} -->
                                        <h5 class="error_message id_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.First Name') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="first_name" class="form-control form-format" required> -->
                                        {!! Form::text('firstname',$firstname,['class'=>'form-control form-format', 'required']) !!}
                                        <h5 class="error_message first_name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Last Name') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="text" name="last_name" class="form-control form-format" required> -->
                                        {!! Form::text('lastname',$lastname,['class'=>'form-control form-format']) !!}
                                        <h5 class="error_message last_name_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Password') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input id="add-password" type="password" name="password" class="form-control form-format" required> -->
                                        {!! Form::password('oldPassword', [
                                                'class'                         => 'form-control form-format',
                                                'placeholder'                   => 'Password',
                                                'required',
                                                'id'                            => 'id-password',
                                                'data-parsley-required-message' => 'Password is required',
                                                'data-parsley-trigger'          => 'change focusout'
                                            ]) !!}
                                        <h5 class="error_message password_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Password Confirmation') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <input type="password" name="confirmPassword" data-parsley-equalto="#add-password" class="form-control form-format" required> -->
                                         {!! Form::password('confirmPassword', [
                                                'class'                         => 'form-control form-format',
                                                'placeholder'                   => 'Password confirmation',
                                                'required',
                                                'id'                            => 'confirm-password',
                                                'data-parsley-required-message' => 'Password confirmation is required',
                                                'data-parsley-trigger'          => 'change focusout',
                                                'data-parsley-equalto'          => '#id-password',
                                                'data-parsley-equalto-message'  => 'Not same as Password',
                                            ]) !!}
                                        <h5 class="error_message confirm_password_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Organization') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                        <!-- <select class="chosen-select input-md form-control form-format"> -->
                                            <!-- <option selected="selected">{!! trans('core.Demo Organization') !!}</option> -->
                                            {!! Form::select('org', $listOrg, $customerid,['class'=>'chosen-select input-md form-control form-format']) !!}
                                        <!-- </select> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label> {!! trans('core.Role') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-9">
                                       
                                        {!! Form::select('roles[]', $listRole, $role,['class'=>'chosen-select input-md form-control form-format','multiple'=>'multiple','id'=>'select-role', 'required']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <h5 class="at_least_one_role"> {!! trans('core.Select at least one role') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label> {!! trans('core.Email (User Name)') !!}<span class="asterisk">*</span> </label>
                                    </div>
                                    <div class="col-md-10" style="margin-left: -24px">
                                        <!-- <input type="email" name="email" data-parsley-type="email" class="form-control form-format" required> -->
                                        {!! Form::text('email',$email,['class'=>'form-control form-format', 'required']) !!}
                                        <h5 class="error_message email_error"> </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> (<span class="asterisk">*</span>) {!! trans('core.required') !!} </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row buttonSet">
                                <div class="col-md-12">
                                    <!-- <input type="submit" class="btn btn-sm btn-default buttonUpdate" value="{!! trans('core.Update') !!}"> -->
                                    <!-- <button type="button" class="btn btn-sm btn-default buttonUpdate" data-form="user-admin"> {!! trans('core.Update') !!} </button>
                                    <button type="button" class="btn btn-sm btn-default buttonCancel"> {!! trans('core.Cancel') !!} </button> -->
                                    @if($form == 'add')
                                    {!! Form::button(trans('core.Add'), ['class' => 'btn btn-sm btn-default buttonAdd action-btn']) !!}
                                    @else
                                        {!! Form::button(trans('core.Update'), ['class' => 'btn btn-sm btn-default buttonUpdate action-btn']) !!}
                                    @endif
                                    {!! Form::button(trans('core.Cancel'), ['class' => 'btn btn-sm btn-default buttonCancel action-btn']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::hidden('id', $id) !!}
                {!! Form::close() !!}
            </div>
        </div>
