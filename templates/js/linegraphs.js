// DASHBOARD - CHART LINE
// -----------------------------------
(function(window, document, $, undefined){

    $(function(){

        var data = [{
            "label": "Complete",
            "color": "#f5994e",
            "data": [
                ["Jan", 8],
                ["Feb", 29],
                ["Mar", 15],
                ["Apr", 20],
                ["May", 5],
                ["Jun", 12],
                ["Jul", 35],
                ["Aug", 10],
                ["Sep", 24]
            ]
        }];

        var options = {
            series: {
                lines: {
                    show: true,
                    fill: 0.01
                },
                points: {
                    show: true,
                    radius: 4
                }
            },
            grid: {
                borderColor: '#eee',
                borderWidth: 1,
                hoverable: true,
                backgroundColor: '#fcfcfc'
            },
            tooltip: true,
            tooltipOpts: {
                content: function (label, x, y) { return x + ' : ' + y; }
            },
            xaxis: {
                tickColor: '#eee',
                mode: 'categories'
            },
            yaxis: {
                // position: 'right' or 'left'
                tickColor: '#eee'
            },
            shadowSize: 0
        };

        var chart = $('.chart-line');
        if(chart.length)
            $.plot(chart, data, options);

    });

})(window, document, window.jQuery);