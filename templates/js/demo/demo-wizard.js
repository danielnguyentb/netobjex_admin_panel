// Forms Demo
// ----------------------------------- 


(function(window, document, $, undefined){

  $(function(){

    // FORM EXAMPLE
    // ----------------------------------- 
    var form = $("#msforms");
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
    form.children("div").steps({
        headerTag: "h4",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {   console.log("onStepChanging");
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanging: function (event, currentIndex)
        {
            console.log("onStepChanging");
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            console.log("onFinished");
            alert("Submitted!");

            // Submit form
            $(this).submit();
        }
    });

    // VERTICAL
    // ----------------------------------- 

    $("#example-vertical").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical"
    });

  });

})(window, document, window.jQuery);
