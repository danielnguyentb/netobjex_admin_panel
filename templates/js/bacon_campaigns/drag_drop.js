 $( document ).ready(function() {
    $(function() {
         //Make every clone image unique.  
         var counts = [0];
         var resizeOpts = {
             handles: "all",
             autoHide: true
         };
         $(".dragIcon").draggable({
             helper: "clone",
             //Create a counter
             start: function() {
                 counts[0]++;
                 $('.ui-state-active').each(function() {
                     $(this).removeClass('ui-state-active');
                 });
             }
         });

         $(document).on("mouseover", '.dropHere', function() {
             var _this = $(this);
             console.log(_this);
             _this.droppable({
                 hoverClass: "ui-state-active",
                 over: function(event, ui) {
                    console.log("HERE");
                     $('.' + _this.data('set')).addClass('ui-state-active');
                     _this.closest('tr').find('td:first-child').addClass('ui-state-active');
                 },
                 out: function(event, ui) {
                     // _this.closest('tr').find('td:first-child').removeClass('ui-state-active');
                     // $('.'+_this.data('set')).removeClass('ui-state-active');
                     $('.ui-state-active').each(function() {
                         $(this).removeClass('ui-state-active');
                         console.log("out");
                     });

                 },
                 drop: function(e, ui) {
                     $('.ui-state-active').each(function() {
                         $(this).removeClass('ui-state-active');
                     });
                     if (ui.draggable.hasClass("dragIcon")) {
                         $(this).append($(ui.helper).clone());

                         //Pointing to the dragIcon class in dropHere and add new class.
                         $("#section-1 .dragIcon").addClass("item-" + counts[0]);
                         $("#section-1 .img").addClass("imgSize-" + counts[0]);

                         //Remove the current class (ui-draggable and dragIcon)
                         $("#section-1 .item-" + counts[0]).removeClass("dragIcon ui-draggable ui-draggable-dragging");

                         $(".item-" + counts[0]).dblclick(function() {
                             $(this).remove();
                         });
                         make_draggable($(".item-" + counts[0]));
                         $(".imgSize-" + counts[0]).resizable(resizeOpts);
                     }

                 }
             });
         });

         var zIndex = 0;
         function make_draggable(elements) {
             // var data = $(".ui-state-active");
             console.log(elements);
             elements.closest('tr').find('td:first-child').text();
             elements.draggable({
                 //containment: 'parent',
                 containment: 'parent',
                 start: function(e, ui) {
                     ui.helper.css('z-index', ++zIndex);
                 },
                 stop: function(e, ui) {}
             });
         }
     });
});
 