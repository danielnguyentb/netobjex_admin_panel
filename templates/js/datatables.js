// Demo datatables
// -----------------------------------


(function(window, document, $, undefined){

    $(function(){

        //
        // AJAX
        //

        $('#beacon_visits').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/datatable.json',
            aoColumns: [
                { mData: 'beacon' },
                { mData: 'location' },
                { mData: 'visits' }
            ]
        });

        $('#group_management').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/group_management.json',
            aoColumns: [
                { mData: 'group_name' },
                { mData: 'action'}
            ]
        });

        $('#enterprise_gateway').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/enterprise_gateway.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'type' },
                { mData: 'action'}
            ]
        });

        $('#beacon').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beacon.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'uuid' },
                { mData: 'major_id' },
                { mData: 'minor_id' },
                { mData: 'status' },
                { mData: 'lost' },
                { mData: 'action'}
            ]
        });
		
		$('#batteries').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/battery.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'date' },
                { mData: 'action'}
            ]
        });

        $('#device').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/device.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'id' },
                { mData: 'device_type' },
                { mData: 'parent_id' },
                { mData: 'action'}
            ]
        });
		$('#equipments').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/equipments.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'Purchase_Date' },
                { mData: 'Serial_Number' },
                { mData: 'action'}
            ]
        });
		$('#drones').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/drones.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'device_type' },
                { mData: 'battery_level' },
                { mData: 'action'}
            ]
        });
		$('#maintenance').dataTable({
            'paging':   false,  // Table pagination
            'ordering': false,  // Column ordering
            'info':     false,  // Bottom left status text
            sAjaxSource: 'server/maintenance.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'next' },
                { mData: 'expense' },
                { mData: 'action'}
            ]
        });
		$('#incident').dataTable({
            'paging':   false,  // Table pagination
            'ordering': false,  // Column ordering
            'info':     false,  // Bottom left status text
            sAjaxSource: 'server/incident.json',
            aoColumns: [
                { mData: 'date' },
                { mData: 'cause' },
                { mData: 'project' },
                { mData: 'action'}
            ]
        });
		
		 $('#device_seteditor').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/device_seteditor.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });
		
		 $('#messaging').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/messaging.json',
            aoColumns: [
                { mData: 'name' },
				{ mData: 'type' },
                { mData: 'action'}
            ]
        });

        $('#beaconset').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beaconset.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'id' },
                { mData: 'major_id' },
                { mData: 'minor_id' },
                { mData: 'action'}
            ]
        });

        $('#location').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/location.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'address' },
                { mData: 'city' },
                { mData: 'state' },
                { mData: 'zip' },
                { mData: 'action'}
            ]
        });

        $('#feed').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/feed.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'url' },
                { mData: 'type' },
                { mData: 'user_id' },
                { mData: 'interval' },
                { mData: 'action'}
            ]
        });

        $('#attribute').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/attribute.json',
            aoColumns: [
                { mData: 'checkbox' },
                { mData: 'attribute' },
                { mData: 'operator' },
                { mData: 'value' },
                { mData: 'and/or' },
                { mData: 'action'}
            ]
        });

        $('#rule').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/rule.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'enterprise_gateway' },
                { mData: 'action'}
            ]
        });

        $('#scheduled_commands').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/scheduled_commands.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#survey').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/surveys.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'num_of_questions' },
                { mData: 'start_date' },
                { mData: 'end_date' },
                { mData: 'published' },
                { mData: 'action'}
            ]
        });

        $('#ad_campaign').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/ad_campaigns.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'type' },
                { mData: 'action'}
            ]
        });

        $('#beacon_campaigns').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beacon_campaigns.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'beacon' },
                { mData: 'start_date' },
                { mData: 'end_date' },
                { mData: 'action'}
            ]
        });

        $('#beacon_notifications').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/beacon_notifications.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'description' },
                { mData: 'notification_type' },
                { mData: 'action'}
            ]
        });

        $('#user_admins').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/user_admins.json',
            aoColumns: [
                { mData: 'id' },
                { mData: 'email' },
                { mData: 'organization' },
                { mData: 'signup_date' },
                { mData: 'role' },
                { mData: 'status' },
                { mData: 'action'}
            ]
        });

        $('#organization_editor').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/organization_editor.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#device_type_management').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/device_type_management.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });

        $('#protocol_management').dataTable({
            'paging':   true,  // Table pagination
            'ordering': true,  // Column ordering
            'info':     true,  // Bottom left status text
            sAjaxSource: 'server/protocol_management.json',
            aoColumns: [
                { mData: 'name' },
                { mData: 'action'}
            ]
        });
    });

})(window, document, window.jQuery);
