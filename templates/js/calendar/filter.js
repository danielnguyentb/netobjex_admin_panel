(function ($) {
    $.fn.filterData = function ($element) {
        var speed = 400;
        $select = this;
        var dfd = jQuery.Deferred();
        return new Promise(function (resolve, reject) {
            $selectedValue = $select.val();
            $rows = $element.find("li");
            if ($selectedValue === "tout") {
                $('.prog_item').fadeTo(speed, 1);
                return true;
            }
            $.each($rows, function (index, element) {
                $(this).find('.prog_item').each(function () {
                    var program = $(this);
                    var link = $(this);
                    if (program.data("genre") !== $selectedValue) {
                        link.fadeTo(speed, 0.3);
                    } else {
                        link.fadeTo(speed, 1);
                    }
                });
            });

            return true;
        });
    }

}(jQuery));