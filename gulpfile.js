var elixir = require('laravel-elixir');
require('laravel-elixir-livereload');
require('./tasks/bower.task.js');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .bower()
        .sass('app.scss')
        .livereload([
            'public/js/vendor.js',
            'public/js/app.js',
            'public/css/vendor.css',
            'public/css/app.css',
            'resources/views/**/*.php'
        ], {liveCSS: true})
        .phpUnit();
});
